import React, { Fragment, useState, useEffect } from 'react';
// import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { useSelector, useDispatch } from "react-redux";
import { Row, Col, Rate, Dropdown } from 'antd';
import { addorRemoveLikeForTrip, addorRemoveLikeForLearning, addorRemoveLikeForAlbum } from "../../services/expert";
import { ModalActions } from "../../redux/models/events";

// import Image from 'next/image';
import shareOption from "../../containers/commonContainer/shareOption";
import { getLikeStatus } from "../../helpers/methods";
const Share = "/images/share_ic.png";
const ShareNew = "/images/Share.svg";
const ShareNewIcon = "/images/newicon/share_new_icon.svg";
const ShareWhite = "/images/share_ic_white.png";
const loveIcon = "/images/love.svg";
const heart = "/images/newicon/heart.svg";
const heartFill = "/images/newicon/heart_fill.svg";

const LikeAndShare = (props) => {

    const dispatch = useDispatch();

    let { allLikes, id, pageType, designType } = props;

    const [likeStatus, setLikeStatus] = useState(false);
    const [isCountLoaded, setIsCountLoaded] = useState(false);
    const [likeCount, setLikeCount] = useState(0);
    const token = useSelector((state) => state.auth.accessToken);
    const userId = useSelector((state) => state.auth.userId);
    const [isLoading, setIsLoading] = useState(false);

    const { openAuthModal } = ModalActions;


    useEffect(() => {
        getTheLikeCount();

        setLikeStatus(getLikeStatus(allLikes, userId));

    }, []);

    const getTheLikeCount = (type) => {
        let count = typeof allLikes !== "undefined" ? allLikes.length : 0;
        setLikeCount(count);
    }

    const updateLikeCountText = (type, id) => {
        let count = likeCount;
        if (typeof type !== "undefined" && type === 'like') {
            count = count + 1;
        } else if (typeof type !== "undefined" && type === 'dislike') {
            count = count - 1;
        }
        setLikeCount(count);
    }

    const onLikeButtonClick = async (event, id) => {
        event.stopPropagation();
        setIsCountLoaded(true);
        if (token) {
            setIsLoading(true);
            let response;
            if (pageType === "trip") {
                response = await addorRemoveLikeForTrip(token, { id });
            } else if (pageType === "workshop") {
                response = await addorRemoveLikeForLearning(token, { id });
            } else if (pageType === "album") {
                response = await addorRemoveLikeForAlbum(token, { id });
                props.refreshAlbum(designType);
            }

            if (response && response.data && response.data.success === "SUCCESS") {
                updateLikeCountText(response.data.data.ans, id);
                setLikeStatus(getLikeStatus(allLikes, userId, response.data.data.ans));
                setIsCountLoaded(false);
                setIsLoading(false);

            } else {
                setIsLoading(false);
            }
        } else {
            //Set the referre URL for the after login redirection
            localStorage.setItem("referrer", window.location.pathname);
            dispatch(openAuthModal(true));
        }
        setIsCountLoaded(false);
    };

    return (
        <Fragment>
            <Row className="card-like-share">
                {designType === "single" &&
                    <>
                        {/* <Col span={3}>
                            <div className="share_btn">
                                <Dropdown overlay={shareOption} placement="bottomCenter">
                                    <Image layout="fill"
                                        src={Share}
                                        alt="Social Share"
                                        className="share_icn"
                                    />
                                </Dropdown>
                            </div>
                        </Col> */}
                        <Col span={10} className="single-page-like">
                            <Col span={24}>
                                <Col span={6}>
                                    {isLoading ?
                                        (<img
                                            src={likeStatus ? heartFill : heart}
                                            alt="Like" className="like-heart-new-icon"
                                        />)
                                        :
                                        (<img src={likeStatus ?
                                            heartFill : heart} alt="Like"
                                            className="like-heart-new-icon"
                                            onClick={(event) => onLikeButtonClick(event, id)} />)}

                                    {/* <label htmlFor="like1" className={`like-heart ${likeStatus ? "red" : ""} ${isLoading ? "disabled" : ""}`}>
                                        <svg viewBox="0 0 24 24">
                                            <path
                                                onClick={(event) => onLikeButtonClick(event, id)}
                                                d="M12 21.35l-1.45-1.32c-5.15-4.67-8.55-7.75-8.55-11.53 0-3.08 2.42-5.5 5.5-5.5 1.74 0 3.41.81 4.5 2.09 1.09-1.28 2.76-2.09 4.5-2.09 3.08 0 5.5 2.42 5.5 5.5 0 3.78-3.4 6.86-8.55 11.54l-1.45 1.31z"></path></svg>
                                    </label> */}

                                </Col>
                                <Col span={18}>
                                    {likeCount !== 0 && (<div className={`${likeCount <= 1 ? 'small' : 'big'} like-count-text`}>
                                        {isCountLoaded && "  Like "}
                                        {!isCountLoaded && <><span>{likeCount}</span> <span> {likeCount > 1 ? 'Likes' : 'Like'}</span></>}
                                    </div>)}

                                </Col>
                            </Col>
                            <Col span={24} className="save-new-icon-parent">
                                <Col span={6}>
                                    <Dropdown overlay={shareOption} placement="bottomCenter">
                                        <img src={ShareNewIcon} alt="Social Share" className="share_new_icon" />
                                    </Dropdown>
                                </Col>
                                {/* <Col span={18}>
                                    <div className="like-count-text">
                                        13 Interested
                                    </div>

                                </Col> */}
                            </Col>
                        </Col>

                    </>
                }
                {designType === "multiple" &&
                    <>
                        <Col xs={13} sm={13} md={13} lg={13} xl={13}>
                            <Rate
                                allowHalf
                                defaultValue={0}
                                className="an-14"
                            />
                        </Col>
                        <Col xs={11} sm={11} md={11} lg={11} xl={11}>
                            <div className="heart_fill trip_set_icon">
                                <input type="checkbox" id="like1" />
                                <div className="text-right">
                                    <Dropdown overlay={shareOption} placement="bottomCenter">
                                        <img
                                            src={Share}
                                            alt="Social Share"
                                            className="share_icn"
                                        />
                                    </Dropdown>
                                    <label htmlFor="like1" className={`like-heart ${likeStatus ? "red" : ""} ${isLoading ? "disabled" : ""}`}>
                                        <svg viewBox="0 0 24 24">
                                            <path
                                                onClick={(event) => onLikeButtonClick(event, id)}
                                                d="M12 21.35l-1.45-1.32c-5.15-4.67-8.55-7.75-8.55-11.53 0-3.08 2.42-5.5 5.5-5.5 1.74 0 3.41.81 4.5 2.09 1.09-1.28 2.76-2.09 4.5-2.09 3.08 0 5.5 2.42 5.5 5.5 0 3.78-3.4 6.86-8.55 11.54l-1.45 1.31z"></path></svg>
                                    </label>
                                    {likeCount !== 0 && (<div className={`${likeCount <= 1 ? 'small' : 'big'} like-count-text`}>
                                        {isCountLoaded && "  Like "}
                                        {!isCountLoaded && <><span>{likeCount}</span> <span> {likeCount > 1 ? 'Likes' : 'Like'}</span></>}
                                    </div>)}

                                </div>

                            </div>
                        </Col>
                    </>
                }
                {designType === "album" &&
                    <div className='text-right'>
                        <Dropdown
                            overlay={shareOption}
                            placement='bottomCenter'
                        >
                            <div className="icon_background">
                                <img
                                    src={ShareWhite}
                                    alt='Social Share'
                                    className='share_icn'
                                />
                            </div>
                        </Dropdown>

                        <label htmlFor='like1' className={`album-like like-heart ${likeStatus ? "red" : ""} ${isLoading ? "disabled" : ""}`}>
                            <svg viewBox='0 0 24 24' onClick={(event) => onLikeButtonClick(event, id)}>
                                <path d='M12 21.35l-1.45-1.32c-5.15-4.67-8.55-7.75-8.55-11.53 0-3.08 2.42-5.5 5.5-5.5 1.74 0 3.41.81 4.5 2.09 1.09-1.28 2.76-2.09 4.5-2.09 3.08 0 5.5 2.42 5.5 5.5 0 3.78-3.4 6.86-8.55 11.54l-1.45 1.31z' />
                            </svg>
                        </label>
                        {likeCount !== 0 && (<div className={`${likeCount <= 1 ? 'small' : 'big'} like-count-text`}>
                            {isCountLoaded && "  Like "}
                            {!isCountLoaded && <><span>{likeCount}</span> <span> {likeCount > 1 ? 'Likes' : 'Like'}</span></>}
                        </div>)}

                    </div>
                }
            </Row>
            {designType === "albumDetail" &&
                <div className="heart_fill">
                    <div className="like_icon">
                        <label htmlFor="like1" className={`album-like like-heart ${likeStatus ? "red" : ""} ${isLoading ? "disabled" : ""}`}>
                            <svg viewBox="0 0 24 24" onClick={(event) => onLikeButtonClick(event, id)}>
                                <path d="M12 21.35l-1.45-1.32c-5.15-4.67-8.55-7.75-8.55-11.53 0-3.08 2.42-5.5 5.5-5.5 1.74 0 3.41.81 4.5 2.09 1.09-1.28 2.76-2.09 4.5-2.09 3.08 0 5.5 2.42 5.5 5.5 0 3.78-3.4 6.86-8.55 11.54l-1.45 1.31z">
                                </path>
                            </svg>
                        </label>
                        {likeCount !== 0 && (<div className={`${likeCount <= 1 ? 'small' : 'big'} like-count-text`}>
                            {isCountLoaded && <span>&nbsp;Like</span>}
                            {!isCountLoaded && <><span>{likeCount}</span> <span> {likeCount > 1 ? 'Likes' : 'Like'}</span></>}
                        </div>)}

                    </div>

                    <div className="shere_Icon">
                        <div className="text-right">
                            <Dropdown overlay={shareOption} placement="bottomCenter">
                                <img
                                    src={Share}
                                    alt="Social Share"
                                    className="share_icn"
                                />
                            </Dropdown>
                        </div>
                    </div>

                </div>
            }

            {designType === "directoryPage" &&
                <>
                    <div>
                        <div className="card_rate">
                            <Rate
                                allowHalf
                                defaultValue={5}
                                className="an-14"
                            />
                            {/* <span className="review_txt">76 reviews</span> */}
                        </div>
                        <div className="card_like">
                            <div className="heart_fill trip_set_icon">
                                <div className="text-right">
                                    <Dropdown overlay={shareOption} placement="bottomCenter">
                                        <img
                                            src={ShareNew}
                                            alt="Social Share"
                                            className="share_icn"
                                        />
                                    </Dropdown>
                                    {/* <label htmlFor="like1" className={`like-heart ${likeStatus ? "red" : ""} ${isLoading ? "disabled" : ""}`}>
                                        <svg viewBox="0 0 24 24">
                                            <path
                                                onClick={(event) => onLikeButtonClick(event, id)}
                                                d="M12 21.35l-1.45-1.32c-5.15-4.67-8.55-7.75-8.55-11.53 0-3.08 2.42-5.5 5.5-5.5 1.74 0 3.41.81 4.5 2.09 1.09-1.28 2.76-2.09 4.5-2.09 3.08 0 5.5 2.42 5.5 5.5 0 3.78-3.4 6.86-8.55 11.54l-1.45 1.31z"></path></svg>
                                    </label> */}
                                    <img
                                        src={loveIcon}
                                        alt="Like"
                                        className="like-heart-new-icon"
                                    />
                                    <div className={`${likeCount <= 1 ? 'small' : 'big'} like-count-text`}>
                                        {isCountLoaded && "  Like "}
                                        {!isCountLoaded && <><span>{likeCount}</span> <span> {likeCount > 1 ? 'Likes' : 'Like'}</span></>}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </>
            }
        </Fragment>
    )
}

export default compose(LikeAndShare)
