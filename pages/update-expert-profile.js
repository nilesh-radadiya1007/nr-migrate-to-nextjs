import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';


const CreateExpert = dynamic(
    () => import("../src/containers/CreateExpert"),
    { ssr: false }
);

const CreateExpertInApp = () => {  
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <CreateExpert />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
)
}

export default CreateExpertInApp;