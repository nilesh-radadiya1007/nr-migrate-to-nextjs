import React, { useEffect, useCallback, useState, Fragment } from "react";
import { Row, Col, List, Avatar, Radio, Form, Menu, message, Select, Icon } from "antd";
import Link from "next/link";
import ReactHtmlParser from "react-html-parser";
import { useSelector, useDispatch } from "react-redux";
import OwlCarousel from "react-owl-carousel";

import { GetLearningDetails, getMyAlbums } from "../services/expert";
import {
  CaptalizeFirst,
  formatDate,
  removeNewLines,
  commaSepratorString,
  DayorDaysNightOrNights,
  getCityFromLocation,
  skillLevelText,
  getCurrencySymbol,
  getPriceType,
  getGroupSize,
  getPriceAfterDiscount,
  isPastDate,
  getSeasonText
} from "../helpers/methods";
import TripMap from "../components/Trips/Map";
import Reservation from "../components/Trips/Reservation";
import Interested from "../components/Trips/Interested";
import Cancellation from "../components/Trips/Cancellation";
import MoreDetails from "../components/Learning/Moredetails";
import AppLoader from "../components/Loader";

import AccommodationPhoto from '../components/Trips/AccommodationPhoto';
import AccommodationPhotoViewPopup from '../components/Trips/AccommodationPhotoView';
import AddPhoto from '../components/Trips/AddPhoto';
import ReactPlayer from 'react-player';
import { useMediaQuery } from "react-responsive";
import { ModalActions } from "../redux/models/events";
import AdditionalDetails from "../components/common/AdditionalDetails";
import AboutThisExpert from "../components/common/AboutThisExpert";
import CovidBanner from "../components/common/CovidBanner";
import Newsletter from '../components/common/NewsLetter';
import { useRouter } from "next/router";

const Placeholder = "/images/upload_image.png";
const Location_Img = "/images/country_ic.png";
const Skill = "/images/skill_ic_filled.png";
const Participate = "/images/participants_ic_filled.png";
const Clock = "/images/duration_ic_filled.png";
const Activity = "/images/activity_ic_filled.png";
const Speak = "/images/speaks_ic.png";

const { Option } = Select;

const ApiKey = process.env.REACT_APP_GOOGLE_MAP_API_KEY;
const URL =
  "http://expeditions-connect-app.s3-website.eu-central-1.amazonaws.com";
// Header Section
const data = [
  {
    title: "Location",
    title3: "Duration",
    title2: "Difficulty level",
    title4: "Activity",
    title5: "Expedition Type",
  },
];

const sliderOptions = {
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
      nav: true,
      dotsEach: 3,
    },
    768: {
      items: 2,
      nav: true,
      dotsEach: 2,
    },
    991: {
      items: 3,
      nav: true,
    },
  },
};



const WorkShopDetails = (props) => {
  const router = useRouter();
  const { id } = router.query;
  // console.log(router.query);
  const dispatch = useDispatch();
  const token = useSelector(state => state.auth.accessToken);
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const [showContent, setShowContent] = useState(false);
  const [learning, setLearning] = useState(null);
  const [slot, setSlot] = useState(null);
  const [showR, setShowR] = useState(false);
  const [showInterest, setShowInterest] = useState(false);
  const [showCanc, setShowCanc] = useState(false);
  const isLogin = useSelector((state) => state.auth.isLogin);
  const role = useSelector((state) => state.auth.role);
  const [datePrice, setDatePrice] = useState("");
  const [datePriceDiscount, setDatePriceDiscount] = useState("");
  const [datePriceCurrency, setDatePriceCurrency] = useState("");
  const [moreDetail, setMoreDetails] = useState(false);
  const [accommoPopUp, setAccommoPopUp] = useState(false);
  const [accommodationPhotoViewPopup, setAccommodationPhotoViewPopup] = useState(false);
  const [currentViewImage, setCurrentViewImage] = useState(0);
  const [photoPopUp, setPhotoPopUp] = useState(false);
  const [currentPhotoView, setCurrentPhotoView] = useState("accomo");
  const [albums, setAlbums] = useState([]);
  const [defaultDateValue, setDefaultDateValue] = useState("");
  const [isDateSelect, setIsDateSelect] = useState("");
  const [loadDates, setloadDates] = useState(true);
  const [loadDatesDropdown, setloadDatesDropdown] = useState(true);
  const [review, setReview] = useState([]);

  const { openAuthModal } = ModalActions;

  const onCloseClick = () => {
    setShowR(false);
    setShowCanc(false);
    setMoreDetails(false);
    setAccommoPopUp(false);
    setAccommodationPhotoViewPopup(false);
    setPhotoPopUp(false);

  };
  const onInstCloseClick = () => setShowInterest(false);
  const onReserveClick = () => {
    if (!slot) {
      return message.error(
        "Please select date slot to make reservation request"
      );
    }
    if (isLogin) {
      if (role !== "enthusiasts") {
        return message.error(
          "You must be logged in as enthusiasts to send request"
        );
      }
      setShowR(true);
    } else {
      localStorage.setItem("referrer", window.location.pathname);
      dispatch(openAuthModal(true));
      // message.error("Please login to make reservation request");
    }
  };
  const selectDate = (date, price, priceCurrency, discount, isRadioClicked = false) => {
    setDatePriceDiscount(discount);
    setSlot(date);
    setDatePrice(price);
    setDatePriceCurrency(priceCurrency);
    if (isRadioClicked) {
      setloadDatesDropdown(false);
      setTimeout(() => {
        setloadDatesDropdown(true);
      }, 10);
    }
  };

  const handleDateChange = (d) => {

    if (d !== "") {
      let seprateData = d.split("|");
      selectDate(seprateData[0], seprateData[1], seprateData[2], seprateData[3]);
    } else {
      setSlot(null);
      setDatePrice(learning.price);
      setDatePriceCurrency(learning.priceCurrency);
      setDatePriceDiscount(learning.discount);
    }
    if (isDateSelect) {
      setloadDates(false);
      setTimeout(() => {
        setloadDates(true);
        setIsDateSelect(false)
      }, 1);
    }
    setDefaultDateValue(d);
  }



  useEffect(() => {
    setTimeout(() => {
      if (router.query.review === "true") {
        if (document.getElementById("scoll-to-here") !== null) {
          document
            .getElementById("scoll-to-here")
            .scrollIntoView({ block: "start", behavior: "smooth" });
        }
      }
    }, 2000);
    // react-hooks/exhaustive-deps
  }, []);

  const onIntseretClick = () => {
    if (isLogin) {
      if (role !== "enthusiasts") {
        return message.error(
          "You must be logged in as enthusiasts to send request"
        );
      }
      setShowInterest(true);
    } else {
      localStorage.setItem("referrer", window.location.pathname);
      dispatch(openAuthModal(true));
      // message.error("Please login to make reservation request");
    }
  };

  const addPhoto = () => {
    setPhotoPopUp(true);
  }

  // const menu = (
  //   <Menu className="share_btn_box">
  //     <Menu.Item>
  //       <FacebookShareButton url={`${URL}/learning-details/${id}`}>
  //         <img src={Facebook} alt="facebook" className="pr10" />
  //         Facebook
  //         </FacebookShareButton>
  //     </Menu.Item>
  //     <Menu.Item>
  //       <WhatsappShareButton url={`${URL}/learning-details/${id}`}>
  //         <img src={WhatsApp} alt="twitter" className="pr10" />
  //         WhatsApp
  //         </WhatsappShareButton>
  //     </Menu.Item>
  //     <Menu.Item>
  //       <TwitterShareButton
  //         url={`${URL}/trips-details/${id}`}
  //         style={{ border: "none" }}
  //       >
  //         <img src={Twitter} alt="twitter" className="pr10" />
  //         Twitter
  //         </TwitterShareButton>
  //     </Menu.Item>
  //   </Menu>
  // );


  const getData = useCallback(async (id) => {
    const result = await GetLearningDetails(id);
    if (result.status === 200) {
      setShowContent(true);
      let learningRes = result.data.data;
      setReview(result.data.review);

      learningRes.inclusion = removeNewLines(learningRes.inclusion);
      learningRes.exclusion = removeNewLines(learningRes.exclusion);

      if (typeof learningRes.accomodation === "undefined" || learningRes.accomodation.trim() === "<p><br></p>") {
        learningRes.accomodation = ""
      }
      if (typeof learningRes.meetingPoint === "undefined" || learningRes.meetingPoint === "null") {
        learningRes.meetingPoint = "";
      }

      if (typeof learningRes.extras === "undefined" || learningRes.extras.trim() === "<p><br></p>" || learningRes.extras.trim() === "null" || learningRes.extras === "") {
        learningRes.extras = "";
      }

      if (typeof learningRes.itenary === "undefined" || (typeof learningRes.itenary !== "undefined" && learningRes.itenary.length > 0 && (typeof learningRes.itenary[0].value === "undefined" || learningRes.itenary[0].value === ""))) {
        learningRes.itenary = [];
      }

      learningRes.whatLearn = (learningRes.whatLearn && learningRes.whatLearn.trim() !== 'null' && learningRes.whatLearn.trim() !== '' && learningRes.whatLearn.trim() !== "<p><br></p>") ? learningRes.whatLearn : "";
      learningRes.attend = (learningRes.attend && learningRes.attend.trim() !== 'null' && learningRes.attend.trim() !== '' && learningRes.attend.trim() !== "<p><br></p>") ? learningRes.attend : "";

      learningRes.season = getSeasonText(learningRes.season);

      setLearning(learningRes);
      getAlbumData();
    }
    // react-hooks/exhaustive-deps
  }, []);

  const getAlbumData = useCallback(async () => {
    const result = await getMyAlbums(token);
    if (result.status === 200) {
      let finalList = [];
      if (result.data.data !== undefined && result.data.data.length > 0) {
        finalList = result.data.data.filter(item => item.isDelete === false)
      }
      setAlbums(finalList);
    }
    // react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getData(id);
  }, [getData, id]);

  const onAccommoImageClick = (index, view) => {
    setCurrentPhotoView(view);
    setCurrentViewImage(index);
    setAccommodationPhotoViewPopup(true);

  };

  const addAccommoPopUp = () => {
    setAccommoPopUp(true);
  }

  const getRefreshTrip = async () => {

    setShowContent(false);
    const result = await GetLearningDetails(id);
    if (result.status === 200) {
      setLearning(result.data.data);
      setTimeout(() => {
        setShowContent(true);
      }, 500);
    }
  }

  const displayLang = (lang) => {
    let resLang = ""
    if (lang.length > 0) {
      // array-callback-return
      lang.map((a, index) => {
        let addCooma = "";
        if (lang.length !== index + 1) {
          addCooma = ", "
        }
        resLang += CaptalizeFirst(a) + addCooma;
      });

    }

    return resLang;
  }


  const onClickDate = (d, i) => {
    if (!slot) {
      return message.error("Please select date slot to make reservation request");
    }
    if (isLogin) {
      if (role !== 'enthusiasts') {
        return message.error("You must be logged in as enthusiasts to send request");
      }
      selectDate(d.fromDate, d.price, d.priceCurrency, d.discount);
      setShowR(true);
    } else {
      localStorage.setItem("referrer", window.location.pathname);
      dispatch(openAuthModal(true));
      // message.error("Please login to make reservation request");
    }

  }

  if (showContent && learning && learning) {
    return (
      <div className="header-container w_bg workshop_view_edit_page workshop_mobile_view">
        <CovidBanner />
        <Row gutter={20}>
          <Col span={18}></Col>
          <Col span={6}>
            <div className=" text-right edt_btn_sec btn-edit-banner-top">
              <Link href={`/update-learning/${id}`}>
                <a><i className="fas fa-pencil-alt"></i> Edit</a>
              </Link>
            </div>
          </Col>
        </Row>
        {!isMaxWidth768 ? (
          <div
            className="gallery_bg"
            style={{ backgroundImage: `url(${learning.cover})` }}
          >
          </div>
        ) : (
          <div
            className="edit-trip-work-img"
          >
            <img src={learning.cover} alt="cover bg img"></img>
          </div>
        )}
        <div className="container align-center">
          <div className="expedition_bg">
            <div className="gallery_sec section_mobile fix_to_top ">
              <Row gutter={20}>
                <Col xs={24} sm={24} md={17} lg={17} xl={17} className="first-col-edit">
                  <h1 className="an-36 text_skill edit-title">
                    {learning.title}
                  </h1>
                  {(learning.country !== 'undefined' && learning.country !== "") && (
                    <h4 className="an-18 work_title trip_border_btm">
                      <Avatar src={Location_Img} className="location_pin" />{" "}
                      <span className="cover-location-title" >{getCityFromLocation(learning.address)}{CaptalizeFirst(learning.country)}</span>
                    </h4>
                  )}
                  {learning.workshopMedium === 'online' && (
                    <h4 className="an-18 work_title trip_border_btm">
                      <Avatar src={Location_Img} className="location_pin cover-location-title" />{" "}
                      Online
                    </h4>
                  )}
                </Col>
                <Col xs={24} sm={24} md={17} lg={17} xl={17} className="cover_img_data cover-img-icon first-col-edit">
                  <Row gutter={20}>
                    <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                      <List
                        itemLayout="horizontal"
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={<Avatar src={Clock} />}
                              title="Duration"
                              description={`${learning.duration} ${DayorDaysNightOrNights('w', learning.duration, learning.durationType)}`}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={10} lg={10} xl={10}>
                      <List
                        itemLayout="horizontal"
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={<Avatar src={Activity} />}
                              title="Activity"
                              description={commaSepratorString(learning.activity)}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                      <List
                        itemLayout="horizontal"
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={<Avatar src={Speak} />}
                              title="Languages"
                              description={displayLang(learning.langauges)}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                      <List
                        itemLayout="horizontal"
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={
                                <Avatar
                                  src={Skill}
                                  className="pl5 fill-width"
                                />
                              }
                              title="Skills Level"
                              description={skillLevelText(learning.skill)}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={10} lg={10} xl={10}>
                      <List
                        itemLayout="horizontal"
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={
                                <Avatar
                                  src={Participate}
                                  className="pl5 fill-width"
                                />
                              }
                              title="Group Size"
                              description={getGroupSize(learning.participants, learning.groupType)}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    {typeof learning.season !== "undefined" && learning.season !== "" &&
                      <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                        <List
                          itemLayout="horizontal"
                          dataSource={data}
                          renderItem={(item) => (
                            <List.Item>
                              <List.Item.Meta
                                avatar={
                                  <Avatar
                                    src={Clock}
                                    className="pl5 fill-width"
                                  />
                                }
                                title="Season"
                                description={learning.season}
                              />
                            </List.Item>
                          )}
                        />
                      </Col>
                    }
                  </Row>
                </Col>
                <Col xs={24} sm={24} md={7} lg={7} xl={7} className="trip_reser_card first-col-none select_date_popup">
                  <div className='select_date_popup_child'>
                    <Row className="header_per_txt work-trip-detail-page">
                      <Col span={18}>
                        <h2 className="an-30 medium-text mb5">
                          <span className="after-discount">{getCurrencySymbol(datePriceCurrency || learning.priceCurrency)} {getPriceAfterDiscount(datePrice || learning.price, learning.discount && slot === null ? learning.discount : datePriceDiscount || "")}</span>
                          {((typeof learning.discount !== "undefined" && learning.discount !== "" && learning.discount !== null && slot === null) || (datePriceDiscount !== "" && datePriceDiscount !== null && datePriceDiscount !== "null" && datePriceDiscount)) &&
                            <span className="orignal-price">{getCurrencySymbol(datePriceCurrency || learning.priceCurrency)} {datePrice || learning.price}</span>
                          }
                        </h2>
                        <p className="mb10 an-14 medium-text">{getPriceType(learning.priceType)}</p>
                      </Col>
                      <Col span={6} className="edit-date-parent">
                        <div className="edt_btn_sec">
                          <Link href={`/update-learning/${id}`}>
                            <a><i className="fas fa-pencil-alt"></i> Edit</a>
                          </Link>
                        </div>
                      </Col>
                      {/* <LikeAndShare allLikes={learning.likes} id={learning._id} pageType={"workshop"} designType="single" /> */}
                    </Row>
                    <Row className="date-button-section">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <div className="select_date mt10">
                          {learning.dateTime.length > 0 ? (
                            <Fragment>
                              <div className="select-date-text pt5 fill-width mb10">
                                {learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).length > 0 ? <span className="flexible-txt">Select Date</span> : <span className="flexible-txt">This workshop is outdated. Please add new date to reactivate.</span>}
                              </div>

                            </Fragment>
                          ) : (
                            <p className="an-14 medium-text pt10 pb10 fill-width flexible-txt">
                              This workshop has flexible dates. Please contact expert for more
                              information.
                            </p>
                          )}
                        </div>
                      </Col>
                      <Col>
                        {learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).length > 0 && (
                          <Row className="trip_date_group">
                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                              <Form.Item>
                                {loadDates &&
                                  <Radio.Group initialValue={0}>
                                    {learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).map((d, i) => {
                                      if (i === 0 || i === 1 || i === 2) {
                                        return (
                                          <span className="date-radio">
                                            <Radio.Button
                                              key={d._id}
                                              value={i}
                                              className={`${!isPastDate(d.fromDate) ? 'disabled' : ''} radio-no-${i}`}
                                              disabled={!isPastDate(d.fromDate)}
                                              onClick={() => {
                                                setDefaultDateValue("");
                                                setIsDateSelect(true)
                                                selectDate(d.fromDate, d.price, d.priceCurrency, d.discount, true);
                                              }
                                              }
                                            >
                                              {formatDate(d.fromDate)}
                                            </Radio.Button>
                                          </span>
                                        );

                                      } else {
                                        return null;
                                      }
                                    })}
                                  </Radio.Group>}
                                {!loadDates &&
                                  <Radio.Group initialValue={0}>
                                    {learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).map((d, i) => {
                                      if (i === 0 || i === 1 || i === 2) {
                                        return (
                                          <span className="date-radio">
                                            <Radio.Button
                                              key={d._id}
                                              value={i}
                                              className={`${!isPastDate(d.fromDate) ? 'disabled' : ''}`}
                                              disabled={!isPastDate(d.fromDate)}
                                            >
                                              {formatDate(d.fromDate)}
                                            </Radio.Button>
                                          </span>
                                        );

                                      } else {
                                        return null;
                                      }
                                    })}
                                  </Radio.Group>}

                              </Form.Item>
                            </Col>
                          </Row>
                        )}
                        {learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).length > 3 && (
                          <Row className="mb25">
                            {loadDatesDropdown &&
                              <Select defaultValue={defaultDateValue} style={{ width: "100%" }} onChange={(e) => handleDateChange(e)} dropdownClassName="select-date-dropdpwn">
                                <Option value="">View More Dates</Option>
                                {learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).map((d, key) => (
                                  <Option className={`${!isPastDate(d.fromDate) ? 'disabled' : ''}`} disabled={!isPastDate(d.fromDate)} value={`${d.fromDate}|${d.price}|${d.priceCurrency}|${d.discount}`}><span className="date-dropdown">{formatDate(d.fromDate)}</span></Option>
                                ))}
                              </Select>}
                            {!loadDatesDropdown &&
                              <Select defaultValue={""} style={{ width: "100%" }} onChange={(e) => handleDateChange(e)} dropdownClassName="select-date-dropdpwn">
                                <Option value="">View More Dates</Option>
                              </Select>}
                          </Row>
                        )}
                        <div className="booking-btn">
                          <span onClick={onReserveClick} className="date-yellow mr10 cursor-pointer" id="workshop_reserve"><Icon type="calendar" />&nbsp;&nbsp;RESERVE</span>
                          <span onClick={onIntseretClick} className="cursor-pointer date-green" id="workshop_get_in_touch"><Icon type="phone" rotate={90} />&nbsp;&nbsp;GET IN TOUCH</span>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </div>
            <Row gutter={20} className={`${learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).length < 1 ? 'pt60 trip_view_edit_detail' : learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).length > 0 && learning.dateTime.filter((dd) => isPastDate(dd.fromDate)).length < 4 ? 'pt50 trip_view_edit_detail' : ' trip_view_edit_detail'}`}>
              <Col className="trip_desc_detail justify" xs={24} sm={24} md={17} lg={17} xl={17}>
                <Row>
                  <Col span={24}>
                    <h3 className=" an-22 medium-text pb10 workshop_detail_txt">Workshop Highlights</h3>
                  </Col>
                  <Col span={19}>
                    <div className="trip_detail_des">
                      <h4 className="medium-text an-18">About This Workshop</h4>
                    </div>
                  </Col>
                  <Col span={5}>
                    <div className=" text-right edt_btn_sec">
                      <Link href={`/update-learning/${id}`}>
                        <a><i className="fas fa-pencil-alt"></i> Edit</a>
                      </Link>
                    </div>
                  </Col>
                </Row>

                <div className="trip_detail_des editor_text_display">
                  <span className="lh-30">
                    {learning.description && learning.description.trim() !== 'null'
                      ? ReactHtmlParser(learning.description)
                      : ''}
                  </span>
                </div>


                <Row>
                  <Col span={19}>
                    <div className='trip_detail_des'>
                      <h4 className='medium-text an-18'>What will you learn?</h4>
                    </div>
                  </Col>
                  <Col span={5}>
                    <div className=' text-right edt_btn_sec'>
                      <Link href={`/update-learning/${id}`}>
                        <a><i className='fas fa-pencil-alt'></i> Edit</a>
                      </Link>
                    </div>
                  </Col>
                </Row>

                <div className='trip_detail_des editor_text_display'>
                  <span className='lh-30'>
                    {learning.whatLearn.trim() !== '' ? ReactHtmlParser(learning.whatLearn) : ''}
                    {learning.whatLearn === "" && "No Details has found"}
                  </span>
                </div>


                <Row>
                  <Col span={19}>
                    <div className='trip_detail_des'>
                      <h4 className='medium-text an-18'>Who should attend?</h4>
                    </div>
                  </Col>
                  <Col span={5}>
                    <div className=' text-right edt_btn_sec'>
                      <Link href={`/update-learning/${id}`}>
                        <a><i className='fas fa-pencil-alt'></i> Edit</a>
                      </Link>
                    </div>
                  </Col>
                </Row>

                <div className='trip_detail_des editor_text_display'>
                  <span className='lh-30'>
                    {learning.attend.trim() !== '' ? ReactHtmlParser(learning.attend) : ''}
                    {learning.attend === "" && "No Details has found"}
                  </span>
                </div>
              </Col>

              <Col xs={24} sm={24} md={7} lg={7} xl={7} className="prl-0">
                <AboutThisExpert type="workshop" expert={learning.expert} />
              </Col>
            </Row>

            {learning.workshopMedium !== "online" && (
              <Row>
                <Col className='metting_point_trip justify' xs={24} sm={24} md={17} lg={17} xl={17}>
                  <Row className='pr30 pt20'>
                    <Col span={18}>
                      <div className='trip_detail_des location-field'>
                        <h4 className='medium-text an-18'>Location</h4>
                      </div>
                    </Col>
                    <Col span={6}>
                      <div className=' text-right edt_btn_sec pr30Itinerary'>
                        <Link href={`/update-learning/${id}`}>
                          <a><i className='fas fa-pencil-alt'></i> Edit</a>
                        </Link>
                      </div>
                    </Col>
                    <Col span={20}>

                      <p className="pt5 mb0 trip_location_des">
                        {learning.address}
                      </p>

                    </Col>

                    <Col span={16} className="trip_meeting_point">
                      <div className=''>
                        <h4 className='medium-text an-18 meting-text'>Meeting Point</h4>
                        <p className='lh-30 desc'>{learning.meetingPoint || "No meeting point given."}</p>
                      </div>
                    </Col>

                  </Row>
                  <Row className='pt0 map_trip_edit'>
                    <Col span={24} className='map'>
                      <TripMap
                        center={learning.location.coordinates}
                        zoom={5}
                        ApiKey={ApiKey}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            )}


            <Row className="extra_info_trip justify mt40">
              <Col xs={24} sm={24} md={17} lg={17} xl={17}>
                <Col span={18} className="">
                  <div className=''>
                    <h4 className='medium-text an-18 pt35 pb25'>
                      Workshop Details
                    </h4>
                  </div>
                </Col>
                <Col span={6}>
                  <div className=' text-right edt_btn_sec pt35 pb25'>
                    <Link href={`/update-learning/${id}`}>
                      <a><i className='fas fa-pencil-alt'></i> Edit</a>
                    </Link>
                  </div>
                </Col>
              </Col>
            </Row>

            <AdditionalDetails
              item={learning}
              type={"workshop"}
              isEdit={true}
              addAccommoPopUp={addAccommoPopUp}
              onAccommoImageClick={onAccommoImageClick}
              review={review}
            />

            <Row className="mt75 mb75">
              <Col xs={24} sm={24} md={17} lg={17} xl={17} >
                <div className="person_sec">
                  <div className="fill-width">
                    <h1 className="medium-text an-28 mb0">
                      <span className="after-discount">{getCurrencySymbol(datePriceCurrency || learning.priceCurrency)} {getPriceAfterDiscount(datePrice || learning.price, learning.discount && slot === null ? learning.discount : datePriceDiscount || "")}</span>
                      {((typeof learning.discount !== "undefined" && learning.discount !== "" && learning.discount !== null && slot === null) || (datePriceDiscount !== "" && datePriceDiscount !== null && datePriceDiscount !== "null" && datePriceDiscount)) &&
                        <span className="orignal-price">{getCurrencySymbol(datePriceCurrency || learning.priceCurrency)} {datePrice || learning.price}</span>
                      }
                    </h1>
                    <p className="an-14 medium-text">{getPriceType(learning.priceType)}</p>
                  </div>
                  <div className='fill-width text-right booking-btn-detail-page'>
                    <span onClick={onReserveClick} className="date-yellow mr10 cursor-pointer" id="workshop_reserve" ><Icon type="calendar" />&nbsp;&nbsp;RESERVE</span>
                    <span onClick={onIntseretClick} className="cursor-pointer date-green" id="workshop_get_in_touch" ><Icon type="phone" rotate={90} />&nbsp;&nbsp;GET IN TOUCH</span>
                  </div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col className="trip_photoset">
                <div className="photo_sec slider_prev_next">
                  <Row>
                    <Col>
                      <Row>
                        <Col span={12}>
                          <h4 className="sub_title">
                            Pictures and Videos from Past Workshops
                          </h4>
                        </Col>
                        <Col className="fix_adit_button" span={12}>
                          <div className='fix_top_pad text-right edt_btn_sec adit_fix' onClick={addPhoto}>
                            <i className='fas fa-pencil-alt'></i> Add Photos
                          </div>
                        </Col>
                      </Row>
                      <Row gutter={[40]}>
                        <OwlCarousel
                          className="owl-theme owl-dots"
                          {...sliderOptions}
                        >
                          {learning.images.length > 0 && learning.images.map((img, index) => {
                            return (

                              < Col key={index} xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row pb25" >
                                {
                                  img.search(".mp4") !== -1 ?

                                    (<ReactPlayer className="video_edit_page" url={img} onClick={() => onAccommoImageClick(index, 'photos')} />)

                                    : (<img src={img} alt="" onClick={() => onAccommoImageClick(index, 'photos')} />)
                                }
                              </Col>
                            )
                          })}
                          {learning.images.length === 0 &&
                            <>
                              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row pb25">
                                <img src={Placeholder} alt="" />
                              </Col>
                              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row pb25">
                                <img src={Placeholder} alt="" />
                              </Col>
                              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row pb25">
                                <img src={Placeholder} alt="" />
                              </Col>
                            </>
                          }
                        </OwlCarousel>

                      </Row>

                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
        </div>
        {
          moreDetail && (
            <MoreDetails
              visible={moreDetail}
              data={learning.dateTime}
              onIntseretClick={onIntseretClick}
              onReserveClick={onReserveClick}
              onClickDate={(d) => onClickDate(d)}
              onCloseClick={onCloseClick}
            />
          )
        }
        {
          showCanc && (
            <Cancellation
              visible={showCanc}
              data={learning.cancellantion}
              onCloseClick={onCloseClick}
            />
          )
        }
        {
          showR && (
            <Reservation
              visible={showR}
              trip={learning.title}
              slot={slot}
              onCloseClick={onCloseClick}
              id={id}
            />
          )
        }
        {
          showInterest && (
            <Interested
              visible={showInterest}
              trip={learning.title}
              onCloseClick={onInstCloseClick}
              id={id}
            />
          )
        }

        {
          accommoPopUp && (
            <AccommodationPhoto
              visible={accommoPopUp}
              onCloseClick={onCloseClick}
              id={id}
              getRefreshTrip={getRefreshTrip}
              type="workshop"
              selectedAlbum={learning.accomodationPhotos}
            />
          )
        }

        {
          accommodationPhotoViewPopup && (
            <AccommodationPhotoViewPopup
              visible={accommodationPhotoViewPopup}
              onCloseClick={onCloseClick}
              id={id}
              allPhotos={currentPhotoView === "accomo" ? learning.accomodationPhotos : learning.images}
              currentViewImage={currentViewImage}
            />
          )
        }

        {
          photoPopUp && (
            <AddPhoto
              visible={photoPopUp}
              onCloseClick={onCloseClick}
              albumList={albums}
              id={id}
              currentImages={learning.images}
              getRefreshTrip={getRefreshTrip}
              type="workshop"
            />
          )
        }

        <Newsletter page="workshop_detail" />
      </div >
    );
  } else {
    return (
      <div className="text-center py20 loader-absolute-class">
        <AppLoader />
      </div>
    );
  }
};
export default WorkShopDetails;
