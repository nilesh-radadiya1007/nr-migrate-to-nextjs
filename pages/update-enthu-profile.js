import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';


const CreateEnthusiest = dynamic(
    () => import("../src/containers/CreateEnthusiest"),
    { ssr: false }
);

const CreateEnthusiestInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <CreateEnthusiest />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default CreateEnthusiestInApp;