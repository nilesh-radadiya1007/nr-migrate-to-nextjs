import axios from 'axios';
import { setHeadersWithAccessToken } from './index';
import { getApiUrl, getPreferredCurrency } from '../helpers/methods';
import { logDOM } from '@testing-library/react';

const API_BASE = getApiUrl();

export const CreateExpertProfile = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/expert`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getPublicProfile = id => {
  return axios
    .get(`${API_BASE}/expert/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const CreateTrips = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/trips`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const CopyTrips = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/trips/copy`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getUpcomingTrip = id => {
  return axios
    .get(`${API_BASE}/trips/upcoming/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getUpcomingWorkShops = id => {
  return axios
    .get(`${API_BASE}/learnings/upcoming/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getTravelMap = id => {
  return axios
    .get(`${API_BASE}/trips/travel-map/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getTripDetail = id => {
  return axios
    .get(`${API_BASE}/trips/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getAllExperts = (
  data = {
    country: "",
    activity: [],
    skills: [],
    langauge: [],
    sortOrder: 1
  }
) => {
  const { country, activity, langauge, sortOrder, skills, isFeatured } = data;

  let act = activity.length ? JSON.stringify(activity) : "";
  let skill = skills !== undefined && skills.length ? JSON.stringify(skills) : "";
  let lan = langauge.length ? JSON.stringify(langauge) : "";
  let isFeaturedData = data.isFeatured ? true : false;

  return axios
    .get(
      `${API_BASE}/expert/all?cnty=${country}&langauge=${lan}&activity=${act}&skills=${skill}&sortOrder=${sortOrder}&currentPage=${data.currentPage}&perPage=${data.perPage}&isFeatured=${isFeaturedData}`
      , {
        headers: {
          'preferredcurrency': getPreferredCurrency()
        }
      })
    .then(e => e)
    .catch(e => e);
};

export const getAllTrips = (data = {
  activity: [],
  country: "",
  difficulty: "",
  month: "",
  price: "",
  suitable: "",
  type: []
}) => {
  const { activity, country, type, month, price, suitable, difficulty, sortBy, pageNo, perPage } = data;
  let typ = type.length ? JSON.stringify(type) : "";
  let act = activity.length ? JSON.stringify(activity) : "";
  let sutbl = suitable.length ? !suitable.includes("all") ? JSON.stringify(suitable) : "" : "";
  return axios.get(`${API_BASE}/trips/all?type=${typ}&country=${country}&activity=${act}&month=${month}&price=${price}&suitable=${sutbl}&difficulty=${difficulty}&sortBy=${sortBy}&currentPage=${pageNo}&perPage=${perPage}`, {
    headers: {
      'preferredcurrency': getPreferredCurrency()
    }
  })
    .then(e => e)
    .catch(e => e);
};

export const getUpcommingTrip = (data = {
  activity: [],
  country: "",
  difficulty: "",
  month: "",
  price: "",
  suitable: "",
  type: []
}) => {
  const { activity, country, type, month, price, suitable, difficulty, sortBy } = data;
  let typ = type.length ? JSON.stringify(type) : "";
  let act = activity.length ? JSON.stringify(activity) : "";
  let sutbl = suitable.length ? !suitable.includes("all") ? JSON.stringify(suitable) : "" : "";
  return axios.get(`${API_BASE}/trips/all?type=${typ}&country=${country}&activity=${act}&month=${month}&price=${price}&suitable=${sutbl}&difficulty=${difficulty}&sortBy=${sortBy}`, {
    headers: {
      'preferredcurrency': getPreferredCurrency()
    }
  })
    .then(e => e)
    .catch(e => e);
};

export const UpdateExpertProfile = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/expert`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getSimilarTrips = (data) => {
  return axios
    .get(`${API_BASE}/trips/get-similar-trips?activity=${data.activity}&type=${data.type}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const uploadCoverOriginalPicture = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/expert/cover-original`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const uploadCoverOriginalPictureEthn = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/enthusiasts/cover-original`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const uploadProfileOriginalPicture = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/expert/cover-profile-image`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const uploadProfileOriginalPictureEthnu = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/enthusiasts/cover-profile-image`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const uploadAccommodationPicture = (token, data, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/trips/${id}`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const uploadCoverPictureEnth = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/enthusiasts/cover`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const createEnthProfile = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/enthusiasts`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const getEnthuProfile = (token) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/enthusiasts`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const UpdateEnthuProfile = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/enthusiasts`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const SendMessage = (token, param, tripId, type = 'trips') => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${type}/send-message/${tripId}`, param, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const addBooking = (token, param, tripId) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/bookings/${tripId}`, param, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getExpertMessasges = token => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/expert/messages`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};


export const getEnthMessasges = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/expert/getAllMessages/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getEnthTripMessasges = (token, id, tripId) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/expert/getAllTripMessages/${id}/${tripId}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getExpertBookings = (token, id, search) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/bookings/getAllBooking/${id}?name=${search.name}&status=${search.status}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const replyToMessasges = (token, param) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/expert/messages`, param, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getEnthuMessasges = token => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/enthusiasts/messages`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getEnthuMessasgesSession = (token, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/enthusiasts/messages/session?status=${params.status}&name=${params.name}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getEnthuTripMessages = (token, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/enthusiasts/messages/session`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const sendEnthuTripMessages = (token, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/enthusiasts/messages/send`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getExpertMessasgesSession = (token, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/expert/messages/session?status=${params.status}&name=${params.name}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const readMessages = (token, role, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${role}/messages/read`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getExpertTripMessages = (token, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/expert/messages/session`, params, {

    })
    .then(e => e)
    .catch(e => e);
};

export const sendExpertTripMessages = (token, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/expert/messages/send`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getAllRecentTrips = () => {
  return axios
    .get(`${API_BASE}/trips/recent`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}


export const CreateLearning = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/learnings`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const UpdateLearning = (id, token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/learnings/${id}`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const UpdateLearningCoverAndOriginalCover = (id, token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/learnings/update-cover/${id}`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const CopyLearning = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/learnings/copy`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getAllLearnings = () => {
  return axios
    .get(`${API_BASE}/learnings/?page=1`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const GetLearnings = (data) => {


  const {
    activity, country, workshopType,
    workshopMedium, month, price, langauges,
    sortBy, skill, suitable, pageNo, perPage
  } = data;
  let act = activity.length ? JSON.stringify(activity) : "";
  let lang = langauges.length ? JSON.stringify(langauges) : "";
  let wsType = workshopType.length ? !workshopType.includes("all") ? JSON.stringify(workshopType) : "" : "";
  let wMedium = workshopMedium.length ? !workshopMedium.includes("all") ? JSON.stringify(workshopMedium) : "" : "";
  let sutbl = suitable.length ? !suitable.includes("all") ? JSON.stringify(suitable) : "" : "";

  return axios
    .get(`${API_BASE}/learnings?langauges=${lang}&workshopMedium=${wMedium}&workshopType=${wsType}&country=${country}&skill=${skill}&activity=${act}&month=${month}&price=${price}&suitable=${sutbl}&sortBy=${sortBy}&perPage=${perPage}&currentPage=${pageNo}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const getUpcommingLearning = (data) => {


  const {
    activity, country, workshopType,
    workshopMedium, month, price, langauges,
    sortBy, skill, suitable
  } = data;
  let act = activity.length ? JSON.stringify(activity) : "";
  let lang = langauges.length ? JSON.stringify(langauges) : "";
  let wsType = workshopType.length ? !workshopType.includes("all") ? JSON.stringify(workshopType) : "" : "";
  let wMedium = workshopMedium.length ? !workshopMedium.includes("all") ? JSON.stringify(workshopMedium) : "" : "";
  let sutbl = suitable.length ? !suitable.includes("all") ? JSON.stringify(suitable) : "" : "";

  return axios
    .get(`${API_BASE}/learnings?langauges=${lang}&workshopMedium=${wMedium}&workshopType=${wsType}&country=${country}&skill=${skill}&activity=${act}&month=${month}&price=${price}&suitable=${sutbl}&sortBy=${sortBy}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const GetLearningDetails = (id) => {
  return axios
    .get(`${API_BASE}/learnings/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const GetLearningMy = (token) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/learnings/my`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const InActiveTrip = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/trips/inActive/${id}`, {}, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const ActiveTrip = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/trips/active/${id}`, {}, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const InActiveWorkshop = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/learnings/inActive/${id}`, {}, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const ActiveWorkshop = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/learnings/active/${id}`, {}, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const GetMyTrips = (token) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/trips`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const getSimilarLearnings = (data) => {
  return axios
    .get(`${API_BASE}/learnings/get-similar-trips?activity=${data.activity}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const UpdateTrip = (id, token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/trips/${id}`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const UpdateTripCoverAndOriginalCover = (id, token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/trips/update-cover/${id}`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const CreateAlbums = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/album`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const updateAlbums = (token, data, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/album/${id}`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getAllAlbums = () => {
  return axios
    .get(`${API_BASE}/album/all`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getMyAlbums = (token) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/album/my`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const getExpertAlbums = id => {
  return axios
    .get(`${API_BASE}/album/expertAlbums/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const deleteAlbums = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .delete(`${API_BASE}/album/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const inActiveAlbums = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/album/inActive/${id}`, {}, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const activeAlbums = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/album/active/${id}`, {}, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const CreateAlbumComment = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/albumComment`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const getAlbumComment = (data) => {
  return axios
    .get(`${API_BASE}/albumComment/${data.albumId}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const Logout = (id) => {
  return axios
    .post(`${API_BASE}/auth/logout`, { id }, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};

export const GetUserStatus = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/auth/status`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const hideMessages = (token, role, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${role}/messages/hide`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const deleteMessages = (token, role, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${role}/messages/remove`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const starMessages = (token, role, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${role}/messages/star`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const unReadSideMessages = (token, role, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${role}/messages/unreadsidebar`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const deleteMessage = (token, role, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${role}/messages/delete`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const editMessage = (token, role, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${role}/messages/edit`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const confirmBooking = (token, role, params) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/${role}/messages/confirm`, params, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const getBookingDetails = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/bookings/details/${id}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const starBooking = (token, id, status) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/bookings/star`, { id, status }, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const hideBooking = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/bookings/hide`, { id }, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const deleteBooking = (token, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/bookings/delete`, { id }, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const uploadAccommodationPhotos = (token, data, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/trips/accomo/${id}`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}


export const getLeraningPlusTripNew = (data, isHomepageCall = false) => {

  if (typeof data === "undefined" || data === "undefined" || isHomepageCall) {
    let isFeaturedData = data.isFeatured ? true : false;

    return axios
      .get(`${API_BASE}/learnings/learnings-plus-trips-new?sortBy=created_at&adventure=${data.adventure}&perPage=${data.perPage}&currentPage=${data.pageNo}&isFeatured=${isFeaturedData}`, {
        headers: {
          'preferredcurrency': getPreferredCurrency()
        }
      })
      .then(e => e)
      .catch(e => e);

  } else {

    const {
      activity, country, workshopType,
      workshopMedium, month, price, langauges,
      sortBy, skill, suitable, difficulty, pageNo, perPage, adventure, isFlexible, year, weekend, certification, longAdventure, isOtherCountry, tmpCountry, tripLength } = data;
    let act = activity.length ? JSON.stringify(activity) : "";
    let lang = langauges.length ? JSON.stringify(langauges) : "";
    let wsType = workshopType.length ? !workshopType.includes("all") ? JSON.stringify(workshopType) : "" : "";
    let wMedium = workshopMedium.length ? !workshopMedium.includes("all") ? JSON.stringify(workshopMedium) : "" : "";
    let suitable1 = suitable.length ? !suitable.includes("all") ? JSON.stringify(suitable) : "" : "";
    let tmpisOtherCountry = isOtherCountry !== "undefined" && isOtherCountry === true ? true : false;
    let lat = localStorage.getItem('lat');
    let lng = localStorage.getItem('lng');
    let kilometer = 50000;
    return axios
      .get(`${API_BASE}/learnings/learnings-plus-trips-new?langauges=${lang}&workshopMedium=${wMedium}&workshopType=${wsType}&country=${country}&skill=${skill}&activity=${act}&month=${month}&price=${price}&suitable=${suitable1}&difficulty=${difficulty}&sortBy=${sortBy}&perPage=${perPage}&currentPage=${pageNo}&adventure=${adventure}&isFlexible=${isFlexible}&year=${year}&weekend=${weekend}&certification=${certification}&longAdventure=${longAdventure}&isOtherCountry=${tmpisOtherCountry}&tmpCountry=${tmpCountry}&lat=${lat}&lng=${lng}&km=${kilometer}&tripLength=${tripLength}`, {
        headers: {
          'preferredcurrency': getPreferredCurrency()
        }
      })
      .then(e => e)
      .catch(e => e);
  }

}

export const getLeraningPlusTrip = (data) => {
  if (typeof data === "undefined" || data === "undefined") {
    return axios
      .get(`${API_BASE}/learnings/learnings-plus-trips?sortBy=created_at`, {
        headers: {
          'preferredcurrency': getPreferredCurrency()
        }
      })
      .then(e => e)
      .catch(e => e);
  } else {
    const {
      activity, country, workshopType,
      workshopMedium, month, price, langauges,
      sortBy, skill, suitable, difficulty, pageNo, perPage
    } = data;
    let act = activity.length ? JSON.stringify(activity) : "";
    let lang = langauges.length ? JSON.stringify(langauges) : "";
    let wsType = workshopType.length ? !workshopType.includes("all") ? JSON.stringify(workshopType) : "" : "";
    let wMedium = workshopMedium.length ? !workshopMedium.includes("all") ? JSON.stringify(workshopMedium) : "" : "";
    let suitable1 = suitable.length ? !suitable.includes("all") ? JSON.stringify(suitable) : "" : "";
    return axios
      .get(`${API_BASE}/learnings/learnings-plus-trips?langauges=${lang}&workshopMedium=${wMedium}&workshopType=${wsType}&country=${country}&skill=${skill}&activity=${act}&month=${month}&price=${price}&suitable=${suitable1}&difficulty=${difficulty}&sortBy=${sortBy}&perPage=${perPage}&currentPage=${pageNo}`, {
        headers: {
          'preferredcurrency': getPreferredCurrency()
        }
      })
      .then(e => e)
      .catch(e => e);
  }
}

export const accommoUpdate = (token, data, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/trips/accommoupdate/${id}`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};


export const uploadCoverBookPicture = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/expert/bookcover`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const uploadPhotosAndVideo = (token, data, id) => {
  setHeadersWithAccessToken(token);
  return axios
    .put(`${API_BASE}/trips/photosAndVideo/${id}`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};


export const Subscribe = (data) => {
  return axios
    .post(`${API_BASE}/subscribe`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};


export const addorRemoveLikeForTrip = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/trips/likes/add-like-on-trip`, data)
    .then(e => e)
    .catch(e => e);
};

export const addorRemoveLikeForLearning = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/learnings/likes/add-like-on-learning`, data)
    .then(e => e)
    .catch(e => e);
}

export const addorRemoveLikeForAlbum = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/album/likes/add-like-on-album`, data)
    .then(e => e)
    .catch(e => e);
};


export const getAllOffers = (data) => {
  let isOffered = data.isOffered ? true : false;
  return axios
    .get(`${API_BASE}/learnings/get-all-offers?sortBy=created_at&adventure=${data.adventure}&perPage=${data.perPage}&currentPage=${data.pageNo}&isOffered=${isOffered}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
}

export const getExpertAndAdventureData = (token, data) => {
  setHeadersWithAccessToken(token);
  console.log(data)
  let admin = data.admin || "";
  let type = data.type || "";
  return axios
    .get(`${API_BASE}/enthusiasts/review/get-expert-adventure?eid=${data.eid}&twid=${data.twid}&admin=${admin}&type=${type}`)
    .then(e => e)
    .catch(e => e);
}

export const postReview = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/enthusiasts/review/submit-review`, data)
    .then(e => e)
    .catch(e => e);
};

// export const getAllReviewByExpert = (isPublicView, tokenOrId) => {
//   let URL = "";
//   console.log('-------------------------');
//   console.log('isPublicView-->',isPublicView);
//   console.log('-------------------------');

//   if (isPublicView) {
//     setHeadersWithAccessToken(tokenOrId);
//     URL = `${API_BASE}/enthusiasts/review/getAllReviewByExpert?eid=${tokenOrId}`;
//   } else {
//     URL = `${API_BASE}/enthusiasts/review/getAllReviewByExpert`;
//   }

//   return axios
//     .get(URL)
//     .then(e => e)
//     .catch(e => e);
// }

export const getAllReviewByExpert = (isPublicView, tokenOrId) => {
  let URL = `${API_BASE}/enthusiasts/review/getAllReviewByExpert?eid=${tokenOrId}`;
  return axios
    .get(URL)
    .then(e => e)
    .catch(e => e);
}

export const Contact = (data) => {
  return axios
    .post(`${API_BASE}/subscribe/contact`, data, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);
};



export const getAdminLearningWorkshop = (data) => {


  let act = data.activity.length ? JSON.stringify(data.activity) : "";
  let cntry = data.country.length ? JSON.stringify(data.country) : "";
  // let lang = langauges.length ? JSON.stringify(langauges) : "";
  return axios
    .get(`${API_BASE}/learnings/learning/trip-workshop-email?adventure=${data.adventure}&country=${cntry}&activity=${act}&admin=${data.admin}`, {
      headers: {
        'preferredcurrency': getPreferredCurrency()
      }
    })
    .then(e => e)
    .catch(e => e);

}