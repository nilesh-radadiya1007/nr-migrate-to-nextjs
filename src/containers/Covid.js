import React from "react";
import { Row, Col } from "antd";
import Link from "next/link";
import { compose } from "redux";
// import Image from 'next/image';

import { useMediaQuery } from "react-responsive";
import Newsletter from '../components/common/NewsLetter';

const safty1 = "/images/covid/safty1.png";
const safty2 = "/images/covid/safty2.png";
const safty3 = "/images/covid/safty3.png";
const safty4 = "/images/covid/safty4.png";
const flexi = "/images/covid/flexi.png";
const instructor = "/images/covid/instructor.png";
const support = "/images/covid/support.png";

const SpecialOffers = (props) => {
    const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });

    return (
        <div className="header-container covid-page">
            <div className="expert_head_bg expert_head_bg_color">
                <div className="bg-image-layer"></div>
                <div className="expert_header_info">
                    <div className="bg-title">
                        <span className="first-txt">Adventuring Safely During</span><span className="second-txt">COVID-19</span>
                    </div>
                </div>
            </div>

            <div className="container covid-sections">
                <Row className="covid-title-parent">
                    <Col span={24} className="border-bottom-green">
                        <div className="covid-title">Our Response to COVID-19</div>
                    </Col>
                    <Col span={24}>
                        <p className="covid-subtitle">There is a lot of uncertainty around the globe at the moment as a result of the COVID-19 pandemic.
                            Our highest priority is the health and safety of our adventurers, experts and local communities.
                            That’s why we’ve put measures in place to enable adventuring to continue safely through these
                            uncertain times.</p>
                    </Col>
                </Row>

                <Row gutter={30} className="safty-section mb60" type="flex" align="middle">
                    <Col lg={12} xxl={12} className="text-section">
                        <div className="content">
                            <p className="content-main-title">Safety <span className="content-yellow">Comes First</span></p>
                            <p className="main-text">Health and safety is our biggest priority at Expeditions Connect. Although we don’t organise
                                expeditions ourselves, our experts have put measures in place to keep our adventurers safe. This
                                may include limiting the sizes of groups, putting distancing measures in place and the use of face masks where appropriate.
                            </p>
                            <p className="main-text">
                                For more information about safety measures for your chosen expedition or activity, you can contact
                                the leading expert directly.
                            </p>
                        </div>
                    </Col>
                    <Col lg={12} xxl={12} className="image-section">
                        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="image-title">
                            <p className="img-title">Smaller Groups</p>
                            <div>
                                <img className="safty-img" src={safty1} alt="" />
                            </div>
                        </Col>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="image-title">
                            <p className="img-title">Maintain Distance</p>
                            <div>
                                <img className="safty-img" src={safty2} alt="" />
                            </div>
                        </Col>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="image-title">
                            <p className="img-title">Use Face Masks</p>
                            <div>
                                <img className="safty-img" src={safty3} alt="" />
                            </div>
                        </Col>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="image-title">
                            <p className="img-title">Wash Hands</p>
                            <div>
                                <img className="safty-img" src={safty4} alt="" />
                            </div>
                        </Col>
                    </Col>
                </Row>

                <Row gutter={30} className="flexi-section mb60" type="flex" align="middle">
                    <Col lg={12} xxl={12} className="right-img-wrap">
                        <img className="covid-images" src={flexi} alt="community" />
                    </Col>
                    <Col lg={12} xxl={12} className="align-images">
                        <div className="content">
                            <p className="content-main-title">Flexible <span className="content-yellow">Booking</span></p>
                            <p className="main-text">We understand that things are uncertain at the moment, but that doesn’t mean that you need to
                                put your adventures on hold. Thanks to the co-operation of our experts, we are able to offer flexible
                                booking, meaning that you can rearrange your booking or obtain a refund if there are any COVID-
                                related surprises. Rest assured, you can book with confidence with Expeditions Connect.
                                For more information about the flexible booking processes for your chosen expedition or activity,
                                you can contact the leading expert directly.
                            </p>

                        </div>
                    </Col>
                </Row>


                <Row gutter={30} className="travel-section mb60" type="flex" align="middle">
                    <Col lg={12} xxl={12} className="align-text">
                        <div className="content">
                            <p className="content-main-title">Follow <span className="content-yellow">Local Guidelines</span></p>
                            <p className="main-text">It's essential to follow local guidelines when you're traveling. This means taking note of official
                                government guidance, as well as corresponding with your travel insurer. It's important to note that
                                your travel insurance may be invalidated if you travel against government guidance.
                            </p>
                        </div>
                    </Col>
                    <Col lg={12} xxl={12} className="align-images">
                        <img className="covid-images travel-image" src={instructor} alt="community" />
                    </Col>
                </Row>


                <Row gutter={30} className="customer-support  mb60" type="flex" align="middle">
                    {!isMaxWidth768 &&
                        <>
                            <Col lg={12} xxl={12} className="align-text">
                                <img className="covid-images customer-image" src={support} alt="community" />
                            </Col>
                            <Col lg={12} xxl={12} className="align-images">
                                <div className="content">
                                    <p className="content-main-title">Outstanding <span className="content-yellow">Customer Support</span></p>
                                    <p className="main-text">We’re proud to offer the highest levels of customer support to our adventurers. Should you find
                                        yourself in an exceptional circumstance, we will always do our best to offer support and assist you in
                                        any way possible. If you have any questions or concerns about adventuring during COVID-19, please
                                        get in touch and we’ll be happy to assist you.
                                    </p>
                                </div>
                            </Col>
                        </>
                    }
                    {isMaxWidth768 &&
                        <>
                            <Col lg={12} xxl={12} className="align-images">
                                <div className="content">
                                    <p className="content-main-title">Outstanding <span className="content-yellow">Customer Support</span></p>
                                    <p className="main-text">We’re proud to offer the highest levels of customer support to our adventurers. Should you find
                                        yourself in an exceptional circumstance, we will always do our best to offer support and assist you in
                                        any way possible. If you have any questions or concerns about adventuring during COVID-19, please
                                        get in touch and we’ll be happy to assist you.
                                    </p>
                                </div>
                            </Col>
                            <Col lg={12} xxl={12} className="align-text">
                                <img className="covid-images customer-image" src={support} alt="community" />
                            </Col>
                        </>
                    }
                </Row>

                <Row gutter={30} className="mb60">
                    <Col span={24} className="border-bottom-green">
                        <p className="content-main-title">Additional  <span className="content-yellow">Resources</span></p>
                    </Col>
                    <Col span={24} className="mb60">
                        <p className="covid-subtitle-additional">We understand that during the COVID-19 pandemic, you may need some extra information to
                            support you in planning your next adventure. Here are some additional resources to help you find
                            out current COVID-9 restrictions worldwide.
                        </p>
                    </Col>
                    <Col span={24}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8} className="additional-info">
                            <p className="ai-title">World Health Organization (WHO)</p>
                            <p className="ai-subtitle">
                                <Link href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019" target="_blank"> <a target="_blank">Learn More</a></Link>

                            </p>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8} className="additional-info">
                            <p className="ai-title">United Nations World<br></br>Tourism Organization (UNWTO)</p>
                            <p className="ai-subtitle">
                                <Link href="https://www.unwto.org/unwto-iata-destination-tracker" target="_blank"> <a target="_blank">Learn More</a></Link>
                            </p>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8} className="additional-info">
                            <p className="ai-title">European Union (EU)</p>
                            <p className="ai-subtitle">
                                <Link href="https://reopen.europa.eu/en/" target="_blank"><a target="_blank">Learn More</a></Link>
                            </p>
                        </Col>
                    </Col>
                </Row>

                <Row gutter={30} className="mb60">
                    <Col span={!isMaxWidth768 ? 10 : 24} offset={!isMaxWidth768 ? 7 : 0}>
                        <div className="note-text">
                            Notes: The above resources are meant for informational purposes only. Expeditions Connect is not
                            responsible for its accuracy. Given how quickly the travel requirements are evolving, you should not
                            rely solely on these tools in making travel arrangements and should confirm the accuracy of any
                            government guidance, restrictions, or requirements
                        </div>
                    </Col>
                </Row>

            </div>
            <Newsletter page="covid" />

        </div >
    );
};

export default compose(SpecialOffers);