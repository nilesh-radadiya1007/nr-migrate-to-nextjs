/* eslint-disable react/display-name */
import React, { Fragment, useEffect, useState } from "react";
import { Form, Upload, Input, Row, Col, Button, Icon } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { compose } from "redux";
import ReactQuill from 'react-quill';

/**
 * App Imports
 */
import { ExpertEvents } from "../../redux/expert/events";
import {
  BioValidator,
  AwardsValidator,
  // InitiativesValidator,
} from "../../helpers/validations";
// import { RemoveHttpCom } from '../../../../helpers/methods';

const { TextArea } = Input;

const CreateExpertStepAwardsAndBio = React.memo((props) => {
  const router = useRouter();
  // const {
  //   location: { pathname },
  // } = router;

  const dispatch = useDispatch();
  const expert = useSelector((state) => state.expert);

  const { stepAwardsAndBio, changeTab } = ExpertEvents;

  const { getFieldDecorator, getFieldValue, setFieldsValue } = props.form;

  const { bio, awards, professionalQualifications, socialContributions, booksAndPublications, professionalExperience } = expert;

  useEffect(() => {
    let awardsVal = awards && awards.length > 0 ? awards : [];
    let professionalExperiencesVal = professionalExperience && professionalExperience.length > 0 ? professionalExperience : [];
    let professionalQualificationsVal = professionalQualifications && professionalQualifications.length > 0 ? professionalQualifications : [];
    let socialContributionsVal = socialContributions && socialContributions.length > 0 ? socialContributions : [];
    let booksAndPublicationsVal = booksAndPublications && booksAndPublications.length > 0 ? booksAndPublications : [];

    setFieldsValue({
      bio,
      awards: awardsVal,
      professionalExperience: professionalExperiencesVal,
      professionalQualifications: professionalQualificationsVal,
      socialContributions: socialContributionsVal,
      booksAndPublications: booksAndPublicationsVal,
    });
  }, [awards, bio, professionalQualifications, socialContributions, booksAndPublications, setFieldsValue, professionalExperience]);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      const awards = [];
      const professionalExperience = [];
      const professionalQualifications = [];
      const socialContributions = [];
      const booksAndPublications = [];
      if (!err) {
        if (values.awards.length > 0) {
          values.awards.map((key, i) => {
            awards.push({
              name: values[`award-name-${i}`],
              authority: values[`award-issueing-authority-${i}`],
              year: values[`award-issued-year-${i}`],
            });
            return null;
          });
        }

        if (values.professionalQualifications.length > 0) {
          values.professionalQualifications.map((key, i) => {
            professionalQualifications.push({
              name: values[`qualification-name-${i}`],
              authority: values[`qualification-issuing-authority-${i}`],
              year: values[`qualification-issued-year-${i}`],
            });
            return null;
          });
        }

        if (values.professionalExperience.length > 0) {
          values.professionalExperience.map((key, i) => {
            professionalExperience.push({
              role: values[`experience-role-${i}`],
              name: values[`experience-organization-${i}`],
              year: values[`experience-issued-year-${i}`],
            });
            return null;
          });
        }

        if (values.socialContributions.length > 0) {
          values.socialContributions.map((key, i) => {
            socialContributions.push({
              name: values[`organization-name-${i}`],
              url: values[`social-contributions-url-${i}`],
              description: values[`social-contributions-description-${i}`] || "",
            });
            return null;
          });
        }

        if (values.booksAndPublications.length > 0) {
          values.booksAndPublications.map((key, i) => {
            booksAndPublications.push({
              name: values[`book-and-publication-name-${i}`],
              url: values[`book-and-publication-url-${i}`],
              // cover: values[`book-and-publication-cover-${i}`],
            });
            return null;
          });
        }

        const awardsAndBioObject = {
          bio: values.bio,
          professionalExperience,
          professionalQualifications,
          awards,
          socialContributions,
          booksAndPublications,
        };
        window.scrollTo({ top: document.getElementById('scroll-to-here'), behavior: 'smooth' });
        dispatch(stepAwardsAndBio(awardsAndBioObject));
      }
    });
  };

  const addAwardsHandler = () => {
    const keys = getFieldValue("awards");
    const nextKeys = keys.concat(new Date().getTime());
    setFieldsValue({ awards: nextKeys });
  };
  const removeAwards = (k) => {
    const keys = getFieldValue("awards");
    setFieldsValue({ awards: keys.filter((key) => key !== k) });
  };

  const addSocialContributionHandler = () => {
    const keys = getFieldValue("socialContributions");
    const nextKeys = keys.concat(new Date().getTime());
    setFieldsValue({ socialContributions: nextKeys });
  };
  const removeSocialContribution = (k) => {
    const keys = getFieldValue("socialContributions");
    setFieldsValue({ socialContributions: keys.filter((key) => key !== k) });
  };

  const addBooksAndPublicationHandler = () => {
    const keys = getFieldValue("booksAndPublications");
    const nextKeys = keys.concat(new Date().getTime());
    setFieldsValue({ booksAndPublications: nextKeys });
  };
  const removeBooksAndPublication = (k) => {
    const keys = getFieldValue("booksAndPublications");
    setFieldsValue({ booksAndPublications: keys.filter((key) => key !== k) });
  };

  const addProfessionalQualificationHandler = () => {
    const keys = getFieldValue("professionalQualifications");
    const nextKeys = keys.concat(new Date().getTime());
    setFieldsValue({ professionalQualifications: nextKeys });
  };

  const removeProfessionalQualification = (k) => {
    const keys = getFieldValue("professionalQualifications");
    setFieldsValue({ professionalQualifications: keys.filter((key) => key !== k) });
  };


  const addProfessionalExperienceHandler = () => {
    const keys = getFieldValue("professionalExperience");
    const nextKeys = keys.concat(new Date().getTime());
    setFieldsValue({ professionalExperience: nextKeys });
  };


  const removeProfessionalExperience = (k) => {
    const keys = getFieldValue("professionalExperience");
    setFieldsValue({ professionalExperience: keys.filter((key) => key !== k) });
  };

  const handleDescriptionChange = (textQuillBubble) => {
    setFieldsValue({ bio: textQuillBubble });
  };

  getFieldDecorator("awards", { initialValue: [] });
  const awardsFiels = getFieldValue("awards");
  const awardsItems = awardsFiels.map((key, i) => (
    <Fragment key={i}>
      <div className="align-end border_sec">
        <Row gutter={24}>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item label="Award Name" className="mb0">
              {getFieldDecorator(`award-name-${i}`, {
                initialValue: key.name,
                ...AwardsValidator,
              })(<Input placeholder="Enter Awards Name" />)}
            </Form.Item>
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item label="Issuing Authority" className="mb0 flex-1">
              {getFieldDecorator(`award-issueing-authority-${i}`, {
                initialValue: key.authority,
                rules: [
                  {
                    required: true,
                    message: "Issuing authority is required",
                  },
                ],
              })(<Input placeholder="Enter Issuing Authority" />)}
            </Form.Item>
          </Col>
        </Row>
        <Icon
          className="delete-icon delete-icon-center"
          type="close"
          onClick={() => removeAwards(key)}
        />
      </div>
    </Fragment>
  ));

  getFieldDecorator("professionalExperience", { initialValue: [] });
  const professionalExperienceFields = getFieldValue(
    "professionalExperience"
  );
  const professionalExperienceItems = professionalExperienceFields.map(
    (key, i) => (
      <Fragment key={i}>
        <div className="align-end border_sec">
          <Row gutter={24}>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Form.Item label="Role & Organization Name" className="mb0 flex-1">
                {getFieldDecorator(`experience-role-${i}`, {
                  initialValue: key.role,
                  rules: [
                    {
                      required: true,
                      message: "Role & Organization Name is required",
                    },
                    {
                      min: 5,
                      message: "Min length 5 is required!",
                    },
                  ],
                })(<Input placeholder="Enter Role & Organization Name is required" />)}
              </Form.Item>
            </Col>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Form.Item label="From Year - To Year" className="mb0 flex-1">
                {getFieldDecorator(`experience-issued-year-${i}`, {
                  initialValue: key.year,
                  rules: [
                    {
                      required: false,
                      message: "Please enter from year - to year (ex. 2019-2020)",
                      pattern: /(^[0-9]+[-]*[0-9]+$)/
                    },
                  ],
                })(<Input placeholder="Enter From year - To year (ex. 2019-2020)" />)}
              </Form.Item>
            </Col>
          </Row>
          <Icon
            className="delete-icon"
            type="close"
            onClick={() => removeProfessionalExperience(key)}
          />
        </div>
      </Fragment>
    )
  );

  getFieldDecorator("professionalQualifications", { initialValue: [] });
  const professionalQualificationFields = getFieldValue(
    "professionalQualifications"
  );
  const professionalQualificationItems = professionalQualificationFields.map(
    (key, i) => (
      <Fragment key={i}>
        <div className="align-end border_sec">
          <Row gutter={24}>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Form.Item label="Qualification Name" className="mb0 flex-1">
                {getFieldDecorator(`qualification-name-${i}`, {
                  initialValue: key.name,
                  rules: [
                    {
                      required: true,
                      message: "Qualification name is required",
                    },
                    {
                      min: 10,
                      message: "Min length 10 is required!",
                    },
                  ],
                })(<Input placeholder="Enter Qualification Name" />)}
              </Form.Item>
            </Col>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Form.Item label="Issuing Authority" className="mb0 flex-1">
                {getFieldDecorator(`qualification-issuing-authority-${i}`, {
                  initialValue: key.authority,
                  rules: [
                    {
                      required: true,
                      message: "Issuing authority is required",
                    },
                  ],
                })(<Input placeholder="Enter Issuing Authority" />)}
              </Form.Item>
            </Col>
          </Row>
          <Icon
            className="delete-icon delete-icon-center"
            type="close"
            onClick={() => removeProfessionalQualification(key)}
          />
        </div>
      </Fragment>
    )
  );

  getFieldDecorator("socialContributions", { initialValue: [] });
  const socialContributionFields = getFieldValue("socialContributions");
  const socialContributionItems = socialContributionFields.map((key, i) => (
    <Fragment key={i}>
      <div className="align-end border_sec">
        <Row gutter={24}>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item
              label="Initiative Name"
              className="mb0 flex-1"
            >
              {getFieldDecorator(`organization-name-${i}`, {
                initialValue: key.name,
                rules: [
                  {
                    required: true,
                    message: "Initiative Name is required",
                  },
                ],
              })(<Input placeholder="Enter Initiative Name" />)}
            </Form.Item>
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item label="URL" className="mb0 flex-1">
              {getFieldDecorator(`social-contributions-url-${i}`, {
                initialValue: key.url
              })(<Input placeholder="Enter Issuing Url" />)}
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <Form.Item label="Description" className="mb0 flex-1">
              {getFieldDecorator(`social-contributions-description-${i}`, {
                initialValue: key.description,
                rules: [
                  {
                    max: 150,
                    message: "Max 150 character you can enter!",
                  }
                ]

              })
                (<Input placeholder="Enter Description" />)}
            </Form.Item>
          </Col>
        </Row>
        <Icon
          className="delete-icon"
          type="close"
          onClick={() => removeSocialContribution(key)}
        />
      </div>
    </Fragment>
  ));

  getFieldDecorator("booksAndPublications", { initialValue: [] });
  const booksAndPublicationsFields = getFieldValue("booksAndPublications");
  const booksAndPublicationsItems = booksAndPublicationsFields.map((key, i) => (
    <Fragment key={i}>
      <div className="align-end border_sec">
        <Row gutter={24} className="books-and-publication">
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item label="Name" className="mb0 flex-1">
              {getFieldDecorator(`book-and-publication-name-${i}`, {
                initialValue: key.name,
                rules: [
                  {
                    required: true,
                    message: "Name is required",
                  },
                ],
              })(<Input placeholder="Enter Book Name" />)}
            </Form.Item>
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item label="URL" className="mb0 flex-1">
              {getFieldDecorator(`book-and-publication-url-${i}`, {
                initialValue: key.url,
              })(<Input placeholder="Enter Url" />)}
            </Form.Item>
          </Col>
        </Row>
        <Icon
          className="delete-icon delete-icon-center"
          type="close"
          onClick={() => removeBooksAndPublication(key)}
        />
      </div>
    </Fragment>
  ));

  // getFieldDecorator('initiatives', { initialValue: [] })
  // const initiativesFields = getFieldValue('initiatives');
  // const initItems = initiativesFields.map((key, i) => (
  //   <Fragment key={i}>
  //     <div className="flex-x align-end" key={key}>
  //       <Form.Item label="Initiative Name" className="mb0 mr30 flex-1">
  //         {getFieldDecorator(`init-name-${i}`, { initialValue: key.name, ...InitiativesValidator })
  //           (<Input placeholder="Enter Awards Name" />)}
  //       </Form.Item>
  //       <Form.Item label="Reference Link (URL)" className="mb0 flex-1">
  //         {getFieldDecorator(`init-link-${i}`, { initialValue: RemoveHttpCom(key.url), rules: [{ required: true, message: 'URL is required' }] })
  //           (<Input addonBefore="https://" placeholder="Enter Initiative Reference Link" />)}
  //       </Form.Item>
  //       <Icon className="delete-icon" type="close" onClick={() => removeInits(key)} />
  //     </div>
  //   </Fragment>
  // ));

  return (
    <div className="step-1-expert-form height_sec">
      <div className="an-20 medium-text success--text step-title">
        Create Expert Profile{" "}
        <span className="an-16"> Awards and Recognition</span>
      </div>
      <div className="an-16 regular-text pt10">
        Please fill in the details below to create your expert profile
      </div>
      <Form
        className="ant-advanced-search-form pt20"
        onSubmit={handleFormSubmit}
      >
        <Row gutter={24}>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            {/* <Form.Item label="Bio">
              {getFieldDecorator(
                "bio",
                BioValidator
              )(<TextArea placeholder="Tell us about your self" />)}
            </Form.Item> */}
            <Form.Item label='Bio' className='flex-1 bio-field desc-min-height' >
              {getFieldDecorator(
                "bio",
                BioValidator
              )(<ReactQuill
                theme='snow'
                modules={{
                  toolbar: false,
                  clipboard: { matchVisual: false }
                }}
                // value={bio && bio.trim() !== 'null' ? bio : ''}
                onChange={handleDescriptionChange}
              />)}
            </Form.Item>
          </Col>
        </Row>

        {/* <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <div className="py15 an-14 medium-text flex-x space-between pt60">
              <div className="an-20 medium-text success--text step-title">
                Professional Experience
              </div>
              <div>
                <Button
                  shape="round"
                  icon="plus"
                  className="award-add-btn mb-24"
                  onClick={addProfessionalExperienceHandler}
                >
                  Add
                </Button>
              </div>
            </div>
            {professionalExperienceItems}
          </Col>
        </Row> */}

        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <div className="py15 an-14 medium-text flex-x space-between pt30">
              <div className="an-20 medium-text success--text step-title">
                Professional Qualifications
              </div>
              <div>
                <Button
                  shape="round"
                  icon="plus"
                  className="award-add-btn mb-24"
                  onClick={addProfessionalQualificationHandler}
                >
                  Add
                </Button>
              </div>
            </div>
            {professionalQualificationItems}
          </Col>
        </Row>

        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <div className="py15 an-14 medium-text flex-x space-between pt30">
              <div className="an-20 medium-text success--text step-title">
                Awards and Recognition
              </div>
              <div>
                <Button
                  shape="round"
                  icon="plus"
                  className="award-add-btn mb-24"
                  onClick={addAwardsHandler}
                >
                  Add
                </Button>
              </div>
            </div>
            {awardsItems}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <div className="py15 an-14 medium-text flex-x space-between pt30">
              <div className="an-20 medium-text success--text step-title">
                Social Initiatives
              </div>
              <div>
                <Button
                  shape="round"
                  icon="plus"
                  className="award-add-btn mb-24"
                  onClick={addSocialContributionHandler}
                >
                  Add
                </Button>
              </div>
            </div>
            {socialContributionItems}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <div className="py15 an-14 medium-text flex-x space-between pt30">
              <div className="an-20 medium-text success--text step-title">
                Books & Publications
              </div>
              <div>
                <Button
                  shape="round"
                  icon="plus"
                  className="award-add-btn mb-24"
                  onClick={addBooksAndPublicationHandler}
                >
                  Add
                </Button>
              </div>
            </div>
            {booksAndPublicationsItems}
          </Col>
        </Row>
        {/* <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <div className="py15 an-14 medium-text flex-x space-between pt30">
              <div className="an-20 medium-text success--text step-title">
                INITIATIVE
              </div>
              <div>
                <Button
                  shape="round"
                  icon="plus"
                  className="award-add-btn mb-24"
                  onClick={addInitHandler}>
                  Add
                </Button>
              </div>
            </div>
            {initItems}
          </Col>
        </Row>*/}
        <Form.Item className="mb0 pt40">
          <Button
            type="primary"
            className="ex_grey_btn"
            onClick={() => [window.scrollTo({ top: document.getElementById('scroll-to-here'), behavior: 'smooth' }), dispatch(changeTab(1))]}
          >
            Back
          </Button>
          <Button
            loading={false}
            type="primary"
            htmlType="submit"
            className="ex__primary_btn ml40"
          >
            Next
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
});

const WrappedCreateExpertStepAwardsAndBio = Form.create({ name: "createProfile" })(
  CreateExpertStepAwardsAndBio
);

export default compose(WrappedCreateExpertStepAwardsAndBio);
