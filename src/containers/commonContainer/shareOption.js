import React from 'react';
import Link from "next/link";
import { Menu } from "antd";
// import Image from 'next/image'
const Facebook = "/images/fb_ic.png";
const Instagram = "/images/instagram_ic.png";
const Twitter = "/images/twitter_ic.png";

const shareOption = () => {
  return (
    <Menu className="share_btn_box">
      <Menu.Item>
        <Link href="/">
          <a><img src={Facebook} alt="facebook" className="pr10" /></a>
          Facebook
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link href="/">
          <a><img src={Instagram} alt="instagram" className="pr10" /></a>
          Instagram
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link href="/" style={{ border: "none" }}>
          <a><img src={Twitter} alt="twitter" className="pr10" /></a>
          Twitter
        </Link>
      </Menu.Item>
    </Menu>
  )
}

export default shareOption;