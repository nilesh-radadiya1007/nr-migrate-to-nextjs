import React, { useState } from "react";
import { Carousel, Form, Input, Button, Modal } from "antd";
import { Subscribe } from '../../services/expert';
// import Image from 'next/image'
import { useRouter } from "next/router";

const LeftArrow = "/images/newicon/left_arrow_black.svg";
const image2 = "/images/home/landingpage/landingpage4.png";
const Success = "/images/single-logo2.png";

const LandingPageHeader = (props) => {
    const router = useRouter()
    const { getFieldDecorator, setFields, setFieldsValue } = props.form;
    const [isLoading, setIsLoading] = useState(false);
    const [showModal, setShowModal] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields(async (err, values) => {
            if (!err) {
                setIsLoading(true);
                values = { ...values, subscribe: true, subscribeFrom: 'landingpage-V2' }
                const result = await Subscribe(values);
                if (result.data.status === "SUCCESS") {
                    router.push("/all-adventure");
                } else if (result.data.status === "FAIL") {
                    setFields({
                        'email': {
                            errors: [{
                                "message": `${values.email} email is already subscribe.`,
                                "field": "email"
                            }]
                        }
                    });

                }
                setIsLoading(false);
            }
        });
    }

    return (
        <div className="landingpage-v2">
            <Carousel autoplay>
                <div className='landingpage-img-div'>
                    <div className="bg-layer">
                        <img className="" src={image2} alt="Banner-for-mobile" />
                    </div>
                </div>
            </Carousel>
            <div className="ex_landing_page">
                <p className="ex_landing_page__title">CONNECT. <span className="ex_landing_page__s-yellow">LEARN. </span> IMPACT</p>
                <div className="border-bottom-green"></div>
                <p className="ex_landing_page__subtitle">Connect with adventure professionals worldwide. Learn through experience. Help protect the planet.</p>
                <div className="ex_landing_page__form">
                    <Button className="ex__primary_btn be-more-button v2" loading={isLoading ? true : false} onClick={() => setShowModal(true)}>
                        GET STARTED FOR FREE <span><img src={LeftArrow} className="left-arrow" alt="left arrow" /></span>
                    </Button>
                </div>
                {/* <button className="home-page-adventure-btn" onClick={() => router.push("/all-adventure")}>START EXPLORING</button> */}
            </div>

            <Modal
                centered
                className="auth-modal success-modal subs-modal"
                width={380}
                closable={true}
                maskClosable={false}
                visible={showModal}
                onCancel={() => setShowModal(false)}
            >
                <div className="text-center">
                <img src={Success} alt="" />
                    <p className="an-18 mb20 subs-modal__title">
                        Discover adventures crafted by professionals
                    </p>
                    <Form onSubmit={handleSubmit} autoComplete="new-password">
                    <div className="ex_landing_page__form">
                        <Form.Item className="registration-email">
                            {getFieldDecorator("email", {
                                rules: [
                                    {
                                        type: "email",
                                        message: "Please enter valid email address."
                                    },
                                    { required: true, message: "Please input your email" }
                                ]
                            })(
                                <Input
                                    type="email"
                                    placeholder="Enter Your Email"
                                    // addonBefore={<img src={mailIcon} alt="Mail icon" />}
                                    className="email-input-box subs-modal__textbox"
                                    id="email-input-box2"

                                    />
                                )}
                            </Form.Item>

                        <Form.Item className="button-div">
                            <Button className="ex__primary_btn be-more-button subs-modal__btn" loading={isLoading ? true : false} onClick={handleSubmit}>
                                START EXPLORING <span>
                                    {/* <img src={LeftArrow} className="left-arrow" alt="left arrow" /> */}
                                    </span>
                                </Button>
                            </Form.Item>
                        </div>
                    </Form>
                </div>
            </Modal>
        </div >


    );
};
const LandingPage = Form.create({ name: "RegisterForm" })(LandingPageHeader);

export default LandingPage;
