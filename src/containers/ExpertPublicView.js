import React, { useEffect, useCallback, useState, Fragment } from 'react';
import { Row, Col, Tabs, Menu, Dropdown, Button, Rate, Card, Popover } from "antd";
import ReactHtmlParser from "react-html-parser";
import { withRouter, useRouter } from 'next/router';
import { compose } from "redux";
import { FacebookShareButton, TwitterShareButton, WhatsappShareButton } from "react-share";
import OwlCarousel from "react-owl-carousel";


/**
 * App Imports
 */
import { getPublicProfile, getUpcomingTrip, getTravelMap, getUpcomingWorkShops, getExpertAlbums, getAllReviewByExpert } from '../services/expert';
import AppLoader from '../components/Loader';
import { CaptalizeFirst, getColorLogoURL, commaSepratorString, displayDifficultyText, getCityFromLocation, getCurrencySymbol, DayorDaysNightOrNights } from '../helpers/methods';
import Map from '../components/Trips/Map';
import MyTrips from '../components/Trips/MyTrips';
import WorkShopTab from '../components/Learning/WorkshopTab';
import AlbumTab from '../containers/AlbumTab';
import ExpCard from "../components/Basic/ExpCard";
import ExpBookCard from '../components/Basic/ExpBookCard';
import { DISPLAY_RESULT } from '../helpers/constants';
import shareOption from './commonContainer/shareOption';
import RecentPhotoViewPopup from '../components/Trips/RecentPhotoViewPopup';
import TripWorkshopAllCardsCarousel from "../components/common/TripWorkshopAllCardsCarousel";
import Newsletter from '../components/common/NewsLetter';
import ExpeditionsConnect from '../components/common/ExpeditionsConnect';
import Reviews from './ReviewListing';

/**
 * Image Imports
 */
const HeaderImg = "/images/upload_image.png";
const Location_Img = "/images/country_ic.png";
const Date_Img = "/images/followers_ic.png";
const Award_Img = "/images/awards_ic.png";
const Facebook = "/images/fb_ic.png";
const Share = "/images/share_ic.png";
const WhatsApp = "/images/whatsapp_ic.png";
const Twitter = "/images/twitter_ic.png";
const Share_Ic = "/images/share_ic.png";
const AddUser_Ic = "/images/add-user.svg";
const Speaks = "/images/speaks_ic.png";
const Group_Img = "/images/Group 5307.svg";

const ApiKey = process.env.REACT_APP_GOOGLE_MAP_API_KEY;
const URL = "http://expeditions-connect-app.s3-website.eu-central-1.amazonaws.com";

const sliderOptions = {
  margin: 20,
  dots: true,
  responsive: {
    0: {
      items: 1,
      dotsEach: 3,
    },
    768: {
      items: 3,
      dotsEach: 3,
    },
    991: {
      items: 3,
      dotsEach: 3,
    },
  },
};

const { TabPane } = Tabs;

const ExpertPublicView = (props) => {
  const router = useRouter();
  const { id } = router.query;

  const [profile, setProfile] = useState(null);
  const [loader, setLoader] = useState(true);
  // const [tripLoader, setTripLoader] = useState(true);
  const [upcomingTrip, setUpcomingTrip] = useState([]);
  const [upcomingWorkShop, setUpcomingWorkShop] = useState([]);
  const [expertAlbums, setExpertAlbums] = useState([]);
  const [travelMap, setTravelMap] = useState(null);
  const [tripsAndWorkShops, setTripsAndWorkShops] = useState([]);
  const [pictures, setPictures] = useState([]);
  const [recentPhotoView, setRecentPhotoView] = useState(false);
  const [currentViewPic, setCurrentViewPic] = useState("");

  const [allPE, setAllPE] = useState([]);
  const [tmpPE, setTmpPE] = useState([]);

  const [allPQ, setAllPQ] = useState([]);
  const [tmpPQ, setTmpPQ] = useState([]);

  const [allAwards, setAllAwards] = useState([]);
  const [tmpAwards, setTmpAwards] = useState([]);

  const [allSocial, setAllSocial] = useState([]);
  const [tmpSocial, setTmpSocial] = useState([]);

  const [isLoadMorePE, setIsLoadMorePE] = useState(true);
  const [isLoadMorePQ, setIsLoadMorePQ] = useState(true);
  const [isLoadMoreAward, setIsLoadMoreAward] = useState(true);
  const [isLoadMoreSocial, setIsLoadMoreSocial] = useState(true);
  const [isAlbumRefresh, setIsAlbumRefresh] = useState(false);


  const [reviewData, setReviewData] = useState([]);
  const [isReviewLoading, setIsReviewLoading] = useState(false);
  const [totalReview, setTotalreview] = useState(0);
  const [expertRating, setExpertRating] = useState(0);

  const menu = (
    <Menu className="share_btn_box">
      <Menu.Item>
        <FacebookShareButton className="soc_icon" url={`${URL}/expert-profile/${id}`}>
          <img src={Facebook} alt="facebook" className="pr10" />Facebook
        </FacebookShareButton>
      </Menu.Item>
      <Menu.Item>
        <WhatsappShareButton className="soc_icon" url={`${URL}/expert-profile/${id}`}>
          <img src={WhatsApp} alt="twitter" className="pr10" />WhatsApp
        </WhatsappShareButton>
      </Menu.Item>
      <Menu.Item>
        <TwitterShareButton className="soc_icon" url={`${URL}/expert-profile/${id}`} style={{ border: "none" }}>
          <img src={Twitter} alt="twitter" className="pr10" />Twitter
        </TwitterShareButton>
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    let results = [];
    let totalTrips = upcomingTrip.length;
    let totalWorkshops = upcomingWorkShop.length;
    let trips = [];
    let workshops = [];

    if (totalTrips > 6 && totalWorkshops > 6) {
      trips = upcomingTrip.slice(0, 6);
      workshops = upcomingWorkShop.slice(0, 6);
    } else if (totalTrips < 6 && totalWorkshops > 6) {
      trips = [...upcomingTrip];
      workshops = upcomingWorkShop.slice(0, DISPLAY_RESULT - totalTrips);
    } else if (totalTrips > 6 && totalWorkshops < 6) {
      workshops = [...upcomingWorkShop];
      trips = upcomingTrip.slice(0, DISPLAY_RESULT - totalWorkshops);
    } else {
      workshops = [...upcomingWorkShop];
      trips = [...upcomingTrip];
    }

    results = [...trips, ...workshops];
    setTripsAndWorkShops(results);
  }, [upcomingTrip, upcomingWorkShop]);

  const getData = useCallback(async () => {
    const result = await getPublicProfile(id);
    if (result.status === 200) {
      let profileData = result.data.data;

      let peData = typeof profileData.professionalExperience !== "undefined" && profileData.professionalExperience.length > 0 ? profileData.professionalExperience : [];
      setAllPE(peData);
      let newDataPE = peData.slice(0, 4);
      setTmpPE(newDataPE);

      setAllPQ(profileData.professionalQualifications);
      let newDataPQ = profileData.professionalQualifications.slice(0, 4);
      setTmpPQ(newDataPQ);

      setAllAwards(profileData.awards);
      let newAwards = profileData.awards.slice(0, 4);
      setTmpAwards(newAwards);

      setAllSocial(profileData.socialContributions);
      let newSocial = profileData.socialContributions.slice(0, 4);
      setTmpSocial(newSocial);

      setTimeout(() => {
        setProfile(profileData);
        setLoader(false);
      }, 200);

    }
  }, [id]);

  const getTrips = useCallback(async () => {
    const result = await getUpcomingTrip(id);
    if (result.status === 200) {

      const data = result.data.data.filter((trip) => trip.active === true);
      setUpcomingTrip(data);
      // setTripLoader(false);
    }
    const coords = await getTravelMap(id);
    if (coords.status === 200) {
      setTravelMap(coords.data.data);
    }
  }, [id]);

  const getExpertReviews = useCallback(async (id) => {

    setIsReviewLoading(true);
    const result = await getAllReviewByExpert(false, id);
    if (result.status === 200) {
      if (result.data.data.length > 0) {
        setReviewData(result.data.data);
        setIsReviewLoading(false)
      } else {
        setIsReviewLoading(false)
      }
    } else {
      setIsReviewLoading(false)
    }
  }, []);


  useEffect(() => {
    setIsReviewLoading(true)
    if (reviewData.length > 0) {
      setTotalreview(reviewData.length);
      let finalRating = 0;
      reviewData.map((item) => {
        finalRating = finalRating + parseFloat(item.rating);
      });
      setExpertRating(Math.round(finalRating / reviewData.length));
    }
    setIsReviewLoading(false)
  }, [reviewData]);

  useEffect(() => {
    setIsReviewLoading(true)
    if (expertRating !== 0) {
      setIsReviewLoading(false)
    }
  }, [expertRating]);

  const getWorkShops = useCallback(async () => {
    const result = await getUpcomingWorkShops(id);
    if (result.status === 200) {

      const data = result.data.data.filter((workshop) => workshop.active === true);
      setUpcomingWorkShop(data);
    }
  }, [id]);

  const getAlbums = useCallback(async () => {
    const result = await getExpertAlbums(id);
    if (result.status === 200) {
      const albums = result.data.data;
      setExpertAlbums(albums);
      const pics = [];
      albums.forEach(album => {
        pics.push(...album.images);
      });
      setPictures(pics);
    }
  }, [id]);


  let currentTabData = router.query;
  if (Object.keys(currentTabData).includes('about') || Object.keys(currentTabData).includes('1')) {
    currentTabData = "1";
  } else if (Object.keys(currentTabData).includes('trip') || Object.keys(currentTabData).includes('2')) {
    currentTabData = "2";
  } else if (Object.keys(currentTabData).includes('workshop') || Object.keys(currentTabData).includes('3')) {
    currentTabData = "3";
  } else if (Object.keys(currentTabData).includes('gallery') || Object.keys(currentTabData).includes('4')) {
    currentTabData = "4";
  } else if (Object.keys(currentTabData).includes('review') || Object.keys(currentTabData).includes('5')) {
    setTimeout(() => {
      if (document.getElementById("scoll-to-here2") !== null) {
        document
          .getElementById("scoll-to-here2")
          .scrollIntoView({ block: "start", behavior: "auto" });
      }
    }, 1000);
    currentTabData = "5";
  }else{
    currentTabData = "1";
  }

  useEffect(() => {
    getData();
    getTrips();
    getWorkShops();
    getAlbums();
    getExpertReviews(id)
  }, [getData, getTrips, getWorkShops, getAlbums, getExpertReviews]);

  const refreshPublicAlbum = (type) => {
    if (type === "albumDetail") {
      setIsAlbumRefresh(true);
    } else {
      setIsAlbumRefresh(false);
    }
    getAlbums();
  }

  const onCloseClick = () => {
    setRecentPhotoView(false);
  };

  const onRecentPhotoView = (pic) => {
    setCurrentViewPic(pic)
    setRecentPhotoView(true);
  };

  const loadMore = (type) => {

    if (type === "pq") {
      setTmpPQ(allPQ);
      setIsLoadMorePQ(false);
    } else if (type === "award") {
      setTmpAwards(allAwards);
      setIsLoadMoreAward(false);
    } else if (type === "social") {
      setTmpSocial(allSocial);
      setIsLoadMoreSocial(false);
    } else if (type === "pe") {
      setTmpPE(allPE);
      setIsLoadMorePE(false);
    }

  }

  if (loader) {
    return (
      <div className="text-center py20 loader-absolute-class">
        <AppLoader />
      </div>
    )
  } else {
    return (
      <div className="expert-profile header-container expert-mobile-view">
        <div className="container-fluid align-center pr0 mobile">
          <div className="page_wrapper mobile_pad_fix mt10">
            <Row>
              <Col span={24} className="header_bg">
                <img className="mb5" width="100%" src={profile?.cover || HeaderImg} alt="Header" />
              </Col>
              <Col xs={24} sm={24} md={10} lg={6} xl={6}>
                <div className="profile_img mt10">
                  <img src={profile.profile} alt="Profile" />
                </div>
              </Col>

              <Col xs={18} sm={15} md={12} lg={12} xl={12}>
                <div className="profile_details">
                  <h2 className="an-26 mt20 medium-text">
                    {`${CaptalizeFirst(profile.firstName)} ${CaptalizeFirst(
                      profile.lastName
                    )}`}{" "}

                    {!isReviewLoading &&
                      <Rate
                        allowHalf
                        disabled
                        defaultValue={expertRating}
                        style={{ color: "#FFBC00" }}
                        className="ml10 an-18"
                      />
                    }
                    {isReviewLoading &&
                      <Rate
                        allowHalf
                        disabled
                        defaultValue={0}
                        style={{ color: "#FFBC00" }}
                        className="ml10 an-18"
                      />
                    }

                  </h2>
                  <h5 className="an-18 regular-text">{`${commaSepratorString(profile.experties)}`}</h5>
                  <p className="an-14 regular-text">
                    <img src={Location_Img} alt="Profile" />
                    <span>{getCityFromLocation(profile.city)}{CaptalizeFirst(profile.country)}</span>
                  </p>
                  <p className="an-14 regular-text">
                    <img src={Speaks} alt="Profile" />
                    {profile.speaks.map((lang, key) => (
                      <span key={key}>
                        {key < profile.speaks.length - 1 ? `${CaptalizeFirst(lang)}, ` : CaptalizeFirst(lang)}
                      </span>
                    ))}
                  </p>
                  {/* <p className="an-14 regular-text">
                    <img src={Date_Img} alt="Profile" />
                    <span>0 Followers</span>
                  </p> */}
                </div>
              </Col>

              <Col xs={6} sm={9} md={10} lg={6} xl={6} className="">

                {/* <div className="share_btn mt15 ml10 pull-right">
                  <Dropdown overlay={menu} placement="bottomLeft">
                    <Button className="share_btn mt20 medium-text an-14">
                      <img src={Share_Ic} alt="Share" /> Share
                    </Button>
                  </Dropdown>
                </div> */}

                <div className="mt15 pull-right follow_btn">
                  <Button className="an-14 mt20" style={{ height: '40px' }}>
                    <img src={AddUser_Ic} alt="Follow" /> Follow
                  </Button>
                </div>
              </Col>
            </Row>
          </div>
        </div>
        <div className="container-fluid align-center pb30">
          <div className="page_wrapper mobile_pad_fix mt10">
            <Row>
              <Col>
                <div className="expert-profile_tabs">
                  <Tabs defaultActiveKey={currentTabData}>
                    <TabPane tab="About" key="1">
                      <Row gutter={[40, 40]} className="revers-direction">
                        <Col className="gutter-row" xs={24} sm={24} md={16} lg={16} xl={16}>
                          <div className="bio_sec">
                            <Row>
                              <Col span={12}>
                                <h4 className="title_line">Bio</h4>
                              </Col>
                            </Row>
                            <span className="lh-24 an-14">{
                              profile.bio && profile.bio.trim() !== 'null'
                                ? ReactHtmlParser(profile.bio)
                                : ''
                            }</span>
                          </div>
                          {/* {tmpPE.length > 0 &&
                            <>
                              <Row type="flex" justify="space-between" className="profile_section--header">
                                <h4>
                                  Professional Experience
                            </h4>
                                </Row>
                              <Row type="flex" justify="space-between" gutter={[20, 20]}>
                                {
                                  tmpPE.length ? tmpPE.map((professionalQualification, index) => (
                                    <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                                      <ExpCard
                                        key={professionalQualification._id}
                                        {...professionalQualification}
                                        icon={Award_Img}
                                        type="pe"
                                      />
                                    </Col>
                                  )) : (
                                      <div>
                                        <div className="mb10 ml10 an-14 regular-text"><h3>No Professional Experience</h3></div>
                                      </div>)
                                }

                                {allPE.length > 4 && isLoadMorePE &&
                                  <>
                                    <div style={{ clear: "both" }}></div>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} className="profile-load-more" style={{ textAlign: "center" }}>
                                      <Button className="an-16 mt5" onClick={() => loadMore("pe")}>
                                        View More
                                </Button>
                                    </Col>
                                  </>
                                }

                              </Row>
                            </>
                          } */}

                          {tmpPQ.length > 0 &&
                            <>
                              <Row type="flex" justify="space-between" className="profile_section--header">
                                <h4>
                                  Professional Qualifications
                                </h4>
                                {/* <img src={ArrowDropDown_Img} alt="da" className="" /> */}
                              </Row>
                              <Row type="flex" justify="space-between" gutter={[20, 20]}>
                                {
                                  tmpPQ.length ? tmpPQ.map((professionalQualification, index) => (
                                    <Col xs={24} sm={24} md={24} lg={12} xl={12} key={index}>
                                      <ExpCard
                                        key={professionalQualification._id}
                                        {...professionalQualification}
                                        icon={Award_Img}
                                      />
                                    </Col>
                                  )) : (
                                    <div>
                                      <div className="mb10 ml10 an-14 regular-text"><h3>No Professional Qualifications</h3></div>
                                    </div>)
                                }

                                {allPQ.length > 4 && isLoadMorePQ &&
                                  <>
                                    <div style={{ clear: "both" }}></div>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} className="profile-load-more" style={{ textAlign: "center" }}>
                                      <Button className="an-16 mt5" onClick={() => loadMore("pq")}>
                                        View More
                                      </Button>
                                    </Col>
                                  </>
                                }

                              </Row>
                            </>
                          }

                          {tmpAwards.length > 0 &&
                            <>
                              <Row type="flex" justify="space-between" className="profile_section--header">
                                <h4>
                                  Awards & Recognitions
                                </h4>
                                {/* <img src={ArrowDropDown_Img} alt="da" /> */}
                              </Row>
                              <Row type="flex" justify="space-between" gutter={[20, 20]}>
                                {
                                  tmpAwards.length ? tmpAwards.map((award, index) => (
                                    <Col xs={24} sm={24} md={24} lg={12} xl={12} key={index}>
                                      <ExpCard
                                        key={award._id}
                                        {...award}
                                        icon={Award_Img}
                                      />
                                    </Col>
                                  )) : (
                                    <div>
                                      <div className="mb10 ml10 an-14 regular-text"><h3>No Awards & Recognitions</h3></div>
                                    </div>)
                                }

                                {allAwards.length > 4 && isLoadMoreAward &&
                                  <>
                                    <div style={{ clear: "both" }}></div>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} className="profile-load-more" style={{ textAlign: "center" }}>
                                      <Button className="an-16 mt5" onClick={() => loadMore("award")}>
                                        View More
                                      </Button>
                                    </Col>
                                  </>
                                }

                              </Row>
                            </>
                          }

                          {tmpSocial.length > 0 &&
                            <>
                              <Row type="flex" justify="space-between" className="profile_section--header">
                                <h4>
                                  Social Initiatives
                                </h4>
                                {/* <img src={ArrowDropDown_Img} alt="da" /> */}
                              </Row>
                              <Row type="flex" justify="space-between" gutter={[20, 20]}>
                                {
                                  tmpSocial.length ? tmpSocial.map((socialContribution, index) => (
                                    <Col span={24} key={index}>
                                      <ExpCard
                                        key={socialContribution._id}
                                        {...socialContribution}
                                        icon={Group_Img}
                                      />
                                    </Col>
                                  )) : (
                                    <div>
                                      <div className="mb10 ml10 an-14 regular-text"><h3>No Social Contributions Found</h3></div>
                                    </div>)
                                }

                                {allSocial.length > 4 && isLoadMoreSocial &&
                                  <>
                                    <div style={{ clear: "both" }}></div>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} className="profile-load-more" style={{ textAlign: "center" }}>
                                      <Button className="an-16 mt5" onClick={() => loadMore("social")}>
                                        View More
                                      </Button>
                                    </Col>
                                  </>
                                }

                              </Row>
                            </>
                          }

                          {profile.booksAndPublications.length > 0 &&
                            <>
                              <Row type="flex" justify="space-between" className="profile_section--header">
                                <h4>
                                  Books and Publications
                                </h4>
                                {/* Commenting out this implementation as it needs further discussion on the way of implementation */}
                                {/* <div>
                              <Button>
                                <img src={Arrow_back_Img} alt="da" />
                              </Button>
                              <Button>
                                <img src={Arrow_back_Img} alt="da" />
                              </Button>
                            </div> */}
                              </Row>
                              <Row className="books_and_publication">
                                <OwlCarousel
                                  className="owl-theme owl-dots slider_prev_next photo_video_sec"
                                  {...sliderOptions}
                                >
                                  {
                                    profile.booksAndPublications.length ? profile.booksAndPublications.map((book, index) => (
                                      <Col key={index}>
                                        <ExpBookCard
                                          {...book}
                                          icon={Group_Img}
                                        />
                                      </Col>
                                    )) : (
                                      <div>
                                        <div className="mb10 an-14 regular-text"><h3>No Books and Publications</h3></div>
                                      </div>)
                                  }
                                </OwlCarousel>
                              </Row>
                            </>
                          }
                        </Col>
                        <Col className="gutter-row" xs={24} sm={24} md={8} lg={8} xl={8}>
                          <div className="buss_bg">
                            <Row>
                              <Col span={12}>
                                <h4 className="title_line">Skills</h4>
                              </Col>
                            </Row>
                            <Row>
                              <Col>
                                {
                                  profile.skills.map((skill, key) => (
                                    <span key={key} className="experties-tag">
                                      {skill}
                                    </span>
                                  ))
                                }
                              </Col>
                            </Row>
                            <Row className="ask_export--button mt10 mb20">
                              <Col>
                                <span>*Have a question about these skills?</span>
                                <Button className="an-16 mt5" id="ask_this_expert">
                                  Ask This Expert
                                </Button>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <div className="photo_sec">
                            {pictures && pictures.length > 0 &&
                              <Row className="standered_border mt70 pt30">
                                <Col>
                                  <Row>
                                    <Col span={24}>
                                      <h4 className="features_sub_title mb30">
                                        Recent Photos and Videos
                                      </h4>
                                    </Col>
                                    {/* <Col span={12}>
                                      <div className="text-right mt30 photo_trip_button">
                                        <Link className="ex__primary_btn br5">
                                          View More
                                        </Link>
                                      </div>
                                    </Col> */}
                                  </Row>
                                  <Row>
                                    <OwlCarousel
                                      className="owl-theme owl-dots slider_prev_next photo_video_sec expert-recent-pic"
                                      {...sliderOptions}
                                    >
                                      {pictures.slice(0, DISPLAY_RESULT).map((pic, index) => {
                                        return (
                                          <Col xs={24} sm={24} md={24} lg={24} xl={24} key={index} className="picture_thumbnail">
                                            <img src={pic} alt="" onClick={() => onRecentPhotoView(pic)} />
                                          </Col>)
                                      })}
                                    </OwlCarousel>
                                  </Row>
                                </Col>
                              </Row>
                            }
                            {tripsAndWorkShops && tripsAndWorkShops.length > 0 &&
                              <>
                                <Row className="pt30 Similar_Tripsdetail pt30">
                                  <Col span={24}>
                                    <h4 className="features_sub_title mb30">Upcoming Trips and Workshops</h4>
                                  </Col>
                                </Row>
                                <Row gutter={10} className="margin_fix_mobile expert_fix_mobile expendition slider_prev_next">
                                  <TripWorkshopAllCardsCarousel
                                    items={tripsAndWorkShops}
                                  />
                                </Row>
                              </>
                            }
                            <Row className="Similar_Tripsdetail pt30">
                              <Col>
                                {
                                  travelMap && travelMap.length > 0 ?
                                    (
                                      <div>
                                        <h4 className="features_sub_title mb30">Travel Map</h4>
                                        <Row>
                                          <div className="map pb30">
                                            <Map
                                              multiple
                                              travelMap={travelMap}
                                              zoom={5}
                                              ApiKey={ApiKey}
                                            />
                                          </div>
                                        </Row>
                                      </div>
                                    ) : ("")
                                }
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                    </TabPane>
                    <TabPane tab="Trips" key="2">
                      <Col>
                        <MyTrips isPublicView={true} token={""} publicTrip={upcomingTrip} travelMap={travelMap} />
                      </Col>
                    </TabPane>
                    <TabPane tab="Workshops" key="3">
                      <Col>
                        <WorkShopTab isPublicView={true} token={""} publicWorkShop={upcomingWorkShop} travelMap={travelMap} />
                      </Col>
                    </TabPane>
                    <TabPane tab="Gallery" key="4">
                      <AlbumTab isPublicView={true} token={""} publicAlbums={expertAlbums} refreshPublicAlbum={refreshPublicAlbum} isAlbumRefresh={isAlbumRefresh} />
                    </TabPane>
                    {typeof reviewData !== "undefined" && reviewData.length > 0 &&
                      <TabPane tab="Reviews" key="5">
                        <div className="" id="scoll-to-here2">
                          <Reviews isPublicView={true} expertId={id} totalReview={totalReview} expertRating={expertRating} reviewData={reviewData}></Reviews>
                        </div>
                      </TabPane>
                    }
                  </Tabs>
                </div>
              </Col>
            </Row>

            {recentPhotoView && (
              <RecentPhotoViewPopup
                visible={recentPhotoView}
                onCloseClick={onCloseClick}
                pic={currentViewPic}
              />
            )}
          </div>
        </div>
        <div className="why-choose-us-section">
          <ExpeditionsConnect className="pt60" />
        </div>
        <Newsletter page="expert_public_view" />
      </div>
    )
  }
}

export default compose(withRouter)(ExpertPublicView);
