import React from 'react';
import Covid from '../src/containers/Covid';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const CovidInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Covid />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default CovidInApp;