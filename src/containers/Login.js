import React, { useState } from 'react';
import { compose } from "redux";
import { useDispatch } from 'react-redux';
import { ModalActions } from '../redux/models/events';

const Login = props => {
  const [showModel, setShowModel] = useState(false);

  const dispatch = useDispatch();
  const { openAuthModal } = ModalActions;
  dispatch(openAuthModal())

  const handleClick = () => {
    setShowModel(false);
    dispatch(openAuthModal())
  }

  return (
    <div></div>
  )
}

export default compose(Login);
