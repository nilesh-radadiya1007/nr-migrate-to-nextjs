import React, { useEffect, useRef, Fragment } from "react";
import { useSelector } from "react-redux";
import Head from 'next/head';
import { Logout } from "../../services/expert";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";


export default function BasicPageWrapper({ children }) {
    const enthu = useSelector((state) => state.enthu);
    const expert = useSelector((state) => state.expert);
    const { role, accessToken } = useSelector((state) => state.auth);

    const prevExpert = useRef();
    const prevEnthu = useRef();

    useEffect(() => {
        prevExpert.current = expert;
        prevEnthu.current = enthu;
    }, [expert, enthu]);

    if (!accessToken) {
        if (prevExpert.current) Logout(prevExpert.current.id);
        if (prevEnthu.current) Logout(prevEnthu.current.id);
    }

    return (
        <Fragment>
            <Head>
                <title>Expeditions Connect</title>
                <meta charSet="utf-8" />
                {/* <link rel="icon" href="%PUBLIC_URL%/favicon.ico" /> */}
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="theme-color" content="#000000" />
                <meta name="description" content="Join Expeditions Connect for free. Explore the best adventure courses, workshops and expeditions directly crafted by professionals." />
                <meta property="og:title" content="Connect. Learn. Impact | Expeditions Connect" />
                <meta property="og:description" content="Join Expeditions Connect for free. Explore the best adventure courses, workshops and expeditions directly crafted by professionals." />
                <meta property="og:image:type" content="image/ong" />
                <meta property="og:image" content="https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/Logos/expeditionLohgo.png" />
                <meta property="og:url" content="https://expeditionsconnect.com/" />
                <meta name="facebook-domain-verification" content="hy2r6qalzpbjg8i3wdiwfp4s8yta9l" />
                {/* <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" /> */}
                {/* <link rel="manifest" href="%PUBLIC_URL%/manifest.json" /> */}
            </Head>

            <section className="home-layout">
                <header className="top-header">
                    <Header />
                </header>
                <main className="main-content">
                    {children}
                </main>
                <footer className="footer">
                    <Footer />
                </footer>
            </section>
        </Fragment>

    )
}

