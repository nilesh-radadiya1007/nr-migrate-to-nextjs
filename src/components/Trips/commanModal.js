import React from "react";
import { Modal, Form, Button } from "antd";

const Success = "/images/success_ic.png";


const commanModal = (props) => {

    return (
        <>
            <Modal
                centered
                className="auth-modal success-modal"
                width={380}
                closable={false}
                maskClosable={false}
                visible={props.isSuccess}
            >
                <div className="text-center">
                    <img src={Success} alt="" />
                    <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
                        Thank You
                        </h1>
                    <p className="an-18 mb20 regular-text">
                        Thank you for your message, We’ll get back to you shortly.
                        </p>

                    <Button type="primary" className="ex__primary_btn" onClick={() => props.onClose()}>
                        Ok
                        </Button>

                </div>
            </Modal>
        </>
    );
};

const WrappedNormalRegisterForm = Form.create({ name: "RegisterForm" })(commanModal);

export default WrappedNormalRegisterForm;
