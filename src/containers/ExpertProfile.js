import React, { useEffect, useState, useCallback } from "react";
import {
  Row,
  Col,
  Tabs,
  Menu,
  Dropdown,
  Button,
  Icon,
  message,
  Rate,
} from "antd";
import ReactHtmlParser from "react-html-parser";
// import { useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { compose } from "redux";
import OwlCarousel from "react-owl-carousel";
// import Image from 'next/image';
import Link from "next/link";
import { useRouter } from 'next/router'

/**
 * App Imports
 */
import { CaptalizeFirst, getCityFromLocation } from "../helpers/methods";

import AppLoader from "../components/Loader";
import { ExpertEvents } from "../redux/expert/events";
import {
  getMyAlbums,
  getUpcomingWorkShops,
  uploadCoverOriginalPicture
} from "../services/expert";
import Map from "../components/Trips/Map";
import { getUpcomingTrip, getTravelMap, getAllReviewByExpert } from "../services/expert";
import ExpCard from "../components/Basic/ExpCard";
import ExpBookCard from "../components/Basic/ExpBookCard";

import WorkShopTab from "../components/Learning/WorkshopTab";
import MyTrips from "../components/Trips/MyTrips";
import AlbumTab from "./AlbumTab";
import { DISPLAY_RESULT } from "../helpers/constants";
import RecentPhotoViewPopup from "../components/Trips/RecentPhotoViewPopup";
import TripWorkshopAllCardsCarousel from "../components/common/TripWorkshopAllCardsCarousel";
import ImageUplaoderAndCropper from "../shared/profile-cover/image-cropper";
import Newsletter from '../components/common/NewsLetter';
import Reviews from './ReviewListing';

const Location_Img = "/images/country_ic.png";
const Award_Img = "/images/awards_ic.png";
const Facebook = "/images/fb_ic.png";
const Instagram = "/images/instagram_ic.png";
const Twitter = "/images/twitter_ic.png";
const Share_Ic = "/images/share_ic.png";
const Speaks = "/images/speaks_ic.png";
const Upload_Img = "/images/upload_image.png";
const Group_Img = "/images/Group 5307.svg";
const default_Book_cover_Img = "/images/default_book_cover.svg";

const ApiKey = process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY;
const sliderOptions = {
  margin: 20,
  dots: true,
  responsive: {
    0: {
      items: 1,
      dotsEach: 3,
    },
    768: {
      items: 3,
      dotsEach: 3,
    },
    991: {
      items: 3,
      dotsEach: 3,
    },
  },
};

const { TabPane } = Tabs;

const menu = (
  <Menu className="share_btn_box">
    <Menu.Item>
      <Link href="/">
        <a>
          <img src={Facebook} alt="facebook" className="pr10" />
          Facebook
        </a>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link href="/">
        <a>
          <img src={Instagram} alt="instagram" className="pr10" />
          Instagram
        </a>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link href="/">
        <a style={{ border: "none" }}>
          <img src={Twitter} alt="twitter" className="pr10" />
          Twitter
        </a>
      </Link>
    </Menu.Item>
  </Menu>
);

const ExpertProfile = (props) => {
  const router = useRouter();
  // console.log(router)
  const expert = useSelector((state) => state.expert);
  const token = useSelector((state) => state.auth.accessToken);
  const { userId } = useSelector((state) => state.auth);
  const [uploadingLoader, setUploadingLoader] = useState(false)
  const [removingLoader, setRemovingLoader] = useState(false)
  const [upcomingTrip, setUpcomingTrip] = useState([]);
  const [upcomingWorkShop, setUpcomingWorkShop] = useState([]);
  const [travelMap, setTravelMap] = useState(null);
  const [tripsAndWorkShops, setTripsAndWorkShops] = useState([]);
  const [pictures, setPictures] = useState([]);
  const [recentPhotoView, setRecentPhotoView] = useState(false);
  const [currentViewPic, setCurrentViewPic] = useState("");

  const [allPQ, setAllPQ] = useState([]);
  const [tmpPQ, setTmpPQ] = useState([]);

  const [allAwards, setAllAwards] = useState([]);
  const [tmpAwards, setTmpAwards] = useState([]);

  const [allSocial, setAllSocial] = useState([]);
  const [tmpSocial, setTmpSocial] = useState([]);

  const [isLoadMorePQ, setIsLoadMorePQ] = useState(true);
  const [isLoadMoreAward, setIsLoadMoreAward] = useState(true);
  const [isLoadMoreSocial, setIsLoadMoreSocial] = useState(true);

  const [reposition, setReposition] = useState(false);
  const dispatch = useDispatch();
  const { changeTab, isEditMode, updateCover } = ExpertEvents;

  const [reviewData, setReviewData] = useState([]);
  const [isReviewLoading, setIsReviewLoading] = useState(false);
  const [totalReview, setTotalreview] = useState(0);
  const [expertRating, setExpertRating] = useState(0);


  const getTrips = useCallback(async (id) => {
    const result = await getUpcomingTrip(id);

    if (result.status === 200) {
      setUpcomingTrip(result.data.data);
    }
    const coords = await getTravelMap(id);
    if (coords.status === 200) {
      setTravelMap(coords.data.data);
    }
  }, []);

  const getPictures = useCallback(async (token) => {
    const result = await getMyAlbums(token);
    if (result.status === 200) {
      if (result.status === 200) {
        const albums = result.data.data;
        const pics = [];
        albums.forEach((album) => {
          pics.push(...album.images);
        });
        setPictures(pics);
      }
    }
  }, []);

  const getExpertReviews = useCallback(async (id) => {

    setIsReviewLoading(true);
    const result = await getAllReviewByExpert(false, id);
    if (result.status === 200) {
      if (result.data.data.length > 0) {
        setReviewData(result.data.data);
        setIsReviewLoading(false)
      } else {
        setIsReviewLoading(false)
      }
    } else {
      setIsReviewLoading(false)
    }
  }, []);

  const { id } = expert;

  useEffect(() => {
    let results = [];
    let totalTrips = upcomingTrip.length;
    let totalWorkshops = upcomingWorkShop.length;
    let trips = [];
    let workshops = [];

    if (totalTrips > 6 && totalWorkshops > 6) {
      trips = upcomingTrip.slice(0, 6);
      workshops = upcomingWorkShop.slice(0, 6);
    } else if (totalTrips < 6 && totalWorkshops > 6) {
      trips = [...upcomingTrip];
      workshops = upcomingWorkShop.slice(0, DISPLAY_RESULT - totalTrips);
    } else if (totalTrips > 6 && totalWorkshops < 6) {
      workshops = [...upcomingWorkShop];
      trips = upcomingTrip.slice(0, DISPLAY_RESULT - totalWorkshops);
    } else {
      workshops = [...upcomingWorkShop];
      trips = [...upcomingTrip];
    }

    results = [...trips, ...workshops];
    setTripsAndWorkShops(results);
  }, [upcomingTrip, upcomingWorkShop]);

  useEffect(() => {
    setAllPQ(expert.professionalQualifications);
    let newDataPQ = expert.professionalQualifications.slice(0, 4);
    setTmpPQ(newDataPQ);

    setAllAwards(expert.awards);
    let newAwards = expert.awards.slice(0, 4);
    setTmpAwards(newAwards);

    setAllSocial(expert.socialContributions);
    let newSocial = expert.socialContributions.slice(0, 4);
    setTmpSocial(newSocial);
  }, [expert]);

  const getWorkShops = useCallback(async (id) => {
    const result = await getUpcomingWorkShops(id);
    if (result.status === 200) {
      const data = result.data.data.filter(
        (workshop) => workshop.active === true
      );
      setUpcomingWorkShop(data);
    }
  }, []);

  useEffect(() => {
    setIsReviewLoading(true)
    if (reviewData.length > 0) {
      setTotalreview(reviewData.length);
      let finalRating = 0;
      reviewData.map((item) => {
        finalRating = finalRating + parseFloat(item.rating);
      });
      setExpertRating(Math.round(finalRating / reviewData.length));
    }
    setIsReviewLoading(false)
  }, [reviewData]);

  useEffect(() => {
    setIsReviewLoading(true)
    if (expertRating !== 0) {
      setIsReviewLoading(false)
    }
  }, [expertRating]);
  
  const location = router.asPath;

  let currentTabData = location ? location.split("?")[1] : "";
  if (currentTabData === 'about' || currentTabData === "1") {
    currentTabData = "1";
  } else if (currentTabData === 'trip' || currentTabData === "2") {
    currentTabData = "2";
  } else if (currentTabData === 'workshop' || currentTabData === "3") {
    currentTabData = "3";
  } else if (currentTabData === 'gallery' || currentTabData === "4") {
    currentTabData = "4";
  } else if (currentTabData === 'review' || currentTabData === "5") {
    if (document.getElementById("scoll-to-here") !== null) {
      setTimeout(() => {
        document
          .getElementById("scoll-to-here")
          .scrollIntoView({ block: "start", behavior: "auto" });
      }, 1000);
    }
    currentTabData = "5";
  }

  useEffect(() => {
    if (currentTabData !== "") {
      if (document.getElementById("scoll-to-here") !== null) {
        setTimeout(() => {
          document
            .getElementById("scoll-to-here")
            .scrollIntoView({ block: "start", behavior: "auto" });
        }, 500);
      }
    }
  }, []);
  useEffect(() => {
    dispatch(isEditMode());
    if (id !== null) {
      getTrips(id);
      getWorkShops(id);
      getPictures(token);
      getExpertReviews(id)
    }
  }, [dispatch, getTrips, getWorkShops, getPictures, id, token, isEditMode, getExpertReviews]);

  useEffect(() => {
    if (id !== null) {
      getExpertReviews(id)
    }
  }, [getExpertReviews])
  const uploadOriginalCover = useCallback(
    async (cover, originalCover) => {
      try {
        const formData = new FormData();
        if (cover !== "") {
          formData.append("cover", cover);
          if (originalCover)
            formData.append("originalCover", originalCover);
        } else {
          formData.append("cover", "");
          formData.append("originalCover", "");
        }
        const result = await uploadCoverOriginalPicture(token, formData);
        if (result.status === 200) {
          dispatch(updateCover(result.data.data));
        }
        setUploadingLoader(false);
        setRemovingLoader(false);
      } catch (err) {
        setUploadingLoader(false);
        setRemovingLoader(false);
        message.error(err.response.data.message);
      }
    },
    [dispatch, token, updateCover]
  );

  const editProfile = () => {
    dispatch(changeTab(1));
    router.push("/update-expert-profile");
  };

  const editBio = () => {
    dispatch(changeTab(2));
    router.push("/update-expert-profile");
  };

  const commaSepratorValueWithoutCase = (lang) => {
    let resLang = "";
    if (lang.length > 0) {
      lang.map((a, index) => {
        let addCooma = "";
        if (lang.length !== index + 1) {
          addCooma = ", ";
        }
        resLang += a + addCooma;
      });
    }

    return resLang;
  };

  const onCloseClick = () => {
    setRecentPhotoView(false);
  };

  const onRecentPhotoView = (pic) => {
    setCurrentViewPic(pic);
    setRecentPhotoView(true);
  };

  const loadMore = (type) => {
    if (type === "pq") {
      setTmpPQ(allPQ);
      setIsLoadMorePQ(false);
    } else if (type === "award") {
      setTmpAwards(allAwards);
      setIsLoadMoreAward(false);
    } else if (type === "social") {
      setTmpSocial(allSocial);
      setIsLoadMoreSocial(false);
    }
  };
  const setCoverImage = (coverFile, originalFile) => {
    if (coverFile && coverFile !== "") {
      setUploadingLoader(true);
      uploadOriginalCover(coverFile, originalFile)
    } else {
      setRemovingLoader(true);
      uploadOriginalCover("", "");
    }
  };

  if (!expert.stepPersonalInfo) {
    return (
      <div className="text-center py20 loader-absolute-class">
        <AppLoader />
      </div>
    );
  } else {
    return (
      <div className="expert-profile header-container expert-mobile-view">
        <div className="container-fluid align-center mobile">
          <div className="page_wrapper page_wrapper_pad mt10">
            <Row>
              <Col span={24} className="header_bg">
                <ImageUplaoderAndCropper removingLoader={removingLoader} uploadingLoader={uploadingLoader} setCoverImage={(file, originalFile) => setCoverImage(file, originalFile)} originalCover={expert.originalCover} imageUrl={expert.cover || Upload_Img} />
              </Col>

              <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                <div className="profile_img mt10">
                  <img src={expert.picture ? expert.picture : Upload_Img} alt="Profile" />
                  <span
                    className="edit_btn medium-text an-14"
                    onClick={editProfile}
                  >
                    <Icon className="edit_pancil" type="pencil" />
                    Edit
                  </span>
                </div>
              </Col>
              <Col xs={18} sm={15} md={12} lg={12} xl={12}>
                <div className="profile_details">
                  <h2 className="an-26 mt20 medium-text">
                    {CaptalizeFirst(expert.firstName) +
                      " " +
                      CaptalizeFirst(expert.lastName)}{" "}
                    {!isReviewLoading &&
                      <Rate
                        allowHalf
                        disabled
                        defaultValue={expertRating}
                        style={{ color: "#FFBC00" }}
                        className="ml10 an-18"
                      />
                    }
                    {isReviewLoading &&
                      <Rate
                        allowHalf
                        disabled
                        defaultValue={0}
                        style={{ color: "#FFBC00" }}
                        className="ml10 an-18"
                      />
                    }

                  </h2>
                  <h5 className="an-18 regular-text">
                    {commaSepratorValueWithoutCase(expert.experties)}
                  </h5>
                  <p className="an-14 regular-text">
                    <img src={Location_Img} alt="Profile" />
                    <span>
                      {getCityFromLocation(expert.city)}
                      {CaptalizeFirst(expert.country)}
                    </span>
                  </p>
                  <p className="an-14 regular-text">
                    <img src={Speaks} alt="Profile" />
                    {expert.speaks.map((lang, key) => (
                      <span key={key}>
                        <span key={key}>
                          {key < expert.speaks.length - 1
                            ? `${CaptalizeFirst(lang)}, `
                            : CaptalizeFirst(lang)}
                        </span>
                      </span>
                    ))}
                  </p>
                </div>
              </Col>
              <Col xs={6} sm={9} md={10} lg={6} xl={6}>
                <div className="share_btn mt20">
                  <span
                    onClick={editProfile}
                    className="edit_btn btn_edit mt10 medium-text an-14 mr10"
                  >
                    <Icon className="edit_pancil" type="pencil" /> Edit
                  </span>
                  <Dropdown overlay={menu} placement="bottomLeft">
                    <Button className="share_btn mt10 medium-text an-14">
                      <img src={Share_Ic} alt="Share" /> Share
                    </Button>
                  </Dropdown>
                </div>
              </Col>
            </Row>
          </div>
        </div>
        <div className="container-fluid align-center">
          <div className="page_wrapper page_wrapper_pad mt10">
            <Row>
              <Col>
                <div className="expert-profile_tabs">
                  <Tabs defaultActiveKey={currentTabData}>
                    <TabPane tab="About" key="1">
                      <Row gutter={[40, 40]} className="revers-direction">
                        <Col
                          className="gutter-row"
                          xs={24}
                          sm={24}
                          md={16}
                          lg={16}
                          xl={16}
                        >
                          <div className="bio_sec">
                            <Row>
                              <Col xs={14} sm={14} md={14} lg={14} xl={14}>
                                <h4 className="title_line">Bio</h4>
                              </Col>
                              <Col
                                xs={10}
                                sm={10}
                                md={10}
                                lg={10}
                                xl={10}
                                className="pt30"
                              >
                                <span
                                  href="#!"
                                  onClick={editBio}
                                  className="edit_profile medium-text an-14 pull-right mt30"
                                >
                                  <Icon className="edit_pancil" type="pencil" />{" "}
                                  Edit
                                </span>
                              </Col>
                            </Row>
                            <span className="lh-24">
                              {expert.bio && expert.bio.trim() !== "null"
                                ? ReactHtmlParser(expert.bio)
                                : ""}
                            </span>
                          </div>
                          <Row
                            type="flex"
                            justify="space-between"
                            className="profile_section--header"
                          >
                            <h4>Professional Qualifications</h4>
                            <span
                              className="edit_profile medium-text an-14 pull-right"
                              onClick={editBio}
                            >
                              <Icon className="edit_pancil" type="pencil" />
                              Edit
                            </span>
                          </Row>
                          <Row
                            type="flex"
                            justify="space-between"
                            gutter={[20, 20]}
                          >
                            {tmpPQ.length ? (
                              tmpPQ.map((professionalQualification, index) => {
                                return (
                                  <Col xs={24} sm={24} md={24} lg={12} xl={12} key={index}>
                                    <ExpCard
                                      {...professionalQualification}
                                      icon={Award_Img}
                                    />
                                  </Col>
                                );
                              })
                            ) : (
                              <div>
                                <div className="mb10 ml10 an-14 regular-text">
                                  <h3>No Professional Qualifications</h3>
                                </div>
                              </div>
                            )}

                            {allPQ.length > 4 && isLoadMorePQ && (
                              <>
                                <div style={{ clear: "both" }}></div>
                                <Col
                                  xs={24}
                                  sm={24}
                                  md={24}
                                  lg={24}
                                  xl={24}
                                  className="profile-load-more"
                                  style={{ textAlign: "center" }}
                                >
                                  <Button
                                    className="an-16 mt5"
                                    onClick={() => loadMore("pq")}
                                  >
                                    View More
                                  </Button>
                                </Col>
                              </>
                            )}
                          </Row>

                          <Row
                            type="flex"
                            justify="space-between"
                            className="profile_section--header"
                          >
                            <h4>Awards & Recognitions</h4>
                            <span
                              className="edit_profile medium-text an-14 pull-right"
                              onClick={editBio}
                            >
                              <Icon className="edit_pancil" type="pencil" />
                              Edit
                            </span>
                          </Row>
                          <Row
                            type="flex"
                            justify="space-between"
                            gutter={[20, 20]}
                          >
                            {tmpAwards.length ? (
                              tmpAwards.map((award, index) => (
                                <Col xs={24} sm={24} md={24} lg={12} xl={12} key={index}>
                                  <ExpCard {...award} icon={Award_Img} />
                                </Col>
                              ))
                            ) : (
                              <div>
                                <div className="mb10 ml10 an-14 regular-text">
                                  <h3>No Awards & Recognitions</h3>
                                </div>
                              </div>
                            )}

                            {allAwards.length > 4 && isLoadMoreAward && (
                              <>
                                <div style={{ clear: "both" }}></div>
                                <Col
                                  xs={24}
                                  sm={24}
                                  md={24}
                                  lg={24}
                                  xl={24}
                                  className="profile-load-more"
                                  style={{ textAlign: "center" }}
                                >
                                  <Button
                                    className="an-16 mt5"
                                    onClick={() => loadMore("award")}
                                  >
                                    View More
                                  </Button>
                                </Col>
                              </>
                            )}
                          </Row>

                          <Row
                            type="flex"
                            justify="space-between"
                            className="profile_section--header"
                          >
                            <h4>Social Initiatives</h4>
                            <span
                              className="edit_profile medium-text an-14 pull-right"
                              onClick={editBio}
                            >
                              <Icon className="edit_pancil" type="pencil" />
                              Edit
                            </span>
                          </Row>
                          <Row
                            type="flex"
                            justify="space-between"
                            gutter={[20, 20]}
                          >
                            {tmpSocial.length ? (
                              tmpSocial.map((socialContribution, index) => (
                                <Col span={24} key={index}>
                                  <ExpCard
                                    {...socialContribution}
                                    icon={Group_Img}
                                  />
                                </Col>
                              ))
                            ) : (
                              <div>
                                <div className="mb10 ml10 an-14 regular-text">
                                  <h3>No Social Contribution</h3>
                                </div>
                              </div>
                            )}

                            {allSocial.length > 4 && isLoadMoreSocial && (
                              <>
                                <div style={{ clear: "both" }}></div>
                                <Col
                                  xs={24}
                                  sm={24}
                                  md={24}
                                  lg={24}
                                  xl={24}
                                  className="profile-load-more"
                                  style={{ textAlign: "center" }}
                                >
                                  <Button
                                    className="an-16 mt5"
                                    onClick={() => loadMore("social")}
                                  >
                                    View More
                                  </Button>
                                </Col>
                              </>
                            )}
                          </Row>

                          <Row
                            type="flex"
                            justify="space-between"
                            className="profile_section--header"
                          >
                            <h4>Books and Publications</h4>
                            <span
                              className="edit_profile medium-text an-14 pull-right"
                              onClick={editBio}
                            >
                              <Icon className="edit_pancil" type="pencil" />
                              Edit
                            </span>
                          </Row>
                          <Row className="books_and_publication">
                            <OwlCarousel
                              className="owl-theme owl-dots slider_prev_next photo_video_sec"
                              {...sliderOptions}
                            >
                              {expert.booksAndPublications.length ? (
                                expert.booksAndPublications.map(
                                  (book, index) => (
                                    <Col
                                      xs={24}
                                      sm={24}
                                      md={24}
                                      lg={24}
                                      xl={24}
                                      key={index}
                                    >
                                      <ExpBookCard
                                        {...book}
                                        icon={default_Book_cover_Img}
                                        edit={true}
                                      />
                                    </Col>
                                  )
                                )
                              ) : (
                                <div>
                                  <div className="mb10 an-14 regular-text">
                                    <h3>No Books and Publications</h3>
                                  </div>
                                </div>
                              )}
                            </OwlCarousel>
                          </Row>
                        </Col>
                        <Col
                          className="gutter-row"
                          xs={24}
                          sm={24}
                          md={8}
                          lg={8}
                          xl={8}
                        >
                          <div className="buss_bg">
                            <Row>
                              <Col span={12}>
                                <h4 className="title_line">Skills</h4>
                              </Col>
                              <Col span={12}>
                                <div className="right_sec pull-right text-right mt30">
                                  <span
                                    href="#!"
                                    onClick={editProfile}
                                    className="edit_btn medium-text an-14"
                                  >
                                    <Icon
                                      className="edit_pancil"
                                      type="pencil"
                                    />{" "}
                                    Edit
                                  </span>
                                </div>
                              </Col>
                            </Row>
                            <Row>
                              <Col>
                                {expert.skills.map((skill, key) => (
                                  <span key={key} className="experties-tag">
                                    {skill}
                                  </span>
                                ))}
                              </Col>
                            </Row>
                            <Row className="ask_export--button mt10 mb20">
                              <Col>
                                <span>
                                  *Have a question about these skills?
                                </span>
                                <Button className="an-16 mt5" id="ask_this_expert" >
                                  Ask This Expert
                                </Button>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mt30">
                        <Col>
                          <div className="photo_sec">
                            {pictures && pictures.length > 0 && (
                              <Row className="standered_border mt70 pt30">
                                <Col>
                                  <Row>
                                    <Col span={24}>
                                      <h4 className="features_sub_title mb30">
                                        Recent Photos and Videos
                                      </h4>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <OwlCarousel
                                      className="owl-theme owl-dots slider_prev_next photo_video_sec expert-recent-pic"
                                      {...sliderOptions}
                                    >
                                      {pictures
                                        .slice(0, DISPLAY_RESULT)
                                        .map((pic, index) => {
                                          return (
                                            <Col
                                              xs={24}
                                              sm={24}
                                              md={24}
                                              lg={24}
                                              xl={24}
                                              key={index}
                                              className="picture_thumbnail"
                                            >
                                              <img
                                                src={pic}
                                                alt=""
                                                onClick={() =>
                                                  onRecentPhotoView(pic)
                                                }
                                              />
                                            </Col>
                                          );
                                        })}
                                    </OwlCarousel>
                                  </Row>
                                </Col>
                              </Row>
                            )}
                            {tripsAndWorkShops && tripsAndWorkShops.length > 0 && (
                              <>
                                <Row className="Similar_Tripsdetail pt30">
                                  <Col span={24}>
                                    <h4 className="features_sub_title mb30">
                                      Upcoming Trips and Workshops
                                    </h4>
                                  </Col>
                                </Row>
                                <Row
                                  gutter={10}
                                  className="margin_fix_mobile expert_fix_mobile expendition img_pic slider_prev_next"
                                >
                                  <TripWorkshopAllCardsCarousel
                                    items={tripsAndWorkShops}
                                  />
                                </Row>
                              </>
                            )}
                            <Row className="Similar_Tripsdetail pt30">
                              <Col>
                                {travelMap && travelMap.length > 0 ? (
                                  <div>
                                    <h4 className="features_sub_title mb30">
                                      Travel Map
                                    </h4>
                                    <Row>
                                      <div className="map pb30">
                                        <Map
                                          multiple
                                          travelMap={travelMap}
                                          zoom={5}
                                          ApiKey={ApiKey}
                                        />
                                      </div>
                                    </Row>
                                  </div>
                                ) : (
                                  ""
                                )}
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                    </TabPane>
                    <TabPane tab="Trips" key="2">
                      <div className="" id="scoll-to-here">
                        <Row>
                          <Col>
                            <MyTrips
                              isPublicView={false}
                              token={token}
                              publicTrip={[]}
                              travelMap={travelMap}
                            />
                          </Col>
                        </Row>
                      </div>
                    </TabPane>
                    <TabPane tab="Workshops" key="3">
                      <Col id="scoll-to-here">
                        <WorkShopTab
                          isPublicView={false}
                          token={token}
                          publicWorkShop={[]}
                          travelMap={travelMap}
                        />
                      </Col>
                    </TabPane>
                    <TabPane tab="Gallery" key="4">
                      <div id="scoll-to-here">
                        <AlbumTab isPublicView={false} token={token} />
                      </div>
                    </TabPane>
                    {typeof reviewData !== "undefined" && reviewData.length > 0 &&
                      <TabPane tab="Review" key="5">
                        <Reviews isPublicView={false} expertId={id} totalReview={totalReview} expertRating={expertRating} reviewData={reviewData}></Reviews>
                      </TabPane>
                    }
                  </Tabs>
                </div>
              </Col>
            </Row>
            {recentPhotoView && (
              <RecentPhotoViewPopup
                visible={recentPhotoView}
                onCloseClick={onCloseClick}
                pic={currentViewPic}
              />
            )}
          </div>
        </div>
        <Newsletter page="expert_profile" />
      </div>
    );
  }
};

export default compose(ExpertProfile);
