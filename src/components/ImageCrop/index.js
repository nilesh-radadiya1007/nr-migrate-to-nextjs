import React, { useState, useRef, useCallback } from "react";
import { Modal, Input } from "antd";
import PropTypes from 'prop-types';
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";

/**
 * Cropping component for image.
 * @param {id} string unique identifier for component(Required)
 * @param {croppingRatio} number specify cropping ratio over image
 * @param {children} any specify component for UI
 * @param {handleChangePicture} method method for sending image to cloud
 * @param {beforeUpload} method Method that return boolean value
 */

const ImageCrop = ({ id, croppingRatio, children, handleChangePicture, beforeUpload, onChange = null }) => {

    const [isProfileModalVisible, setIsProfileModalVisible] = useState(false);
    const [originalFileURL, setOriginalFIleURL] = useState(null);
    const [croppedFile, setCroppedFile] = useState(null);
    const [crop, setCrop] = useState({ unit: "px", width: 300, aspect: croppingRatio });
    const imgRef = useRef(null);
    const [coveImageValue, setCoveImageValue] = useState(null);

    const onLoad = useCallback((img) => {
        imgRef.current = img;
    }, []);

    const onComplete = (c) => {
        var canvas = document.getElementById("c");
        var ctx = canvas.getContext("2d");
        canvas.width = c.width;
        canvas.height = c.height;
        const image = imgRef.current;
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        ctx.drawImage(
            image,
            c.x * scaleX,
            c.y * scaleY,
            c.width * scaleX,
            c.height * scaleY,
            0,
            0,
            c.width,
            c.height
        );

        var dataurl = canvas.toDataURL("image/png")
        var arr = dataurl.split(',');
        if (c.width > 0 && c.height > 0) {
            var mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);

            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }
            var file = new File([u8arr], 'newImage.jpg', { type: mime });
            setCroppedFile(file);
        }
    }

    return (
        <React.Fragment>
            <label htmlFor={id}>
                {children}
            </label>
            <Input
                type='file'
                id={id}
                style={{ display: 'none' }}
                value={coveImageValue}
                onChange={(e) => {
                    e.preventDefault();
                    const reader = new FileReader();
                    reader.addEventListener("load", () => setOriginalFIleURL(reader.result));
                    reader.readAsDataURL(e.target.files[0]);
                    setIsProfileModalVisible(true);
                    e.target.files = null;
                    setCoveImageValue(e.target.value);
                }}
            />
            {/* modal for cropping the image */}
            <Modal visible={isProfileModalVisible}
                onCancel={() => {
                    setCroppedFile(null);
                    setOriginalFIleURL(null);
                    setCoveImageValue(null);
                    setIsProfileModalVisible(!isProfileModalVisible);
                }}
                onOk={() => {
                    if (beforeUpload(croppedFile)) {
                        const info = { file: { status: 'done', originFileObj: croppedFile } };
                        if (onChange) {
                            onChange({ file: info.file, fileList: [info.file] });
                        }
                        handleChangePicture(info);
                        setCroppedFile(null);
                        setOriginalFIleURL(null);
                        setCoveImageValue(null);
                        setIsProfileModalVisible(!isProfileModalVisible);
                    }
                }}
                width='80%'
                style={{ textAlign: 'center' }}
                closable={false}
            >
                <canvas id="c" style={{ display: 'none' }}></canvas>
                <ReactCrop
                    src={originalFileURL}
                    onImageLoaded={onLoad}
                    crop={crop}
                    onChange={(c) => setCrop(c)}
                    onComplete={onComplete}
                    ruleOfThirds
                    minHeight={100}
                    minWidth={100}
                />
            </Modal>
        </React.Fragment>
    )
}

ImageCrop.defaultProps = {
    beforeUpload: () => { return true }
}

ImageCrop.propTypes = {
    id: PropTypes.string.isRequired,
    aspectRatio: PropTypes.number.isRequired,
    children: PropTypes.any.isRequired,
    handleChangePicture: PropTypes.func.isRequired,
    beforeUpload: PropTypes.func
}

export default ImageCrop;