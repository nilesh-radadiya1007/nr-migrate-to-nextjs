import React, { useState } from "react";
import { Modal, Row, Col, Form, Input, Button, Select, DatePicker, message, Checkbox } from "antd";
import { compose } from "redux";
import { useSelector } from 'react-redux';
import moment from 'moment';
import { SendMessage, addBooking } from '../../services/expert';

const Close = "/images/close_ic.png";
const { TextArea } = Input;

const Reservation = (props) => {

  const { getFieldDecorator } = props.form;
  const token = useSelector(state => state.auth.accessToken);
  const enthu = useSelector(state => state.enthu);

  const [loader, setLoader] = useState(false);
  const [termCondition, setTermCondition] = useState(false);
  const sendMessageToExpert = async (params, booking) => {
    setLoader(true);
    const data = { message: params }
    try {
      const result = await SendMessage(token, data, props.id, props.type ? props.type : 'trips');
      const result1 = await addBooking(token, booking, props.id);
      if (result.status === 200 && result1.status === 200) {
        setLoader(false);
        message.success('Reservation request submitted successfully');
        props.onCloseClick();
      }
    } catch (error) {
      setLoader(false)
      message.error('Error submitting request');
    }
  };

  const onFormSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {

      if (!err) {

        if (!termCondition) {
          return message.error("Checked Terms & Conditions");
        }

        const date = values.date.format('Do, MMM YY')
        const messageTxt = `<div class="send-msg">
        <h3 class="send-title"> Booking Request </h3>
        <p class="trip-name">Trip: <span>${props.trip}</span></p>
        <p class="name">Name: <span>${values.name}</span></p>
        <p class="phone">Phone: <span>${typeof values.phone === "undefined" ? "Not Available" : values.phone}</span></p>
        <p class="people">People: <span>${values.people}</span></p>
        <p class="date">Date: <span>${date}</span></p>
        <div class="desc">
          ${values.description}
        </div>
        </div>
      `
        const booking = {
          Phone: typeof values.phone === "undefined" ? "Not Available" : values.phone,
          People: values.people,
          Date: values.date,
          slot: props.slotnum,
        }
        sendMessageToExpert(messageTxt, booking)
      }
    })
  }

  const onTermConditionChange = (e) => {
    if (e.target.checked) {
      setTermCondition(true);
    } else {
      setTermCondition(false);
    }

  }

  const disabledDate = (current) => {
    // Can not select days before today and today
    if (props.slot === null) {
      return current && current < moment().endOf('day');
    } else {
      return;
    }

  }

  return (
    <Modal
      centered
      className="auth-modal reserve_mdl"
      width={380}
      closable={false}
      maskClosable={false}
      visible={props.visible}>
      <div className="text-left sec_border">
        <Row>
          <Col span={20}>
            <h4 className="sub_title">Reservation Request</h4>
            <p className="an-14 medium-text pt10">
              Please fill the form for your reservation
            </p>
          </Col>
          <Col span={4} className="text-right">
            <img src={Close} alt="close" onClick={props.onCloseClick} />
          </Col>
        </Row>
      </div>
      <Form className="ant-advanced-search-form pt20" onSubmit={onFormSubmit}>
        <div className="reserve_frm_bg">
          <Row gutter={[20]}>
            <Col span={12} className="gutter-row">
              <Form.Item label="Name" className="mb0 flex-1">
                {
                  getFieldDecorator("name", {
                    rules: [{ required: true, message: "Name is required field" }],
                    initialValue: `${enthu.firstName} ${enthu.lastName}`
                  })
                    (<Input placeholder="Enter Name" disabled={true} />)
                }
              </Form.Item>
            </Col>
            <Col span={12} className="gutter-row">
              <Form.Item label="Date">
                {
                  getFieldDecorator("date", {
                    rules: [{ required: true, message: "Date is required field" }],
                    initialValue: props.slot !== null ? moment(props.slot) : moment()
                  })
                    (<DatePicker disabledDate={disabledDate} disabled={props.slot !== null ? true : false} />)
                }
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[20]}>
            <Col span={12} className="gutter-row">
              <Form.Item label="Number of People">
                {
                  getFieldDecorator("people", { rules: [{ required: true, message: "Number of People is required field" }] })
                    (<Input placeholder="Number of People" />)
                }
              </Form.Item>
            </Col>
            <Col span={12} className="gutter-row">
              <Form.Item label="Contact No." className="mb0 flex-1">
                {
                  getFieldDecorator("phone", { rules: [{ required: false, message: "Please enter Contact No" }] })
                    (<Input placeholder="Enter Contact No." />)
                }

              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[20]}>
            <Col span={24} className="gutter-row">
              <Form.Item label="Description" className="mb0 flex-1">
                {
                  getFieldDecorator("description", { rules: [{ required: true, message: "Description is a required field" }] })
                    (<TextArea rows={4} placeholder="Description..." />)
                }
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[20]}>
            <Col span={24} className="gutter-row term_conditon_ckb ">
              <div className="term_condition_txt">
                <span>
                  Expeditions Connect currently do not accept any form of payments on the Platform. All payments, bookings related matters and disputes will be directly handled by Adventure Providers(Experts) and Customers.
                </span>
              </div>
            </Col>
          </Row>
          <Row gutter={[20]}>
            <Col span={24} className="gutter-row term_conditon_ckb ">
              <Form.Item >
                {
                  getFieldDecorator("termcondition", { rules: [{ required: true, message: "Checked Terms & Conditions" }] })
                    (<Checkbox className="term_condition_txt" onChange={onTermConditionChange}>
                      By Selecting this checkbox, You agree to the Terms & Conditions of Expeditions Connect.
                  </Checkbox>)
                }
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="text-right mt40">
                <Button
                  type="primary"
                  className="ex_grey_btn mr20"
                  onClick={props.onCloseClick}>
                  Cancel
                </Button>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loader}
                  className="ex__primary_btn">
                  Send
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      </Form>
    </Modal>
  );
};

const WrappedReservation = Form.create({ name: "Reservation" })(Reservation);
export default compose(WrappedReservation);
