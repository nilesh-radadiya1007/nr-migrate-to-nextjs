import React, { useState } from "react";
import { Row, Col, Form, Input, Button, Modal, Select, Checkbox } from "antd";
import { Contact } from '../services/expert';

import { ContactPhoneValidator } from '../helpers/validations';
import Link from "next/link";
import { useRouter } from "next/router";
import queryString from "query-string";

import Newsletter from '../components/common/NewsLetter';

const Success = "images/success_ic.png";

const Option = Select.Option;
function ContactUs(props) {

    let urlParams = queryString.parse(window.location.search);

    const router = useRouter();

    const { getFieldDecorator } = props.form;
    const [isLoading, setIsLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [inquieryType, setInquieryType] = useState(urlParams.type === "covid" ? "Tailormade Adventures" : "");

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 6 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 },
        },
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0,
            },
            sm: {
                span: 18,
                offset: 6,
            },
        },
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields(async (err, values) => {
            if (!err) {
                if (urlParams.type === "covid") {
                    values.reason = "Tailormade Adventures";
                }
                // urlParams.type === "covid"
                if (values.newsletterAllow !== undefined && values.newsletterAllow) {
                    values.newsletterAllow = 'yes';
                } else {
                    values.newsletterAllow = 'no';
                }
                setIsLoading(true);
                const result = await Contact(values);
                if (result.status === 200) {
                    setIsLoading(false);
                    setIsSuccess(true);
                } else {
                    setIsLoading(false);
                }
            }
        });
    }

    return (
        <>
            <div className="contact-page expert_container">
                <div className="contact-container">
                    <p className="title">{urlParams.type === "covid" ? "Make your own adventure" : "Contact Us"}</p>
                    <p className="sub-title">{urlParams.type === "covid" ? 'Whatever your dream adventure looks like, our team of coordinators and experts will help you craft the perfect expedition for your next getaway.' : 'Got a question? We’d love to hear from you. Send us a message and we’ll respond as soon as possible.'}</p>

                    <p className="sub-title off-subtitle">{urlParams.type === "covid" ? 'Fill out the form to get started and we’ll get back to you right away!' : ''}</p>
                    <Row gutter={[25]}>
                        <Form {...formItemLayout} onSubmit={(e) => handleSubmit(e)}>
                            {urlParams.type !== "covid" &&
                                <Form.Item label="Inquiry Type">
                                    {getFieldDecorator('reason', {
                                        initialValue: inquieryType || "",
                                        rules: [
                                            {
                                                required: false,
                                                message: 'Please select inquiry type.',
                                            }
                                        ],
                                    })(
                                        <Select
                                            initialValue={inquieryType || ""}
                                            placeholder={"Select Type"}
                                        >
                                            <Option value="">Select Type</Option>
                                            <Option value="General Information">General Information</Option>
                                            <Option value="Trip Inquiry">Trip Inquiry</Option>
                                            <Option value="Partnership Request">Partnership Request</Option>
                                            <Option value="Tailormade Adventures">Tailormade Adventures</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                            }
                            <Form.Item label="Name">
                                {getFieldDecorator('name', {
                                    rules: [
                                        {
                                            required: true,
                                            message: 'Please enter name.',
                                        },
                                    ],
                                })(<Input placeholder="Enter Name" />)}
                            </Form.Item>

                            <Form.Item label="Email">
                                {getFieldDecorator('email', {
                                    rules: [
                                        {
                                            type: 'email',
                                            message: 'Please enter valid email address.',
                                        },
                                        {
                                            required: true,
                                            message: 'Please enter email address.',
                                        },
                                    ],
                                })(<Input placeholder="Enter Email" />)}
                            </Form.Item>

                            <Form.Item label="Phone">
                                {getFieldDecorator("phone", ContactPhoneValidator)
                                    (<Input placeholder="Enter Phone Number" />)}
                            </Form.Item>



                            <Form.Item label="Message" hasFeedback className="text-area-class">
                                {getFieldDecorator('message', {
                                    rules: [
                                        {
                                            required: true,
                                            message: 'Please enter message',
                                        }
                                    ],
                                })(<Input.TextArea placeholder="Enter Message" rows={8} />)}
                            </Form.Item>
                            <Form.Item {...tailFormItemLayout}>
                                {
                                    getFieldDecorator("newsletterAllow", { valuePropName: "checked", initialValue: false, rules: [{ required: false, message: "Checked Terms & Conditions" }] })
                                        (<Checkbox className="term_condition_txt" >
                                            I would like receive latest news and offers from Expeditions Connect.
                                        </Checkbox>)
                                }
                            </Form.Item>
                            <Form.Item {...tailFormItemLayout}>
                                {isLoading ? (<Button type="primary" className="contact-submit" htmlType="submit" loading disabled>
                                    SEND A MESSAGE
                                </Button>) : <Button type="primary" className="contact-submit" htmlType="submit">
                                    SEND A MESSAGE
                                </Button>}

                            </Form.Item>
                            <Form.Item {...tailFormItemLayout}>
                                <p className="disclaimer">
                                    By Clicking 'Send' button you agree to our
                                    <Link href="/terms-and-conditions">
                                        <a target="_blank"> Terms of Service </a>
                                    </Link> and
                                    <Link href="/privacy-policy">
                                        <a target="_blank">Privacy Policy</a>
                                    </Link>.
                                </p>
                            </Form.Item>
                        </Form>
                    </Row>
                </div>
            </div>
            <Newsletter page="contact" />

            {isSuccess &&
                <Modal
                    centered
                    className="auth-modal success-modal"
                    width={380}
                    closable={false}
                    maskClosable={false}
                    visible={isSuccess}
                >
                    <div className="text-center">
                        <img src={Success} alt="" />
                        <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
                            Thank You
                        </h1>
                        <p className="an-18 mb20 regular-text">
                            Thank you for contact us, We’ll respond as soon as possible.

                        </p>

                        <Button type="primary" className="ex__primary_btn" onClick={() => router.push('/')}>
                            Ok
                        </Button>

                    </div>
                </Modal>
            }
        </>

    );
}


const WrappedNormalRegisterForm = Form.create({ name: "RegisterForm" })(ContactUs);
export default (WrappedNormalRegisterForm);