import React from 'react';
import Home1 from '../src/containers/Home1';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const HomepageInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Home1 />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default HomepageInApp
