import React from "react";
import { Modal, Button } from "antd";
import { compose } from "redux";
/**
 * Image Import
 */
const Success = "/images/success_ic.png";

const TripModal = (props) => {

  const handleClick = () => {
    props.onClose();
  }

  return (
    <Modal
      centered
      className="auth-modal success-modal"
      width={380}
      closable={false}
      maskClosable={false}
      visible={props.visible}
    >
      <div className="text-center">
        <img src={Success} alt="" />
        <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
          {props.title}
        </h1>
        <p className="an-18 mb20 regular-text">
          {props.message}
        </p>
        <Button type="primary" className="done_btn medium-text an-16 ex__primary_btn" onClick={handleClick}>
          CLOSE
        </Button>
      </div>
    </Modal>
  );
};

export default compose(TripModal);
