import React, { Fragment, useState, useEffect } from 'react';
import { compose } from "redux";
import { useSelector, useDispatch } from "react-redux";
import { Row, Col, Rate, Dropdown } from 'antd';
import { addorRemoveLikeForTrip, addorRemoveLikeForLearning, addorRemoveLikeForAlbum } from "../../services/expert";
import { ModalActions } from "../../redux/models/events";
// import Share from "../../../public/images/newicon/share_new_icon.svg";
// import heart from "../../../public/images/newicon/heart.svg";
// import heartFill from "../../../public/images/newicon/heart_fill.svg";
// import save from "../../../public/images/newicon/save_new.svg";

import shareOption from "../../containers/commonContainer/shareOption";
import { getLikeStatus } from "../../helpers/methods";

const LikeAndShare = (props) => {

    const dispatch = useDispatch();

    let { allLikes, id, pageType, designType } = props;

    const [likeStatus, setLikeStatus] = useState(false);
    const [isCountLoaded, setIsCountLoaded] = useState(false);
    const [likeCount, setLikeCount] = useState(0);
    const token = useSelector((state) => state.auth.accessToken);
    const userId = useSelector((state) => state.auth.userId);
    const [isLoading, setIsLoading] = useState(false);

    const { openAuthModal } = ModalActions;


    useEffect(() => {
        getTheLikeCount();
        setLikeStatus(getLikeStatus(allLikes, userId));
    }, []);

    const getTheLikeCount = (type) => {
        let count = typeof allLikes !== "undefined" ? allLikes.length : 0;
        setLikeCount(count);
    }

    const updateLikeCountText = (type, id) => {
        let count = likeCount;
        if (typeof type !== "undefined" && type === 'like') {
            count = count + 1;
        } else if (typeof type !== "undefined" && type === 'dislike') {
            count = count - 1;
        }
        setLikeCount(count);
    }

    const onLikeButtonClick = async (event, id) => {
        event.stopPropagation();
        setIsCountLoaded(true);
        if (token) {
            setIsLoading(true);
            let response;
            if (pageType === "trip") {
                response = await addorRemoveLikeForTrip(token, { id });
            } else if (pageType === "workshop") {
                response = await addorRemoveLikeForLearning(token, { id });
            } else if (pageType === "album") {
                response = await addorRemoveLikeForAlbum(token, { id });
                props.refreshAlbum(designType);
            }

            if (response && response.data && response.data.success === "SUCCESS") {
                updateLikeCountText(response.data.data.ans, id);
                setLikeStatus(getLikeStatus(allLikes, userId, response.data.data.ans));
                setIsCountLoaded(false);
                setIsLoading(false);

            } else {
                setIsLoading(false);
            }
        } else {
            //Set the referre URL for the after login redirection
            localStorage.setItem("referrer", window.location.pathname);
            dispatch(openAuthModal(true));
        }
        setIsCountLoaded(false);
    };

    return (
        <Fragment>
            <Row>
                {designType === "directoryPage" &&
                    <div className="">
                        <div className="like-share-save">
                            <div>
                                {isLoading ?
                                (<img
                                    src={likeStatus ? '/images/newicon/heart_fill.svg' : '/images/newicon/heart.svg'}
                                    alt="Like" className="like-heart-new-icon"
                                />)
                                :
                                (<img src={likeStatus ?
                                    '/images/newicon/heart_fill.svg' : '/images/newicon/heart.svg'} alt="Like"
                                    className="like-heart-new-icon"
                                    onClick={(event) => onLikeButtonClick(event, id)} />)}
                            </div>
                            <Dropdown overlay={shareOption} placement="bottomCenter">
                                <div className="share_new_icon">
                                    <img src={'/images/newicon/share_new_icon.svg'} alt="Social Share"  />
                                </div> 
                            </Dropdown>
                            <div className="save-new-icon">
                                <img
                                    src={'/images/newicon/save_new.svg'}
                                    alt="Like" 
                                />
                            </div>      
                        </div>

                        <div className={`${likeCount <= 1 ? 'small' : 'big'} like-count-text-on-card mb15`}>
                            {likeCount !== 0 &&
                                <>
                                    {isCountLoaded && "  like "}
                                    {!isCountLoaded && <><span>{likeCount}</span> <span> {likeCount > 1 ? 'likes' : 'like'}</span></>}
                                </>
                            }
                        </div>

                    </div>
                }
            </Row>
        </Fragment>
    )
}

export default compose(LikeAndShare)
