import React, { useRef } from 'react';
// import { useSelector } from "react-redux";
import { Card, Form, Modal, Row, Col, Carousel, Dropdown, Input } from 'antd';
// import OwlCarousel from "react-owl-carousel";

// const ArrowImage = '/images/arrow_imag.png';

// const expeditionsOptions = {
//     items: 3,
//     nav: true,
//     loop: false,
//     responsiveClass: true,
//     responsive: {
//         0: {
//             items: 1,
//             nav: true,
//             dots: false,
//         },
//         768: {
//             items: 2,
//             nav: true,
//             dots: false,
//         },
//         991: {
//             items: 3,
//             nav: true,
//             dots: false,
//         },
//     },
// };

const RecentPhotoViewPopup = React.memo((props) => {

    const slider = useRef();
    const { visible, onCloseClick, pic } = props;

    return (
        <Modal
            visible={visible}
            onCancel={onCloseClick}
            footer={false}
            className="accommodation_popup header_text album_select_detail_main"
        >
            <div className="album_select_detail">
                <Row>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <Carousel
                            autoplay
                            className="album_carousel"
                            ref={ref => { slider.current = ref; }}
                        >

                            <div>
                                <div className="div_image_fix cursor" style={{ backgroundImage: `url(${pic})` }}>

                                </div>

                            </div>

                        </Carousel>

                    </Col>
                </Row>
            </div>
        </Modal >
    );
});

export default Form.create({ name: 'album' })(RecentPhotoViewPopup);
