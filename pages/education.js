import React from 'react'
import Education from '../src/containers/Education';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const EducationInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Education />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default EducationInApp;