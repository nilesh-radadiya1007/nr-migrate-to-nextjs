import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';


const ExpertProfile = dynamic(
    () => import("../src/containers/ExpertProfile"),
    { ssr: false }
);
const ExpertProfileInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <ExpertProfile />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default ExpertProfileInApp;