import React, { Fragment } from "react";
import Link from 'next/link';
import { compose } from "redux";
import { Row, Col } from "antd";
import OwlCarousel from "react-owl-carousel";
// import Image from 'next/image';
const adventure3 = "/images/home/landingpage/adventure3_upcoming.jpg";
const adventure4 = "/images/home/landingpage/adventure4.jpg";
const icon8 = "/images/home/landingpage/icon1.png";
const icon2 = "/images/home/landingpage/icon2.png";

const sliderOptions = {
    margin: 35,
    nav: true,
    responsive: {
        0: {
            items: 1,
            nav: true,
            dotsEach: 3,
        },
        768: {
            items: 2,
            nav: true,
        },
        991: {
            items: 2,
            nav: true,
        },
    },
};

const EventAndAdventuresBigCard = (props) => {

    let adventureData = [

        {
            "id": "4",
            "title": "UPCOMING ADVENTURES",
            "link": "/all-adventure?isFlexible=false",
            "img": adventure3,
            "icon": icon2
        },
        {
            "id": "5",
            "title": "GIVE YOURSELF SOME TIME",
            "link": "/all-adventure?longAdventure=true",
            "img": adventure4,
            "icon": icon8
        },


    ]

    return (
        <Row gutter={[30, 30]}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24} className="pt25 pb45 accomo_thumb">
                <OwlCarousel
                    className="owl-theme modular-card"
                    {...sliderOptions}
                    dots={true}
                >
                    {adventureData.map((item, index) => {
                        return (
                            <Fragment key={index}>
                                <div>
                                    <Link href={item.link}>
                                        <a>
                                            <div>
                                                <img src={item.img} alt="Img" className="adventure_section_img" />
                                                <div className="adventure_section_txt">
                                                    <img src={item.icon} alt="Img" className="adventure_section_icon_img" />
                                                    {item.title}
                                                </div>
                                            </div>
                                        </a>
                                    </Link>
                                </div>
                            </Fragment >
                        );
                    })}
                </OwlCarousel>
            </Col>
        </Row >

    );
};

export default compose(EventAndAdventuresBigCard);
