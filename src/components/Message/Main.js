import React from 'react';

import Profile from './Profile';
import MessageArea from './Area';

const Main = (props) => {
  const { chat } = props;
  return (
    <div className='container fix_container_width'>
      <div className='right-card'>
        {!chat && <React.Fragment><p style={{margin: "0 auto", padding: "10px", textAlign: 'center'}}>Please select any trip from sidebar.</p></React.Fragment>}
        {chat && (
          <React.Fragment>
            <Profile profile={chat} {...props} />
            <MessageArea {...props} />
          </React.Fragment>
        )}
      </div>
    </div>
  );
};

export default Main;
