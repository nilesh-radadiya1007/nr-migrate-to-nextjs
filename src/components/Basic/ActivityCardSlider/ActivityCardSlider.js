import cx from "classnames";
import Carousel from "nuka-carousel";
import React, { useRef, useEffect, useState } from "react";
import { compose } from "redux";
import ActivityCard from "../ActivityCard/ActivityCard";
import s from "./ActivityCardSlider.module.scss";
import { useWindowResize } from "beautiful-react-hooks";
import { useMediaQuery } from "react-responsive";

const ActivityCardSlider = (props) => {
  const carouselRef = useRef();
  const [currentSlideIndex, setCurrentSlideIndex] = useState(0);
  const [innerWidth, setInnerWidth] = useState(window.innerWidth);
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });

  useWindowResize((event) => {
    setInnerWidth(window.innerWidth);
  });

  // To fix initial load height issue
  useEffect(() => {
    setTimeout(() => {
      window.dispatchEvent(new Event("resize"));
    }, 0);
    setTimeout(() => {
      window.dispatchEvent(new Event("resize"));
    }, 1000);
  });

  useEffect(() => {
    console.log("carouselRef.current :>> ", carouselRef.current);
  }, []);

  // Pagination calculations
  // const totalNoOfActivityCards = props.data.length;
  const slidesToShow = isMaxWidth768 ? 1 : (innerWidth - 300) / 424;
  // const noOfDots = totalNoOfActivityCards - Math.floor(slidesToShow) + 1;
  // const paginationDots = [];
  // for (let i = 0; i < noOfDots; i++) {
  //   paginationDots.push(
  //     <div
  //       className={cx(s.PaginationDot, {
  //         [s.PaginationDot__Active]: i === Math.ceil(currentSlideIndex),
  //       })}
  //     />
  //   );
  // }

  // // Handle next or prev button click
  // const updateSlideIndex = (type) => {
  //   if (type === "++") {
  //     carouselRef.current.nextSlide();
  //   } else if (type === "--") {
  //     carouselRef.current.previousSlide();
  //   }
  // };

  return (
    <div
      className={cx(s.ActivityCardSlider, {
        [props.classNameŚ]: props.className,
      })}
    >
      <Carousel
        slidesToShow={slidesToShow}
        cellSpacing={10}
        ref={(c) => (carouselRef.current = c)}
        slideIndex={currentSlideIndex}
        afterSlide={(slideIndex) => setCurrentSlideIndex(slideIndex)}
        renderCenterLeftControls={null}
        renderCenterRightControls={null}
        renderBottomCenterControls={null}
        swiping
        dragging
      >
        {props.data.map((value, i) => (
          <ActivityCard
            key={i}
            activityData={value}
            cardRedirectURL={props.cardRedirectURL}
          />
        ))}
      </Carousel>
      {/* <div className={s.ActivityCard__Pagination}>
        <Button
          disabled={Math.ceil(currentSlideIndex) === 0}
          shape="circle"
          onClick={() => updateSlideIndex("--")}
        >
          <LeftOutlined className={s.ActivityCard__Pagination_LeftArrow} />
        </Button>

        {paginationDots}

        <Button
          disabled={Math.ceil(currentSlideIndex) === noOfDots}
          shape="circle"
          onClick={() => updateSlideIndex("++")}
        >
          <RightOutlined className={s.ActivityCard__Pagination_RightArrow} />
        </Button>
      </div> */}
    </div>
  );
};

export default compose(ActivityCardSlider);
