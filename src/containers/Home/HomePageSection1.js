import React, { useState } from "react";
import Link from "next/link";
import { compose } from "redux";
import { useSelector } from "react-redux";
import { Button, Select, Icon, Input } from "antd";
// import Onetree from "../../assets/images/partner/mask3.png";
// import Leavenotrace from "../../assets/images/partner/leavenotrace.png";
// import locatorIconActive from "assets/images/newicon/locator_active.svg";

import WatchVideoRevealHomepage from "../../components/Basic/WatchVideoReveal/WatchVideoRevealHomepage";

import { useMediaQuery } from "react-responsive";
import { ActivityList } from "../../helpers/constants";
import countries from "../../helpers/countries";
const { Option } = Select;
const InputGroup = Input.Group;

function Home(props) {

    const [isFullScreenVideo, setIsFullScreenVideo] = useState(false);

    const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });

    const [activityDList, setActivityList] = useState(ActivityList);

    return (
        <div className="">
            <div className="expert_head_bg expert_head_bg_color">
                <div className={`${isFullScreenVideo ? 'video expert_header_info' : 'expert_header_info'}`}>
                    <div className="bg-title">
                        <span className="second-txt">Take on the Challenge.</span><span className="first-txt">Embrace the Adventure</span>
                    </div>
                    <div className="bg-sub-title">
                        <span>“Connect and learn with the best adventure experts in your field <br></br> with Expeditions Connect. We qualify, you explore.” </span>
                    </div>
                    <div className="search-textbox">
                        <InputGroup compact>
                            <Select
                                defaultValue=""
                                showSearch
                                placeholder="All Location"
                                optionFilterProp="children"
                                showArrow={true}
                                onChange={(e) => props.setCountry(e)}
                                suffixIcon={<Icon type="caret-down" style={{ fontSize: '17px', color: '#000000', marginTop: " -1px" }} />}
                                className="homepage-search-location"
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                <Option value="">All Location</Option>
                                {countries.map((exp, i) => (
                                    <Option key={i} value={exp.name}>
                                        {exp.name}
                                    </Option>
                                ))}
                            </Select>
                            <Select
                                showSearch
                                placeholder="Select Activity"
                                optionFilterProp="children"
                                onChange={(e) => props.setActivityName(e)}
                                showArrow={true}
                                className="homepage-search-select"
                                suffixIcon={<Icon type="caret-down" style={{ fontSize: '17px', color: '#000000', marginTop: " -1px" }} />}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {activityDList.map((exp, i) => (
                                    <Option className="color-black" value={exp.name}>{exp.name}</Option>
                                ))}
                            </Select>
                            <Button icon="search" id="homepage_search" className="search-btn-homepage" onClick={(e) => props.handleSearch(e)}>Search</Button>
                        </InputGroup>
                    </div>
                    <div className="extra-filters">
                        <div className="extra-parent">
                            <Link href='/all-adventure?view=map'>
                                <a><div className={`extra-child active`} > <span><img src={'/images/newicon/locator_active.svg'} alt="Map Icon" /></span>  Adventures Near Me </div></a>
                            </Link>
                        </div>
                        <div className="extra-parent">
                            <Link href='/experts'>
                                <a>
                                    <div className={`extra-child`} >
                                        <span>
                                            <img src={'/images/home/landingpage/expert.png'} alt="Expert Icon" /></span>
                                        Find an Expert
                                    </div>
                                </a>
                            </Link>
                        </div>
                        <div className="extra-parent">
                            <Link href='/all-adventure?adventure=workshop&isFlexible=true'>
                                <a><div className={`extra-child`} > <span><img src={'/images/home/landingpage/skills.png'} alt="Skills Icon" /></span>  Up Your Skills </div></a>
                            </Link>
                        </div>
                        <div className="extra-parent">
                            <Link href='/all-adventure'>
                                <a><div className={`extra-child`} > <span><img src={'/images/home/landingpage/searchicon.png'} alt="Search Icon" /></span>  Inspire Me </div></a>
                            </Link>
                        </div>
                        {/*<div className="extra-parent">
                            <Link to='/all-adventure?activity=["Scuba-Diving"]&isFlexible=true'>
                                <div className={`extra-child`} > <span><img src={require('assets/images/newicon/expertise/scubadiving.svg')} alt="Map Icon" /></span> {isMaxWidth768 ? 'Diving' : 'Scuba  Diving'}  </div>
                            </Link>
                        </div> */}
                    </div>
                    <div className="parter-section" style={{ display: "none" }}>
                        <img className="onetree-logo" src={'/images/partner/mask3.png'}></img>
                        <img className="leavenotarace-logo" src={'/images/partner/leavenotrace.png'}></img>
                    </div>
                </div>
                <WatchVideoRevealHomepage
                    setIsFullScreenVideo={setIsFullScreenVideo}
                    isFullScreenVideo={isFullScreenVideo}
                />
            </div>
        </div >
    );
};
export default compose(Home);
