import { message } from 'antd';
import * as moment from 'moment';
import Cookies from 'universal-cookie';
// import SkillRed from "../../public/images/skill_red.svg";
// import SkillGreen from "../../public/images/skill_green.svg";
// import SkillOrange from "../../public/images/skill_orange.svg";


// import Skills1 from "../../public/images/newicon/skills1.svg";
// import Skills2 from "../../public/images/newicon/skills2.svg";
// import Skills3 from "../../public/images/newicon/skills3.svg";
// import Skills4 from "../../public/images/newicon/skills4.svg";
import { DifficultyList, SkillsList, PRICE_TYPE, GROUP_TYPE } from "../../src/helpers/constants";
import currencies from './currencies';
import { string } from 'prop-types';

const cookies = new Cookies();

const Skills1 = '/images/newicon/skills1.svg';
const Skills2 = "/images/newicon/skills2.svg";
const Skills3 = "/images/newicon/skills3.svg";
const Skills4 = "/images/newicon/skills4.svg";

const SkillRed = "/images/skill_red.svg";
const SkillGreen = "/images/skill_green.svg";
const SkillOrange = "/images/skill_orange.svg";

export const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
};

export const beforeUpload = (file, size = 20, errorMessage = "Image must smaller than 20MB!") => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png" || file.type === "video/mp4" || file.type === "video/3gp" || file.type === "video/wmv" || file.type === "video/webm" || file.type === "video/flv";
  if (!isJpgOrPng) {
    message.error("You can only upload image and video file!");
  }
  const isLt2M = file.size / 1024 / 1024 < size;
  if (!isLt2M) {
    message.error(errorMessage);
  }
  return isJpgOrPng && isLt2M;
};

export const CaptalizeFirst = (string) => {

  if (typeof string !== "undefined" && string !== "") {
    if (string) {
      string = string.toString();
      return string.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    } else {
      return string;
    }
  } else {
    return string;
  }

}

export const RemoveHttpCom = (string) => {
  if (string === undefined || string === "" || string === null || string === " ") {
    return string;
  }
  const ols = string.replace(/^https?:\/\//, '').replace(/^http?:\/\//, '');
  return ols;
}

export const setDefaultValueForHttp = (string) => {
  if (string === undefined || string === "" || string === null || string === " ") {
    return string;
  }
  const httpType = string.indexOf("https") > -1 ? "https://" : "http://";
  return httpType;
}

export const getApiUrl = () => {
  if (process.env.NEXT_PUBLIC_HOST_ENV === 'staging') {
    return 'https://api.expeditionsconnect.com/api/v1';
    // return process.env.NEXT_PUBLIC_APIBASE_STAGING;
  } else if (process.env.NEXT_PUBLIC_HOST_ENV === 'production') {
    return 'https://api.expeditionsconnect.com/api/v1';
    // return process.env.NEXT_PUBLIC_APIBASE_LIVE;

  }
  // return 'http://ec2-18-192-69-23.eu-central-1.compute.amazonaws.com/api/v1';
  return 'https://api.expeditionsconnect.com/api/v1';
  // return process.env.NEXT_PUBLIC_APIBASE;
}

export const removeLinebreaks = (str) => {
  return str.replace(/[\r\n]+/gm, "<br>");
}

export const removeLinebr = (str) => {
  if (str) {
    return str.replace(/<br ?\/?>/ig, "\n");
  } else {
    return str;
  }
}

export const getExtension = (filename) => {
  const i = filename.lastIndexOf('.');
  return (i < 0) ? '' : filename.substr(i);
};

export const getFilename = (url) => {
  var filename = url.substring(url.lastIndexOf('/') + 1);
  return (filename);
};

export const formatPhoneNumber = (phoneNumberString) => {
  var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
  var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    var intlCode = (match[1] ? '+1 ' : '')
    return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
  }
  return null
}

export const formatDate = (date) => {
  if (date) {
    return moment(date).format("D. MMM YY");
  } else {
    return date;
  }
}

export const isPastDate = (date) => {
  let ans = moment().isBefore(date);
  return ans;
}

export const removeNewLines = (str) => {
  return str.replace(/<p><br><\/p>|<li><br><\/li>/gi, ' ');
}


export const difficultyLevelDisplay = (level) => {
  level = parseInt(level);


  if (level <= 24) {
    return 'Light';
  } else if (level >= 25 && level <= 49) {
    return 'Moderate';
  } else if (level >= 50 && level <= 74) {
    return 'Difficult';
  } else if (level >= 75 && level <= 99) {
    return 'Tough';
  } else if (level >= 100 && level <= 100) {
    return 'All Levels';
  } else {
    return 'Light';
  }
}

export const skillsLevelDisplay = (level) => {
  level = parseInt(level);

  if (level <= 32) {
    return 'Beginner';
  } else if (level >= 33 && level <= 65) {
    return 'Medium';
  } else if (level >= 66 && level <= 99) {
    return 'Advanced';
  } else if (level >= 100 && level <= 100) {
    return 'All Levels';
  } else {
    return 'All';
  }
}


// Paraneters 
// Type - workshop/trip
// level - level of skill and difficlutly
// design - old/new
export const getColorLogoURL = (type, level, design = "old") => {

  if (typeof level !== "undefined") {

    level = level.toString();
    level = level.split(',').map(Number);
    level = Math.max(...level);

    if (type === "trip") {

      if (level <= 24) {
        return design === "new" ? Skills1 : SkillGreen;
      } else if (level >= 25 && level <= 49) {
        return design === "new" ? Skills2 : SkillOrange;
      } else if (level >= 50 && level <= 74) {
        return design === "new" ? Skills3 : SkillRed;
      } else if (level >= 75 && level <= 99) {
        return design === "new" ? Skills4 : SkillRed;
      } else if (level >= 100 && level <= 100) {
        return design === "new" ? Skills4 : SkillRed;
      } else {
        return design === "new" ? Skills1 : SkillGreen;
      }

    } else {

      if (level <= 32) {
        return design === "new" ? Skills1 : SkillGreen;
      } else if (level >= 33 && level <= 65) {
        return design === "new" ? Skills2 : SkillOrange;
      } else if (level >= 66 && level <= 99) {
        return design === "new" ? Skills3 : SkillRed;
      } else if (level >= 100 && level <= 100) {
        return design === "new" ? Skills4 : SkillRed;
      } else {
        return design === "new" ? Skills1 : SkillGreen;
      }
    }
  } else {
    return design === "new" ? Skills1 : SkillGreen;
  }

}

// export const getColorLogoURL = (type, level) => {
//   level = parseInt(level);

//   if (type === "trip") {

//     if (level <= 24) {
//       return SkillGreen;
//     } else if (level >= 25 && level <= 49) {
//       return SkillOrange;
//     } else if (level >= 50 && level <= 74) {
//       return SkillRed
//     } else if (level >= 75 && level <= 99) {
//       return SkillRed;
//     } else if (level >= 100 && level <= 100) {
//       return SkillRed;
//     } else {
//       return SkillGreen;
//     }

//   } else {

//     if (level <= 32) {
//       return SkillGreen;
//     } else if (level >= 33 && level <= 65) {
//       return SkillOrange;
//     } else if (level >= 66 && level <= 99) {
//       return SkillRed
//     } else if (level >= 100 && level <= 100) {
//       return SkillRed;
//     } else {
//       return SkillGreen;
//     }
//   }

// }

export const commaSepratorString = (lang) => {
  let resLang = ""
  if (typeof lang !== "undefined") {
    if (lang.length > 0) {
      lang.map((a, index) => {
        let addCooma = "";
        if (lang.length !== index + 1) {
          addCooma = ", "
        }
        resLang += (a) + addCooma;
      });
    } else {
      resLang = "";
    }
  } else {
    resLang = "";
  }

  return resLang;
}

export const commaSepratorStringSingle = (lang) => {
  let resLang = "";
  if (typeof lang !== "undefined" && lang.length > 0) {
    resLang = lang[0];
  }
  return resLang;
}

export const commaSepratorStringCaps = (lang) => {
  let resLang = ""
  if (lang.length > 0) {
    lang.map((a, index) => {
      let addCooma = "";
      if (lang.length !== index + 1) {
        addCooma = ", "
      }
      resLang += CaptalizeFirst(a) + addCooma;
    });
  }
  return resLang;
}

export const DayorDaysNightOrNights = (type, duration, durationType = 'days') => {

  duration = parseInt(duration);
  if (durationType === 'days') {
    if (type === 'w') {
      if (duration === 1) {
        return 'Day';
      } else {
        return 'Days';
      }
    } else if (type === 't') {
      if (duration === 1) {
        return 'Day';
      } else {
        return 'Days';
      }
    } else {
      return '';
    }
  } else if (durationType === 'hours') {
    if (type === 'w') {
      if (duration === 1) {
        return 'Hour';
      } else {
        return 'Hours';
      }
    } else if (type === 't') {
      if (duration === 1) {
        return 'Hour';
      } else {
        return 'Hours';
      }
    } else {
      return '';
    }
  } else if (durationType === 'weeks') {
    if (type === 'w') {
      if (duration === 1) {
        return 'Week';
      } else {
        return 'Weeks';
      }
    } else if (type === 't') {
      if (duration === 1) {
        return 'week';
      } else {
        return 'Weeks';
      }
    } else {
      return '';
    }
  }

}

export const addSpaceAfterComma = (str) => {
  return str.replace(/,(?=[^\s])/g, ", ");
}

export const displayDifficultyText = (level) => {

  let resLevel = "";
  if (typeof level !== "undefined" && level !== "") {

    level = level.toString();
    level = level.split(',');

    if (level.length > 0) {
      level.map((a, index) => {
        let addCooma = "";
        if (level.length !== index + 1) {
          addCooma = ", "
        }
        let obj = DifficultyList.find(o => o.value === a);
        resLevel += typeof obj !== "undefined" ? obj.name + addCooma : "";
      });

    }
  }
  return resLevel;

}

export const skillLevelText = (level) => {

  let resLevel = "";
  if (typeof level !== "undefined" && level !== "") {
    level = level.split(',');
    if (level.length > 0) {
      level.map((a, index) => {
        let addCooma = "";
        if (level.length !== index + 1) {
          addCooma = ", "
        }
        let obj = SkillsList.find(o => o.value === a);
        resLevel += typeof obj !== "undefined" ? obj.name + addCooma : "";
      });

    }
  }

  return resLevel;

}

export const getCurrencySymbol = (currencyCode, isGetCode = false) => {
  if (isGetCode) {
    const currency = currencies.find(currency => currency.currencyCode === currencyCode);
    return currency.currencySymbol + currency.currencyCode;
  } else {
    const currency = currencies.find(currency => currency.currencyCode === currencyCode);
    return currency.currencySymbol;
  }
}

export const getCityFromLocation = (location) => {


  let city = "";

  if (typeof location !== "undefined" && location !== "") {
    location = location.split(',');

    if (location.length > 0) {
      city = OnlyFirstCaptalizeFirst(location[0]) + ", ";
    }
  }
  return city;

}




export const OnlyFirstCaptalizeFirst = (str) => {

  if (typeof str !== "undefined" && str.trim() !== "") {
    str = str.split(" ");
    for (let i = 0, x = str.length; i < x; i++) {
      if (typeof str[i][0] !== "undefined" && str[i][0] !== "") {
        str[i] = str[i][0].toUpperCase() + str[i].substr(1);
      } else {
        str[i] = str[i].substr(1);
      }
    }
    return str.join(" ");
  } else {
    return str;
  }

}
export const setPreferredCurrency = (currencyCode) => {
  cookies.set('preferredCurrency', currencyCode);
}

export const getPreferredCurrency = () => {
  return cookies.get('preferredCurrency') || 'USD';
}

export const getCurrencies = (isPopular = false) => {
  let allCurrencies = currencies.filter((currency) => !currency.isDisabled);
  if (isPopular) {
    allCurrencies = allCurrencies.filter((currency) => currency.isPopular);
  }

  allCurrencies = allCurrencies.reduce((result, currency) => {
    return {
      ...result,
      [currency.currencyCode]: {
        currencyCode: currency.currencyCode,
        currencyName: currency.currencyName,
        currencySymbol: currency.currencySymbol || currency.currencyCode,
      },
    };
  }, {});
  return Object.keys(allCurrencies)
    .map((currencyCode) => allCurrencies[currencyCode])
    .sort((cur1, cur2) => {
      if (cur1.currencyName < cur2.currencyName) {
        return -1;
      }
      if (cur1.currencyName > cur2.currencyName) {
        return 1;
      }
      return 0;
    });
};


export const getLikeStatus = (allLikes, userId, type) => {
  let likeStatus = false;
  if (typeof allLikes !== "undefined" && allLikes.length > 0) {
    allLikes.forEach((like) => {
      if (like.userId === userId || likeStatus) {
        likeStatus = true;
      }
    });
  }
  if (typeof type !== "undefined" && type === 'like') {
    likeStatus = true;
  } else if (typeof type !== "undefined" && type === 'dislike') {
    likeStatus = false;
  }
  return likeStatus;
}

// Function will return the Price Type
export const getPriceType = (type) => {
  if (typeof type !== "undefined" && type !== "") {
    let obj = PRICE_TYPE.find(o => o.value == type);
    return obj.name;
  } else {
    return "Per Person";
  }
}
// Function will return the Price Type
export const getGroupSize = (size, type) => {
  let label = `Up to ${size} ${(size === 1 || size === "1") ? 'Person' : 'People'}`;
  if (typeof type !== "undefined" && type !== "") {
    let obj = GROUP_TYPE.find(o => o.value == type);
    label = `${obj.name} ${size} ${(size === 1 || size === "1") ? 'Person' : 'People'}`;
  }
  return label;
}


// Function will return price after calculating discount
export const getPriceAfterDiscount = (basePrice, discount) => {

  if (typeof discount !== "undefined" && typeof discount !== "" && discount !== "null") {
    let discountAmt = basePrice * discount / 100;
    return Math.round(basePrice - discountAmt);
  } else {
    return basePrice;
  }

}

export const returnTheRating = (currentRating) => {
  if (typeof currentRating !== "undefined" && currentRating !== "") {
    let afterRound = Math.round(currentRating);
    if (afterRound < currentRating) {
      currentRating = afterRound + 0.5;
    } else {
      currentRating = Number.isInteger(currentRating) ? currentRating : afterRound - 0.5;
    }
    return currentRating;
  } else {
    return currentRating;
  }

}

export const getSeasonText = (season) => {
  if (typeof season !== "undefined" && season !== "" && season !== null) {
    return season;
  } else {
    return "";
  }

}