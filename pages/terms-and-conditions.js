import React from 'react';
import TermsAndConditions from '../src/containers/TermsAndConditions';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const TermsAndConditionsInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <TermsAndConditions />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default TermsAndConditionsInApp;