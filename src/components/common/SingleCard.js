import React, { Fragment } from 'react';
// import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { Row, Col, Rate, Card, Popover, Dropdown, Button, Icon } from 'antd';
// import Image from 'next/image'

/**
 * App Imports
 */
import { CaptalizeFirst, getColorLogoURL, DayorDaysNightOrNights, displayDifficultyText, skillLevelText, getCurrencySymbol, getPriceAfterDiscount, commaSepratorStringSingle } from "../../helpers/methods";
// import MoreTripOption from "../../../public/images/more_vert-24px.svg";
import LikeAndShare from "./LikeandshareSingle";
import ReactHtmlParser from "react-html-parser";


const ApiKey = process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY;


const SingleCard = (props) => {
    const { t, view, index, type, onMouseEnter, onMouseLeave, closeCard, isLogin, workshopsOption, tripsOption, isPublicView } = props;


    const onTripClick = (type, id) => {
        if (type == "trip"  && typeof window !== "undefined" ) {
            if (isPublicView !== undefined && !isPublicView) {
                window.open(`/trip-edit/${id}`, "_blank");
            } else {
                window.open(`/trips-details/${id}`, "_blank");
            }
        } else {
            if (isPublicView !== undefined && !isPublicView) {
                window.open(`/learning-edit/${id}`, "_blank");
            } else {
                window.open(`/learning-details/${id}`, "_blank");
            }
        }
    };

    const handleReviewRedirect = (e) => {
        e.stopPropagation();
        if (t.pageType === "trip") {
            if (isPublicView !== undefined && !isPublicView) {
                window.open(`/trip-edit/${t.id}?review=true`, "_blank");
            } else {
                window.open(`/trips-details/${t.id}?review=true`, "_blank");
            }
        } else {
            if (isPublicView !== undefined && !isPublicView) {
                window.open(`/learning-edit/${t.id}?review=true`, "_blank");
            } else {
                window.open(`/learning-details/${t.id}?review=true`, "_blank");
            }
        }

    }

    const returnTheRating = (currentRating) => {
        if (typeof currentRating !== "undefined" && currentRating !== "") {
            let afterRound = Math.round(currentRating);
            if (afterRound < currentRating) {
                currentRating = afterRound + 0.5;
            } else {
                currentRating = Number.isInteger(currentRating) ? currentRating : afterRound - 0.5;
            }
            return currentRating;
        } else {
            return currentRating;
        }

    }

    return (
        <Fragment>
            <Card
                key={"card_" + index}
                hoverable
                onMouseEnter={() => type == 'mapCard' ? onMouseEnter(t.id) : ''}
                onMouseLeave={() => type == 'mapCard' ? onMouseLeave(t.id) : ''}
                cover={<>
                        <img 
                            onClick={() => t.pageType === "trip" ? onTripClick("trip", t.id) : onTripClick("workshop", t.id)}
                            alt="example"
                            src={t.cover}
                            // width={2400}
                            // height={1598} 
                        />
                    
                    <div className="image_text_set_on_card">
                        <div className="an-13 card-1st-line">
                            <span className="duration-lowercase">{t.duration ? t.duration : ""} {DayorDaysNightOrNights('t', t.duration ? t.duration : "", t.durationType)}</span>
                            <span className="secondLine">{((typeof t.country !== "undefined" && t.country !== "undefined") && t.country !== "") ? CaptalizeFirst(t.country) : CaptalizeFirst(t.medium)}<span className="secondLine"></span></span>
                            <span>{typeof t.activity !== "undefined" && t.activity.length > 0 ? commaSepratorStringSingle(t.activity) : ""}</span>
                        </div>
                    </div>
                </>}
            >
                {typeof t.medium !== "undefined" && t.medium === "online" && (
                    <span className="card_tag an-10 medium-text">
                        {CaptalizeFirst(typeof t.medium !== "undefined" ? t.medium : t.medium)}{" "}
                    </span>
                )}
                {isLogin && !isPublicView && type === "expertAdventureCard" ? <></> :
                    <>
                        {
                            typeof t.discount !== "undefined" && t.discount !== "" && t.discount !== null && (
                                <span className="discount_tag an-10 medium-text">
                                    <span>-{t.discount}%</span>
                                </span>
                            )
                        }
                    </>
                }
                {isLogin && !isPublicView && type === "expertAdventureCard" &&
                    <>
                        <span className="edit_card_tag an-10 medium-text">
                            <Popover
                                placement="bottom"
                                content={t.pageType === "trip" ? tripsOption(t._doc ? t._doc : t) : workshopsOption(t)}
                                trigger="hover">
                                <div className="more_icon_upcoming_tips">
                                    <img
                                        src={'/images/more_vert-24px.svg'} alt="MoreTrip_Option"
                                    />
                                </div>
                            </Popover>
                        </span>
                        {t.active === false &&
                            <div className="inactive_online_button">
                                <span href="#!" className="inactive edit_btn medium-text an-14 pull-right mt30">
                                    Deactive
                                </span>
                            </div>
                        }
                    </>
                }
                {type === "googleMapCard" &&
                    <span className="card_close_tag an-10 medium-text">
                        <Icon type="close-circle" onClick={() => closeCard()} />
                    </span>
                }
                <div className="" onClick={() => t.pageType === "trip" ? onTripClick("trip", t.id) : onTripClick("workshop", t.id)}>

                    {/* On Card Like, Share and Save section */}
                    {/* {type !== "googleMapCard" &&
                        <div className="like-and-share-card">
                            <LikeAndShare allLikes={t.likes} id={t.id} pageType={t.pageType} designType="directoryPage" />
                        </div>
                    } */}

                    {/* title of trip or workshop */}
                    <p className="mb10 an-15 medium-text card-main-title">
                        {CaptalizeFirst(t._doc ? t._doc.title : t.title)}
                    </p>

                    {type !== "googleMapCard" &&
                        <p className="mb15 an-15 medium-text card-main-description">
                            {ReactHtmlParser(typeof t.description !== "undefined" && t.description !== "" ? t.description.replace(/<[^>]+>/g, '') : "")}
                        </p>
                    }
                    <Row className="price_line">
                        <Col xs={18} sm={18} md={18} lg={21} xl={21}>
                            <h3 className="an-16 price-tag">
                                <span className="price_txt">From &nbsp;{getCurrencySymbol(t._doc ? t._doc.priceCurrency : t.priceCurrency)}</span>
                                <span className="price_txt">{getPriceAfterDiscount(t._doc ? t._doc.price : t.price, t.discount)}</span>
                                {t.discount !== undefined && t.discount !== "" && t.discount !== null &&
                                    <span className="card_discount_price">&nbsp;{getCurrencySymbol(t._doc ? t._doc.priceCurrency : t.priceCurrency)}&nbsp;{t._doc ? t._doc.price : t.price}</span>
                                }
                            </h3>
                            {typeof t.review !== "undefined" ?
                                <>
                                    <div onClick={(e) => handleReviewRedirect(e)}>
                                        <Popover placement="bottom" content={t.review.reviewRating} trigger="hover" overlayClassName="card-title-hover">
                                            <Rate
                                                allowHalf
                                                disabled
                                                defaultValue={returnTheRating(t.review.reviewRating)}
                                                className="an-14"
                                                style={{ color: "#FFBC00" }}
                                            />
                                            <span className="review-text">{t.review.reviewLength === 0 ? `No Review` : t.review.reviewLength === 1 ? `${t.review.reviewLength + ' Review'}` : `${t.review.reviewLength} Reviews`}</span>
                                        </Popover>
                                    </div>

                                </> :
                                <>
                                    <Rate
                                        allowHalf
                                        disabled
                                        defaultValue={0}
                                        className="an-14"
                                        style={{ color: "#FFBC00" }}
                                    />
                                    <span className="review-text">No Review</span>
                                </>
                            }
                        </Col>

                        {/* Trip and workshop difficulty and skill level logos */}
                        {type !== "googleMapCard" &&
                            <Col xs={6} sm={6} md={6} lg={3} xl={3}>
                                <div className="text-right">
                                    <Popover placement="bottom" content={`${t.pageType == "trip" ? displayDifficultyText(typeof t.difficulty !== "undefined" ? t.difficulty : "") : skillLevelText(typeof t.skill !== "undefined" ? t.skill : "")}`} trigger="hover">
                                        <img
                                            src={t.pageType == "trip" ? getColorLogoURL("trip", typeof t.difficulty !== "undefined" ? t.difficulty : "", 'new') : getColorLogoURL("workshop", typeof t.skill !== "undefined" ? t.skill : "", 'new')}
                                            alt="Skil level" className="skill-level-img"
                                        />
                                    </Popover>
                                </div>
                            </Col>
                        }
                    </Row>
                </div>

            </Card>
        </Fragment >
    )

}

export default compose(SingleCard)
