import { ENTHU_STEP_1, ENTHU_STEP_2, ENTU_TAB_REQ } from "../../helpers/actions";

const initState = {
  id: null,
  picture: null,
  originalPicture:null,
  name: "",
  country: null,
  address: "",
  phone: "",
  interested: [],
  dob: null,
  firstName: "",
  lastName: "",
  city: "",
  activity: [],
  speaks: [],
  aboutme: "",
  cover: null,
  originalCover:null,
  active: true,
  step1: false,
  step2: false,
  loader: false,
  tab: 1,
  isEditMode: false,
}

function enthu(state = initState, action) {
  switch (action.type) {
    case ENTHU_STEP_1:
      return {
        ...state,
        name: action.data.name,
        firstName: action.data.firstName,
        lastName: action.data.lastName,
        city: action.data.city,
        speaks: action.data.speaks,
        country: action.data.country,
        phone: action.data.phone,
        dob: action.data.dob,
        picture: action.data.picture,
        originalPicture: action.data.originalPicture,
        address: action.data.address,
        step1: true,
        tab: 2
      }
    case ENTHU_STEP_2:
      return {
        ...state,
        loader: true,
      }
    case "STOP_LOADER":
      return {
        ...state,
        loader: false,
      }
    case "SAVE_ENTHU_PROFILE":
      
      return {
        ...state,
        picture: action.profile,
        originalPicture: action.originalProfile,
        name: action.firstName,
        country: action.country,
        address: action.address,
        phone: action.phone,
        dob: action.dob,
        id: action.user,
        cover: action.cover,
        originalCover: action.originalCover,
        interested: action.interested,
        firstName: action.firstName,
        lastName: action.lastName,
        city: action.city,
        aboutme: action.aboutme,
        speaks: action.speaks,
        activity: action.activity,
        tab: 1,
        step1: true,
        step2: true,
      }
    case "ENTHU_EDIT_MODE":
      return {
        ...state,
        isEditMode: true,
      }
    case ENTU_TAB_REQ:
      return {
        ...state,
        tab: action.data,
      }
    case "UPDATE_ENTHU" :
      return {
        ...state,
        loader: true
      }
    case "UPDATE_ENTHU_COVER":
      return {
        ...state,
        cover: action.cover,
        originalCover: action.originalCover
      }
    case "NULL_ENTH":
      return {
        id: null,
        picture: null,
        originalPicture:null,
        name: "",
        country: null,
        address: "",
        phone: "",
        interested: [],
        dob: null,
        firstName: "",
        lastName: "",
        city: "",
        activity: [],
        speaks: [],
        aboutme: "",
        cover: null,
        originalCover:null,
        active: true,
        step1: false,
        step2: false,
        loader: false,
        tab: 1,
        isEditMode: false,
      }
    default:
      return state;
  }
}

export default enthu;