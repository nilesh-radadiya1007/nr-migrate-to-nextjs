import React, { useEffect, useRef, Fragment } from "react";
import { useSelector } from "react-redux";

/**
 * App Imports
 */
import { Logout } from "../services/expert";

import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";

const BaseLayout = () => {
  const enthu = useSelector((state) => state.enthu);
  const expert = useSelector((state) => state.expert);
  const { role, accessToken } = useSelector((state) => state.auth);
  const prevExpert = useRef();
  const prevEnthu = useRef();

  useEffect(() => {
    prevExpert.current = expert;
    prevEnthu.current = enthu;
  }, [expert, enthu]);

  if (!accessToken) {
    if (prevExpert.current) Logout(prevExpert.current.id);
    if (prevEnthu.current) Logout(prevEnthu.current.id);
  }

  return (
    <Fragment>
      <section className="home-layout">
        <header className="top-header">
          <Header />
        </header>
        <main className="main-content">
          
        </main>
        <footer className="footer">
          <Footer />
        </footer>
      </section>
    </Fragment>
  );
};

export default BaseLayout;
