import React, { useEffect, useCallback } from "react";
import s from "./WatchVideoReveal.module.scss";
import { compose } from "redux";
import ReactPlayer from "react-player";
import { Button } from "antd";
import { useState } from "react";
import cx from "classnames";
import { useMediaQuery } from "react-responsive";
const playButtonIcon = "/images/home/playButtonIcon.svg";
const watchVideoBackground = "/images/home/watchVideoBackground.svg";


const WatchVideoReveal = (props) => {
  const [isFullScreen, setisFullScreen] = useState(false);
  const isMaxWidth1200 = useMediaQuery({ query: "(max-width: 1200px)" });

  const setFullScreen = useCallback(() => {
    setisFullScreen(true);
    props.setIsFullScreenVideo(true);
    const element = document.getElementsByClassName("header-container1")[0];
    element.style.background = "rgba(255, 255, 255, 0.6)";
  });

  useEffect(() => {
    // setTimeout(() => {
    //   // const element = document.getElementsByClassName("header-container1")[0];
    //   // element.style.background = "rgba(255, 255, 255, 0.6)";
    //   // setFullScreen();
    // }, 2000);
     const element = document.getElementsByClassName("header-container1")[0];
      element.style.background = "rgba(255, 255, 255, 0.0)";
  }, []);

  useEffect(() => {
    if (props.isFullScreenVideo) {
      setFullScreen();
    }
  }, [props.isFullScreenVideo, setFullScreen]);

  return (
    <>
      <div
        className={cx(s.WatchVideoReveal, {
          [s.fullScreen]: isFullScreen,
        })}
      >
        <ReactPlayer
          url="./home-video.mp4"
          loop
          playing={isFullScreen}
          muted
          className={s.ReactPlayer}
        />
        <div className={s.Mask}></div>
      </div>
      {!isFullScreen && !isMaxWidth1200 && (
        <div className={s.WatchVideoButtonContainer}>
          <img
            src={watchVideoBackground}
            alt=""
            className={s.WatchVideoButtonContainer__BackgroundImage}
          />
          <Button className={s.WatchVideoButton} onClick={setFullScreen}>
            Watch Video
            <img
              src={playButtonIcon}
              alt=""
              className={s.WatchVideoButton_Image}
            />
          </Button>
        </div>
      )}
    </>
  );
};

export default compose(WatchVideoReveal);
