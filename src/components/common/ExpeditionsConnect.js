import React from 'react';

export default function ExpeditionsConnect(props) {
    // const classs = props?.className ?`mb20 why-choose-us ${props.className}`:'mb20 why-choose-us';
    return (
        <div className={props?.className ?`mb20 why-choose-us ${props.className}`:'mb20 why-choose-us'}>
            <h3 className="why-choose-main-title mb70">Why Choose <span className="color-yellow">Expeditions Connect</span></h3>
            <div>
                <div className="why-choose-main">
                    <div className="why-choose-icons-wrapper">
                        <img src={'/images/newicon/why1.svg'} alt="Why choose" />
                    </div>
                    <div className="why-title">World Class Experts</div>
                    <div className="border-bottom-yellow"></div>
                    <div className="why-choose-txt">Discover top adventure experts and mentors from around the globe, all under one roof.</div>
                </div>
                <div className="why-choose-main">
                    <div className="why-choose-icons-wrapper">
                        <img src={'/images/newicon/why2.svg'} alt="Why choose" />
                    </div>
                    <div className="why-title">Direct Connection</div>
                    <div className="border-bottom-yellow"></div>
                    <div className="why-choose-txt">No middleman, no hassle. Direct connection with the best professionals in your chosen field.</div>
                </div>
                <div className="why-choose-main">
                    <div className="why-choose-icons-wrapper">
                        <img src={'/images/newicon/why3.svg'} alt="Why choose" />
                    </div>
                    <div className="why-title">More Than Just Adventures</div>
                    <div className="border-bottom-yellow"></div>
                    <div className="why-choose-txt">Access a wide range of workshops, courses and a whole host of learning opportunities directly crafted by experts.</div>
                </div>
                <div className="why-choose-main">
                    <div className="why-choose-icons-wrapper">
                        <img src={'/images/newicon/why4.svg'} alt="Why choose" />
                    </div>
                    <div className="why-title">100% Free to Join</div>
                    <div className="border-bottom-yellow"></div>
                    <div className="why-choose-txt">It’s totally free to join and explore our community. You only pay when you book an experience or activity.</div>
                </div>
            </div>
        </div>
    )
}