import { Dropdown, Input, Menu, Button } from "antd";

import cx from "classnames";
import ActivityCardSlider from "../components/Basic/ActivityCardSlider";
import WatchVideoReveal from "../components/Basic/WatchVideoReveal";
import React, { useState } from "react";
import Link from "next/link";
import TextLoop from "react-text-loop";
import { compose } from "redux";
import { Subscribe } from '../services/expert';
import NewsletterModal from '../components/common/NewsletterModal';
import Newsletter from '../components/common/NewsLetter';

/**
 * Auth Modal
 */
import AuthModal from "../components/Auth/AuthModal";
import { useMediaQuery } from "react-responsive";
import { useRouter } from "next/router";

const beMoreConnectedImage = "/images/home/beMoreConnectedImage.png";
const beMoreInspiredImage = "/images/home/beMoreInspiredImage.png";
const beMoreSustainableImage = "/images/home/beMoreSustainableImage.png";
const callingAllExpertsBackground = "/images/home/callingAllExpertsBackground.svg";
const callingOurExperts_Image1 = "/images/home/callingOurExperts_Image1.png";
const callingOurExperts_Image2 = "/images/home/callingOurExperts_Image2.png";
const callingOurExperts_Image3 = "/images/home/callingOurExperts_Image3.png";
const exploreWithExperts_Image1 = "/images/home/exploreWithExperts_Image1.png";
const exploreWithExperts_Image2 = "/images/home/exploreWithExperts_Image2.png";
const exploreWithExperts_Image3 = "/images/home/exploreWithExperts_Image3.png";
const exploreWithExperts_Image4 = "/images/home/exploreWithExperts_Image4.png";
const joinTheExpeditionBackground = "/images/home/joinTheExpeditionBackground.svg";
const joinTheExpedition_compas = "/images/home/joinTheExpedition_compas.png";
const joinTheExpedition_flashlight = "/images/home/joinTheExpedition_flashlight.png";
const joinTheExpedition_Hummer = "/images/home/joinTheExpedition_Hummer.png";
const learningWithExperts_feature1 = "/images/home/learningWithExperts_feature1.gif";
const learningWithExperts_feature2 = "/images/home/learningWithExperts_feature2.gif";
const learningWithExperts_feature3 = "/images/home/learningWithExperts_feature3.gif";
const learningWithExperts_feature4 = "/images/home/learningWithExperts_feature4.gif";
const learningWithExperts_feature5 = "/images/home/learningWithExperts_feature5.gif";
const learningWithExperts_feature6 = "/images/home/learningWithExperts_feature6.gif";
const locationPinIcon = "/images/home/locationPinIcon.svg";
const quoteCloseIcon = "/images/home/quoteCloseIcon.svg";
const quoteOpenIcon = "/images/home/quoteOpenIcon.svg";
const trackingImage = "/images/home/trackingImage.png";
const whatFacinatesYouImage2 = "/images/home/whatFacinatesYouImage2.png";
const whatFacinatesYouImage3 = "/images/home/whatFacinatesYouImage3.png";
const whatFacinatesYouImage4 = "/images/home/whatFacinatesYouImage4.png";
const watchVideoIconMobile = "/images/home/watchVideoIconMobile.svg";

const allCategoriesCardsData = [
  {
    days: 5,
    activityType: "Climbing & Trekking",
    activityName: "Alpine Climbing Course",
    location: "Chamonix, France",
    price: 1800,
    priceCurrency: "USD",
    activityLevel: "easy",
    rating: 5,
    image: whatFacinatesYouImage3,
  },
  {
    days: 3,
    activityType: "Nature & Wildlife",
    activityName: "Wildlife Photography Workshop",
    location: "Scotland",
    price: 2000,
    priceCurrency: "USD",
    activityLevel: "medium",
    rating: 5,
    image: whatFacinatesYouImage2,
    label: "Online",
  },
  {
    days: 1,
    activityType: "Climbing & Trekking",
    activityName: "Avalanche Safety Course",
    location: "Online",
    price: 150,
    priceCurrency: "USD",
    activityLevel: "difficult",
    rating: 5,
    image: trackingImage,
    label: "Online",
  },
  {
    days: 3,
    activityType: "Diving & Snorkelling",
    activityName: "Learn Open Water Diving",
    location: "Malta",
    price: 500,
    priceCurrency: "USD",
    activityLevel: "easy",
    rating: 5,
    image: whatFacinatesYouImage4,
  },
];

const exploreWithExpertsData = [
  {
    days: 10,
    activityType: "Climbing & Trekking",
    activityName: "Everest Basecamp Trek",
    location: "Nepal",
    price: 7500,
    priceCurrency: "USD",
    activityLevel: "easy",
    rating: 5,
    image: exploreWithExperts_Image1,
  },
  {
    days: 6,
    activityType: "Climbing & Trekking",
    activityName: "Kilimanjaro Climbing Expedition",
    location: "Tanzania",
    price: 2750,
    priceCurrency: "USD",
    activityLevel: "medium",
    rating: 5,
    image: exploreWithExperts_Image2,
  },
  {
    days: 7,
    activityType: "Nature & Wildlife",
    activityName: "The Great Migration Photo Safari",
    location: "Kenya",
    price: 4500,
    priceCurrency: "USD",
    activityLevel: "difficult",
    rating: 5,
    image: exploreWithExperts_Image3,
  },
  {
    days: 8,
    activityType: "Nature & Wildlife",
    activityName: "Birding in Costa Rica",
    location: "Costa Rica",
    price: 2600,
    priceCurrency: "USD",
    activityLevel: "easy",
    rating: 5,
    image: exploreWithExperts_Image4,
  },
];

const Home = (props) => {
  const router = useRouter();
  const [showLogin, setShowLogin] = useState(false);
  const openSignup = () => setShowLogin(true);
  const [isFullScreenVideo, setIsFullScreenVideo] = useState(false);
  const [isShowMoreCategories, setIsShowMoreCategories] = useState(false);
  const isMaxWidth1200 = useMediaQuery({ query: "(max-width: 1200px)" });
  const isMaxWidth992 = useMediaQuery({ query: "(max-width: 992px)" });
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [successPopup, setSuccessPopup] = useState(false);


  const handleSubmit = async e => {
    e.preventDefault();
    let valid = false;

    let val = document.getElementById('email-input-box').value;
    let element = document.getElementById('subscribe-error');
    document.getElementById('subscribe-success').style.display = "none";

    if (email.trim() === "") {
      element.style.display = "block";
    } else if (email.trim() !== "" && !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(val)) {
      element.innerHTML = 'Please enter valid email address';
      element.style.display = "block";
      valid = false;
    } else {
      element.style.display = "none";
      valid = true;
    }

    if (valid) {
      setIsLoading(true);
      const formData = new FormData();
      formData.append("email", val);
      formData.append("subscribe", true);
      const result = await Subscribe(formData);
      setEmail('');
      if (result.data.status === "SUCCESS") {
        setSuccessPopup(true);
      } else if (result.data.status === "FAIL") {
        element.innerHTML = "This email already exists. Please enter a new email address.";
        element.style.display = "block";
        setTimeout(() => {
          element.style.display = "none";
        }, 10000);
      }

      setIsLoading(false);
    }

  };

  const handleChange = e => {
    setEmail(e.target.value);
    document.getElementById('subscribe-success').style.display = "none";
    if (e.target.value === "") {
      document.getElementById('subscribe-error').style.display = "block";
    } else {
      document.getElementById('subscribe-error').style.display = "none";
    }
  }

  return (
    <div className="home">
      <div className="section1">
        <div
          className={cx("content", {
            "is-full-screen-video": isFullScreenVideo,
          })}
        >
          <div className="bold-text font-size-96">
            <div>BE MORE</div>
            <TextLoop
              className="text-loop"
              interval={2000}
              springConfig={{ stiffness: 540, damping: 30 }}
            >
              <span style={{ backgroundColor: "#42c58a" }}>CONNECTED</span>
              <span style={{ backgroundColor: "#4ec7f3" }}>Inspired</span>
              <span style={{ backgroundColor: "#ffba00" }} className="abcd">
                Sustainable
              </span>
            </TextLoop>
            <div className="with-us-text">WITH US!</div>
          </div>
          <div className="get-started font-size-26">
            <div>
              Join the global skill-sharing community for adventure experts
              and like-minded learners, and make a positive impact on the planet.

            </div>
            <button
              onClick={openSignup}
              className="ex__primary_btn landing-get-started"
            >
              GET STARTED for FREE
            </button>
            {isMaxWidth1200 && !isFullScreenVideo && (
              <button
                className="watchVideoMobileButton"
                onClick={() => setIsFullScreenVideo(true)}
              >
                Watch Video
                <img src={watchVideoIconMobile} alt="" />
              </button>
            )}
          </div>
        </div>
        <WatchVideoReveal
          setIsFullScreenVideo={setIsFullScreenVideo}
          isFullScreenVideo={isFullScreenVideo}
        />
      </div>

      <div className="section2">
        <h2 className="rubik-54-medium header">Find What Fascinates You</h2>
        <div className="categories">
          <div className="active category all-category">All categories</div>
          <div className="other-categories">
            <div className="first-row">
              <Link href="/learning">
                <a className="category">Climbing and Trekking</a>
              </Link>
              <Link href="/learning">
                <a className="category">Nature and Wildlife</a>
              </Link>
              <Link href="/learning">
                <a className="category">Scuba-Diving and Snorkelling</a>
              </Link>
              {!isMaxWidth992 && (
                <div className="see-more-button-container">
                  <Button
                    className="category"
                    onClick={() => {
                      setIsShowMoreCategories(!isShowMoreCategories);
                    }}
                  >
                    ....
                  </Button>
                </div>
              )}

              {isMaxWidth992 && (
                <Link href="/learning">
                  <a className="category">Skiing and Snowboarding</a>
                </Link>
              )}
            </div>
            {isShowMoreCategories && (
              <Link href="/learning">
                <a className="category">Skiing and Snowboarding</a>
              </Link>
            )}
          </div>

          {/* <Dropdown
            overlay={
              <Menu>
                <Menu.Item key="0">
                  <a href="http://www.alipay.com/">Skiing and Snowboarding</a>
                </Menu.Item>
              </Menu>
            }
            trigger={["click"]}
            className="category more-menu"
          >
            <p>....</p>
          </Dropdown> */}
        </div>
        <ActivityCardSlider
          className="activity-cards"
          data={allCategoriesCardsData}
          cardRedirectURL="/learning"
        />
        <button
          className="ex__primary_btn "
          onClick={() => router.push("/learning")}
        >
          SEE ALL LEARNINGS
        </button>
      </div>

      {/* Join the Expedition */}
      <div className="join-the-expedition">
        <img src={joinTheExpeditionBackground} alt="" className="bgImage" />
        <div className="join-the-expedition-content">
          <h1 className="header rubik-54-medium">Join the Expedition</h1>
          <div className="our-aim">
            {/* Images */}
            <div className="our-aim-images">
              <img
                src={joinTheExpedition_flashlight}
                alt=""
                className="flashlight"
              />
              <img src={joinTheExpedition_Hummer} alt="" className="hammer" />
            </div>

            {/* Text */}
            <p className="our-aim-text">
              <img
                src={joinTheExpedition_compas}
                alt=""
                className="compassImage"
              />
              <img src={quoteOpenIcon} alt="" className="quoteOpen" />
              <img src={quoteCloseIcon} alt="" className="quoteClose" />
              <b>Our aim</b> is to connect enthusiasts with expert adventurers.
              <br />
              <br />
              We are on a mission to create a sustainable future for our planet
              by creating a community of like-minded passionate explorers - a
              place where those seeking their wanderlust can learn from
              experienced professionals.
            </p>
          </div>
        </div>
      </div>

      {/* Be more sections */}
      <div className="be-more-sections">
        {/* Be more connected */}
        <div className="be-more-section">
          <div className="be-more-section-body">
            <div className="be-more-content">
              <div>
                <div className="be-more-header rubik-54-medium">
                  <div>Be More</div>
                  <div>CONNECTED</div>
                </div>
                <div className="be-more-summary">
                  Discover and connect with skilled
                  professionals worldwide
                </div>
                <div className="be-more-description">
                  As part of the community you’ll be able to connect with adventure
                  professionals anywhere in the world. You can also share your passion
                  for adventure with fellow explorers from across the globe.
                </div>
                <button
                  className="ex__primary_btn be-more-button"
                  onClick={() => router.push("/experts")}
                >
                  BROWSE EXPERTS
                </button>
              </div>
            </div>
            <div className="be-more-image-container">
              <img src={beMoreConnectedImage} alt="" />
            </div>
          </div>
          <div className="be-more-section-footer">
            <div className="dash" />
            <div className="text">
              Ignite your wanderlust and learn from passionate and experienced professionals
              in your area of interest. Whatever your level or ability, they’ll help you reach your adventure goals through personal experiences.
            </div>
          </div>
        </div>

        {/* Be more Inspired */}
        <div className="be-more-section be-more-inspired">
          <div className="be-more-section-body">
            <div className="be-more-image-container-left">
              <img src={beMoreInspiredImage} alt="" />
            </div>

            <div className="be-more-content">
              <div>
                <div className="be-more-header rubik-54-medium">
                  <div>Be More</div>
                  <div>INSPIRED</div>
                </div>
                <div className="be-more-summary">
                  Explore real-life adventures organized
                  and led by true field experts
                </div>
                <div className="be-more-description">
                  Whether you’re a beginner or a seasoned explorer, get access to
                  a huge pool of learning resources directly crafted by field experts in a variety of adventure activities.
                </div>
                <button
                  className="ex__primary_btn be-more-button"
                  onClick={() => router.push("/learning")}
                >
                  BROWSE OFFERINGS
                </button>
              </div>
            </div>
          </div>
          <div className="be-more-section-footer">
            <div className="dash" />
            <div className="text">
              Find qualified experts to guide you on your next expedition.
              Bypass mass-market tours and activities sold by agents.
              Discover adventures you can’t find anywhere else.

            </div>
          </div>
        </div>

        {/* Be more Sustainable */}
        <div className="be-more-section be-more-sustainable">
          <div className="be-more-section-body">
            <div className="be-more-content">
              <div>
                <div className="be-more-header rubik-54-medium">
                  <div>Be More</div>
                  <div>SUSTAINABLE</div>
                </div>
                <div className="be-more-summary">
                  Enjoy extraordinary experiences while
                  leaving a positive environmental impact
                </div>
                <div className="be-more-description">
                  Our community helps to bring like-minded adventurers
                  closer to the environment and each other. Together we
                  can enjoy extraordinary learning experiences while furthering a sustainable future.
                </div>
                <button className="ex__primary_btn be-more-button">
                  OUR STORY
                </button>
              </div>
            </div>
            <div className="be-more-image-container">
              <img src={beMoreSustainableImage} alt="" />
            </div>
          </div>
        </div>

        {/* Learning with experts */}
        <div className="learning-with-experts">
          <img src={joinTheExpeditionBackground} alt="" className="bgImage" />
          <div className="learning-with-experts-body">
            <h1 className="header rubik-54-medium">Learning with Experts</h1>
            <p className="description">
              Broaden your knowledge and hone in on your passions with our community
              of international experts. Take your passion for adventure to the next level.
            </p>
            <div className="card">
              <div className="point">
                <div className="card-header">
                  <img src={locationPinIcon} alt="" />
                  Online resources
                </div>
                <div className="card-description">
                  Whatever your area of interest may be, with Expeditions
                  Connect you’ll find online courses, workshops, webinars, and
                  many other learning materials to broaden your horizons. Always
                  carefully crafted and thought-out by experts.
                </div>
              </div>
              <div className="point">
                <div className="card-header">
                  <img src={locationPinIcon} alt="" />
                  On the ground
                </div>
                <div className="card-description">
                  Learn through experience with real life courses, workshops, and
                  expeditions in incredible locations across the world. You can also
                  volunteer in social and ecological programs, undertake an internship
                  to gain hands-on knowledge, or even join the crew of an inspiring expert.
                </div>
              </div>
            </div>

            <div className="features">
              <div className="features-row1">
                <div className="feature">
                  <img src={learningWithExperts_feature1} alt="" />
                  <h3>Courses</h3>
                </div>
                <div className="feature">
                  <img src={learningWithExperts_feature2} alt="" />
                  <h3>Workshops</h3>
                </div>
                {!isMaxWidth768 && (
                  <div className="feature">
                    <img src={learningWithExperts_feature3} alt="" />
                    <h3>Learning expeditions</h3>
                  </div>
                )}
              </div>
              {isMaxWidth768 && (
                <div className="features-row1">
                  <div className="feature">
                    <img src={learningWithExperts_feature3} alt="" />
                    <h3>Learning expeditions</h3>
                  </div>
                  <div className="feature">
                    <img src={learningWithExperts_feature4} alt="" />
                    <h3>Webinars</h3>
                  </div>
                </div>
              )}

              <div className="features-row2">
                {!isMaxWidth768 && (
                  <div className="feature">
                    <img src={learningWithExperts_feature4} alt="" />
                    <h3>Webinars</h3>
                  </div>
                )}
                <div className="feature">
                  <img src={learningWithExperts_feature5} alt="" />
                  <h3>Talks</h3>
                </div>
                <div className="feature">
                  <img src={learningWithExperts_feature6} alt="" />
                  <h3>Volunteer programs</h3>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Explore with experts */}
        <div className="explore-with-experts">
          <h1 className="rubik-54-medium">Explore with Experts</h1>
          <p className="description">
            Amazing experiences for all budgets and abilities, run by
            professionals who know how to keep you safe. Learn about the natural
            world, leave a postive impact and make memories to last a lifetime.
          </p>
          <ActivityCardSlider
            data={exploreWithExpertsData}
            cardRedirectURL="/expeditions"
          />
          <button
            className="ex__primary_btn "
            onClick={() => router.push("/expeditions")}
          >
            SEE ALL EXPEDITIONS
          </button>
        </div>

        {/* Calling All experts */}
        <div className="calling-our-experts">
          <div className="calling-our-experts-content">
            <div>
              <h1 className="rubik-54-medium">Calling All Experts</h1>
              <h3 className="calling-our-experts-summary">
                Share your knowledge, skills and expertise with people from
                around the globe
              </h3>
              <p className="calling-our-experts-description">
                Inspire passionate explorers to discover the wonders of the
                natural world through travel, adventure and education.
                <br />
                <br />
                Are you offering a trip, running a course, event or workshop, or
                publishing new content? Share it with adventure enthusiasts all
                over the world.
              </p>
              <button className="ex__primary_btn " onClick={openSignup}>
                BECOME AN EXPERT
              </button>
            </div>
          </div>
          <div className="experts-container">
            <img
              src={callingAllExpertsBackground}
              alt=""
              className="experts-container-background"
            />
            <div className="experts">
              <div className="expert">
                <img src={callingOurExperts_Image1} alt="" />
                <div className="expert-details">
                  <h1 className="expert-name">TERRY HUDSON</h1>
                  <p className="expert-expertise">Conservationist</p>
                </div>
              </div>
              <div className="expert">
                <img src={callingOurExperts_Image2} alt="" />
                <div className="expert-details">
                  <h1 className="expert-name">DENISE OLIVER </h1>
                  <p className="expert-expertise">Expedition Leader</p>
                </div>
              </div>
              <div className="expert">
                <img src={callingOurExperts_Image3} alt="" />
                <div className="expert-details">
                  <h1 className="expert-name">RACHEL BANKS</h1>
                  <p className="expert-expertise">Mountain Guide</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Join Our Adventure Community */}
        {/* <div className="join-our-community">
          <h1 className="rubik-54-medium text-center">
            Join Our Adventure Community
          </h1>
          <Input
            placeholder="Enter your email address"
            addonBefore={<img src={mailIcon} alt="Mail icon" />}
            addonAfter={
              isLoading ?
                (<Button className="ex__primary_btn remove_hover" loading>Subscribe</Button>) :
                (<Button className="ex__primary_btn remove_hover" onClick={handleSubmit}>Subscribe</Button>)
            }
            onChange={handleChange}
            className="email-input-box"
            id="email-input-box"
            value={email}
          />
          <div class="ant-form-explain" id="subscribe-error">Please enter your email</div>
          <div id="subscribe-success">Thank You For Join Our Adventure Community</div>
          {isMaxWidth768 && (
            isLoading ?
              (<Button className="ex__primary_btn mobile-button" loading>Subscribe</Button>) :
              (<Button className="ex__primary_btn mobile-button" onClick={handleSubmit}>Subscribe</Button>)

          )}
        </div> */}

      </div>
      {showLogin && (
        <AuthModal visible={showLogin} onCancel={() => setShowLogin(false)} />
      )}
      {successPopup &&
        <NewsletterModal
          visible={successPopup}
          onClose={() => setSuccessPopup(false)}
        />
      }
      <Newsletter page="home1" />

    </div>
  );
};
export default compose(Home);
