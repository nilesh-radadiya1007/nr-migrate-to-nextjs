/* eslint-disable @next/next/link-passhref */
import React from "react";
import Link from "next/link";
import ActiveLink from "../ActiveLink";
import { Popover, Select } from "antd";
import { CaretDownOutlined } from "@ant-design/icons";
import Cookies from 'universal-cookie';
import { updatePreferredCurrency } from '../../services/auth';
import currencies from "../../helpers/currencies";
import { setPreferredCurrency } from "../../helpers/methods";
import CurrencySelectionModal from "./CurrencySelectionModal";

const { Option } = Select;
const disabledClass = "flex-x align-center medium-text text-upper an-16 header-nav-list another mr10 disabled-menu";
const enableClass = "flex-x align-center medium-text text-upper an-16 header-nav-list another mr10";

function EnthusiastMenu(props) {
  const { isProfileCompleted } = props;
  const handleClick = () => {
    props.onClose();
  };

  const cookies = new Cookies();

  const preferredCurrency = cookies.get('preferredCurrency') || "USD";

  // const onChangeCurrency = async (e) => {
  //   setPreferredCurrency(e);
  //   // cookies.set('preferredCurrency', e);
  //   await updatePreferredCurrency(e);
  //   window.location.reload();
  // }

  const enthuContent = (
    <div className="profile_bg">
      <div onClick={handleClick} className="primary--text py5 cursor-pointer">
        <ActiveLink activeClassName="active" href="/"><a>Home</a></ActiveLink>
      </div>
      <div onClick={handleClick} className="primary--text py5 cursor-pointer">
        <ActiveLink activeClassName="active" href="/experts"><a>Experts</a></ActiveLink>
      </div>
    </div>
  );

  return (
    <div className="mobile_menu" style={{ display: "flex" }}>
      {/* <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <Popover placement="topLeft" content={enthuContent} trigger="click">
          <span style={{ color: '#000', fontFamily: 'rubik', fontSize: '18px' }}>Browse</span>
          <CaretDownOutlined />
        </Popover>
      </div> */}
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <ActiveLink activeClassName="active" onClick={handleClick} href="/">
          <a onClick={handleClick}><div>Home</div></a>
        </ActiveLink>
      </div>
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <ActiveLink activeClassName="active" onClick={handleClick} href="/experts">
          <a onClick={handleClick}><div>Experts</div></a>
        </ActiveLink>
      </div>
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <ActiveLink activeClassName="active" onClick={handleClick} href="/all-adventure">
          <a onClick={handleClick}><div>Adventures</div></a>
        </ActiveLink>
      </div>
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <ActiveLink activeClassName="active" onClick={handleClick} href="/special-offers">
          <a onClick={handleClick}><div>Special Offers</div></a>
        </ActiveLink>
      </div>
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <ActiveLink activeClassName="active" onClick={handleClick} href="/messages">
          <a onClick={handleClick}><div>Messages ({props.count})</div></a>
        </ActiveLink>
      </div>
      {/* <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <Link href="/bookings">
          <div>My Bookings</div>
        </Link>
      </div> */}
      <div className="flex-x align-center medium-text an-16 header-nav-list mr5 currency-selection exp">
        <CurrencySelectionModal></CurrencySelectionModal>
      </div>
    </div>
  );
};

export default EnthusiastMenu;
