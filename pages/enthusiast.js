import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const Enthusiast= dynamic(
    () => import("../src/containers/Enthusiast"),
    { ssr: false }
);

const EnthusiastApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Enthusiast />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default EnthusiastApp;
