import React, { useEffect, useState, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import expBookCardStyle from "./ExpBookCard.module.scss";
import { compose } from "redux";
import { Icon, Upload, message, Spin } from "antd";
import { EditOutlined } from '@ant-design/icons';
import { getBase64 } from "../../../helpers/methods";
import { uploadCoverBookPicture } from "../../../services/expert";

const Default_Book_Cover = '/images/default_book_cover.png'
const antIcon = <Icon type='loading' style={{ fontSize: 24 }} spin />;

const ExpBookCard = (props) => {

  const [imageUrl, setImageUrl] = useState();
  const [upload, setUpload] = useState(false);
  const token = useSelector((state) => state.auth.accessToken);
  const dispatch = useDispatch();

  const handleChangeBookPicture = (info) => {

    if (info.file.status === "uploading") {
      return;
    }
    if (info.file.status === "done") {
      getBase64(info.file.originFileObj, (imageUrl) => {
        setImageUrl(imageUrl);
      });
      const formData = new FormData();
      formData.append("cover", info.file.originFileObj);
      formData.append("bookid", props._id);
      uploadCover(formData);
    }
  };

  const uploadCover = useCallback(
    async (data) => {
      try {
        setUpload(true);
        const result = await uploadCoverBookPicture(token, data);
        if (result.status === 200) {
          setUpload(false);
        }
      } catch (err) {
        setUpload(false);
        message.error(err.response.data.message);
      }
    },
    [token]
  );




  return (
    <div className={expBookCardStyle.book_container} key={props._id}>
      <div className={expBookCardStyle.cover}>
        {upload ? (
          <div style={{ textAlign: "center" }}  >
            <Spin indicator={antIcon} />
          </div>) : (
            <>
              <img src={imageUrl || props.cover || Default_Book_Cover} alt={props.name} />
              <Upload
                name="avatar"
                className="avatar-uploader"
                onChange={handleChangeBookPicture}
                showUploadList={false}
                customRequest={({ file, onSuccess }) =>
                  setTimeout(() => onSuccess("ok"), 0)
                }
              >
                {typeof props.edit !== "undefined" && props.edit &&
                  < span className="book-edit-icon">
                    Edit
                  </span>
                }
              </Upload>
            </>
          )}



      </div>
      <div className={expBookCardStyle.title}>
        <a href={props.url} rel="noopener noreferrer" target='_blank'>
          <span className="an-14 medium-text">{props.name}</span>
        </a>
      </div>
    </div >
  );
};

export default compose(ExpBookCard);
