import React, { useState, useEffect, useCallback } from 'react';
import { Form, Input, Row, Col, Select, Button, Upload, DatePicker } from "antd";
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { compose } from "redux";
// import Image from 'next/image'

import { getBase64 } from '../../helpers/methods';
import { PhoneValidatorEnthu } from '../../helpers/validations';
import langauges from '../../helpers/langauges';
import countries from '../../helpers/countries';
import { EnthuEvents } from '../../redux/enthu/events';
import ImageUplaoderAndCropper from '../../shared/profile-cover/image-cropper';
import { uploadProfileOriginalPictureEthnu } from '../../services/expert';
import { LoadingOutlined } from '@ant-design/icons';

const uploadIcon = '/images/upload-icon.png';

const { Option } = Select;

const Step1 = (props) => {
  const { getFieldDecorator, setFieldsValue } = props.form;
  const [imageUrl, setImageUrl] = useState();
  const { picture, name,originalPicture, country, address, phone, dob, isEditMode, firstName, lastName, city, speaks } = useSelector(state => state.enthu);
  const allData = useSelector(state => state.enthu);
  const [imageCover, setImageCover] = useState(originalPicture);
  const [imageCoverObject, setImageCoverObject] = useState(undefined);
  const [imageOriginalCoverObject, setImageOriginalCoverObject] = useState(undefined);
  const [isImageChanged, setIsImageChanged] = useState(false);
  const token = useSelector((state) => state.auth.accessToken);
  const dispatch = useDispatch();
  const [ loader, setLoader] = useState(false)
  const { step1 } = EnthuEvents;
  useEffect(() => {
    if (originalPicture !== null) {
      setImageCover(originalPicture)
    }
  }, [originalPicture])

  const setPicture = useCallback(
    (picture) => {
      if (picture !== null && picture !== undefined) {
        if (typeof picture === "string") {
          setImageUrl(picture);
        } else {
          getBase64(picture, (imageUrl) => {
            setImageUrl(imageUrl);
          });
        }
      }
      if (originalPicture !== null && originalPicture !== undefined) {
        if (typeof originalPicture !== "string") {
          getBase64(originalPicture, (imageUrl) => {
            setImageCover(imageUrl);
          });
        }
      }
    }, [isEditMode])

  useEffect(() => {
    setPicture(picture);
    setFieldsValue({
      picture: picture,
      originalPicture: originalPicture,
      name,
      address,
      country,
      phone,
      dob: dob ? moment(dob) : null,
      firstName,
      lastName,
      city,
      speaks
    });
  }, [address, country, dob, name, phone, picture, setFieldsValue, setPicture, firstName, lastName, city, speaks,originalPicture])
  const uploadOriginalCover = useCallback(
    async (data) => {
      try {
        const formData = new FormData();
        if(data !== ""){
          formData.append("profile", data);
          if(imageOriginalCoverObject)
          formData.append("originalProfile", imageOriginalCoverObject);
        }else{
          formData.append("profile", "");
          formData.append("originalProfile", "");
        }
        const result = await uploadProfileOriginalPictureEthnu(token, formData);
      } catch (err) {
      }
    },
    [token,imageOriginalCoverObject]
  );
  const handleFormSubmit = e => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        setLoader(true);
        if(isImageChanged){
          if(token){
            if(imageCoverObject){
              await uploadOriginalCover(imageCoverObject);
            }else{
              await uploadOriginalCover("");
            }
          }
          if(imageOriginalCoverObject){
            values={...values,originalPicture:imageOriginalCoverObject}
          }else{
            values={...values,originalPicture:imageCover}
          }if(imageCoverObject){
            values={...values,picture:imageCoverObject}
          }
        }else if(imageUrl){
          values={...values,picture:imageUrl,originalPicture:imageCover}
        }
        // window.scrollTo({ top: document.getElementById('scroll-to-here'), behavior: 'smooth' });
        dispatch(step1({...values}));
        setLoader(false);
        setIsImageChanged(false);
      }
    });
  };

  const setCoverImage = (coverFile, originalFile) => {
    setIsImageChanged(true);
    if(coverFile && coverFile !== ""){
      if(originalFile){
        setImageOriginalCoverObject(originalFile);
        if (typeof originalFile !== "string") {
          getBase64(originalFile, (imageUrl) => {
            setImageCover(imageUrl);
          });
        }
      }
      setImageCoverObject(coverFile);
    }else{
      setImageOriginalCoverObject("");
      setImageCoverObject("");
    }
  }

  return (
    <div className="step-1-expert-form height_sec_enthu">
      <div className="an-20 medium-text success--text step-title">
        PERSONAL INFO
      </div>
      <div className="an-16 regular-text pt10">
        Please fill in the details below to create your enthusiast profile
      </div>
      <div className="profile-picture-style">
        <img
          className={imageUrl?`profile-img`:`profile-img-default`}
          src={imageUrl || uploadIcon}
          alt="cover"
          style={{
            width: imageUrl ? "100%" : "",
            height: imageUrl ? "100%" : "",
            objectFit: "fill",
          }}
        />
      </div>
      <Form className="ant-advanced-search-form" onSubmit={handleFormSubmit}>
        <div className="form-profile-container pt20">
          <div className="pr20">
            <Form.Item label="" className="mb0">
            <div className="profile-image-reposition">
                  <ImageUplaoderAndCropper 
                    imageUrl={imageUrl}
                    setCoverImage={(file,originalFile)=>setCoverImage(file,originalFile)} 
                    originalCover={imageCover}
                    setProfileImage={(evt)=>setImageUrl(evt)}
                    page="profile"
                  />
            </div>
            </Form.Item>
          </div>
          <div>
            <div className="pt60">
              <Row gutter={24}>
                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="First Name">
                    {getFieldDecorator("firstName", { rules: [{ required: true, message: 'First Name is required' }] })
                      (<Input placeholder="Enter First Name" />)}
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Last Name">
                    {getFieldDecorator("lastName", { rules: [{ required: true, message: 'Last Name is required' }] })
                      (<Input placeholder="Enter Last Name" />)}
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Country">
                    {getFieldDecorator("country", { rules: [{ required: true, message: "Please select your country!" }] })
                      (<Select showSearch placeholder="Select Your Country">
                        {countries.map((con, i) => <Option key={i} value={con.name}>{con.name}</Option>)}
                      </Select>)}
                  </Form.Item>
                </Col>

                {/* <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="City">
                    {getFieldDecorator("city", { rules: [{ required: true, message: "Please select your city!" }] })
                      (<Select showSearch placeholder="Select Your City">
                        {countries.map((con, i) => <Option key={i} value={con.name}>{con.name}</Option>)}
                      </Select>)}
                  </Form.Item>
                </Col> */}

                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="City">
                    {getFieldDecorator("city", { rules: [{ required: true, message: 'City is required' }] })
                      (<Input placeholder="Enter City" />)}
                  </Form.Item>
                </Col>


                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Address">
                    {getFieldDecorator("address", { rules: [{ required: false, message: 'Address is required' }] })
                      (<Input placeholder="Enter Address" />)}
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Phone Number">
                    {getFieldDecorator("phone", PhoneValidatorEnthu)
                      (<Input placeholder="Enter Phone Number" />)}
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Languages">
                    {getFieldDecorator("speaks", { rules: [{ required: false }] })
                      (<Select mode="multiple" showSearch placeholder="Select Your Languages">
                        {langauges.map((lang, i) => <Option key={i} value={lang.name}>{lang.name}</Option>)}
                      </Select>)}
                  </Form.Item>
                </Col>


                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Date of Birth">
                    {getFieldDecorator("dob", { rules: [{ required: true, message: 'Please Enter Date of Birth' }] })
                      (<DatePicker format="YYYY-MM-DD" className="fill-width" placeholder="DD/MM/YYYY" />)
                    }
                  </Form.Item>
                </Col>


              </Row>
            </div>
          </div>
        </div>
        <Form.Item className="mb0 pt25">
          <Button type="primary" htmlType="submit" className="ex__primary_btn">
            Next {loader && <LoadingOutlined />}
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

const WrappedCreateEnthStep1 = Form.create({ name: "createProfile" })(Step1);

export default compose(WrappedCreateEnthStep1);
