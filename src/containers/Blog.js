import React from "react";
import Iframe from 'react-iframe'
import { BLOG_URL } from '../helpers/constants';

export default function Blog() {
    return (
        <div>
            <Iframe
                url={BLOG_URL}
                position="absolute"
                width="100%"
                id="myId"
                className="blog-website"
                height="100%"
                styles={{ height: "25px" }}
            />
        </div>
    );
}
