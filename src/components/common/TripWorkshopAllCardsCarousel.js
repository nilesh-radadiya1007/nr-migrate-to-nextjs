import React, { Fragment } from 'react';
import { compose } from "redux";
import { Col } from 'antd';
import OwlCarousel from "react-owl-carousel";
import { DISPLAY_RESULT } from "../../helpers/constants";

import SingleCard from "../../components/common/SingleCard";

const sliderOptions = {
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
      nav: true,
      dotsEach: 3,
    },
    768: {
      items: 2,
      nav: true,
    },
    991: {
      items: 3,
      nav: true,
    },
  },
};

const TripWorkshopAllCards = (props) => {
  let { items } = props;

  return (
    <Fragment>
      <OwlCarousel
        className="owl-theme modular-card new-card-design exprt-slider-owl"
        {...sliderOptions}
        dots={true}
      >
        {items.slice(0, DISPLAY_RESULT).map((l, index) => {

          return (
            <Col xs={24} sm={24} md={24} lg={24} xl={24} key={l.id} className="gutter-row pb25">
              <SingleCard
                t={l}
                index={index}
                type='expertAdventureCard'
              />
            </Col>
          )
        })}
      </OwlCarousel>
    </Fragment>
  )

}

export default compose(TripWorkshopAllCards)