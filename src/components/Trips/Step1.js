/* eslint-disable react/display-name */
import React, { useState, useEffect, useCallback } from "react";
import {
  Form,
  Row,
  Col,
  Input,
  Select,
  Button,
  Breadcrumb,
  Tooltip,
  Icon
} from "antd";
import { useSelector, useDispatch } from "react-redux";
import Cookies from "universal-cookie";
import { LoadingOutlined } from "@ant-design/icons";
import { UpdateTripCoverAndOriginalCover } from "../../services/expert";
import {
  ActivityList,
  SuitableFor,
  DifficultyList,
  PRICE_TYPE,
  GROUP_TYPE,
  DISCOUNT_FIELDS,
} from "../../helpers/constants";
import { getBase64 } from "../../helpers/methods";
import { TripsEvents } from "../../redux/trips/events";
import langauges from "../../helpers/langauges";
import ImageUplaoderAndCropper from "../../shared/image-cropper/image-cropper";

const { Option } = Select;

const Step1 = React.memo((props) => {
  const [loader, setLoader] = useState(false)
  const token = useSelector((state) => state.auth.accessToken);
  const id = useSelector((state) => state.trips.id);
  const [imageUrl, setImageUrl] = useState();
  const [fileList, setFileList] = useState([]);
  const [isEdit, setIsEdit] = useState(false);
  const { getFieldDecorator, setFieldsValue } = props.form;
  let {
    title,
    cover,
    originalCover,
    imgList,
    type,
    activity,
    price,
    suitable,
    difficulty,
    duration,
    skill,
    language,
    participants,
    durationType,
    priceType,
    groupType,
    discount,
    season,
    discountType
  } = useSelector((state) => state.trips);

  const [imageCover, setImageCover] = useState(originalCover);
  const [imageCoverObject, setImageCoverObject] = useState(undefined);
  const [imageOriginalCoverObject, setImageOriginalCoverObject] = useState(undefined);
  const [isImageChanged, setIsImageChanged] = useState(false);
  useEffect(() => {
    if (originalCover !== null) {
      setImageCover(originalCover);
    }
  }, originalCover);

  const cookies = new Cookies();
  let preferredCurrency = cookies.get("preferredCurrency");
  const dispatch = useDispatch();
  const [durationAdd, setDurationAdd] = useState(durationType || "days");
  const [currency] = useState(preferredCurrency || "USD");
  const [refreshFieldLoader, setrefreshFieldLoader] = useState(false);
  const [priceTypeNew, setPriceTypeNew] = useState(priceType || "3");
  const [groupTypeNew, setGroupTypeNew] = useState(groupType || "1");
  const [discountTypeNew, setDiscountTypeNew] = useState(discountType || 1);

  const { step1 } = TripsEvents;

  const onAddonChange = (e) => {
    setDurationAdd(e);
  };

  const selectBefore = (
    <Select defaultValue={durationAdd} onChange={onAddonChange} style={{ width: 125 }}>
      <Option value="days">Days</Option>
      <Option value="hours">Hours</Option>
      <Option value="weeks">Weeks</Option>
    </Select>
  );

  useEffect(() => {
    setPriceTypeNew(priceType);
  }, [priceType]);

  useEffect(() => {
    setGroupTypeNew(groupType);
  }, [groupType]);

  useEffect(() => {
    setDiscountTypeNew(discountType);
  }, [discountType]);

  const selectAfterPriceType = (
    <Select defaultValue={priceTypeNew} onChange={(e) => setPriceTypeNew(e)} style={{ width: 125 }}>
      {PRICE_TYPE.filter((item) => item.value !== 5).map((type, i) => {
        return (
          <Option key={i} value={type.value}>{type.name}</Option>
        )
      })}
    </Select>
  );

  const selectAfterGroupType = (
    <Select defaultValue={groupTypeNew} onChange={(e) => setGroupTypeNew(e)} style={{ width: 125 }}>
      {GROUP_TYPE.map((type, i) => {
        return (
          <Option key={i} value={type.value}>{type.name}</Option>
        )
      })}
    </Select>
  );

  const selectBeforeDiscount = (
    <Select defaultValue={discountTypeNew} onChange={(e) => setDiscountTypeNew(e)} style={{ width: 125 }}>
      {DISCOUNT_FIELDS.map((type, i) => {
        return (
          <Option key={i} value={type.value}>{type.name}</Option>
        )
      })}
    </Select>
  );

  const setPicture = useCallback((picture) => {
    if (picture !== null && picture !== undefined) {
      if (typeof picture === "string") {
        setImageUrl(picture);
      } else {
        getBase64(picture, (imageUrl) => {
          setImageUrl(imageUrl);
        });
      }
    }
  }, []);

  useEffect(() => {
    setPicture(cover);
    setrefreshFieldLoader(true);
    setDurationAdd(durationType);
    setDiscountTypeNew(discountType);

    if (typeof language === "string") {
      language = language.split(",");
    }
    if (typeof skill === "string") {
      if (skill !== "") {
        skill = skill.split(",");
      } else {
        skill = [];
      }
    }
    if (typeof difficulty === "string") {
      difficulty = difficulty.split(",");
    }
    if (typeof suitable === "string") {
      suitable = suitable.split(",");
    }

    setFieldsValue({
      cover,
      originalCover,
      title,
      price,
      suitable,
      duration,
      difficulty,
      skill,
      language,
      participants,
      activity: typeof activity === "string" ? JSON.parse(activity) : [],
      type: typeof type === "string" ? JSON.parse(type) : [],
      fileList,
      discount: typeof discount !== "undefined" ? discount : "",
      season: typeof season !== "undefined" ? season : ""
    });

    setTimeout(() => {
      setrefreshFieldLoader(false);
    }, 100);

    if (isEdit === false && imgList && imgList.length > 0) {
      let finalData = [];
      setImageUrl(imgList[0]);
      imgList.map((img, index) => {
        finalData.push({
          uid: index,
          name: img.split("albums/")[1],
          status: "done",
          url: img,
        });
        if (imgList.length === index + 1) {
          setFileList(finalData);
          setIsEdit(true);
        }
      });
    }
  }, [
    activity,
    fileList,
    cover,
    originalCover,
    difficulty,
    duration,
    price,
    setFieldsValue,
    suitable,
    title,
    type,
    skill,
    language,
    setPicture,
    participants,
    durationType,
    priceType,
    groupType,
    discount,
    season,
    discountType
  ]);

  const uploadOriginalCover = useCallback(
    async (data) => {
      try {
        if (data !== "") {
          const formData = new FormData();
          formData.append("cover", data);
          if (imageOriginalCoverObject)
            formData.append("originalCover", imageOriginalCoverObject);
          await UpdateTripCoverAndOriginalCover(id, token, formData);
        } else {
          const formData = new FormData();
          formData.append("cover", "");
          formData.append("originalCover", "");
          await UpdateTripCoverAndOriginalCover(id, token, formData);
        }

      } catch (err) {
      }
    },
    [id, token, imageOriginalCoverObject]
  );

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        setLoader(true);
        if (isImageChanged) {
          if (id) {
            if (imageCoverObject) {
              await uploadOriginalCover(imageCoverObject);
            } else {
              await uploadOriginalCover("");
            }
          }
          if (imageOriginalCoverObject) {
            values = { ...values, originalCover: imageCoverObject }
          } else if (!id) {
            values = { ...values, originalCover: imageCover }
          }
          if (imageCoverObject) {
            values = { ...values, cover: imageCoverObject }
          }
        } else {
          if (!id) {
            values = { ...values, originalCover: imageCover }
          }
        }

        setIsEdit(false);
        dispatch(
          step1({
            ...values,
            priceCurrency: currency,
            durationType: durationAdd,
            priceType: priceTypeNew,
            groupType: groupTypeNew,
            discountType: discountTypeNew
          })
        );
        setLoader(false);
        setIsImageChanged(false);
      }
    });
  };

  const setCoverImage = (coverFile, originalFile) => {
    setIsImageChanged(true);
    if (coverFile && coverFile !== "") {
      if (originalFile) {
        setImageOriginalCoverObject(originalFile);
      }
      setImageCoverObject(coverFile);
    } else {
      setImageOriginalCoverObject("");
      setImageCoverObject("");
    }
  };

  const onImageError = () => {
    setFieldsValue({
      cover: null
    });
  }

  return (
    <div className="step-1-expert-form height_sec_enthu learn_sec ">
      <Breadcrumb separator=">">
        <Breadcrumb.Item className="an-20 medium-text success--text step-title">
          Create a Trip
        </Breadcrumb.Item>
        <Breadcrumb.Item className="an-16 regular-text pt10">
          Basic Information
        </Breadcrumb.Item>
      </Breadcrumb>
      <Form
        className="ant-advanced-search-form creacte_rip_section "
        onSubmit={handleSubmit}
      >
        <div
          className={`form-profile-container pt20 ${
            fileList.length === 0 && "img-set-icon"
            }`}
        >
          <Form.Item label="" className="mb20">
            {
              getFieldDecorator("cover", {
                rules: [
                  { required: true, message: "Please Upload Cover photo!" },
                ],
              })(
                <div>
                  <div className={`step1-img step1-profile`}>
                    <ImageUplaoderAndCropper onImageError={() => onImageError()} setCoverImage={(evt, check) => setCoverImage(evt, check)} imageUrl={imageUrl} originalCover={originalCover} />
                  </div>
                </div>)
            }
          </Form.Item>
          <div>
            <Row gutter={24}>
              <Col className="" xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Trip Name">
                  {getFieldDecorator("title", {
                    rules: [
                      { required: true, message: "Please Enter Trip Name" },
                    ],
                  })(
                    <Input
                      className="trip_input"
                      placeholder="Enter Trip Name"
                    />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col className="" xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Activity">
                  {getFieldDecorator("activity", {
                    rules: [
                      { required: true, message: "Please Select Activity" },
                    ],
                  })(
                    <Select
                      mode="multiple"
                      showSearch
                      placeholder="Select Activity"
                    >
                      {ActivityList.map((exp, i) => (
                        <Option key={i} value={exp.name}>
                          {exp.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col className="base_price_trip_expert" xs={24} sm={12} md={12} lg={12} xl={12}>
                <Tooltip
                  placement="left"
                  title="Please go to currency selection field in the header if you want to change the currency selection for your account"
                >
                  <Form.Item label="Starting Price">
                    {getFieldDecorator("price", {
                      rules: [
                        { required: true, message: "Please Enter Price" },
                      ],
                    })(
                      !refreshFieldLoader ? <Input
                        addonAfter={selectAfterPriceType}
                        addonBefore={currency}
                        type="number"
                        min={1}
                        placeholder="Enter Price"
                      /> : <Input
                          addonBefore={currency}
                          type="number"
                          min={1}
                          placeholder="Enter Price"
                        />
                    )}
                  </Form.Item>
                </Tooltip>
              </Col>
              <Col
                className="base_price_trip_expert discount_field" xs={24} sm={12} md={12} lg={12} xl={12} >
                <Form.Item label="Special Offer (%)">
                  {getFieldDecorator("discount")
                    (
                      !refreshFieldLoader ? <Input
                        addonBefore={selectBeforeDiscount}
                        type="number"
                        min={0}
                        placeholder="Discount"
                      /> : <Input
                          type="number"
                          min={0}
                          placeholder="Discount"
                        />
                    )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col className="base_price_trip_expert" xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Duration">
                  {getFieldDecorator("duration", {
                    rules: [
                      { required: true, message: "Enter Duration" },
                    ],
                  })(
                    !refreshFieldLoader ? <Input
                      addonAfter={selectBefore}
                      type="number"
                      min={1}
                      placeholder="Duration"
                    />
                      : <Input
                        type="number"
                        min={1}
                        placeholder="Duration"
                      />
                  )
                  }
                </Form.Item>
              </Col>
              <Col className="base_price_trip_expert" xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label={
                  <span>
                    Season&nbsp;&nbsp;
                    <Tooltip title={<span>Please enter seasons in below format: <br /> JAN,FEB  <br /> JAN-MAR</span>}>
                      <Icon type="question-circle" theme="filled" />
                    </Tooltip>
                  </span>
                }
                >
                  {getFieldDecorator("season", {
                    rules: [
                      {
                        required: false,
                        message: "Enter Months as JAN,FEB or JAN-MAY",
                        pattern: new RegExp("^[a-zA-Z-, ]*$"),
                        message: "Only alphabet, space, hyphen(-) and comma(,) are allowed"
                      },
                    ],
                  })(
                    <Input
                      placeholder="Enter Months as JAN,FEB or JAN-MAY"
                    />
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col className="" xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Group size" className="group-size-field">
                  {getFieldDecorator("participants", {
                    rules: [{ required: true, message: "Please add" }],
                  })(
                    !refreshFieldLoader ?
                      <Input
                        type="number"
                        min={1}
                        addonAfter={selectAfterGroupType}
                        placeholder="Enter Max Participants"
                      /> : <Input
                        type="number"
                        min={1}
                        placeholder="Enter Max Participants"
                      />
                  )}
                </Form.Item>
              </Col>
              <Col className="" xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Suitable for">
                  {getFieldDecorator("suitable", {
                    rules: [
                      { required: true, message: "Please Select Suitable for" },
                    ],
                  })(
                    <Select
                      showSearch
                      placeholder="Select Suitable"
                      mode="multiple"
                    >
                      {SuitableFor.map((exp, i) => (
                        <Option key={i} value={exp.value}>
                          {exp.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col className="" xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Languages">
                  {getFieldDecorator("language", {
                    rules: [
                      { required: true, message: "Please Select Suitable for" },
                    ],
                  })(
                    <Select
                      className="languages"
                      mode="multiple"
                      showSearch
                      placeholder="Select Language"
                    >
                      {langauges.map((exp, i) => (
                        <Option key={i} value={exp.name}>
                          {exp.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col className="" xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item
                  className="Difficultylevel_trip_sub"
                  label="Difficulty level"
                >
                  {getFieldDecorator("difficulty", {
                    rules: [
                      {
                        required: true,
                        message: "Please Select Difficulty Level",
                      },
                    ],
                  })(
                    <Select
                      className="difficulty"
                      mode="multiple"
                      showSearch
                      placeholder="Select Difficulty Level"
                    >
                      {DifficultyList.map((exp, i) => (
                        <Option key={i} value={exp.value}>
                          {exp.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </div>
        </div>
        <Form.Item className="mb0 pt25">
          <Button type="primary" htmlType="submit" className="ex__primary_btn">
            Save & Next  {loader && <LoadingOutlined />}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
});

const WrappedCreateStep1 = Form.create({ name: "createTrips" })(Step1);

export default WrappedCreateStep1;
