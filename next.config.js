const withOptimizedImages = require('next-optimized-images');
const withPlugins = require('next-compose-plugins');
const withImages = require('next-images')
const path = require('path')
// const { PHASE_DEVELOPMENT_SERVER, PHASE_PRODUCTION_BUILD } = require('next/constants')

module.exports = {
  exportPathMap: async function (
    defaultPathMap,
    { dev, dir, outDir, distDir, buildId }
  ) {
    return {
      '/': { page: '/' },
      '/export-profile/[id]': { page: 'export-profile', query: { id: '' } }
    }
  },
}

module.exports = withPlugins(
  [
    [
      withOptimizedImages,
      {
        // / config for next-optimized-images /
        optimizeImages: false,
        // your config for other plugins or the general next.js here...
      },
    ],
  ],
  {
    trailingSlash: false,
  },

);

module.exports = {
  images: {
    loader: 'imgix',
    path: 'https://expeditionsconnect.com/',
  },
}

module.exports = withImages()

module.exports = {
  reactStrictMode: false,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  webpack: (config, {
    webpack
  }) => {
    config.plugins.push(
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
      })
    );
    return config;
  },
  images: {
    domains: ['blog.expeditionsconnect.com', 'expeditions-connect-bucket.s3.eu-central-1.amazonaws.com', 'http://ec-extra-port.s3-website.eu-central-1.amazonaws.com', 'localhost'],
  },
  async redirects() {
    return [
      {
        source: '/login',
        destination: '/',
        permanent: true
      },

    ]
  },
}

// module.exports = {
//   reactStrictMode: false,
// }

// module.exports = withImages({
//   webpack(config, options) {
//     return config
//   }
// })

// module.exports = (phase) => {

//   // npm run dev or next dev
//   const isDev = phase === PHASE_DEVELOPMENT_SERVER;

//   // npm run dev or next dev
//   const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== 'l';

//   const isStage = phase = PHASE_PRODUCTION_BUILD && process.env.STAGING === 'l';

//   env:{

//   }
// }


