import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';


const EnthusiestProfile = dynamic(
    () => import("../src/containers/EnthusiestProfile"),
    { ssr: false }
);

const EnthusiestProfileInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <EnthusiestProfile />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default EnthusiestProfileInApp;