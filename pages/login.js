import React from 'react';
import Login from '../src/containers/Login';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const LoginInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Login />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default LoginInApp;