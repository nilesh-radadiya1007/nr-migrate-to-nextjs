import React, { useState, useEffect, useCallback, useRef } from "react";
import {
  Row,
  Col,
  Select,
  Radio,
  Checkbox,
  Button,
  Popover,
  Menu,
  Icon,
  Input,
} from "antd";
// import { withRouter, useHistory, Link } from "react-router-dom";
import { useRouter } from "next/router";
import { compose } from "redux";
import axios from "axios";
import Cookies from "universal-cookie";
// import queryString from "query-string";

/**
 * App Imports
 */
import {
  ActivityList,
  DISPLAY_PER_PAGE_MOBILE,
  DISPLAY_PER_PAGE,
  ADVENTURE_CATEGORY
} from "../helpers/constants";
import { getCurrencySymbol } from "../helpers/methods";
import countries from "../helpers/countries";
import { getLeraningPlusTripNew } from "../services/expert";
import AppLoader from "../components/Loader";
import langs from "../helpers/langauges";
import { useMediaQuery } from "react-responsive";
// import mapIcon from "../assets/images/map.svg";
// import locatorIcon from "../assets/images/newicon/locator.svg";
// import menuIcon from "../assets/images/menu.svg";
// import GropuIcon from "../assets/images/group_activity.svg";
// import AdventurerIcon from "../assets/images/adventurer.svg";
// import PinIcon from "../assets/images/Pin.svg";
import AdventuresMap from "../shared/map/map";
import SingleCard from "../components/common/SingleCard";
import Newsletter from '../components/common/NewsLetter';
import ExpeditionsConnect from "../components/common/ExpeditionsConnect";
// import FeaturedAdventures from '../components/common/FeaturedAdventures';
// import WhyExpedictions from '../components/common/whyExpedictions';

const ApiKey = process.env.REACT_APP_GOOGLE_MAP_API_KEY;
const cookies = new Cookies();

const preferredCurrency = cookies.get("preferredCurrency") || "USD";

const { Option, OptGroup } = Select;
let tempTrips = [];
let tmpAllData = [];
let tmpotherDataCount = 0;

function Adventure(props) {
  const router = useRouter();
  let currentMonth = new Date().getMonth() + 1;
  const [currentYear, setCurrentYear] = useState("");
  const [yearsLength, setYearsLength] = useState([]);
  const [selectedYear, setSelectedYear] = useState("");
  const [allDateSelected, setAllDateSelected] = useState(true);
  useEffect(() => {
    if (!currentYear) {
      let curreYr = new Date().getFullYear();
      setCurrentYear(curreYr);
      let tmpArray = [];
      for (var x = curreYr; x < curreYr + 3; x++) {
        tmpArray.push(x);
      }
      setYearsLength(tmpArray);
    }
  }, []);

  // const history = useHistory();
  const inputEl = useRef(null);
  let urlParams = router.query;

  const [trips, setTrips] = useState([]);
  const [mapTrips, setMapTrips] = useState([]);
  const [loader, setLoader] = useState(false);
  const [initialLoader, setInitialLoader] = useState(true);

  const [activity, setActivity] = useState([]);
  const [country, setCountry] = useState(urlParams.country || "");
  const [adventure, setAdventure] = useState(urlParams.adventure || "group");
  const [month, setMonth] = useState(
    urlParams.month ? urlParams.month.split(",") : [] || []
  );
  const [price, setPrice] = useState(urlParams.price || "");
  const [suitable, setSuitable] = useState([]);
  const [difficulty, setDifficulty] = useState(urlParams.difficulty || "");
  const [sortBy, setSortBy] = useState(urlParams.sortBy || "distance");
  const [langauges, setLangauges] = useState([]);
  const [skill, setSkill] = useState("");
  const [workshopType, setWorkshopType] = useState([]);
  const [workshopMedium, setWorkshopMedium] = useState([]);

  const [currentPage] = useState(
    router.pathname === "/learning" ? "learning" : "expeditions"
  );
  const [pageNo, setPageNo] = useState(1);
  const [totalRecords, setTotalRecords] = useState(0);
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const [perPage, setPerPage] = useState(
    isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE
  );
  const [currentView, setCurrentView] = useState("grid");
  const [hoveredCardId, setHoverCardId] = useState("");
  const [activityText, setActivityText] = useState("Select Activity");
  const [rotateArrow, setRotateArrow] = useState(false);
  const [activeFilter, setActiveFilter] = useState(false);
  const [activityDList, setActivityList] = useState(ActivityList);
  const [countryCoordinates, setCountryCoordinates] = useState();
  const [centerLatLong, setCenterLatLong] = useState();
  const [tmpvar, setTmpvar] = useState(false);
  const [isLoaded, setisLoaded] = useState(true);
  const [isMapLoading, setIsMapLoading] = useState(false);

  //States for the hide show filter dropdown
  const [dateVisible, setdateVisible] = useState(false);
  const [priceVisible, setpriceVisible] = useState(false);
  const [langVisible, setlangVisible] = useState(false);
  const [filterVisible, setfilterVisible] = useState(false);
  const [isFlexible, setIsFlexible] = useState(true);
  const [weekend, setWeekend] = useState(urlParams.weekend || "");
  const [certification, setCertification] = useState(urlParams.certification || "");
  const [longAdventure, setLongAdventure] = useState(urlParams.longAdventure || "");
  const [otherPageNo, setOtherPageNo] = useState(1);
  const [otherDataCount, setOtherDataCount] = useState(0);
  const [featuredAdventure, setFeaturedAdventure] = useState([]);
  const [tripLengthVisible, setTripLengthVisible] = useState(false);
  const [tripLength, setTripLength] = useState(urlParams.weekend ? "weekend" : "");


  const getAllTripsHandlerForAll = useCallback(async (value, sortBy) => {
    try {
      setLoader(true);
      const result = await getLeraningPlusTripNew(value);
      if (result.status === 200) {
        setTmpvar(true);
        tempTrips = [...tempTrips, ...result.data.data.data];
        var resArr = [];
        tempTrips.forEach(function (item) {
          var i = resArr.findIndex((x) => x.id === item.id);
          if (i <= -1) {
            resArr.push(item);
          }
        });
        setTrips(resArr);
        // setMapTrips(tempTrips)
        setTotalRecords(result.data.data.count);
        setInitialLoader(false);
        setLoader(false);
        if (currentView !== "map") {
          refreshResultWithAllData(value, sortBy);
        }
      } else {
        setLoader(false);
        setInitialLoader(false);
      }
      setTmpvar(false);
    } catch (err) {
      setLoader(false);
      setInitialLoader(false);
    }
  }, []);

  const getAllTripsHandlerForAllOtherCountry = useCallback(async (value, sortBy) => {
    try {
      setLoader(true);
      const result = await getLeraningPlusTripNew(value);
      if (result.status === 200) {

        tmpAllData = [...tmpAllData, ...result.data.data.data];
        setOtherDataCount(result.data.data.count);

        tmpotherDataCount = tmpotherDataCount + result.data.data.data.length;

        var resArr = [];
        tmpAllData.forEach(function (item) {
          var i = resArr.findIndex((x) => x.id == item.id);
          if (i <= -1) {
            resArr.push(item);
          }
        });
        setLoader(false);
        setTrips(resArr);
      }
    } catch (err) {
      setLoader(false);
      setInitialLoader(false);
    }
  }, []);

  const getAllTripsHandlerForAllData = useCallback(async (value) => {
    try {
      const result = await getLeraningPlusTripNew(value);
      if (result.status === 200) {
        let datResponse = result.data.data.data;
        var resArr = [];
        datResponse.forEach(function (item) {
          var i = resArr.findIndex((x) => x.id === item.id);
          if (i <= -1) {
            resArr.push(item);
          }
        });

        let replaceFirst = resArr.slice(0, isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE);
        tmpAllData = resArr;
        setTimeout(() => {
          setTrips(replaceFirst);
        }, 1000);
        setMapTrips(tmpAllData);
        setIsMapLoading(false);
      }
    } catch (err) { }
  }, []);

  const getAllFeaturedTripsAndWorkshops = useCallback(async (value) => {
    try {
      setLoader(true);
      const result = await getLeraningPlusTripNew(value, true);
      if (result.status === 200) {
        setFeaturedAdventure(result.data.data.data);
        setLoader(false);
      } else {
        setLoader(false);
      }
    } catch (err) {
      setLoader(false);
    }
  }, []);

  useEffect(() => {
    const allSearch = { pageNo, perPage, adventure, isFeatured: true };
    getAllFeaturedTripsAndWorkshops(allSearch);
  }, [])

  useEffect(() => {
    if (Object.keys(router.query).length > 0) {
      filterFromURLParams(urlParams);
    }
    // console.log('working..')
  }, []);

  const filterFromURLParams = (urlParams) => {
    setTrips([]);
    tempTrips = [];

    let tmpActivity = [],
      tmpLanguages = [],
      tmpSuitable = [],
      tmpWorkshopType = [],
      tmpWorkshopMedium = [];
    setisLoaded(false);

    if (
      urlParams.activity !== undefined &&
      urlParams.activity.length !== 0 &&
      urlParams.activity !== ""
    ) {
      tmpActivity = urlParams.activity.replace(/'/g, '"');
      tmpActivity = JSON.parse(tmpActivity);
      onChange(tmpActivity || []);
      setActivity(tmpActivity || []);
    } else {
      onChange(tmpActivity || []);
      setActivity(tmpActivity || []);
    }

    if (
      urlParams.langauges !== undefined &&
      urlParams.langauges.length !== 0 &&
      urlParams.langauges !== ""
    ) {
      tmpLanguages = urlParams.langauges.replace(/'/g, '"');
      tmpLanguages = JSON.parse(tmpLanguages);
    }
    if (
      urlParams.suitable !== undefined &&
      urlParams.suitable.length !== 0 &&
      urlParams.suitable !== ""
    ) {
      tmpSuitable = urlParams.suitable.replace(/'/g, '"');
      tmpSuitable = JSON.parse(tmpSuitable);
    }
    if (
      urlParams.workshopType !== undefined &&
      urlParams.workshopType.length !== 0 &&
      urlParams.workshopType !== ""
    ) {
      tmpWorkshopType = urlParams.workshopType.replace(/'/g, '"');
      tmpWorkshopType = JSON.parse(tmpWorkshopType);
    }
    if (
      urlParams.workshopMedium !== undefined &&
      urlParams.workshopMedium.length !== 0 &&
      urlParams.workshopMedium !== ""
    ) {
      tmpWorkshopMedium = urlParams.workshopMedium.replace(/'/g, '"');
      tmpWorkshopMedium = JSON.parse(tmpWorkshopMedium);
    }

    setLangauges(tmpLanguages || []);
    setAdventure(urlParams.adventure || "group");
    setCountry(urlParams.country || "");
    setMonth(urlParams.month ? urlParams.month.split(",") : [] || []);
    setSelectedYear(urlParams.year || "");
    if (urlParams.month) {
      setAllDateSelected(false);
    }
    setPrice(urlParams.price || "");
    setSuitable(tmpSuitable || []);
    setWorkshopType(tmpWorkshopType || []);
    setWorkshopMedium(tmpWorkshopMedium || []);
    setSkill(urlParams.skill || "");
    setDifficulty(urlParams.difficulty || "");
    setSortBy(urlParams.sortBy || "distance");
    setWeekend(urlParams.weekend || "");
    setCertification(urlParams.certification || "");
    setLongAdventure(urlParams.longAdventure || "");
    setTripLength((urlParams.tripLength || urlParams.weekend) ? urlParams.tripLength : "");
    if (urlParams.isFlexible) {
      setIsFlexible(urlParams.isFlexible === "true" ? true : false);
    } else {
      setIsFlexible(true);
    }

    const allSearch = {
      activity: tmpActivity || [],
      country: urlParams.country || "",
      month: urlParams.month || "",
      price: urlParams.price || "",
      langauges: tmpLanguages || "",
      suitable: tmpSuitable || "",
      difficulty: urlParams.difficulty || "",
      skill: urlParams.skill || "",
      workshopType: tmpWorkshopType || "",
      workshopMedium: tmpWorkshopMedium || "",
      perPage,
      adventure: urlParams.adventure || "group",
      sortBy: urlParams.sortBy || "distance",
      isFlexible:
        urlParams.isFlexible && urlParams.isFlexible === "true" ? true : false,
      year: urlParams.year || "",
      weekend: urlParams.weekend || "",
      certification: urlParams.certification || "",
      longAdventure: urlParams.longAdventure || "",
      tripLength: urlParams.weekend ? "weekend" : urlParams.tripLength ? urlParams.tripLength : ""
    };

    getAllTripsHandlerForAll(allSearch, urlParams.sortBy || "distance");
    setisLoaded(true);
  };

  const clicktoShowMore = (tmppageNo) => {
    setLoader(true);
    setPageNo(tmppageNo);
    if (sortBy === "asc") {
      tmpAllData.sort((a, b) =>
        a.title > b.title ? 1 : b.title > a.title ? -1 : 0
      );
    } else if (sortBy === "desc") {
      tmpAllData.sort((a, b) =>
        a.title < b.title ? 1 : b.title < a.title ? -1 : 0
      );
    }

    // let ans = tmpAllData.slice(((isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE)  (tmppageNo - 1)), (isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE)  (tmppageNo));
    let ans = tmpAllData.slice(
      (isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE) *
      (tmppageNo - 1),
      (isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE) * tmppageNo
    );
    setTrips([...trips, ...ans]);
    setLoader(false);
  };

  useEffect(() => {
    if (Object.keys(router.query).length > 0) {
      let urlParams = router.query;
      setActiveFilter(true);
      filterFromURLParams(urlParams);
    } else {
      refreshResult(sortBy);
    }
  }, [currentPage, perPage]);

  useEffect(() => {
    tempTrips = [];
  }, [currentView]);

  useEffect(() => {
    let urlParams = router.query

    setIsMapLoading(false);
    setTimeout(() => {
      if (urlParams.view === "map") {
        inputEl.current.click();
      }
    }, 1000);
  }, []);
  const refreshResult = (sortBy = "") => {
    setMapTrips([]);
    const allSearch = {
      activity,
      country,
      month,
      price,
      langauges,
      suitable,
      difficulty,
      sortBy: sortBy,
      skill,
      workshopType,
      workshopMedium,
      perPage,
      adventure,
      isFlexible,
      year: selectedYear,
      weekend,
      certification,
      longAdventure,
      tripLength
    };
    getAllTripsHandlerForAll(allSearch, sortBy);
  };

  const ShowMoreFromOtherCountry = (currentPage) => {
    setMapTrips([]);
    const allSearch = {
      activity,
      country: "",
      month,
      price,
      langauges,
      suitable,
      difficulty,
      sortBy: sortBy,
      skill,
      workshopType,
      workshopMedium,
      perPage,
      adventure,
      isFlexible,
      year: selectedYear,
      weekend,
      certification,
      longAdventure,
      isOtherCountry: true,
      pageNo: otherPageNo,
      tmpCountry: country
    };
    setOtherPageNo(currentPage + 1);
    getAllTripsHandlerForAllOtherCountry(allSearch, sortBy);
  };

  const refreshResultWithAllData = (value, sortBy = "") => {
    value.perPage = 1000;
    setMapTrips([]);
    getAllTripsHandlerForAllData(value);
  };

  const onSortOrderChanged = (e) => {
    changeURL(e);
    setSortBy(e);
    tempTrips = [];
    setInitialLoader(true);
    setTrips([]);
    setPageNo(1);
    setOtherDataCount(0);
    if (e === "desc") {
      setLoader(false);
      let ans = {};
      if (currentView === "map") {
        ans = tmpAllData.reverse();
      } else {
        ans = tmpAllData.reverse().slice(0, DISPLAY_PER_PAGE);
      }
      setTrips(ans);
      setMapTrips(ans);
      setInitialLoader(false);
      setLoader(false);
    } else if (e === "distance") {
      setLoader(true);
      refreshResult(e);
      setInitialLoader(false);
      // setLoader(false);
    } else {

      if (tmpAllData.length === 0) {
        refreshResult(e);
      } else {
        setMapTrips([]);
        let ans = {};
        if (currentView === "map") {
          let newtmpAllData = sortTheData(tmpAllData);
          ans = newtmpAllData.reverse();
        } else {
          let newtmpAllData = sortTheData(tmpAllData);
          newtmpAllData = newtmpAllData.reverse();
          ans = newtmpAllData.slice(0, DISPLAY_PER_PAGE);
        }
        setTrips(ans);
        setMapTrips(ans);
      }
      setInitialLoader(false);
      setLoader(false);
    }

  };

  const sortTheData = (allData, type = "asc") => {
    console.log(allData);
    let tmpSortData = allData.sort((a, b) => {
      let fa = typeof a.title !== "undefined" && a.title !== "" ? a.title.trim().toLowerCase() : "";
      let fb = typeof b.title !== "undefined" && b.title !== "" ? b.title.trim().toLowerCase() : "";
      if (fa > fb) {
        return -1;
      }
      if (fa < fb) {
        return 1;
      }
      return 0;
    });
    console.log(tmpSortData);
    // if (type === "desc") {
    //   return allData.reverse();
    // }
    return tmpSortData;
  }

  const onYeadrChange = (e) => {
    setMonth([]);
    setSelectedYear(e);
    if (e === "") setAllDateSelected(true);
  };

  const onFilterChange = (e, type) => {
    if (type === "activity") {
      setRotateArrow(!rotateArrow);
      setActivity(e);
    }
    if (type === "price") {
      setPrice(e.target.value);
    }
    if (type === "language") {
      setLangauges(e);
    }
    if (type === "tripLength") {
      setTripLength(e.target.value);
    }
    if (type === "date") {
      if (e !== "") {
        if (month.indexOf(e) >= 0) {
          let filtered = month.filter(function (str) {
            return str !== e;
          });
          setMonth(filtered);
        } else {
          setMonth([...month, e]);
        }
        setAllDateSelected(false);
      } else {
        setIsFlexible(true);
        setAllDateSelected(true);
        setMonth([]);
      }
    }
    if (type === "adventureType") {
      setSkill("");
      setDifficulty("");
      setWorkshopMedium([]);
      setWorkshopType([]);
      setAdventure(e);
    }
  };

  //Update trips according to the marker positions
  const setTripsOnMapChange = (boundedTrips) => {
    try {
      setMapTrips([]);
      let tripsData = [];
      if (boundedTrips && boundedTrips.length > 0) {
        boundedTrips.map((trip, index) => {
          if (trip) {
            const tempTrips = tmpAllData.find((x) => x.id === trip);
            if (tempTrips) tripsData.push(tempTrips);
          }
          return true;
        });
      }

      setMapTrips(tripsData.length > 0 ? tripsData : []);
      setIsMapLoading(tripsData.length > 0 ? false : true);
    } catch (err) { }
  };

  const priceContent = (
    <div className="">
      <Radio.Group
        defaultValue={price}
        onChange={(e) => onFilterChange(e, "price")}
      >
        <Radio value="">Any</Radio>
        <Radio value="0-500">
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}0 -{" "}
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}500
        </Radio>
        <Radio value="501-1000">
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}501 -{" "}
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}1000
        </Radio>
        <Radio value="1001-2000">
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}1001 -{" "}
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}2000
        </Radio>
        <Radio value="1001-2000">
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}1001 -{" "}
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}2000
        </Radio>
        <Radio value="2001-3000">
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}2001 -{" "}
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}3000
        </Radio>
        <Radio value="3001-5000">
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}3001 -{" "}
          {getCurrencySymbol(preferredCurrency) || preferredCurrency}5000
        </Radio>
        <Radio value="5001-10000000">
          {" "}
          More Than {getCurrencySymbol(preferredCurrency) || preferredCurrency}
          5001{" "}
        </Radio>
      </Radio.Group>
      <div className="btn-apply">
        <Button onClick={(e) => handleApply()}>Apply</Button>
      </div>
    </div>
  );

  const tripLengthContent = (
    <div className="">
      <Radio.Group
        defaultValue={tripLength}
        onChange={(e) => onFilterChange(e, "tripLength")}
      >
        <Radio value="">All</Radio>
        <Radio value="weekend">
          Weekends
        </Radio>
        <Radio value="7less">
          Up to 7 days
        </Radio>
        <Radio value="7plus">
          Longer than 7 days
        </Radio>

      </Radio.Group>
      <div className="btn-apply">
        <Button onClick={(e) => handleApply()}>Apply</Button>
      </div>
    </div>
  );

  const LanguageContent = (
    <div>
      <Select
        mode="multiple"
        onChange={(e) => onFilterChange(e, "language")}
        style={{ width: "200px" }}
        placeholder="Select Language"
        showArrow="true"
        defaultValue={langauges}
        suffixIcon={
          <Icon
            type="caret-down"
            style={{ fontSize: "17px", color: "#000000" }}
          />
        }
      >
        {langs.map((exp, i) => (
          <Option key={i} value={exp.name}>
            {exp.name}
          </Option>
        ))}
      </Select>
      <div className="btn-apply">
        <Button onClick={(e) => handleApply()}>Apply</Button>
      </div>
    </div>
  );

  const advanceFilterContent = (
    <div>
      {/* <Row gutter={0} > */}
      <Col xs={12} sm={12} md={12} lg={24} xl={24}>
        <div className="filter-by">FILTER BY</div>
      </Col>
      <div style={{ clear: "both" }}></div>
      <Col xs={12} sm={12} md={12} lg={24} xl={24}>
        <div>
          {/* {(adventure === "workshop" || adventure === "online") && */}
          <div className="filters-sec">
            <div className="">
              <div className="filter-by-subheading">SKILLS LEVEL</div>
              <Radio.Group
                defaultValue={skill}
                onChange={(e) => [setSkill(e.target.value), setPageNo(1)]}
              >
                <Radio value="">All</Radio>
                <Radio value="0">Beginner</Radio>
                <Radio value="33">Medium</Radio>
                <Radio value="66">Advanced</Radio>
              </Radio.Group>
            </div>
          </div>
          {/* } */}

          {/* {(adventure === "trip") && */}
          <div className="filters-sec">
            <div className="filter-by-subheading">DIFFICULTY LEVEL</div>
            <Radio.Group
              defaultValue={difficulty}
              onChange={(e) => [setDifficulty(e.target.value), setPageNo(1)]}
            >
              <Radio value="">All</Radio>
              <Radio value="0">Light</Radio>
              <Radio value="25">Moderate</Radio>
              <Radio value="50">Difficult</Radio>
              <Radio value="75">Tough</Radio>
            </Radio.Group>
          </div>
          {/* } */}

          <div className="filters-sec">
            <div className="checkbox_bg">
              <div className="filter-by-subheading">SUITABLE FOR</div>
              <Checkbox.Group
                style={{ width: "100%" }}
                defaultValue={suitable}
                onChange={(e) => [setSuitable(e), setPageNo(1)]}
              >
                <Checkbox value="all">All</Checkbox>
                <Checkbox value="individual">Solo</Checkbox>
                <Checkbox value="groups">Groups</Checkbox>
                <Checkbox value="couples">Couples</Checkbox>
                <Checkbox value="families">Families</Checkbox>
              </Checkbox.Group>
            </div>
          </div>

          {/* {(adventure === "workshop" || adventure === "online") && */}
          <>
            <div className="filters-sec">
              <div className="filter-by-subheading">WORKSHOP TYPE</div>
              <Checkbox.Group
                style={{ width: "100%" }}
                defaultValue={workshopType}
                onChange={(e) => [setWorkshopType(e), setPageNo(1)]}
              >
                <Checkbox value="all">All</Checkbox>
                <Checkbox value="one-one">One-to-One</Checkbox>
                <Checkbox value="group">Group</Checkbox>
                <Checkbox value="customized">Customized</Checkbox>
              </Checkbox.Group>
            </div>

            <div className="filters-sec">
              <div className="filter-by-subheading">WORKSHOP MEDIUM</div>
              <Checkbox.Group
                style={{ width: "100%" }}
                defaultValue={workshopMedium}
                onChange={(e) => [setWorkshopMedium(e), setPageNo(1)]}
              >
                <Checkbox value="all">All</Checkbox>
                <Checkbox value="online">Online</Checkbox>
                <Checkbox value="classroom">Classroom</Checkbox>
                <Checkbox value="onsite">Onsite</Checkbox>
              </Checkbox.Group>
            </div>
          </>
          {/* } */}
        </div>
      </Col>

      <div style={{ clear: "both" }}></div>
      <div className="btn-apply">
        <Button onClick={(e) => handleApply()}>Apply</Button>
      </div>
    </div>
  );

  const onFlexiDateChange = (e) => setIsFlexible(e.target.checked);

  const DateContent = (
    <div className="months_div">
      <Select
        suffixIcon={
          <Icon
            type="caret-down"
            style={{ fontSize: "17px", color: "#000000" }}
          />
        }
        onChange={(e) => onYeadrChange(e)}
        placeholder="Select Year"
        showArrow="true"
        className="year-dropdwon"
        defaultValue={selectedYear}
      >
        {selectedYear === "" && (
          <Option key={"all"} value={""}>
            Select Year
          </Option>
        )}
        {selectedYear !== "" && (
          <Option key={"all1"} value={""}>
            All
          </Option>
        )}
        {yearsLength &&
          yearsLength.length > 0 &&
          yearsLength.map((year, index) => (
            <Option key={year} value={year}>
              {year}
            </Option>
          ))}
      </Select>
      <ul>
        <li
          onClick={() => onFilterChange("", "date")}
          value=""
          className={month.length === 0 ? "slected" : ""}
        >
          &nbsp;All
        </li>
        <li
          onClick={() => onFilterChange(`31-${selectedYear}-01-01`, "date")}
          className={
            currentMonth > 1 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`31-${selectedYear}-01-01`) >= 0
                ? "slected"
                : ""
          }
        >
          Jan
        </li>
        <li
          onClick={() => onFilterChange(`28-${selectedYear}-02-01`, "date")}
          className={
            currentMonth > 2 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`28-${selectedYear}-02-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Feb{" "}
        </li>
        <li
          onClick={() => onFilterChange(`31-${selectedYear}-03-01`, "date")}
          className={
            currentMonth > 3 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`31-${selectedYear}-03-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Mar{" "}
        </li>
        <li
          onClick={() => onFilterChange(`30-${selectedYear}-04-01`, "date")}
          className={
            currentMonth > 4 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`30-${selectedYear}-04-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Apr{" "}
        </li>
        <li
          onClick={() => onFilterChange(`31-${selectedYear}-05-01`, "date")}
          className={
            currentMonth > 5 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`31-${selectedYear}-05-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          May{" "}
        </li>
        <li
          onClick={() => onFilterChange(`30-${selectedYear}-06-01`, "date")}
          className={
            currentMonth > 6 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`30-${selectedYear}-06-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Jun{" "}
        </li>
        <li
          onClick={() => onFilterChange(`31-${selectedYear}-07-01`, "date")}
          className={
            currentMonth > 7 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`31-${selectedYear}-07-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Jul{" "}
        </li>
        <li
          onClick={() => onFilterChange(`31-${selectedYear}-08-01`, "date")}
          className={
            currentMonth > 8 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`31-${selectedYear}-08-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Aug{" "}
        </li>
        <li
          onClick={() => onFilterChange(`30-${selectedYear}-09-01`, "date")}
          className={
            currentMonth > 9 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`30-${selectedYear}-09-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Sep{" "}
        </li>
        <li
          onClick={() => onFilterChange(`31-${selectedYear}-10-01`, "date")}
          className={
            currentMonth > 10 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`31-${selectedYear}-10-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Oct{" "}
        </li>
        <li
          onClick={() => onFilterChange(`30-${selectedYear}-11-01`, "date")}
          className={
            currentMonth > 11 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`30-${selectedYear}-11-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Nov{" "}
        </li>
        <li
          onClick={() => onFilterChange(`31-${selectedYear}-12-01`, "date")}
          className={
            currentMonth > 12 && selectedYear <= currentYear
              ? "hidden-selected"
              : month.indexOf(`31-${selectedYear}-12-01`) >= 0
                ? "slected"
                : ""
          }
        >
          {" "}
          Dec{" "}
        </li>
      </ul>
      <div className="flexi-checkbox">
        <Checkbox
          checked={isFlexible}
          onChange={onFlexiDateChange}
          className=""
        >
          <span className="flexi-checkbox-txt">
            Show flexible trips/workshops
          </span>
        </Checkbox>
      </div>

      <div className="btn-apply">
        <Button onClick={(e) => handleApply()}>Apply</Button>
      </div>
    </div>
  );

  const handleSearch = async (e) => {
    tempTrips = [];
    tmpotherDataCount = 0;
    setOtherDataCount(0);
    setOtherPageNo(1);
    changeURL(sortBy);
    setInitialLoader(true);
    setTrips([]);
    setPageNo(1);
    refreshResult(sortBy);
    setCenterLatLong();
    //get coordinates of selected country
    const res = await axios.get(
      `https://maps.googleapis.com/maps/api/geocode/json?address=${country}&key=${ApiKey}`
    );
    if (res.data.results) {
      setCountryCoordinates(res.data.results[0].geometry.location);
    }
  };

  const handleApply = (e) => {
    if (month.length === 0) {
      setAllDateSelected(true);
    }
    tempTrips = [];
    tmpotherDataCount = 0;
    setOtherDataCount(0);
    setOtherPageNo(1);
    changeURL(sortBy);
    setInitialLoader(true);
    setTrips([]);
    setPageNo(1);
    setActiveFilter(true);
    refreshResult(sortBy);
    setdateVisible(false);
    setpriceVisible(false);
    setlangVisible(false);
    setfilterVisible(false);
    setTripLengthVisible(false);
  };

  const changeURL = (sortByData = "") => {
    let act = activity.length ? JSON.stringify(activity) : "";
    let lang = langauges.length ? JSON.stringify(langauges) : "";
    let wsType = workshopType.length
      ? !workshopType.includes("all")
        ? JSON.stringify(workshopType)
        : ""
      : "";
    let wMedium = workshopMedium.length
      ? !workshopMedium.includes("all")
        ? JSON.stringify(workshopMedium)
        : ""
      : "";
    let suitable1 = suitable.length
      ? !suitable.includes("all")
        ? JSON.stringify(suitable)
        : ""
      : "";

    router.push(
      `/all-adventure?langauges=${lang}&adventure=${adventure}&workshopMedium=${wMedium}&workshopType=${wsType}&country=${country}&skill=${skill}&activity=${act}&month=${month}&price=${price}&suitable=${suitable1}&difficulty=${difficulty}&sortBy=${sortByData}&isFlexible=${isFlexible}&year=${selectedYear}&weekend=${weekend}&certification=${certification}&longAdventure=${longAdventure}&tripLength=${tripLength}`
    );
  };

  const setCardMarkerHover = (id) => setHoverCardId(id);

  const resetCardMarkerHover = () => setHoverCardId("");

  const onChange = (checkedValues) => {
    setActivity(checkedValues);
    if (checkedValues.length === 1) {
      setActivityText(checkedValues[0]);
    } else if (checkedValues.length >= 2) {
      setActivityText(checkedValues[0] + " +" + (checkedValues.length - 1));
    } else {
      setActivityText("Select Activity");
    }
  };

  const onSearchChange = (e, type) => {
    if (e.target.value !== "") {
      if (type === "activity") {
        let filterActivity = ActivityList.filter((ac) => {
          return ac.name.toLowerCase().includes(e.target.value.toLowerCase());
        });
        setActivityList(filterActivity);
      }
    } else {
      setActivityList(ActivityList);
    }
  };

  const activityMenu = (
    <>
      {isLoaded && (
        <div>
          <div className="search-dropdown">
            <Input
              placeholder="Search langauge"
              allowClear
              onChange={(e) => onSearchChange(e, "activity")}
            />
          </div>
          <Checkbox.Group onChange={onChange} defaultValue={activity}>
            {activityDList.map((exp, i) => (
              <Menu key={i}>
                <Menu.Item>
                  <Checkbox key={i} value={exp.name}>
                    {exp.name}
                  </Checkbox>
                </Menu.Item>
              </Menu>
            ))}
          </Checkbox.Group>
        </div>
      )}
    </>
  );

  const findNearMe = async (key) => {
    tempTrips = [];
    setCurrentView("map");
    // setLoader(true);
    if (country !== "") {
      setCountry("");
      const allSearch = {
        activity,
        country: "",
        month,
        price,
        langauges,
        suitable,
        difficulty,
        sortBy: sortBy,
        skill,
        workshopType,
        workshopMedium,
        perPage,
        adventure,
        weekend,
        certification,
        longAdventure
      };
      refreshResultWithAllData(allSearch);
    }
    setCountryCoordinates();
    setCenterLatLong();

    if ("geolocation" in navigator) {
      if (window !== undefined) {
        window.navigator.geolocation.getCurrentPosition(async function (
          position
        ) {
          // console.log(position);
          setCenterLatLong({
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          });
        });
      }
    } else {
      alert("Please allow location for the map");
    }
    setLoader(false);

  };

  return (
    <>
      <div className="header-container adventure-page">
        <div className="container-fluid align-center">
          <div className="filter_sec_bg learning_sec">
            <div className="filter-section">
              <div className="filter-section-child">
                <Row gutter={0}>
                  <Col
                    xs={24}
                    sm={24}
                    md={7}
                    lg={7}
                    xl={7}
                    className="types right-border"
                  >
                    <div className="filter-type-img">
                      <img
                        src={'/images/adventurer.svg'}
                        alt="Filter Type"
                        className="group"
                      />
                    </div>
                    <div className="filter-type-right">
                      <div className="filter-sub-heading">Adventures</div>
                      <div className="filter-heading adventure-filter-text">
                        {isLoaded && (
                          <Select
                            suffixIcon={
                              <Icon
                                type="caret-down"
                                style={{ fontSize: "17px", color: "#000000" }}
                              />
                            }
                            onChange={(e) => onFilterChange(e, "adventureType")}
                            placeholder="All Adventures"
                            showArrow={true}
                            className="adventure-dropdown"
                            optionFilterProp="children"
                            defaultValue={adventure}
                          >
                            <Option key={"group"} value={"group"}>
                              All Adventures
                            </Option>
                            <Option key={"trip"} value={"trip"}>
                              Trips & Expeditions
                            </Option>
                            <Option key={"workshop"} value={"workshop"}>
                              Courses & Workshops
                            </Option>
                            <Option key={"online"} value={"online"}>
                              Online Adventures
                            </Option>
                          </Select>
                        )}
                      </div>
                    </div>
                  </Col>
                  <Col
                    xs={24}
                    sm={24}
                    md={7}
                    lg={7}
                    xl={7}
                    className="types right-border"
                  >
                    <div className="filter-type-img a_type">
                      <img src={'/images/group_activity.svg'} alt="Filter Type" className="type" />
                    </div>
                    <div className="filter-type-right">
                      <div className="filter-sub-heading">Activities</div>
                      {isLoaded && (
                        <Popover
                          placement="bottomLeft"
                          content={activityMenu}
                          overlayClassName="activity-dropdown"
                          trigger="click"
                          onVisibleChange={() => setRotateArrow(!rotateArrow)}
                        >
                          <div
                            className="filter-heading activity-filter-txt"
                            style={{ cursor: "pointer" }}
                          >
                            {activityText}
                            <span
                              className={`${rotateArrow
                                ? "rotate activity-drop-arrow"
                                : "activity-drop-arrow"
                                }`}
                              style={{ float: "right" }}
                            >
                              <svg
                                viewBox="0 0 1024 1024"
                                focusable="false"
                                className=""
                                data-icon="caret-down"
                                width="1em"
                                height="1em"
                                fill="currentColor"
                                aria-hidden="true"
                              >
                                <path d="M840.4 300H183.6c-19.7 0-30.7 20.8-18.5 35l328.4 380.8c9.4 10.9 27.5 10.9 37 0L858.9 335c12.2-14.2 1.2-35-18.5-35z"></path>
                              </svg>
                            </span>
                          </div>
                        </Popover>
                      )}
                    </div>
                  </Col>
                  <Col xs={24} sm={24} md={7} lg={7} xl={7} className="types">
                    <div className="filter-type-img">
                      <img src={'/images/Pin.svg'} alt="Filter Type" className="pin" />
                    </div>
                    <div className="filter-type-right">
                      <div className="filter-sub-heading">Country</div>
                      <div className="filter-heading">
                        {isLoaded && (
                          <Select
                            showSearch
                            showArrow={true}
                            onChange={(e) => [setCountry(e), setPageNo(1)]}
                            defaultValue={country}
                            placeholder="Select Country"
                            suffixIcon={
                              <Icon
                                type="caret-down"
                                style={{ fontSize: "17px", color: "#000000" }}
                              />
                            }
                          >
                            <Option value="">All</Option>
                            <OptGroup label="By Continent">

                              <Option key={"Europe"} value={"group|Europe"}>Europe</Option>
                              <Option key={"Africa"} value={"group|Africa"}>Africa</Option>
                              <Option key={"Asia"} value={"group|Asia"}>Asia</Option>
                              <Option key={"SouthAmerica"} value={"group|SouthAmerica"}>South America</Option>
                              <Option key={"NorthAmerica"} value={"group|NorthAmerica"}>North America</Option>
                              <Option key={"Antarctica"} value={"group|Antarctica"}>Antarctica</Option>
                              <Option key={"Oceania"} value={"group|Oceania"}>Oceania</Option>

                            </OptGroup>
                            <OptGroup label="By Country">
                              {countries.map((exp, i) => (
                                <Option key={i} value={exp.name}>
                                  {exp.name}
                                </Option>
                              ))}
                            </OptGroup>
                          </Select>
                        )}
                      </div>
                    </div>
                  </Col>
                  <Col
                    xs={24}
                    sm={24}
                    md={3}
                    lg={3}
                    xl={3}
                    className="search-parent"
                  >
                    <Button id="adventure_search" className="search-btn" onClick={() => handleSearch()}>
                      Search
                    </Button>
                  </Col>
                </Row>
              </div>
              <Row gutter={0}>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <div className="extra-filters">
                    <div className="extra-parent ">
                      <div className={`extra-child local-adventure`}>
                        <span>
                          <img
                            src={'/images/newicon/locator.svg'}
                            alt="Map Icon"
                            className="near_me_icon"
                          />
                        </span>
                        <span
                          onClick={findNearMe}
                          ref={inputEl}
                          id="findAdventureNearMe"
                        >
                          Find Adventures Near Me
                        </span>
                      </div>
                    </div>

                    <div className="extra-parent">
                      <div
                        className={`${activeFilter &&
                          ((month !== "" && allDateSelected === false) ||
                            isFlexible || selectedYear !== "")
                          ? "active extra-child"
                          : "extra-child"
                          }`}
                      >
                        {isLoaded && (
                          <Popover
                            placement="bottom"
                            content={DateContent}
                            visible={dateVisible}
                            onVisibleChange={(e) => setdateVisible(e)}
                            trigger="click"
                            overlayClassName="date-filter-popup"
                          >
                            Dates
                          </Popover>
                        )}
                      </div>
                    </div>

                    <div className="extra-parent">
                      <div
                        className={`${(tripLength !== "")
                          ? "active extra-child"
                          : "extra-child"
                          }`}
                      >
                        <Popover
                          placement="bottom"
                          content={tripLengthContent}
                          visible={tripLengthVisible}
                          onVisibleChange={(e) => setTripLengthVisible(e)}
                          trigger="click"
                          overlayClassName="filter"
                        >
                          Length
                        </Popover>

                      </div>
                    </div>
                    <div className="extra-parent">
                      {isLoaded && (
                        <div
                          className={`${activeFilter && price !== ""
                            ? "active extra-child"
                            : "extra-child"
                            }`}
                        >
                          <Popover
                            placement="bottom"
                            content={priceContent}
                            visible={priceVisible}
                            onVisibleChange={(e) => setpriceVisible(e)}
                            trigger="click"
                            overlayClassName="filter"
                          >
                            Price
                          </Popover>
                        </div>
                      )}
                    </div>
                    <div className="extra-parent">
                      {isLoaded && (
                        <div
                          className={`${activeFilter && langauges.length !== 0
                            ? "active extra-child"
                            : "extra-child"
                            }`}
                        >
                          <Popover
                            placement="bottom"
                            content={LanguageContent}
                            visible={langVisible}
                            onVisibleChange={(e) => setlangVisible(e)}
                            trigger="click"
                            overlayClassName="language-popup"
                          >
                            Langauges
                          </Popover>
                        </div>
                      )}
                    </div>

                    <div className="extra-parent">
                      <div
                        className={`${activeFilter &&
                          (skill !== "" ||
                            suitable.length > 0 ||
                            difficulty !== "" ||
                            workshopType.length > 0 ||
                            workshopMedium.length > 0)
                          ? "active extra-child no-border"
                          : "extra-child no-border"
                          }`}
                      >
                        {isLoaded && (
                          <Popover
                            placement="bottom"
                            content={advanceFilterContent}
                            trigger="click"
                            visible={filterVisible}
                            onVisibleChange={(e) => setfilterVisible(e)}
                            overlayClassName="advance-filter-popup"
                          >
                            FILTERS +
                          </Popover>
                        )}
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
            {/* <Row className="adventure-cataegory-parent">
              <div className="adventure-category">
                {ADVENTURE_CATEGORY.map((item) => {
                  return (
                    <Link to={item.link} onClick={() => changeURL()}>
                      <div className="category-name">{item.title}</div>
                    </Link>
                  )
                })}
              </div>
            </Row> */}
            {/* {!initialLoader &&
              <Row>
                <div className="new-card-design adventure-featured">
                  <FeaturedAdventures
                    item={featuredAdventure}
                    isPublicView={true}
                  />
                </div>
              </Row>
            } */}
            <Row gutter={0} className="border-bottom-1 mt25 pb20">
              <Col xs={24} sm={24} md={6} lg={6} xl={6} className="pt25 pr10 text-left sorted_by_select" >
                <div className="total_records">
                  {currentView === "grid" ? (totalRecords + otherDataCount) : mapTrips.length}{" "}
                  Results
                </div>
              </Col>

              <Col xs={24} sm={24} md={18} lg={18} xl={18} className="pt25 pr0 text-right sorted_by_select" >
                <div className="filter-view-section">
                  <div
                    className={`filter-view-child grid ${currentView === "grid" ? "active" : ""
                      }`}
                    onClick={() => {
                      setTotalRecords(tmpAllData.length);
                      setPerPage(
                        isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE
                      );
                      setCurrentView("grid");
                    }}
                  >
                    <img src={'/images/menu.svg'} alt="Menu Icon" />
                    Grid
                  </div>
                  <div
                    className={`filter-view-child mapview ${currentView === "map" ? "active" : ""
                      }`}
                    onClick={() => {
                      setTotalRecords(0);
                      setCurrentView("map");
                    }}
                  >
                    {" "}
                    <img src={'/images/map.svg'} alt="Map Icon" />
                    Map
                  </div>
                  <div className="filter-view-child sort-By">
                    SORT BY:
                    {isLoaded && (
                      <Select
                        defaultValue={sortBy}
                        onChange={(e) => onSortOrderChanged(e)}
                        className="pr0"
                      >
                        <Option value="distance">Distance</Option>
                        <Option value="asc">A-Z</Option>
                        <Option value="desc">Z-A</Option>
                      </Select>
                    )}
                  </div>
                </div>
              </Col>
            </Row>

            <Row gutter={0}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="web_grid new-card-design">
                <div className="" id="scroll-here">
                  <Row gutter={[20, 25]} style={{ marginTop: "10px" }}>
                    {initialLoader ? (
                      <div className="text-center py20 loader-absolute-class min-height">
                        <AppLoader />
                      </div>
                    ) : (
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <div>
                          {currentView === "grid" &&
                            trips.map((t, index) => {
                              return (
                                <Col
                                  xs={24}
                                  sm={24}
                                  md={12}
                                  lg={8}
                                  xl={8}
                                  key={index}
                                  className="gutter-row"
                                >
                                  <SingleCard
                                    t={t}
                                    index={index}
                                    type="directoryCard"
                                  />
                                </Col>
                              );
                            })}
                          {currentView === "map" && (
                            <>
                              <Row gutter={[20, 25]} className="map-view">
                                <Col
                                  xs={24}
                                  sm={24}
                                  md={8}
                                  lg={8}
                                  xl={8}
                                  className={`${isMaxWidth768
                                    ? "hide map-view-card"
                                    : "map-view-card"
                                    }`}
                                  style={{ paddingTop: "0px" }}
                                >
                                  <div>
                                    {loader ? (
                                      <div className="text-center">
                                        <AppLoader />
                                      </div>
                                    ) : (
                                      isMapLoading && (
                                        <div className="mb10 an-14 medium-text text-center mt20">
                                          <h4>
                                            Sorry, no results found for your
                                            selection. Please search another
                                            location on map or adjust the filters.
                                          </h4>
                                        </div>
                                      )
                                    )}

                                    {mapTrips
                                      ? mapTrips.map((t, index) => {
                                        return (
                                          <Col
                                            xs={24}
                                            sm={24}
                                            md={12}
                                            lg={24}
                                            xl={24}
                                            key={index}
                                            className={`${index === 0
                                              ? "ptop0 gutter-row"
                                              : "gutter-row"
                                              }`}
                                          >
                                            <SingleCard
                                              t={t}
                                              index={index}
                                              onMouseEnter={(id) =>
                                                setCardMarkerHover(id)
                                              }
                                              onMouseLeave={(id) =>
                                                resetCardMarkerHover(id)
                                              }
                                              type="mapCard"
                                            />
                                          </Col>
                                        );
                                      })
                                      : trips.map((t, index) => {
                                        return (
                                          <Col
                                            xs={24}
                                            sm={24}
                                            md={12}
                                            lg={24}
                                            xl={24}
                                            key={index}
                                            className={`${index === 0
                                              ? "ptop0 gutter-row"
                                              : "gutter-row"
                                              }`}
                                          >
                                            <SingleCard
                                              t={t}
                                              index={index}
                                              onMouseEnter={(id) =>
                                                setCardMarkerHover(id)
                                              }
                                              onMouseLeave={(id) =>
                                                resetCardMarkerHover(id)
                                              }
                                              type="mapCard"
                                            />
                                          </Col>
                                        );
                                      })}
                                  </div>
                                </Col>
                                <Col
                                  xs={24}
                                  sm={24}
                                  md={16}
                                  lg={16}
                                  xl={16}
                                  className="ptop0 padding-zero"
                                  style={{ paddingLeft: "0px" }}
                                >
                                  {/* {mapTrips && mapTrips.length > 0 && */}
                                  <>
                                    <div className="close-map">
                                      {" "}
                                      <Icon
                                        type="close-circle"
                                        onClick={() => setCurrentView("grid")}
                                      />
                                    </div>
                                    <AdventuresMap
                                      country={country}
                                      multiple
                                      locations={tmpAllData}
                                      zoom={5}
                                      ApiKey={ApiKey}
                                      height={650}
                                      hoveredCardId={hoveredCardId}
                                      myLocation={centerLatLong}
                                      setTripsOnMapChange={(boundedTrips) =>
                                        setTripsOnMapChange(boundedTrips)
                                      }
                                      countryCoordinates={countryCoordinates}
                                      map={"adventureMap"}
                                    />
                                  </>
                                  {/* } */}
                                </Col>
                              </Row>
                            </>
                          )}
                        </div>
                      </Col>
                    )}
                  </Row>
                </div>
              </Col>
            </Row>

            {!initialLoader && currentView !== "map" && (
              <Row gutter={0} className="mt25">
                <Row>
                  {loader ? (
                    <div className="text-center py20 loader-absolute-class">
                      <AppLoader />
                    </div>
                  ) : trips.length === 0 ? (
                    <div className="an-14 medium-text" style={{ marginBottom: "500px" }}>
                      <h4>Sorry, no results found</h4>
                    </div>
                  ) : (
                    <>
                      {trips.length >= totalRecords || trips.length < perPage ? (
                        // <div className="show-more no-more-result">SHOW MORE</div>
                        <>
                          {((tmpotherDataCount !== 0 && otherDataCount !== 0 && tmpotherDataCount >= otherDataCount) || adventure === "online") ?
                            (<div className="show-more no-more-result">NO MORE RESULTS</div>) :
                            (<div className="show-more" onClick={() => ShowMoreFromOtherCountry(otherPageNo)}>SHOW MORE RESULTS</div>)
                          }
                        </>
                      ) : (
                        <div
                          className="show-more"
                          onClick={() => clicktoShowMore(pageNo + 1)}
                        >
                          SHOW MORE
                        </div>
                      )}
                    </>
                  )}
                </Row>
              </Row>
            )}
          </div>
        </div>
      </div>
      <div className="why-choose-us-section">
        <ExpeditionsConnect className="pt60" />
      </div>
      <Newsletter page="adventure" />
    </>
  );
};

export default compose(Adventure);
