import React from 'react';
import dynamic from 'next/dynamic';
import { Provider } from "react-redux";
import { store, persistor } from '../src/redux/store';
import { PersistGate } from "redux-persist/integration/react";

const ExpertsMain = dynamic(
    () => import("../src/containers/ExpertsMain"),
    { ssr: false }
);

const Home = () => {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <ExpertsMain />
            </PersistGate>
        </Provider>

    )
}

export default Home;
