import React, { useEffect, useCallback, useState, Fragment } from 'react';
import { Row, Col, List, Avatar, Radio, Form, message, Select, Icon } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import ReactHtmlParser from 'react-html-parser';
import { compose } from 'redux';
import { useSelector, useDispatch } from 'react-redux';
import OwlCarousel from "react-owl-carousel";

import {
  CaptalizeFirst,
  formatDate,
  removeNewLines,
  commaSepratorString,
  DayorDaysNightOrNights,
  addSpaceAfterComma,
  getCityFromLocation,
  displayDifficultyText,
  getCurrencySymbol,
  getPriceType,
  getGroupSize,
  getPriceAfterDiscount,
  isPastDate,
  getSeasonText
} from '../helpers/methods';
import TripMap from '../components/Trips/Map';
import AppLoader from '../components/Loader';

import { getTripDetail, getMyAlbums } from '../services/expert';

import Reservation from '../components/Trips/Reservation';
import Interested from '../components/Trips/Interested';
import Cancellation from '../components/Trips/Cancellation';
import MoreDetails from '../components/Learning/Moredetails';
import AddPhoto from '../components/Trips/AddPhoto';

import AccommodationPhoto from '../components/Trips/AccommodationPhoto';
import AccommodationPhotoViewPopup from '../components/Trips/AccommodationPhotoView';
import ReactPlayer from 'react-player';
// import LikeAndShare from "../components/common/Likeandshare";
import { useMediaQuery } from "react-responsive";
import { ModalActions } from "../redux/models/events";
import AdditionalDetails from "../components/common/AdditionalDetails";
import AboutThisExpert from "../components/common/AboutThisExpert";
import CovidBanner from "../components/common/CovidBanner";
import Newsletter from '../components/common/NewsLetter';
import SuccessModal from "../components/Trips/commanModal";

// Image Import
const Placeholder = '/images/upload_image.png';
const Location_Img = '/images/country_ic.png';
const Skill = '/images/skill_ic_filled.png';
const Participate = '/images/participants_ic_filled.png';
const Clock = '/images/duration_ic_filled.png';
const Activity = '/images/activity_ic_filled.png';
const Speak = '/images/speaks_ic.png';

const { Option } = Select;
const ApiKey = process.env.REACT_APP_GOOGLE_MAP_API_KEY;

// Header Section
const data = [
  {
    title: 'Location',
    title3: 'Duration',
    title2: 'Difficulty level',
    title4: 'Activity',
    title5: 'Expedition Type',
  },
];

const sliderOptions = {
  className: "owl-theme accommodation_img_view slider_prev_next",
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
      nav: true,
      dotsEach: 3,
    },
    768: {
      items: 2,
      nav: true,
    },
    991: {
      items: 3,
      nav: true,
    },
  },
};


const TripsEditView = (props) => {
  const router = useRouter();
  const { id, contact } = router.query;
  // const id = props.id;
  // let currentParams = props.history.location.search;
  const dispatch = useDispatch();
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const token = useSelector(state => state.auth.accessToken);

  const [loader, setLoader] = useState(true);
  const [trip, setTrip] = useState(null);
  const [slot, setSlot] = useState(null);
  const isLogin = useSelector((state) => state.auth.isLogin);
  const role = useSelector((state) => state.auth.role);

  const [showCanc, setShowCanc] = useState(false);
  const [datePrice, setDatePrice] = useState('');
  const [datePriceCurrency, setDatePriceCurrency] = useState('');
  const [moreDetail, setMoreDetails] = useState(false);
  const [photoPopUp, setPhotoPopUp] = useState(false);
  const [showInterest, setShowInterest] = useState(contact ? true : false);
  const [showR, setShowR] = useState(false);
  const [accommoPopUp, setAccommoPopUp] = useState(false);
  const [accommodationPhotoViewPopup, setAccommodationPhotoViewPopup] = useState(false);
  const [currentViewImage, setCurrentViewImage] = useState(0);
  const [albums, setAlbums] = useState([]);
  const [currentPhotoView, setCurrentPhotoView] = useState("accomo");
  const [datePriceDiscount, setDatePriceDiscount] = useState("");
  const [defaultDateValue, setDefaultDateValue] = useState("");
  const [isDateSelect, setIsDateSelect] = useState("");
  const [loadDates, setloadDates] = useState(true);
  const [loadDatesDropdown, setloadDatesDropdown] = useState(true);
  const [review, setReview] = useState([]);
  const [isSuccess, setIsSuccess] = useState(false);

  const { openAuthModal } = ModalActions;

  const onCloseClick = () => {
    setShowR(false);
    setShowCanc(false);
    setMoreDetails(false);
    setPhotoPopUp(false);
    setAccommoPopUp(false);
    setAccommodationPhotoViewPopup(false);

  };

  const onInstCloseClick = (isThankYou = false) => {
    setIsSuccess(isThankYou);
    setShowInterest(false)
  };

  // react-hooks/exhaustive-deps
  const getData = useCallback(async (id) => {
    const result = await getTripDetail(id);

    if (result.status === 200) {
      let tripRes = result.data.data;
      setReview(result.data.review);
      tripRes.language = addSpaceAfterComma(tripRes.language);
      tripRes.inclusion = removeNewLines(tripRes.inclusion);
      tripRes.exclusion = removeNewLines(tripRes.exclusion);

      if (typeof tripRes.accomodation === "undefined" || tripRes.accomodation.trim() === "<p><br></p>") {
        tripRes.accomodation = ""
      }
      if (typeof tripRes.meetingPoint === "undefined" || tripRes.meetingPoint === "null") {
        tripRes.meetingPoint = "";
      }

      if (typeof tripRes.extras === "undefined" || tripRes.extras.trim() === "<p><br></p>" || tripRes.extras.trim() === "null" || tripRes.extras === "") {
        tripRes.extras = "";
      }

      if (typeof tripRes.itenary === "undefined" || (typeof tripRes.itenary !== "undefined" && tripRes.itenary.length > 0 && (typeof tripRes.itenary[0].value === "undefined" || tripRes.itenary[0].value === ""))) {
        tripRes.itenary = [];
      }

      tripRes.whatLearn = (tripRes.whatLearn && tripRes.whatLearn.trim() !== 'null' && tripRes.whatLearn.trim() !== '' && tripRes.whatLearn.trim() !== "<p><br></p>") ? tripRes.whatLearn : "";
      tripRes.attend = (tripRes.attend && tripRes.attend.trim() !== 'null' && tripRes.attend.trim() !== '' && tripRes.attend.trim() !== "<p><br></p>") ? tripRes.attend : "";

      tripRes.season = getSeasonText(tripRes.season);

      setTrip(tripRes);
      setLoader(false);
      // react-hooks/exhaustive-deps
      getAlbumData();
    }
    // react-hooks/exhaustive-deps
  }, []);

  const getAlbumData = useCallback(async () => {
    const result = await getMyAlbums(token);
    if (result.status === 200) {
      let finalList = [];
      if (result.data.data !== undefined && result.data.data.length > 0) {
        finalList = result.data.data.filter(item => item.isDelete === false)
      }
      setAlbums(finalList);
    }
    // react-hooks/exhaustive-deps
  }, []);

  // react-hooks/exhaustive-deps 
  useEffect(() => {
    setTimeout(() => {
      if (router.query.review === "true") {
        if (document.getElementById("scoll-to-here") !== null) {
          document
            .getElementById("scoll-to-here")
            .scrollIntoView({ block: "start", behavior: "smooth" });
        }
      }
    }, 2000);
  }, []);

  const selectDate = (date, price, priceCurrency, discount, isRadioClicked = false) => {
    setDatePriceDiscount(discount);
    setSlot(date);
    setDatePrice(price);
    setDatePriceCurrency(priceCurrency);
    if (isRadioClicked) {
      setloadDatesDropdown(false);
      setTimeout(() => {
        setloadDatesDropdown(true);
      }, 10);
    }
  };

  const handleDateChange = (d) => {

    if (d !== "") {
      let seprateData = d.split("|");
      selectDate(seprateData[0], seprateData[1], seprateData[2], seprateData[3]);
    } else {
      setSlot(null);
      setDatePrice(trip.price);
      setDatePriceCurrency(trip.priceCurrency);
      setDatePriceDiscount(trip.discount);
    }
    if (isDateSelect) {
      setloadDates(false);
      setTimeout(() => {
        setloadDates(true);
        setIsDateSelect(false)
      }, 1);
    }
    setDefaultDateValue(d);
  }

  const onIntseretClick = () => {
    setShowInterest(true);

    // if (isLogin) {
    //   if (role !== 'enthusiasts') {
    //     return message.error(
    //       'You must be logged in as enthusiasts to send request',
    //     );
    //   }
    //   setShowInterest(true);
    // } else {
    //   localStorage.setItem("referrer", window.location.pathname);
    //   dispatch(openAuthModal(true));
    //   // message.error('Please login to make reservation request');
    // }
  };

  const onReserveClick = () => {
    if (!slot) {
      return message.error(
        'Please select date slot to make reservation request',
      );
    }
    if (isLogin) {
      if (role !== 'enthusiasts') {
        return message.error(
          'You must be logged in as enthusiasts to send request',
        );
      }
      setShowR(true);
    } else {
      localStorage.setItem("referrer", window.location.pathname);
      dispatch(openAuthModal(true));
      // message.error('Please login to make reservation request');
    }
  };

  const onClickDate = (d) => {
    if (!slot) {
      return message.error(
        'Please select date slot to make reservation request',
      );
    }
    if (isLogin) {
      if (role !== 'enthusiasts') {
        return message.error(
          'You must be logged in as enthusiasts to send request',
        );
      }
      selectDate(d.fromDate, d.price, d.priceCurrency, d.discount);
      setShowR(true);
    } else {
      localStorage.setItem("referrer", window.location.pathname);
      dispatch(openAuthModal(true));
      // message.error('Please login to make reservation request');
    }

  }

  useEffect(() => {
    getData(id);
  }, [getData, id]);

  useEffect(() => {
    getAlbumData();
  }, [getAlbumData, token]);


  const addPhoto = () => {
    setPhotoPopUp(true);
  }

  const addAccommoPopUp = () => {
    setAccommoPopUp(true);
  }

  const onAccommoImageClick = (index, view) => {
    setCurrentPhotoView(view);
    setCurrentViewImage(index);
    setAccommodationPhotoViewPopup(true);
  };

  const getRefreshTrip = async () => {
    setLoader(true);
    const result = await getTripDetail(id);
    if (result.status === 200) {
      setTrip(result.data.data);
      setLoader(false);
    }
  }

  if (loader) {
    return (
      <div className='text-center py20 loader-absolute-class'>
        <AppLoader />
      </div>
    );
  } else {
    return (
      <div className='header-container w_bg set_trip_mobile trip_mobile_view'>
        <CovidBanner />
        <Row gutter={20}>
          <Col span={18}></Col>
          <Col span={6}>
            <div className=" text-right edt_btn_sec btn-edit-banner-top">
              <Link href={`/update-trip/${id}`}>
                <a><i className="fas fa-pencil-alt"></i> Edit</a>
              </Link>
            </div>
          </Col>
        </Row>
        {!isMaxWidth768 ? (
          <div
            className="gallery_bg"
            style={{ backgroundImage: `url(${trip.cover})` }}
          >
          </div>
        ) : (
          <div
            className="edit-trip-work-img"
          >
            <img src={trip.cover} alt="cover"></img>
          </div>
        )}

        <div className='container align-center section_mobile'>
          <div className='expedition_bg tripedit_view'>
            <div className='gallery_sec section_mobile fix_to_top'>
              <Row gutter={20}>
                <Col xs={24} sm={24} md={17} lg={17} xl={17} className="first-col-edit">
                  <h1 className="an-36 text_skill edit-title">
                    {trip.title}
                  </h1>
                  {(trip.country !== 'undefined' && trip.country !== "") && (
                    <h4 className="an-18 work_title trip_border_btm">
                      <Avatar src={Location_Img} className="location_pin" />{" "}
                      <span className="cover-location-title">{getCityFromLocation(trip.address)}{CaptalizeFirst(trip.country)}</span>
                    </h4>
                  )}
                </Col>
                <Col xs={24} sm={24} md={17} lg={17} xl={17} className="cover_img_data cover-img-icon first-col-edit">
                  <Row gutter={20}>
                    <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                      <List
                        itemLayout='horizontal'
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={<Avatar src={Clock} />}
                              title='Duration'
                              className='pl5'
                              description={`${trip.duration} ${DayorDaysNightOrNights('t', trip.duration, trip.durationType)}`}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={10} lg={10} xl={10}>
                      <List
                        itemLayout='horizontal'
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={<Avatar src={Activity} />}
                              title='Activity'
                              description={commaSepratorString(trip.activity)}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                      <List
                        itemLayout='horizontal'
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={<Avatar src={Speak} />}
                              title='Language'
                              description={`${trip.language}`}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                      <List
                        itemLayout='horizontal'
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={
                                <Avatar
                                  src={Skill}
                                  className='pl5 fill-width'
                                />
                              }
                              title='Difficulty Level'
                              // description={difficultyLevelDisplay(trip.difficulty)}
                              description={displayDifficultyText(trip.difficulty)}
                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col xs={14} sm={12} md={10} lg={10} xl={10} className="trip_cover_detail">
                      <List
                        itemLayout='horizontal'
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={
                                <Avatar
                                  src={Participate}
                                  className='pl5 fill-width'
                                />
                              }
                              title='Group Size'
                              description={getGroupSize(trip.participants, trip.groupType)}

                            />
                          </List.Item>
                        )}
                      />
                    </Col>
                    {typeof trip.season !== "undefined" && trip.season !== "" &&
                      <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                        <List
                          itemLayout="horizontal"
                          dataSource={data}
                          renderItem={(item) => (
                            <List.Item>
                              <List.Item.Meta
                                avatar={
                                  <Avatar
                                    src={Clock}
                                    className="pl5 fill-width"
                                  />
                                }
                                title="Season"
                                description={trip.season}
                              />
                            </List.Item>
                          )}
                        />
                      </Col>
                    }

                  </Row>
                  {/* <Row>
                    <Col span={24}>
                      <div className='text-right edt_btn_sec header_edit'>
                        <Link to={`/update-trip/${id}`}>
                          <i className='fas fa-pencil-alt'></i> Edit
                        </Link>
                      </div>
                    </Col>
                  </Row> */}
                </Col>
                <Col xs={24} sm={24} md={7} lg={7} xl={7} className="trip_reser_card first-col-none select_date_popup">
                  <div className='select_date_popup_child'>
                    <Row className="header_per_txt work-trip-detail-page">
                      <Col span={18}>
                        <h2 className="an-30 medium-text mb5">
                          <span className="after-discount">{getCurrencySymbol(datePriceCurrency || trip.priceCurrency)} {getPriceAfterDiscount(datePrice || trip.price, trip.discount && slot === null ? trip.discount : datePriceDiscount || "")}</span>
                          {((typeof trip.discount !== "undefined" && trip.discount !== "" && trip.discount !== null && slot === null) || (datePriceDiscount !== "" && datePriceDiscount !== null && datePriceDiscount !== "null" && datePriceDiscount)) &&
                            <span className="orignal-price">{getCurrencySymbol(datePriceCurrency || trip.priceCurrency)} {datePrice || trip.price}</span>
                          }
                        </h2>
                        <p className="mb10 an-14 medium-text">{getPriceType(trip.priceType)}</p>
                      </Col>
                      <Col span={6} className="edit-date-parent">
                        <div className="edt_btn_sec">
                          <Link href={`/update-trip/${id}`}>
                            <a><i className="fas fa-pencil-alt"></i> Edit</a>
                          </Link>
                        </div>
                      </Col>
                      {/* <LikeAndShare allLikes={trip.likes} id={trip._id} pageType={"trip"} designType="single" /> */}
                    </Row>
                    <Row className="date-button-section">
                      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <div className="select_date mt10">
                          {trip.dateTime.length > 0 ? (
                            <Fragment>
                              <div className="select-date-text pt5 fill-width mb10">
                                {trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).length > 0 ? <span className="flexible-txt">Select Date</span> : <span className="flexible-txt">This trip is outdated. Please add new date to reactivate.</span>}
                              </div>
                            </Fragment>
                          ) : (
                            <p className="an-14 medium-text pt10 pb10 fill-width flexible-txt">
                              This trip has flexible dates. Please contact expert for more
                              information.
                            </p>
                          )}
                        </div>
                      </Col>
                      <Col>
                        {trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).length > 0 && (
                          <Row className="trip_date_group">
                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                              <Form.Item>
                                {loadDates &&
                                  <Radio.Group initialValue={0}>
                                    {trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).map((d, i) => {
                                      if (i === 0 || i === 1 || i === 2) {
                                        return (
                                          <span className="date-radio">
                                            <Radio.Button
                                              key={d._id}
                                              value={i}
                                              className={`${!isPastDate(d.fromDate) ? 'disabled' : ''} radio-no-${i}`}
                                              disabled={!isPastDate(d.fromDate)}
                                              onClick={() => {
                                                setDefaultDateValue("");
                                                setIsDateSelect(true)
                                                selectDate(d.fromDate, d.price, d.priceCurrency, d.discount, true);
                                              }
                                              }
                                            >
                                              {formatDate(d.fromDate)}
                                            </Radio.Button>
                                          </span>
                                        );

                                      } else {
                                        return null;
                                      }
                                    })}
                                  </Radio.Group>}
                                {!loadDates &&
                                  <Radio.Group initialValue={0}>
                                    {trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).map((d, i) => {
                                      if (i === 0 || i === 1 || i === 2) {
                                        return (
                                          <span className="date-radio">
                                            <Radio.Button
                                              key={d._id}
                                              value={i}
                                              className={`${!isPastDate(d.fromDate) ? 'disabled' : ''}`}
                                              disabled={!isPastDate(d.fromDate)}
                                            >
                                              {formatDate(d.fromDate)}
                                            </Radio.Button>
                                          </span>
                                        );

                                      } else {
                                        return null;
                                      }
                                    })}
                                  </Radio.Group>}

                              </Form.Item>
                            </Col>
                          </Row>
                        )}
                        {trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).length > 3 && (
                          <Row className="mb25">
                            {loadDatesDropdown &&
                              <Select defaultValue={defaultDateValue} style={{ width: "100%" }} onChange={(e) => handleDateChange(e)} dropdownClassName="select-date-dropdpwn">
                                <Option value="">View More Dates</Option>
                                {trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).map((d, key) => (
                                  <Option className={`${!isPastDate(d.fromDate) ? 'disabled' : ''}`} disabled={!isPastDate(d.fromDate)} value={`${d.fromDate}|${d.price}|${d.priceCurrency}|${d.discount}`}><span className="date-dropdown">{formatDate(d.fromDate)}</span></Option>
                                ))}
                              </Select>}
                            {!loadDatesDropdown &&
                              <Select defaultValue={""} style={{ width: "100%" }} onChange={(e) => handleDateChange(e)} dropdownClassName="select-date-dropdpwn">
                                <Option value="">View More Dates</Option>
                              </Select>}
                          </Row>
                        )}
                        <div className="booking-btn">
                          <span onClick={onReserveClick} className="date-yellow mr10 cursor-pointer" id="trip_reserve" ><Icon type="calendar" />&nbsp;&nbsp;RESERVE</span>
                          <span onClick={onIntseretClick} className="cursor-pointer date-green" id="trip_get_in_touch"><Icon type="phone" rotate={90} />&nbsp;&nbsp;GET IN TOUCH</span>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </div>
            <Row gutter={20} className={`${trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).length < 1 ? 'pt60 trip_view_edit_detail' : trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).length > 0 && trip.dateTime.filter((dd) => isPastDate(dd.fromDate)).length < 4 ? 'pt50 trip_view_edit_detail' : ' trip_view_edit_detail'}`}>
              <Col className="trip_desc_detail justify " xs={24} sm={24} md={17} lg={17} xl={17}>
                <Row>
                  <Col span={24}>
                    <h3 className=' an-22 medium-text pb10 heading_trip_detail'>Trip Highlights</h3>
                  </Col>
                  <Col span={18}>
                    <div className='trip_detail_des'>
                      <h4 className='medium-text an-18'>About This Trip</h4>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className=' text-right edt_btn_sec'>
                      <Link href={`/update-trip/${id}`}>
                        <a><i className='fas fa-pencil-alt'></i> Edit</a>
                      </Link>
                    </div>
                  </Col>
                </Row>
                <div className='trip_detail_des editor_text_display'>
                  <span className='lh-30'>
                    {/* {console.log('trip.description => ', trip.description.split("\r"))}
                    {ReactHtmlParser(trip.description)} */}
                    {trip.description && trip.description.trim() !== 'null'
                      ? ReactHtmlParser(trip.description)
                      : ''}
                  </span>
                </div>

                <Row>
                  <Col span={18}>
                    <div className='trip_detail_des'>
                      <h4 className='medium-text an-18'>What will you learn?</h4>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className=' text-right edt_btn_sec'>
                      <Link href={`/update-trip/${id}`}>
                        <a><i className='fas fa-pencil-alt'></i> Edit</a>
                      </Link>
                    </div>
                  </Col>
                </Row>

                <div className='trip_detail_des editor_text_display'>
                  <span className='lh-30'>
                    {trip.whatLearn.trim() !== '' ? ReactHtmlParser(trip.whatLearn) : ''}
                    {trip.whatLearn.trim() === "" && "No Details has found"}
                  </span>
                </div>

                <Row>
                  <Col span={18}>
                    <div className='trip_detail_des'>
                      <h4 className='medium-text an-18'>Who should attend?</h4>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className=' text-right edt_btn_sec'>
                      <Link href={`/update-trip/${id}`}>
                        <a><i className='fas fa-pencil-alt'></i> Edit</a>
                      </Link>
                    </div>
                  </Col>
                </Row>

                <div className='trip_detail_des editor_text_display'>
                  <span className='lh-30'>
                    {trip.attend.trim() !== '' ? ReactHtmlParser(trip.attend) : ''}
                    {trip.attend.trim() === '' && "No Details has found"}
                  </span>
                </div>
              </Col>

              <Col xs={24} sm={24} md={7} lg={7} xl={7} className="prl-0">
                <AboutThisExpert type="trip" expert={trip.expert} />
              </Col>
            </Row>
            <Row>
              <Col className='metting_point_trip' xs={24} sm={24} md={17} lg={17} xl={17}>
                <Row className='pt20 pr30'>
                  <Col span={18}>
                    <div className='trip_detail_des location-field'>
                      <h4 className='medium-text an-18'>Location</h4>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className=' text-right edt_btn_sec'>
                      <Link href={`/update-trip/${id}`}>
                        <a><i className='fas fa-pencil-alt'></i> Edit</a>
                      </Link>
                    </div>
                  </Col>
                  <Col span={20}>

                    <p className="pt5 mb0 trip_location_des">
                      {CaptalizeFirst(trip.address)}
                    </p>

                  </Col>

                  <Col span={16} className="trip_meeting_point">
                    <div className=''>
                      <h4 className='medium-text an-18 meting-text'>Meeting Point</h4>
                      <p className='lh-30 desc'>{trip.meetingPoint || "No meeting point given."}</p>
                    </div>
                  </Col>

                </Row>
                <Row className='pt0 map_trip_edit'>
                  <Col span={24} className='map'>
                    <TripMap
                      center={trip.location.coordinates}
                      zoom={5}
                      ApiKey={ApiKey}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row className="extra_info_trip justify mt40">
              <Col xs={24} sm={24} md={17} lg={17} xl={17}>
                <Col span={18} className="">
                  <div className=''>
                    <h4 className='medium-text an-18 pt35 pb25'>
                      Trip Details
                    </h4>
                  </div>
                </Col>
                <Col span={6}>
                  <div className=' text-right edt_btn_sec pt35 pb25'>
                    <Link href={`/update-trip/${id}`}>
                      <a><i className='fas fa-pencil-alt'></i> Edit</a>
                    </Link>
                  </div>
                </Col>
              </Col>
            </Row>

            <AdditionalDetails
              item={trip}
              type={"trip"}
              isEdit={true}
              addAccommoPopUp={addAccommoPopUp}
              onAccommoImageClick={onAccommoImageClick}
              review={review}
            />

            <Row className="trip_prize_section justify mt75 mb75">
              <Col xs={24} sm={24} md={17} lg={17} xl={17} >
                <div className='person_sec'>
                  <div className='fill-width'>
                    <h1 className="medium-text an-28 mb0">
                      <span className="after-discount">{getCurrencySymbol(datePriceCurrency || trip.priceCurrency)} {getPriceAfterDiscount(datePrice || trip.price, trip.discount && slot === null ? trip.discount : datePriceDiscount || "")}</span>
                      {((typeof trip.discount !== "undefined" && trip.discount !== "" && trip.discount !== null && slot === null) || (datePriceDiscount !== "" && datePriceDiscount !== null && datePriceDiscount !== "null" && datePriceDiscount)) &&
                        <span className="orignal-price">{getCurrencySymbol(datePriceCurrency || trip.priceCurrency)} {datePrice || trip.price}</span>
                      }
                    </h1>
                    <p className='an-14 medium-text'>{getPriceType(trip.priceType)}</p>
                  </div>
                  <div className='fill-width text-right booking-btn-detail-page'>
                    <span onClick={onReserveClick} className="date-yellow mr10 cursor-pointer" id="trip_reserve" ><Icon type="calendar" />&nbsp;&nbsp;RESERVE</span>
                    <span onClick={onIntseretClick} className="cursor-pointer date-green" id="trip_get_in_touch"><Icon type="phone" rotate={90} />&nbsp;&nbsp;GET IN TOUCH</span>
                  </div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col className="trip_photoset">
                <div className='photo_sec'>
                  <Row>
                    <Col>
                      <Row className="pb20">
                        <Col span={12}>
                          <h4 className='sub_title'>
                            Pictures and Videos from Past Trips
                          </h4>
                        </Col>
                        <Col className="fix_adit_button" span={12}>
                          <div className='fix_top_pad text-right edt_btn_sec adit_fix' onClick={addPhoto}>
                            <i className='fas fa-pencil-alt'></i> Add Photos
                          </div>
                        </Col>
                      </Row>
                      <Row gutter={[40]}>
                        <OwlCarousel
                          {...sliderOptions}
                        >
                          {trip.images.length > 0 && trip.images.map((img, index) => {
                            return (

                              < Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row pb25" >
                                {
                                  img.search(".mp4") !== -1 ?

                                    (<ReactPlayer className="video_edit_page" url={img} onClick={() => onAccommoImageClick(index, 'photos')} />)

                                    : (<img src={img} alt="" onClick={() => onAccommoImageClick(index, 'photos')} />)
                                }
                              </Col>
                            )
                          })}
                          {trip.images.length === 0 &&
                            <>
                              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row pb25">
                                <img src={Placeholder} alt="" />
                              </Col>
                              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row pb25">
                                <img src={Placeholder} alt="" />
                              </Col>
                              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row pb25">
                                <img src={Placeholder} alt="" />
                              </Col>
                            </>
                          }

                        </OwlCarousel>

                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
        </div>
        {
          moreDetail && (
            <MoreDetails
              visible={moreDetail}
              data={trip.dateTime}
              onIntseretClick={onIntseretClick}
              onReserveClick={onReserveClick}
              onClickDate={(d) => onClickDate(d)}
              onCloseClick={onCloseClick}
            />
          )
        }
        {
          showCanc && (
            <Cancellation
              visible={showCanc}
              data={trip.cancellations}
              onCloseClick={onCloseClick}
            />
          )
        }
        {
          showR && (
            <Reservation
              visible={showR}
              trip={trip.title}
              slot={slot}
              onCloseClick={onCloseClick}
              id={id}
            />
          )
        }
        {
          showInterest && (
            <Interested
              visible={showInterest}
              trip={trip.title}
              onCloseClick={onInstCloseClick}
              id={id}
              type="trip"
            />
          )
        }
        {
          photoPopUp && (
            <AddPhoto
              visible={photoPopUp}
              onCloseClick={onCloseClick}
              albumList={albums}
              id={id}
              currentImages={trip.images}
              getRefreshTrip={getRefreshTrip}
              type="trip"
            />
          )
        }
        {
          accommoPopUp && (
            <AccommodationPhoto
              visible={accommoPopUp}
              onCloseClick={onCloseClick}
              id={id}
              getRefreshTrip={getRefreshTrip}
              type="trip"
              allPhotos={trip.accomodationPhotos}
              selectedAlbum={trip.accomodationPhotos}
            />
          )
        }

        {
          accommodationPhotoViewPopup && (
            <AccommodationPhotoViewPopup
              visible={accommodationPhotoViewPopup}
              onCloseClick={onCloseClick}
              id={id}
              allPhotos={currentPhotoView === "accomo" ? trip.accomodationPhotos : trip.images}
              currentViewImage={currentViewImage}
            />
          )
        }

        <Newsletter page="trip_view" />

        {isSuccess &&
          <SuccessModal
            onClose={() => setIsSuccess(false)}
            isSuccess={isSuccess}
          />
        }

      </div >
    );
  }
};
export default compose(TripsEditView);
