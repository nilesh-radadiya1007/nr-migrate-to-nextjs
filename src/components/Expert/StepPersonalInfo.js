import React, { useState, useEffect, useCallback } from 'react';
import { Form, Input, Row, Col, Select, Button } from "antd";
import { useSelector, useDispatch } from 'react-redux';


import { getBase64 } from '../../helpers/methods';
import countries from '../../helpers/countries';
import langauges from '../../helpers/langauges';
import { expertiseList } from '../../helpers/constants';
import { ActivityList } from '../../helpers/constants';
import { ExpertEvents } from '../../redux/expert/events';
import { FirstNameValidator, LastNameValidator } from '../../helpers/validations';
import ImageUplaoderAndCropper from '../../shared/profile-cover/image-cropper';
import { uploadProfileOriginalPicture } from '../../services/expert';
import { LoadingOutlined } from '@ant-design/icons';
import PlacesAutocomplete, { getLatLng, geocodeByAddress } from "react-places-autocomplete";
// import ImageCrop from '../../components/ImageCrop';

const uploadIcon = '/images/upload-icon.png';


const { Option } = Select;

const CreateExpertStepPersonalInfo = (props) => {
  const [loader, setLoader] = useState(false)
  const token = useSelector((state) => state.auth.accessToken);
  const { getFieldDecorator, setFieldsValue } = props.form;
  const [imageUrl, setImageUrl] = useState();
  const [latLng, setLatLng] = useState([]);
  const { picture, originalPicture, firstName, lastName, country, city, experties, speaks, skills, isEditMode, coordinates } = useSelector(state => state.expert);
  const [imageCover, setImageCover] = useState(originalPicture);
  const { stepPersonalInfo } = ExpertEvents;
  const dispatch = useDispatch();
  const [imageCoverObject, setImageCoverObject] = useState(undefined);
  const [imageOriginalCoverObject, setImageOriginalCoverObject] = useState(undefined);
  const [isImageChanged, setIsImageChanged] = useState(false);
  useEffect(() => {
    if (originalPicture !== null) {
      setImageCover(originalPicture)
    }
  }, [originalPicture])


  const setPicture = useCallback(
    (picture) => {
      if (picture !== null && picture !== undefined) {
        if (typeof picture === "string") {
          setImageUrl(picture);
        } else {
          getBase64(picture, (imageUrl) => {
            setImageUrl(imageUrl);
          });
        }
      }
      if (originalPicture !== null && originalPicture !== undefined) {
        if (typeof originalPicture !== "string") {
          getBase64(originalPicture, (imageUrl) => {
            setImageCover(imageUrl);
          });
        }
      }
    }, [isEditMode])

  useEffect(() => {
    setPicture(picture);
    setFieldsValue({
      picture: picture,
      originalPicture: originalPicture,
      firstName: firstName,
      lastName: lastName,
      experties: experties,
      speaks: speaks,
      country: country,
      city: city,
      skills: skills
    });
    setLatLng(coordinates);
  }, [country, city, experties, firstName, lastName, picture, setFieldsValue, setPicture, speaks, skills, coordinates, originalPicture]);

  const onCityChange = city => setFieldsValue({ city });

  const onSelectCity = city => {
    setFieldsValue({ city })
    geocodeByAddress(city)
      .then((results) => getLatLng(results[0]))
      .then((ll) => setLatLng(ll))
      .catch((error) => console.error("Error", error));

  };

  const uploadOriginalCover = useCallback(
    async (data) => {
      try {
        const formData = new FormData();
        if (data !== "") {
          formData.append("profile", data);
          if (imageOriginalCoverObject)
            formData.append("originalProfile", imageOriginalCoverObject);
        } else {
          formData.append("profile", "");
          formData.append("originalProfile", "");
        }
        const result = await uploadProfileOriginalPicture(token, formData);
      } catch (err) {
      }
    },
    [token, imageOriginalCoverObject]
  );

  const handleFormSubmit = e => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        setLoader(true);
        if (isImageChanged) {
          if (token) {
            if (imageCoverObject) {
              await uploadOriginalCover(imageCoverObject);
            } else {
              await uploadOriginalCover("");
            }
          }

          if (imageOriginalCoverObject) {
            values = { ...values, originalPicture: imageOriginalCoverObject }
          } else {
            values = { ...values, originalPicture: imageCover }
          }
          if (imageCoverObject) {
            values = { ...values, picture: imageCoverObject }
          }
        } else if (imageUrl) {
          values = { ...values, picture: imageUrl, originalPicture: imageCover }
        }
        window.scrollTo({ top: document.getElementById('scroll-to-here'), behavior: 'smooth' });
        values.location = latLng ? [latLng.lng ? latLng.lng : latLng[0], latLng.lat ? latLng.lat : latLng[1]] : [10.4515, 51.1657];
        dispatch(stepPersonalInfo({ ...values }));
        setLoader(false);
        setIsImageChanged(false);
      }
    });
  };


  const setCoverImage = (coverFile, originalFile) => {
    setIsImageChanged(true);
    if (coverFile && coverFile !== "") {
      if (originalFile) {
        setImageOriginalCoverObject(originalFile);
        if (typeof originalFile !== "string") {
          getBase64(originalFile, (imageUrl) => {
            setImageCover(imageUrl);
          });
        }
      }
      setImageCoverObject(coverFile);
    } else {
      setImageOriginalCoverObject("");
      setImageCoverObject("");
    }
  }



  return (
    <div className="step-1-expert-form height_sec profile-crop">
      <div className="an-20 medium-text success--text step-title">
        Create Expert Profile <span className="an-16">  Personal Info</span>
      </div>
      <div className="an-16 regular-text pt10">
        Please fill in the details below to create your expert profile
      </div>
      <div className="profile-picture-style">
        <img
          className={imageUrl ? `profile-img` : `profile-img-default`}
          src={imageUrl || uploadIcon}
          alt="cover"
          style={{
            width: imageUrl ? "100%" : "",
            height: imageUrl ? "100%" : "",
            objectFit: "fill",
          }}
        />
      </div>
      <Form className="ant-advanced-search-form" onSubmit={handleFormSubmit}>
        <div className="form-profile-container ">
          <div className="pr20">
            <Form.Item label="" className="mt20 mb20">
              <div className="profile-image-reposition">
                <ImageUplaoderAndCropper
                  imageUrl={imageUrl}
                  setCoverImage={(file, originalFile) => setCoverImage(file, originalFile)}
                  originalCover={imageCover}
                  setProfileImage={(evt) => setImageUrl(evt)}
                  page="profile"
                />
              </div>
            </Form.Item>
          </div>
          <div className="profile-stp1-info">
            <div className="pt20">
              <Row gutter={24}>
                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="First Name">
                    {getFieldDecorator("firstName", FirstNameValidator)
                      (<Input placeholder="Enter First Name" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Last Name">
                    {getFieldDecorator("lastName", LastNameValidator)
                      (<Input placeholder="Enter Last Name" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Country">
                    {getFieldDecorator("country", { rules: [{ required: true, message: "Please select your country!" }] })
                      (<Select showSearch showArrow placeholder="Select Your Country">
                        {countries.map((con, i) => <Option key={i} value={con.name}>{con.name}</Option>)}
                      </Select>)}
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item
                    label='City, Location'
                  >
                    {getFieldDecorator("city", { initialValue: city, rules: [{ required: true, message: "Please enter your city!" }] })
                      (<PlacesAutocomplete
                        onChange={onCityChange}
                        onSelect={onSelectCity}
                      >
                        {({
                          getInputProps,
                          suggestions,
                          getSuggestionItemProps,
                          loading,
                        }) => (
                          <div>
                            <Input
                              {...getInputProps({
                                placeholder: 'Search Places ...',
                                className: 'location-search-input',
                              })}
                            />
                            <div className='autocomplete-dropdown-container'>
                              {loading && (
                                <div style={{ marginTop: 20 }}>Loading...</div>
                              )}
                              {suggestions.map((suggestion, i) => {
                                const className = suggestion.active
                                  ? 'suggestion-item--active'
                                  : 'suggestion-item';
                                return (
                                  <div key={i}
                                    {...getSuggestionItemProps(suggestion, {
                                      className,
                                    })}
                                  >
                                    <span>{suggestion.description}</span>
                                    <br />
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                        )}
                      </PlacesAutocomplete>)}
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Languages">
                    {getFieldDecorator("speaks", { rules: [{ required: true, message: "Please select your Languages!" }] })
                      (<Select mode="multiple" showArrow placeholder="Select Your Languages">
                        {langauges.map((lang, i) => <Option key={i} value={lang.name}>{lang.name}</Option>)}
                      </Select>)}
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                  <Form.Item label="Expertise">
                    {getFieldDecorator("experties", { rules: [{ required: true, message: "Please select your expertise!" }] })
                      (<Select mode="multiple" showSearch showArrow placeholder="Select Your Expertise">
                        {expertiseList.map((exp, i) => <Option key={i} value={exp.name}>{exp.name}</Option>)}
                      </Select>)}
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <Form.Item label="Skills">
                    {getFieldDecorator("skills", { rules: [{ required: true, message: "Please enter your skills!" }] })
                      (<Select mode="tags" showSearch placeholder="Enter Your Skills">
                        {ActivityList.map((exp, i) => <Option key={i} value={exp.name}>{exp.name}</Option>)}
                      </Select>)}
                  </Form.Item>
                </Col>
              </Row>
            </div>
          </div>
        </div>
        <Form.Item className="mb0 pt25 mt30 next-bck-stp3">
          <Button type="primary" htmlType="submit" className="ex__primary_btn">
            Next {loader && <LoadingOutlined />}
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

const WrappedCreateExpertStepPersonalInfo = Form.create({ name: "createProfile" })(CreateExpertStepPersonalInfo);

export default WrappedCreateExpertStepPersonalInfo;
