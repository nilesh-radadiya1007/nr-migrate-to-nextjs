import React from "react";
import { Icon } from "antd";
import { useDispatch, useSelector } from "react-redux";

import CreateExpertStepPersonalInfo from "../components/Expert/StepPersonalInfo";
import CreateExpertStepBusinessDetails from "../components/Expert/StepBusinessDetails";
import CreateExpertStepAwardsAndBio from "../components/Expert/StepAwardsAndBio";
import CreateExpertSuccess from "../components/Expert/CreateExpertSuccess";
import UpdateExpertSuccess from "../components/Expert/UpdateExpertSuccess";
import { useMediaQuery } from "react-responsive";
import { ExpertEvents } from "../redux/expert/events";

const CreateExpert = () => {
  const {
    tab,
    stepPersonalInfo,
    stepBusinessDetails,
    stepAwardsAndBio,
  } = useSelector((state) => state.expert);
  const showSuccess = useSelector((state) => state.modal.successModel);
  const updateModal = useSelector((state) => state.modal.updateModal);
  const tabClass =
    "tab-grid an-15 medium-text dark-text flex-x align-center space-between";
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const dispatch = useDispatch();
  const { changeTab } = ExpertEvents;

  const handleTabChange = (tab) => {
    dispatch(changeTab(tab));
  };

  return (
    <>
      {!isMaxWidth768 && (
        <div className="create-expert-container flex-x container py30">
          <div className="create-profile-tab">
            <div className={`${tabClass} ${tab === 1 ? "active" : ""}`} onClick={() => handleTabChange(1)}>
              Personal Info {stepPersonalInfo ? <Icon type="check" /> : ""}
            </div>
            <div className={`${tabClass} ${tab === 2 ? "active" : ""}`} onClick={() =>{stepAwardsAndBio ? handleTabChange(2) : alert("Please fill out the all info and click Next") }}>
              Awards & Recognition
              {stepAwardsAndBio ? <Icon type="check" /> : ""}
            </div>
            <div className={`${tabClass} ${tab === 3 ? "active" : ""}`} onClick={() => {stepBusinessDetails ? handleTabChange(3) : alert("Please fill out the all info and click Next") }}>
              Business Details
              {stepBusinessDetails ? <Icon type="check" /> : ""}
            </div>
          </div>
          <div className="create-profile-routes">
            {tab === 1 && <CreateExpertStepPersonalInfo />}
            {tab === 2 && <CreateExpertStepAwardsAndBio />}
            {tab === 3 && <CreateExpertStepBusinessDetails />}
          </div>
          {showSuccess && <CreateExpertSuccess visible={showSuccess} />}
          {updateModal && <UpdateExpertSuccess visible={updateModal} />}
        </div>
      )}

      {isMaxWidth768 && (
        <div
          className="create-expert-container container py30 expert-edit-create-mobile"
          id="scroll-to-here"
        >
          <div className="create-profile-tab">
            <div
              className={`${tabClass} ${tab === 1 ? "active" : ""
                }`}
              onClick={() => handleTabChange(1)}
            >
              Personal Info {stepPersonalInfo ? <Icon type="check" /> : ""}
            </div>

            <div
              className={`${tabClass} ${tab === 2 ? "active" : ""
                }`}
                onClick={() => {stepAwardsAndBio ? handleTabChange(2) : alert("Please fill out the all info and click Next") }}
            >
              Awards & Recognition
              {stepAwardsAndBio ? <Icon type="check" /> : ""}
            </div>
            <div
              className={`${tabClass} ${tab === 3 ? "active" : ""
                }`}
                onClick={() => {stepBusinessDetails ? handleTabChange(3) : alert("Please fill out the all info and click Next") }}
            >
              Business Details
              {stepBusinessDetails ? <Icon type="check" /> : ""}
            </div>
          </div>
          <div className="create-profile-routes">
            {tab === 1 && <CreateExpertStepPersonalInfo />}
            {tab === 2 && <CreateExpertStepAwardsAndBio />}
            {tab === 3 && <CreateExpertStepBusinessDetails />}
          </div>
          {showSuccess && <CreateExpertSuccess visible={showSuccess} />}
          {updateModal && <UpdateExpertSuccess visible={updateModal} />}
        </div>
      )}
    </>
  );
};

export default CreateExpert;
