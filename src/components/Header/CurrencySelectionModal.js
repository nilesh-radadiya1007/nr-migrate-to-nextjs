import React, { useState } from "react";
import { Select, Icon } from "antd";
import Cookies from "universal-cookie";
import { useSelector, useDispatch } from "react-redux";
import {
  setPreferredCurrency,
  getCurrencies,
} from "../../helpers/methods";
import { updatePreferredCurrency } from "../../services/auth";
import { AuthActions } from "../../redux/auth/events";
// import USDFlag from "../../../public/images/flag/usdflag.png";
// import EURFlag from "../../../public/images/flag/euro.png";
// import GBPFlag from "../../../public/images/flag/britishpount.png";
// import Image from 'next/image';


const { Option } = Select;
const CurrencySelection = (props) => {
  const [visible, setVisible] = useState(false);
  const dispatch = useDispatch();
  const { isLogin } = useSelector((state) => state.auth);
  const { setPreferedCurrency } = AuthActions;

  const onChangeCurrency = async (currency) => {
    
    setPreferredCurrency(currency);
    dispatch(setPreferedCurrency(currency));
    if (isLogin) {
      await updatePreferredCurrency(currency);
    }
    setVisible(false);
    window.location.reload();
  };

  const currencies = getCurrencies();
  const popularCurrencies = getCurrencies(true);

  const cookies = new Cookies();

  const preferredCurrency = cookies.get("preferredCurrency") || "USD";

  const showModal = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  return (
    <>
      <Select
        name="currency"
        style={{ width: 110 }}
        optionFilterProp="children"
        defaultValue={preferredCurrency || "USD"}
        onChange={(e) => onChangeCurrency(e)}
        dropdownClassName="currency-selection-select"
        suffixIcon={<Icon role="caret-down" type="caret-down" />}
        role="list"
      >
        <Option value="GBP"><img className="flag-img" src={"/images/flag/britishpount.svg"} alt=""/>GBP</Option>
        <Option value="EUR"><img className="flag-img" src={"/images/flag/euro.svg"} alt="" />EUR</Option>
        <Option value="USD"><img className="flag-img" src={"/images/flag/usdflag.svg"} alt="" />USD</Option>
      </Select>
      {/* <Button
        onClick={showModal}
        className="medium-text an-16 currency-selection__button"
      >
        {getCurrencySymbol(preferredCurrency, true) || preferredCurrency}
      </Button>
      <Modal
        centered
        title="Select Your Preferred Currency"
        width={"70vw"}
        footer={null}
        closable={true}
        onCancel={handleCancel}
        visible={visible}
      >
        <div className="currency-selection-container">
          <div className="section__header">Popular Currencies</div>
          {popularCurrencies
            .map((currency) => {
              return (
                <div className="currency-item" key={currency.alpha3Code}>
                  <div
                    className="currency"
                    onClick={() => onChangeCurrency(currency.currencyCode)}
                  >
                    <span className="currency__text">
                      {currency.currencySymbol || currency.currencyCode}
                    </span>
                    <span className="currency__secondary-text">
                      {currency.currencyName}
                    </span>
                  </div>
                </div>
              );
            })}
          <div className="section__header">All Currencies</div>
          {currencies
            .map((currency) => {
              return (
                <div className="currency-item" key={currency.alpha3Code}>
                  <div
                    className="currency"
                    onClick={() => onChangeCurrency(currency.currencyCode)}
                  >
                    <span className="currency__text">
                      {currency.currencySymbol || currency.currencyCode}
                    </span>
                    <span className="currency__secondary-text">
                      {currency.currencyName}
                    </span>
                  </div>
                </div>
              );
            })}
        </div>
      </Modal> */}
    </>
  );
};

export default CurrencySelection;
