import React, { useEffect, useCallback, useState, Fragment } from 'react';
// import { withRouter } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { compose } from 'redux';
import { Row, Col, Select, Radio, Button, Pagination } from 'antd';
// import Image from 'next/image';

import {
  CopyTrips,
  CopyLearning,
  GetMyTrips,
  GetLearningMy,
  InActiveTrip,
  InActiveWorkshop,
  ActiveTrip,
  ActiveWorkshop,
} from '../../services/expert';
import AppLoader from '../../components/Loader';
import Map from '../../components/Trips/Map';
import TripsWorkshopCardView from './TripsWorkshopCardView';
import SuccessModel from '../../components/modal';
import { ModalActions } from "../../redux/models/events";
import { useRouter } from 'next/router';

const Grid = '/images/expeditions_directory/grid_disable_ic.png';
const Pin = '/images/expeditions_directory/pin_disable.png';


const ApiKey = process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY;
const { Option } = Select;
const {
  openApprovalModal,
} = ModalActions;

const TripsWorkshopView = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const {
    view,
    token,
    publicTrip,
    publicWorkShop,
    isPublicView,
    travelMap,
  } = props;
  const [upcomingTrip, setUpcomingTrip] = useState([]);
  const [allTrip, setAllTrip] = useState([]);
  const [loader, setLoader] = useState(false);
  const [activeUpcomingTrip, setactiveUpcomingTrip] = useState(false);
  const [activePastTrip, setactivePastTrip] = useState(false);
  const [activeUpcomingWorkshop, setactiveUpcomingWorkshop] = useState(false);
  const [activePastWorkshop, setactivePastWorkshop] = useState(false);
  const [workshops, setWorkshops] = useState([]);
  const [allWorkshops, setAllWorkshops] = useState([]);
  const [pageMaxValue, setPageMaxValue] = useState(9);
  const [pageOffset, setPageOffset] = useState(0);
  const [pageLimit, setPageLimit] = useState(9);
  const [isMapView, setIsMapView] = useState(false);
  const [updateModal, setUpdateModal] = useState(false);
  const [message, setMessage] = useState('');
  const { isLogin, role } = useSelector((state) => state.auth);
  const [current, setCurrent] = useState(1);
  const [copyId, setCopyId] = useState("");
  const [copyType, setCopyType] = useState("");
  const { approved } = useSelector((state) => state.expert);


  const getTrips = useCallback(async (token) => {
    // setLoader(true);
    if (publicTrip && publicTrip.length > 0) {
      setUpcomingTrip(publicTrip);
      setAllTrip(publicTrip);
      setLoader(false);
    } else {
      const result = await GetMyTrips(token);
      if (result.status === 200) {
        setUpcomingTrip(result.data.data);
        setAllTrip(result.data.data);
        setLoader(false);
      }
    }
  }, [publicTrip]);

  const getWorkshops = useCallback(async (token) => {
    if (publicWorkShop && publicWorkShop.length > 0) {
      setWorkshops(publicWorkShop);
      setAllWorkshops(publicWorkShop);
    } else {
      const result = await GetLearningMy(token);
      if (result.status === 200) {
        setWorkshops(result.data.data);
        setAllWorkshops(result.data.data);
      }
    }
  }, [publicWorkShop]);

  useEffect(() => {
    if (!publicWorkShop) {
      getTrips(token);
    }
    if (!publicTrip) {
      getWorkshops(token);
    }
  }, [getTrips, getWorkshops, publicTrip, publicWorkShop, token]);

  const handleChange = (value) => {
    setCurrent(value);
    setPageOffset((value * pageMaxValue) - pageMaxValue);
    setPageLimit((value * pageMaxValue))
  };

  const showSizehandleChange = (value, size) => {
    setPageMaxValue(size);

    setPageOffset((1 * size) - size);
    setPageLimit((1 * size))
    setCurrent(1);
  };

  function itemRender(current, type, originalElement) {
    if (type === 'prev') {
      return <a>Prev</a>;
    }
    if (type === 'next') {
      return <a>Next</a>;
    }
    return originalElement;
  }

  const getUpcomingTrip = () => {
    let finalTrip = [];
    setCurrent(1);
    setPageOffset((1 * pageMaxValue) - pageMaxValue);
    setPageLimit((1 * pageMaxValue))

    if (view === 'trips' && activeUpcomingTrip === false) {
      setactiveUpcomingTrip(true);
      setactivePastTrip(false);
      allTrip.forEach((tripData, index) => {
        let tripDetail = tripData._doc ? tripData._doc : tripData;
        if (tripDetail.dateTime.length > 0) {
          if (
            tripDetail.dateType === 2 ||
            new Date(tripDetail.dateTime[0].fromDate) > new Date()
          ) {
            finalTrip.push(tripData);
          }
        }
        if (tripDetail.dateType === 2) {
          finalTrip.push(tripData);
        }
      });
      setUpcomingTrip(finalTrip);
    } else {
      setactiveUpcomingTrip(false);
      setUpcomingTrip(allTrip);
    }
  };

  const getUpcomingWorkshop = () => {
    let finalWorkshop = [];
    setCurrent(1);
    setPageOffset((1 * pageMaxValue) - pageMaxValue);
    setPageLimit((1 * pageMaxValue))
    if (view === 'workshop' && activeUpcomingWorkshop === false) {
      setactiveUpcomingWorkshop(true);
      setactivePastWorkshop(false);
      allWorkshops.forEach((workshopData, index) => {
        if (workshopData.dateTime.length > 0) {
          if (
            new Date(workshopData.dateTime[0].fromDate) > new Date() ||
            workshopData.dateType === 2
          ) {
            finalWorkshop.push(workshopData);
          }
        }
        if (workshopData.dateType === 2) {
          finalWorkshop.push(workshopData);
        }
      });
      setWorkshops(finalWorkshop);
    } else {
      setactiveUpcomingWorkshop(false);
      setWorkshops(allWorkshops);
    }
  };

  const getPastTrip = () => {
    setCurrent(1);
    setPageOffset((1 * pageMaxValue) - pageMaxValue);
    setPageLimit((1 * pageMaxValue));

    let finalTrip = [];
    if (view === 'trips' && activePastTrip === false) {
      setactivePastTrip(true);
      setactiveUpcomingTrip(false);
      allTrip.forEach((tripData) => {
        let dd = tripData._doc ? tripData._doc : tripData;
        if (dd.dateTime.length > 0) {
          if (
            tripData._doc
              ? tripData._doc.dateTime[0].fromDate.length > 0
              : tripData.dateTime.length > 0
          ) {
            if (
              new Date(
                tripData._doc
                  ? tripData._doc.dateTime[0].fromDate
                  : tripData.dateTime[0].fromDate,
              ) < new Date()
            ) {
              finalTrip.push(tripData);
            }
          }
        }
      });
      setUpcomingTrip(finalTrip);
    } else {
      setactivePastTrip(false);
      setUpcomingTrip(allTrip);
    }
  };

  const getPastWorkshop = () => {
    setCurrent(1);
    setPageOffset((1 * pageMaxValue) - pageMaxValue);
    setPageLimit((1 * pageMaxValue))
    let finalWorkshop = [];
    if (view === 'workshop' && activePastWorkshop === false) {
      setactivePastWorkshop(true);
      setactiveUpcomingWorkshop(false);
      allWorkshops.forEach((workshopData, index) => {
        if (workshopData.dateTime.length > 0) {
          if (new Date(workshopData.dateTime[0].fromDate) < new Date()) {
            finalWorkshop.push(workshopData);
          }
        }
      });
      setWorkshops(finalWorkshop);
    } else {
      setactivePastWorkshop(false);
      setWorkshops(allWorkshops);
    }
  };

  const aTozSort = (option) => {
    if (view === 'trips') {
      if (option == 1) {
        upcomingTrip.sort(function (a, b) {
          if (a._doc.title < b._doc.title) {
            return -1;
          }
          if (a._doc.title > b._doc.title) {
            return 1;
          }
          return 0;
        });
        setUpcomingTrip([...upcomingTrip]);
      } else {
        upcomingTrip.sort(function (a, b) {
          if (a._doc.title > b._doc.title) {
            return -1;
          }
          if (a._doc.title < b._doc.title) {
            return 1;
          }
          return 0;
        });
        setUpcomingTrip([...upcomingTrip]);
      }
    } else {
      if (option == 1) {
        workshops.sort(function (a, b) {
          if (a.title < b.title) {
            return -1;
          }
          if (a.title > b.title) {
            return 1;
          }
          return 0;
        });
        setWorkshops([...workshops]);
      } else {
        workshops.sort(function (a, b) {
          if (a.title > b.title) {
            return -1;
          }
          if (a.title < b.title) {
            return 1;
          }
          return 0;
        });
        setWorkshops([...workshops]);
      }
    }
  };

  const handleChangeType = (typeVal) => {
    if (typeVal === '1') {
      if (view === 'trips') {
        getUpcomingTrip();
      } else {
        getUpcomingWorkshop();
      }
    } else if (typeVal === '-1') {
      if (view === 'trips') {
        getPastTrip();
      } else {
        getPastWorkshop();
      }
    } else {
      if (view === 'trips') {
        setUpcomingTrip(allTrip);
      } else {
        setWorkshops(allWorkshops);
      }
    }
  };

  const inActiveTrip = async (id) => {
    const result = await InActiveTrip(token, id);
    if (result) {
      getTrips(token);
    }
  };

  const inActiveWorkshop = async (id) => {
    const result = await InActiveWorkshop(token, id);
    if (result) {
      getWorkshops(token);
    }
  };

  const activeTrip = async (id) => {
    const result = await ActiveTrip(token, id);
    if (result) {
      getTrips(token);
    }
  };

  const activeWorkshop = async (id) => {
    const result = await ActiveWorkshop(token, id);
    if (result) {
      getWorkshops(token);
    }
  };

  const onChange = (e) => {
    if (e.target.value === 'grid') {
      setIsMapView(false);
    } else if (e.target.value === 'map') {
      setIsMapView(true);
    }
  };

  const copyTrip = async (d) => {
    let formData = new FormData();
    formData.append('id', d.id);
    let result = await CopyTrips(token, formData);

    if (result && result.status === 201) {
      setCopyId(result.data.data.id);
      setCopyType("trip");
      setMessage(`Trip copied successfully with name ${result.data.data.title}.`)
      getTrips(token);
      setUpdateModal(true);
    }
  };

  const copyWorkShop = async (workshop) => {
    let formData = new FormData();
    formData.append('id', workshop.id);
    let result = await CopyLearning(token, formData);
    if (result && result.status === 201) {

      setCopyId(result.data.data._id);
      setCopyType("workshop");
      setMessage(`Workshop copied successfully with name ${result.data.data.title}.`);
      getWorkshops(token);
      setUpdateModal(true);
    }
  };

  const checkActive = (type) => {
    if (type === "trip") {
      if (!approved) {
        dispatch(openApprovalModal("trip"));
      } else {
        router.push('/create-trips')
      }
    } else if (type === "workshop") {
      if (!approved) {
        dispatch(openApprovalModal("workshop"));
      } else {
        router.push('/create-learnings')
      }

    }
  }

  if (upcomingTrip) {
    return (
      <Fragment>
        <div className='container-fluid align-center '>
          <div className='filter_sec_bg learning_sec  top_trip'>
            <Row gutter={40} className='trip_detail'>
              {view === 'trips' ? (
                <Col
                  xs={24}
                  sm={24}
                  md={16}
                  lg={5}
                  xl={7}
                  className={`first_trip ${isPublicView && 'title_set'}`}
                >
                  <div className='expidition_bg'>
                    <h4 className='sub_title'>Trips</h4>
                  </div>
                </Col>
              ) : (
                  <Col
                    xs={24}
                    sm={24}
                    md={16}
                    lg={5}
                    xl={5}
                    className={`first_trip ${
                      isPublicView && 'title_set_workshop'
                      }`}
                  >
                    <div className='expidition_bg'>
                      <h4 className='sub_title'>Workshops</h4>
                    </div>
                  </Col>
                )}
              {view === 'trips' ? (
                <Col
                  className='trip_button pt20'
                  xs={24}
                  sm={24}
                  md={16}
                  lg={9}
                  xl={7}
                >
                  <Button
                    type='primary'
                    size='default'
                    shape='round'
                    className={`upcoming_button ${
                      activeUpcomingTrip && 'active_upcoming_trip'
                      }`}
                    onClick={getUpcomingTrip}
                  >
                    Upcoming Trips
                  </Button>
                  <Button
                    type='primary'
                    size='default'
                    shape='round'
                    className={`${activePastTrip && 'active_past_trip'}`}
                    onClick={getPastTrip}
                  >
                    Past Trips
                  </Button>
                </Col>
              ) : (
                  <Col
                    className='trip_button pt20'
                    xs={24}
                    sm={24}
                    md={16}
                    lg={9}
                    xl={8}
                  >
                    <Button
                      type='primary'
                      size='default'
                      shape='round'
                      className={`upcoming_button ${
                        activeUpcomingWorkshop && 'active_upcoming_workshop'
                        }`}
                      onClick={() => getUpcomingWorkshop()}
                    >
                      Upcoming Workshops
                  </Button>
                    <Button
                      type='primary'
                      size='default'
                      shape='round'
                      className={`${
                        activePastWorkshop && 'active_past_workshop'
                        }`}
                      onClick={() => getPastWorkshop()}
                    >
                      Past Workshops
                  </Button>
                  </Col>
                )}
              <Col xs={24} sm={24} md={16} lg={4} xl={5} className='trip_short mt10'>
                <Select
                  defaultValue='Sort by'
                  onChange={aTozSort}
                  style={{ width: 200 }}
                  className='pr20'
                >
                  <Option value='1'>A-Z</Option>
                  <Option value='-1'>Z-A</Option>
                </Select>
              </Col>
              <Col
                xs={24}
                sm={24}
                md={16}
                lg={4}
                xl={4}
                className='trip_short_by mt10'
              >
                <Select
                  defaultValue={'0'}
                  onChange={handleChangeType}
                  style={{ width: 200 }}
                  className='pr20'
                >
                  <Option value='0'>{`${
                    view === 'trips' ? 'All Trips' : 'All Workshops'
                    }`}</Option>
                  <Option value='1'>{`Upcoming ${
                    view === 'trips' ? 'Trips' : 'Workshops'
                    }`}</Option>
                  <Option value='-1'>{`Past ${
                    view === 'trips' ? 'Trips' : 'Workshops'
                    }`}</Option>
                </Select>
              </Col>
              {view === 'trips' ? (
                isLogin && role === 'expert' && isPublicView === false ? (
                  <Col
                    xs={24}
                    sm={24}
                    md={8}
                    lg={6}
                    xl={5}
                    className='filter_tab mt10'
                  >
                    <Button
                      className='view-more-trips an-20 mt10 button_set'
                      onClick={() => checkActive('trip')}
                    >
                      Create Trip
                    </Button>
                  </Col>
                ) : (
                    <Col
                      xs={24}
                      sm={24}
                      md={8}
                      lg={6}
                      xl={4}
                      className='filter_tab text-right grid_fixed mt10'
                    >
                      <Radio.Group
                        defaultValue='grid'
                        buttonStyle='solid'
                        className='grid_set'
                        onChange={onChange}
                      >
                        <Radio.Button value='grid'>
                          <img src={Grid} alt='grid' />
                        </Radio.Button>
                        <Radio.Button value='map'>
                          <img src={Pin} alt='map' />
                        </Radio.Button>
                      </Radio.Group>
                    </Col>
                  )
              ) : isLogin && role === 'expert' && isPublicView === false ? (
                <Col
                  xs={24}
                  sm={24}
                  md={8}
                  lg={6}
                  xl={6}
                  className='filter_tab mt10'
                >
                  <Button
                    className='view-more-trips an-20 mt10 button_set'
                    onClick={() => checkActive('workshop')}
                  >
                    Create Workshop
                  </Button>
                </Col>
              ) : (
                    <Col
                      xs={24}
                      sm={24}
                      md={8}
                      lg={6}
                      xl={4}
                      className='filter_tab mt10'
                    >
                      <Radio.Group
                        defaultValue='grid'
                        buttonStyle='solid'
                        className='grid_set'
                        onChange={onChange}
                      >
                        <Radio.Button value='grid'>
                          <img src={Grid} alt='grid' />
                        </Radio.Button>
                        <Radio.Button value='map'>
                          <img src={Pin} alt='pin' />
                        </Radio.Button>
                      </Radio.Group>
                    </Col>
                  )}
            </Row>
            <Row gutter={[40, 16]}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className='expidition_bg expendition'>
                  <Row gutter={[15, 25]}>
                    {loader ? (
                      <div className='text-center py20 loader-absolute-class'>
                        <AppLoader />
                      </div>
                    ) : view === 'trips' ? (
                      upcomingTrip.length === 0 ? (
                        <div className='mb10 an-14 medium-text text-center mt100'>
                          <h4>Currently, there are no Trips</h4>
                        </div>
                      ) : isMapView ? (
                        travelMap &&
                        travelMap.length > 0 && (
                          <div>
                            {/* <h4 className="sub_title">Travel Map</h4> */}
                            <Row gutter={[40, 16]}>
                              <Map
                                multiple
                                travelMap={travelMap}
                                zoom={2}
                                ApiKey={ApiKey}
                              />
                            </Row>
                          </div>
                        )
                      ) : (
                            <TripsWorkshopCardView
                              upcomingTrip={upcomingTrip}
                              view={view}
                              pageMaxValue={pageLimit}
                              pageMinValue={pageOffset}
                              isLogin={isLogin}
                              inActiveTrip={(d) => inActiveTrip(d)}
                              activeTrip={(d) => activeTrip(d)}
                              isPublicView={isPublicView}
                              travelMap={travelMap}
                              copyTrip={(d) => copyTrip(d)}
                              role={role}
                            />
                          )
                    ) : workshops.length === 0 ? (
                      <div className='mb10 an-14 medium-text text-center mt100'>
                        <h4>Currently, there are no Workshops</h4>
                      </div>
                    ) : isMapView ? (
                      travelMap &&
                      travelMap.length > 0 && (
                        <div>
                          {/* <h4 className="sub_title">Travel Map</h4> */}
                          <Row gutter={[40, 16]}>
                            <Map
                              multiple
                              travelMap={travelMap}
                              zoom={2}
                              ApiKey={ApiKey}
                            />
                          </Row>
                        </div>
                      )
                    ) : (
                              <TripsWorkshopCardView
                                upcomingTrip={workshops}
                                view={view}
                                pageMaxValue={pageLimit}
                                pageMinValue={pageOffset}
                                isLogin={isLogin}
                                activeWorkshop={(d) => activeWorkshop(d)}
                                inActiveWorkshop={(l) => inActiveWorkshop(l)}
                                isPublicView={isPublicView}
                                travelMap={travelMap}
                                role={role}
                                copyWorkShop={(workshop) => copyWorkShop(workshop)}
                              />
                            )}
                  </Row>
                </div>
              </Col>
              <div className='pagination_trip'>
                {upcomingTrip.length > 0 || workshops.length > 0 ? (
                  <Pagination
                    showSizeChanger
                    onShowSizeChange={showSizehandleChange}
                    total={
                      view === 'trips' ? upcomingTrip.length : workshops.length
                    }
                    defaultPageSize={9}
                    defaultCurrent={1}
                    responsive={true}
                    itemRender={itemRender}
                    onChange={handleChange}
                    current={current}
                  />
                ) : (
                    ''
                  )}
              </div>
              {role === "expert" &&
                <Col>
                  {travelMap && travelMap.length > 0 && (
                    <div>
                      {/* <h4 className="sub_title">Travel Map</h4> */}
                      <Row>
                        <Map
                          multiple
                          travelMap={travelMap}
                          zoom={2}
                          ApiKey={ApiKey}
                        />
                      </Row>
                    </div>)
                  }
                </Col>
              }
            </Row>
          </div>
        </div>
        {updateModal && (
          <SuccessModel
            visible={updateModal}
            title={'Successfully copied'}
            message={message}
            onClose={() => setUpdateModal(false)}
            redirect={false}
            copyId={copyId}
            copyType={copyType}
          />
        )}
      </Fragment>
    );
  } else {
    return null;
  }
};

export default compose(TripsWorkshopView);
