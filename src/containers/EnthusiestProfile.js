import React, { useState, useCallback, useEffect } from 'react';
import { Row, Col, Tabs, Icon, Upload, message, Button, Dropdown, Menu } from "antd";
import { useSelector, useDispatch } from 'react-redux';
import { compose } from "redux";
import { useRouter } from 'next/router';

// Image Import

import ReactHtmlParser from "react-html-parser";
// import Image from 'next/image';



import { CaptalizeFirst, getBase64 } from '../helpers/methods';
import {
  uploadCoverPictureEnth, getAllRecentTrips,
  getAllLearnings, getAllAlbums, uploadCoverOriginalPictureEthn
} from '../services/expert';
import { EnthuEvents } from '../redux/enthu/events';

import ImageUplaoderAndCropper from '../shared/profile-cover/image-cropper';

const Location_Img = "/images/country_ic.png";
const ProfileHolder = '/images/placeholder.png'
const Speaks_Img = "/images/speaks_ic.png";
const Share_Ic = "/images/share_ic.png";
const AddUser_Ic = "/images/add-user.svg";
const Facebook = "/images/fb_ic.png";
const WhatsApp = "/images/whatsapp_ic.png";
const Twitter = "/images/twitter_ic.png";
const Upload_Img = "/images/upload_image.png";


const { TabPane } = Tabs;

const menu = (
  <Menu className="share_btn_box">
    <Menu.Item>
      <img src={Facebook} alt="facebook" className="pr10" />Facebook
    </Menu.Item>
    <Menu.Item>
      <img src={WhatsApp} alt="twitter" className="pr10" />WhatsApp
    </Menu.Item>
    <Menu.Item>
      <img src={Twitter} alt="twitter" className="pr10" />Twitter
    </Menu.Item>
  </Menu>
);

const EnthusiestProfile = (props) => {
  // let { match: { params: { id } } } = props;
  const router = useRouter()
  const { id } = router.query;
  // console.log("id", id);

  const profile = useSelector(state => state.enthu);

  const token = useSelector(state => state.auth.accessToken);
  const [uploadingLoader, setUploadingLoader] = useState(false)
  const [removingLoader, setRemovingLoader] = useState(false)
  const [upcomingTrip, setUpcomingTrip] = useState([]);
  const [upcomingWorkShop, setUpcomingWorkShop] = useState([]);
  const [albumsData, setAlbumsData] = useState([]);
  const dispatch = useDispatch();

  const { changeTab, isEditMode, updateCover } = EnthuEvents;

  const getAllTripsHandler = useCallback(async () => {
    try {
      const result = await getAllRecentTrips();
      if (result.status === 200) {
        const data = result.data.data.filter((trip) => !trip.active === false)
        setUpcomingTrip(data);
      }
    } catch (err) { }
  }, []);

  const getWorkshops = useCallback(async () => {
    const result = await getAllLearnings();
    if (result.status === 200) {
      const data = result.data.data.data.filter((trip) => !trip.active === false)
      setUpcomingWorkShop(data);
    }
  }, []);

  const getAlbumsData = useCallback(async () => {
    const result = await getAllAlbums();
    if (result.status === 200) {
      setAlbumsData(result.data.data);
    }
  }, []);

  useEffect(() => {
    dispatch(isEditMode())
    getAllTripsHandler();
    getWorkshops();
    getAlbumsData();
  }, [dispatch, isEditMode, getAllTripsHandler, getWorkshops, getAlbumsData])


  const displayLang = (lang) => {
    let resLang = ""
    if (lang.length > 0) {
      lang.map((a, index) => {
        let addCooma = "";
        if (lang.length !== index + 1) {
          addCooma = ", "
        }
        resLang += CaptalizeFirst(a) + addCooma;
      });

    }

    return resLang;
  }


  const editSecondPage = () => {
    dispatch(changeTab(2));
    router.push("/update-enthu-profile");

  };


  const onEditClick = () => {
    dispatch(changeTab(1));
    router.push('/update-enthu-profile')
  }

  const uploadOriginalCover = useCallback(
    async (cover, originalCover) => {
      try {
        const formData = new FormData();
        if (cover !== "") {
          formData.append("cover", cover);
          if (originalCover)
            formData.append("originalCover", originalCover);
        } else {
          formData.append("cover", "");
          formData.append("originalCover", "");
        }
        const result = await uploadCoverOriginalPictureEthn(token, formData);
        if (result.status === 200) {
          dispatch(updateCover(result.data.data));
        }
        setUploadingLoader(false);
        setRemovingLoader(false);
      } catch (err) {
        setUploadingLoader(false);
        setRemovingLoader(false);
        message.error(err.response.data.message);
      }
    },
    [dispatch, token, updateCover]
  );

  const setCoverImage = (coverFile, originalFile) => {
    if (coverFile && coverFile !== "") {
      setUploadingLoader(true);
      uploadOriginalCover(coverFile, originalFile)
    } else {
      setRemovingLoader(true);
      uploadOriginalCover("", "");
    }
  };

  if (profile.dob) {
    return (
      <div className="header-container enth_page">
        <div className="container-fluid align-center pb30 mobile">
          <div className="page_wrapper mt10">
            <Row>
              <Col span={24} className="header_bg">
                <ImageUplaoderAndCropper removingLoader={removingLoader} uploadingLoader={uploadingLoader} setCoverImage={(file, originalFile) => setCoverImage(file, originalFile)} originalCover={profile.originalCover} imageUrl={profile.cover || Upload_Img} />
              </Col>
              <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                <div className="profile_img">
                  <img src={profile.picture || ProfileHolder} alt="Profile" />
                  {(id === "") && <span onClick={onEditClick} className="edit_btn medium-text an-14">
                    <Icon className="edit_pancil" type="pencil" />Edit
                  </span>
                  }
                </div>
              </Col>
              <Col xs={18} sm={15} md={12} lg={12} xl={12}>
                <div className="profile_details">
                  <h2 className="an-26 medium-text mb20 mt20">
                    {CaptalizeFirst(profile.firstName)} {' '} {CaptalizeFirst(profile.lastName)}
                  </h2>
                  {/* <h5 className="medium-text an-14">{profile.phone}</h5> */}
                  <p className="an-14 regular-text">
                    <img src={Location_Img} alt="Profile" />
                    <span>{profile.country}{(typeof profile.city !== "undefined" && typeof profile.city !== "") ? ", " + CaptalizeFirst(profile.city) : ''}
                      {/* {(typeof profile.address !== "undefined" && typeof profile.address !== "") ? ", " + CaptalizeFirst(profile.address) : ''} */}
                    </span>
                  </p>
                  {typeof profile.speaks !== "undefined" && profile.speaks.length > 0 ? (
                    <p className="an-14 regular-text">
                      <img src={Speaks_Img} alt="Profile" />
                      <span>{typeof profile.speaks !== "undefined" && profile.speaks.length > 0 ? displayLang(profile.speaks) : ""}</span>
                    </p>
                  ) : ""}

                  {/* <p className="an-14 medium-text">
                    <Image layout="fill" src={Date_Img} alt="Profile" />
                    <span>{moment(profile.dob).format('LL')}</span>
                  </p> */}
                </div>
              </Col>
              <Col xs={6} sm={9} md={10} lg={6} xl={6}>
                <div className="share_btn pull-right enth_follow_btn">
                  <Dropdown overlay={menu} placement="bottomLeft">
                    <Button className="share_btn mt15 medium-text an-14">
                      &nbsp;&nbsp;<img src={Share_Ic} alt="Share" /> Share&nbsp;&nbsp;
                    </Button>
                  </Dropdown>
                </div>

                <div className="mt15  pull-right enth_follow_btn follow_btn follow-btn-ethn">
                  <Button className="an-14" style={{ height: '40px' }}>
                    <img src={AddUser_Ic} alt="Follow" /> Follow
                  </Button>
                </div>


              </Col>
            </Row>
          </div>
        </div>
        <div className="container-fluid align-center pb30">
          <div className="page_wrapper mt10">
            <Row>
              <Col>
                <div className="enthu-details">
                  <Tabs defaultActiveKey="1">
                    <TabPane tab="About" key="1">
                      <Row gutter={[40, 40]}>
                        <Col className="gutter-row" span={24}>
                          <div className="bio_sec">
                            <Row>
                              <Col span={12}>
                                <h4 className="title_line">About Me</h4>
                              </Col>
                              <Col span={12}>
                                <div className="share_btn mt15">
                                  {id === "" &&
                                    (<span onClick={editSecondPage} className="edit_btn medium-text an-14">
                                      <Icon className="edit_pancil" type="pencil" /> Edit
                                    </span>)}
                                </div>
                              </Col>
                            </Row>
                            <p>{typeof profile.aboutme !== "undefined" && ReactHtmlParser(profile.aboutme)}</p>
                          </div>
                          <div className="award_sec">
                            <Row>
                              <Col span={12}>
                                <h4 className="title_line">
                                  Preferred Activity
                                </h4>
                                <Row gutter={[20, 20]}>
                                  <Col>
                                    {(typeof profile.activity !== "undefined" && profile.activity.length) ? profile.activity.map((skill, key) => (

                                      <span key={key} className="experties-tag">
                                        {skill}
                                      </span>
                                    )) :
                                      (
                                        <div>
                                          <div className="mb10 ml10 an-14 medium-text"><h3>No Preferred Activity</h3></div>
                                        </div>
                                      )
                                    }
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={12}>
                                <div className="share_btn mt15">
                                  {id === "" &&
                                    (<span onClick={editSecondPage} className="edit_btn medium-text an-14">
                                      <Icon className="edit_pancil" type="pencil" /> Edit
                                    </span>)}
                                </div>
                              </Col>
                            </Row>
                            <Row>
                              <Col span={12}>
                                <h4 className="title_line">Interested In</h4>
                                <Row gutter={[20, 20]}>
                                  <Col>
                                    {(typeof profile.interested !== "undefined" && profile.interested.length) ? profile.interested.map((skill, key) => (

                                      <span key={key} className="experties-tag">
                                        {skill}
                                      </span>

                                    )) :
                                      (
                                        <div>
                                          <div className="mb10 an-14 ml10 medium-text"><h3>No Interested In Found</h3></div>
                                        </div>
                                      )
                                    }
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={12}>
                                <div className="share_btn mt15">
                                  {id === "" &&
                                    (<span onClick={editSecondPage} className="edit_btn medium-text an-14">
                                      <Icon className="edit_pancil" type="pencil" /> Edit
                                    </span>)}
                                </div>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>

                    </TabPane>
                    {/* <TabPane tab="Trips" key="2">
                      <MyTrips isPublicView={true} token={""} publicTrip={upcomingTrip} travelMap={travelMap} />
                    </TabPane>
                    <TabPane tab="Workshops" key="3">
                      <WorkShopTab isPublicView={true} token={""} publicWorkShop={upcomingWorkShop} travelMap={travelMap} />
                    </TabPane> */}
                    <TabPane tab="Album" key="4" className="pt25 pb80 pr20 pl20" style={{ paddingBottom: "80px" }}>
                      <Row gutter={[40, 40]}>
                        <Col className="pt25 pb80 pr20 pl20">
                          This page is under construction
                        </Col>
                      </Row>
                      {/* <AlbumTab isPublicView={true} token={""} publicAlbums={albumsData} /> */}
                    </TabPane>
                    <TabPane tab="Following" key="5" className="pt25 pb80 pr20 pl20" style={{ paddingBottom: "80px" }}>
                      <Row gutter={[40, 40]}>
                        <Col className="pt25 p80 pr20 pl20">
                          No Following Found
                        </Col>
                      </Row>
                    </TabPane>
                    {/* <TabPane tab="Messages" key="6">
                      <EnthuMessages />
                    </TabPane> */}
                  </Tabs>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div >
    )
  } else {
    return null;
  }
}

export default compose(EnthusiestProfile);