import React, { useState, useEffect } from 'react';
import { compose } from "redux";
import { Row, Col, Input, Rate } from 'antd';
import AppLoader from "../components/Loader";
import moment from "moment";
import { CaptalizeFirst, returnTheRating } from "../helpers/methods";
import ShowMoreText from "react-show-more-text";

const { TextArea } = Input;

const Review = props => {

    const { expertId, isPublicView, expertRating, totalReview, reviewData } = props;

    // const [reviewData, setReviewData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isRateLoading, setIsRateLoading] = useState(true);
    const [noReview, setNoReview] = useState(false);


    useEffect(() => {
        setIsLoading(true);
        if (reviewData.length > 0) {
            setIsRateLoading(false);
            setIsLoading(false);
        } else {
            setNoReview(true);
        }
    }, [reviewData]);
    useEffect(() => {

    }, [expertRating]);

    const getDateFormat = (date) => {
        if (moment(date).format("MMM Do YY") === moment().format("MMM Do YY")) {
            return moment(date).format("MMMM YYYY");
        }
        return moment(date).format("MMMM YYYY");
    };

    // const ReadMore = ({ children }) => {
    //     const text = children;
    //     const [isReadMore, setIsReadMore] = useState(true);
    //     const toggleReadMore = () => {
    //       setIsReadMore(!isReadMore);
    //     };
    //     return (
    //       <p className="text">
    //         {isReadMore ? text.slice(0, 150) : text}
    //         <span onClick={toggleReadMore} className="read-or-hide">
    //           {isReadMore ? "...read more" : " show less"}
    //         </span>
    //       </p>
    //     );
    //   };

    return (
        <div className="review-view-page">
            {!isLoading &&
                <>
                    <Row>
                        <Col xs={24} sm={24} md={24} lg={24} xl={24} className="totla-review">
                            <p className="total-review-title">{totalReview === 0 ? "No Review" : totalReview === 1 ? `${totalReview} Review` : `${totalReview}  Reviews`}
                                <div className="yellow-border"></div>
                            </p>
                            <div>
                                {isRateLoading && <><Rate
                                    allowHalf
                                    disabled
                                    defaultValue={0}
                                    style={{ color: "#FFBC00" }}
                                    className="ml20 an-18 rating-star"
                                /> </>}

                                {!isRateLoading && <><Rate
                                    allowHalf
                                    disabled
                                    defaultValue={returnTheRating(expertRating)}
                                    style={{ color: "#FFBC00" }}
                                    className="ml20 an-18 rating-star"
                                /> </>}
                            </div>

                        </Col>
                    </Row>
                    <Row>
                        {reviewData.map((item, index) => {

                            let title = "", adventureType = "", country = "Abc", date = "";
                            if (typeof item.learning !== "undefined" && item.learning !== "" && item.learning !== null) {
                                title = item.learning.title;
                                adventureType = "Workshop";
                                if (typeof item.learning.country !== "undefined" && item.learning.country !== "undefined" && item.learning.country !== "") {
                                    country = CaptalizeFirst(item.learning.country);
                                } else if (typeof item.learning.workshopMedium !== "undefined") {
                                    country = CaptalizeFirst(item.learning.workshopMedium);
                                }

                            } else if (typeof item.trip !== "undefined" && item.trip !== null && item.trip !== "") {
                                title = item.trip.title;
                                adventureType = "Trip";
                                if (typeof item.trip.country !== "undefined" && typeof item.trip.country !== "undefined") {
                                    country = CaptalizeFirst(item.trip.country);
                                }
                            } else {
                                if (item.isSuperAdmin) {
                                    title = item.tripWorkshopTitle ? item.tripWorkshopTitle : "";
                                }
                            }

                            let Name = "";
                            if (item.fullName !== "") {
                                Name = item.fullName;
                            } else {
                                Name = item.enthu.firstName ? item.enthu.firstName : "";
                                Name += item.enthu.lastName ? " " + item.enthu.lastName : "";
                            }

                            let defaultRating = parseFloat(item.rating);
                            return (
                                <>
                                    <Row key={index} className={`review-grid ${(reviewData.length - 1) !== index ? 'border-bottom' : ''}`}>

                                        <Col xs={24} sm={24} md={24} lg={24} xl={24} className="review-parent-left">
                                            <div className="review-parent-left__title">
                                                <div>
                                                    <span className="">{Name}, </span>
                                                    <span className="review-parent-left__secondLine">{getDateFormat(item.attendDate)}</span>
                                                    <p className="review-parent-left__main-title">{title}</p>
                                                </div>
                                                <div className="rr-review-parent">
                                                    <div className="rr-child rr-child-rate"><Rate disabled defaultValue={returnTheRating(defaultRating)} style={{ color: "#FFBC00" }} /></div>
                                                </div>

                                            </div>
                                            <p className="review-parent-left__subtitle">
                                                <ShowMoreText
                                                    lines={5}
                                                    more="View more"
                                                    less="View less"
                                                    className="content-css"
                                                    anchorClass="my-anchor-css-class"
                                                    expanded={false}
                                                    truncatedEndingComponent={"... "}
                                                >
                                                    {typeof item.review !== "undefined" ? item.review : ""}
                                                </ShowMoreText>
                                                {/* <ReadMore>
                                                {typeof item.review !== "undefined" ? item.review : ""}
                                                </ReadMore> */}
                                                {/* 
                                                <ReadMoreReact text={typeof item.review !== "undefined" ? item.review : ""}
                                                    min={0}
                                                    ideal={760}
                                                    max={760}
                                                    readMoreText={"View More"} /> */}
                                            </p>
                                        </Col>
                                    </Row>
                                </>
                            )
                        })}

                        {reviewData.length === 0 &&
                            <div className="text-center py20 loader-absolute-class min-height">
                                No Review
                        </div>
                        }

                    </Row>
                </>
            }
            {isLoading && !noReview &&
                <div className="text-center py20 loader-absolute-class min-height">
                    <AppLoader />
                </div>
            }
            {isLoading && noReview &&
                <div className="text-center py20 loader-absolute-class min-height">
                    No Review
                </div>
            }
        </div>
    )
}

export default compose(Review);