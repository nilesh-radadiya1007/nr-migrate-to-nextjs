import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';


const WorkShopEdit = dynamic(
    () => import("../../src/containers/WorkshopDetails"),
    { ssr: false }
);

const WorkShopEditInApp = (props) => {
    // console.log(props);
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <WorkShopEdit />
            </BasicPageWrapper>
        </BasicWrapperProvider>
    )
}

// export async function getServerSideProps({ params }) {
//     return { props: { id: params.id } }
// }


export default WorkShopEditInApp;