import React, { useState, useEffect } from "react";
import Link from "next/link";
import { compose } from "redux";
// import { useSelector } from "react-redux";
import { Button, Select, Icon, Input, Row, Col } from "antd";


import WatchVideoRevealHomepage from "../../components/Basic/WatchVideoReveal/WatchVideoRevealHomepage";

import { useMediaQuery } from "react-responsive";
import { ActivityList, ActivityListNew } from "../../helpers/constants";
import countries from "../../helpers/countries";
const { Option, OptGroup } = Select;
const InputGroup = Input.Group;

const Onetree = "/images/partner/mask3.png";
const Leavenotrace = "/images/partner/leavenotrace.png";
// const locatorIconActive from "assets/images/newicon/locator_active.svg";
const searchIcon = "/images/home/search_icon.png";

function HomePageHeader1(props) {

    const [isFullScreenVideo, setIsFullScreenVideo] = useState(false);

    const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });

    const [activityDList, setActivityList] = useState(ActivityList);
    const [acticityOptions, setActivityOptions] = useState("");

    useEffect(() => {
        renderActivityList()
    }, [])

    const renderActivityList = () => {
        const tmpGroup = [
            { key: 'mountaineering', title: "Mountaineering" },
            { key: 'nature', title: "Nature and Wildlife" },
            { key: 'water', title: "Water Sports" },
            { key: 'other', title: "Other" }
        ];
        // let tmpData = tmpGroup.map((e) => {
        //     return (<OptGroup label={e.title}>
        //         {ActivityListNew.map((activity) => {
        //             if (e.key === activity.group) {
        //                 return (
        //                     <Option key={activity.name} value={activity.name}>{activity.name}</Option>
        //                 )
        //             }
        //         })}
        //     </OptGroup>
        //     )
        // });
        let tmpActivityListNew = ActivityListNew.sort(function (a, b) {
            if (a.name > b.name) {
                return 1;
            }
            if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        // console.log(tmpActivityListNew);

        let tmpData = tmpActivityListNew.map((activity) => {
            return (
                <Option key={activity.name} value={activity.name}>{activity.name}</Option>
            )
        });
        setActivityOptions(tmpData);
    }

    const redirect = () => {
        window.location.href = 'https://blog.expeditionsconnect.com/'
    }

    return (
        <div className="">
            <div className={`expert_head_bg expert_head_bg_color ${isMaxWidth768 ? 'mobile' : ''}`}>
                <div className="bg-image-layer"></div>
                <div className={`${isFullScreenVideo ? 'video expert_header_info' : 'expert_header_info'}`}>
                    <div className="bg-title">
                        <span className="first-txt">Take On The Challenge.</span>
                        <br />
                        <span className="second-txt">Embrace The Adventure.</span>
                    </div>
                    <div className="bg-sub-title">
                        {!isMaxWidth768 && <span>Explore the best adventure courses, workshops and expeditions <br />directly crafted by professionals. </span>}
                        {isMaxWidth768 && <span>Explore the best adventure courses, workshops and expeditions directly crafted by professionals.</span>}
                    </div>
                    {isMaxWidth768 && (<div className="search-textbox-mobile">

                        <Row gutter={0}>
                            <Col xs={24} sm={24} md={10} lg={10} xl={10} className="types adventure" >
                                <div className="filter-type-right">
                                    <div className="filter-heading adventure-filter-text">
                                        {acticityOptions !== "" &&
                                            <Select
                                                name="allAdventures"
                                                suffixIcon={
                                                    <Icon
                                                        type="caret-down"
                                                        role="caret-down"
                                                        style={{ fontSize: "17px", color: "#000000" }}
                                                    />
                                                }
                                                onChange={(e) => props.setActivityName(e)}
                                                placeholder="All Adventures"
                                                showArrow={true}
                                                className="adventure-dropdown"
                                                optionFilterProp="children"
                                                dropdownClassName="home-activity-list"
                                            >
                                                <Option value="">All Adventures</Option>
                                                {acticityOptions}
                                            </Select>
                                        }
                                    </div>
                                </div>
                            </Col>
                            <Col xs={21} sm={24} md={10} lg={10} xl={10} className="types country">
                                <div className="filter-type-right">
                                    <div className="filter-heading">

                                        <Select
                                            name="location"
                                            showSearch
                                            showArrow={true}
                                            onChange={(e) => props.setCountry(e)}
                                            defaultValue={""}
                                            placeholder="Location"
                                            dropdownClassName="home-activity-list"
                                            suffixIcon={
                                                <Icon
                                                    role="caret-down"
                                                    type="caret-down"
                                                    style={{ fontSize: "17px", color: "#000000" }}
                                                />
                                            }
                                        >
                                            <Option value="">Location</Option>
                                            <OptGroup label="By Continent">
                                                <Option key={"Europe"} value={"group|Europe"}>Europe</Option>
                                                <Option key={"Africa"} value={"group|Africa"}>Africa</Option>
                                                <Option key={"Asia"} value={"group|Asia"}>Asia</Option>
                                                <Option key={"SouthAmerica"} value={"group|SouthAmerica"}>South America</Option>
                                                <Option key={"NorthAmerica"} value={"group|NorthAmerica"}>North America</Option>
                                                <Option key={"Antarctica"} value={"group|Antarctica"}>Antarctica</Option>
                                                <Option key={"Oceania"} value={"group|Oceania"}>Oceania</Option>
                                            </OptGroup>
                                            <OptGroup label="By Country">
                                                {countries.map((exp, i) => (
                                                    <Option key={i} value={exp.name}>
                                                        {exp.name}
                                                    </Option>
                                                ))}
                                            </OptGroup>
                                        </Select>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={3} sm={24} md={4} lg={4} xl={4} className="search-parent">
                                {isMaxWidth768 && <Button role="button" id="adventure_search" className="search-btn-btn" onClick={(e) => props.handleSearch(e)}>
                                    <Icon role="search" type="search" />
                                </Button>}

                            </Col>
                        </Row>
                    </div>)}
                    {!isMaxWidth768 && <div className="search-textbox">

                        <Row gutter={0}>
                            <Col xs={24} sm={24} md={10} lg={10} xl={10} className="types adventure" >
                                <div className="filter-type-right">
                                    <div className="filter-sub-heading">I'm Looking For</div>
                                    <div className="filter-heading adventure-filter-text">
                                        {acticityOptions !== "" &&
                                            <Select
                                                name="allAdventures"
                                                suffixIcon={
                                                    <Icon
                                                        role="caret-down"
                                                        type="caret-down"
                                                        style={{ fontSize: "17px", color: "#000000" }}
                                                    />
                                                }
                                                onChange={(e) => props.setActivityName(e)}
                                                placeholder="All Adventures"
                                                showArrow={true}
                                                className="adventure-dropdown"
                                                optionFilterProp="children"
                                                dropdownClassName="home-activity-list"
                                            >
                                                <Option value="">All Adventures</Option>
                                                {acticityOptions}
                                            </Select>
                                        }
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} sm={24} md={10} lg={10} xl={10} className="types country">
                                <div className="filter-type-right">
                                    <div className="filter-sub-heading">Where</div>
                                    <div className="filter-heading">

                                        <Select
                                            showSearch
                                            showArrow={true}
                                            onChange={(e) => props.setCountry(e)}
                                            defaultValue={""}
                                            name="onEarth"
                                            placeholder="On Earth"
                                            dropdownClassName="home-activity-list"
                                            suffixIcon={
                                                <Icon
                                                    role="caret-down"
                                                    type="caret-down"
                                                    style={{ fontSize: "17px", color: "#000000" }}
                                                />
                                            }
                                        >
                                            <Option value="">On Earth</Option>
                                            <OptGroup label="By Continent">
                                                <Option key={"Europe"} value={"group|Europe"}>Europe</Option>
                                                <Option key={"Africa"} value={"group|Africa"}>Africa</Option>
                                                <Option key={"Asia"} value={"group|Asia"}>Asia</Option>
                                                <Option key={"SouthAmerica"} value={"group|SouthAmerica"}>South America</Option>
                                                <Option key={"NorthAmerica"} value={"group|NorthAmerica"}>North America</Option>
                                                <Option key={"Antarctica"} value={"group|Antarctica"}>Antarctica</Option>
                                                <Option key={"Oceania"} value={"group|Oceania"}>Oceania</Option>
                                            </OptGroup>
                                            <OptGroup label="By Country">
                                                {countries.map((exp, i) => (
                                                    <Option key={i} value={exp.name}>
                                                        {exp.name}
                                                    </Option>
                                                ))}
                                            </OptGroup>
                                        </Select>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} sm={24} md={4} lg={4} xl={4} className="search-parent">
                                {!isMaxWidth768 && <Button role="button" id="adventure_search" className="search-btn" onClick={(e) => props.handleSearch(e)}>
                                    <img src={'/images/home/search_icon.svg'} alt="Map Icon" />
                                </Button>}
                                {isMaxWidth768 && <Button role="button" id="adventure_search" className="search-btn-btn" onClick={(e) => props.handleSearch(e)}>
                                    Search
                                </Button>}

                            </Col>
                        </Row>
                    </div>}

                    <div className="extra-filters">
                        <div className="extra-parent">
                            <Link href='/all-adventure'>
                                <a><div className={`extra-child active`} > <span><img src={'/images/home/pin_icon.svg'} alt="Map Icon" /></span>  Adventures Near Me </div></a>
                            </Link>
                        </div>
                        <div className="extra-parent second">
                            <Link href='/experts'>
                                <a>
                                    <div className={`extra-child`} >
                                        <span>
                                            <img src={'/images/home/expert_icon.svg'} alt="Expert Icon" /></span>
                                        Find an Expert
                                    </div>
                                </a>
                            </Link>
                        </div>
                        <div className="extra-parent third">
                            <Link href='/all-adventure?adventure=workshop&isFlexible=true'>
                                <a><div className={`extra-child`} > <span><img src={'/images/home/skill_icon.svg'} alt="Skills Icon" /></span>  Up Your Skills </div></a>
                            </Link>
                        </div>
                        <div className="extra-parent forth">
                            {/* <Link to='/all-adventure'> */}
                            <div className={`extra-child`} onClick={() => redirect()} > <span><img src={'/images/home/star_icon.svg'} alt="Search Icon" /></span>  Inspire Me </div>
                            {/* </Link> */}
                        </div>
                        {/*<div className="extra-parent">
                            <Link to='/all-adventure?activity=["Scuba-Diving"]&isFlexible=true'>
                                <div className={`extra-child`} > <span><img src={require('assets/images/newicon/expertise/scubadiving.svg')} alt="Map Icon" /></span> {isMaxWidth768 ? 'Diving' : 'Scuba  Diving'}  </div>
                            </Link>
                        </div> */}
                    </div>
                    <div className="parter-section" style={{ display: "none" }}>
                        <img className="onetree-logo" src={Onetree} />
                        <img className="leavenotarace-logo" src={Leavenotrace} />
                    </div>
                </div>
                <WatchVideoRevealHomepage
                    setIsFullScreenVideo={setIsFullScreenVideo}
                    isFullScreenVideo={isFullScreenVideo}
                />
            </div>
        </div >
    );
};
export default compose(HomePageHeader1);
