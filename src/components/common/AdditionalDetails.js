import React, { Fragment, useState } from 'react';
import { compose } from "redux";
import { Collapse, Icon, Row, Col, Card, Rate } from 'antd';
import ReactHtmlParser from 'react-html-parser';
import { notFound, notFoundData } from "../../helpers/constants";
import OwlCarousel from "react-owl-carousel";
import { returnTheRating } from "../../helpers/methods";
import moment from "moment";
import IconAndTitle from './AdditionalDetailsIconAndTitle';
import ShowMoreText from "react-show-more-text";
import { useRouter } from 'next/router';

const minusIcon = "/images/newicon/minus.svg";
const plusIcon = "/images/newicon/plus.svg";
const { Panel } = Collapse;

const sliderOptions = {
    className: "owl-theme accommodation_img_view slider_prev_next",
    margin: 10,
    nav: true,
    responsive: {
        0: {
            items: 1,
            nav: true,
            dotsEach: 3,
        },
        768: {
            items: 2,
            nav: true,
        },
        991: {
            items: 3,
            nav: true,
        },
    },
};

const AdditionalDetails = (props) => {
    const router = useRouter();
    const { item, type, isEdit, addAccommoPopUp, onAccommoImageClick, review } = props;


    const loadItiniary = () => {
        if (typeof item.itenary !== "undefined" && item.itenary.length > 0) {
            return (
                item.itenary.map((iti, index) => {
                    let tmpDay = "";
                    if (iti.dayRange !== undefined && iti.dayRange !== "") {
                        if (iti.dayRange.indexOf('-') === -1) {
                            if (item.itenary.length !== 1) {
                                tmpDay = `Day ${iti.dayRange}.`;
                            } else {
                                tmpDay = "";
                            }
                        } else {
                            let newDay = iti.dayRange.split('-');
                            let tmpnewDay = ""
                            if (newDay.length > 0) {
                                newDay.map((item, index) => {
                                    tmpnewDay += "Day " + item + ".";
                                    if (newDay.length !== index + 1) {
                                        tmpnewDay = tmpnewDay + " - ";
                                    }
                                })
                            }
                            tmpDay = tmpnewDay;
                        }
                    } else {
                        if (item.itenary.length !== 0) {
                            tmpDay = 'Day ' + (parseInt(iti.day) + 1) + ".";
                        }
                    }
                    let tmpMeal = iti.meals !== undefined ? JSON.parse(iti.meals) : [];
                    return (
                        <div className="" key={index}>
                            <p className="i-main-title">
                                <>
                                    {`${tmpDay !== "" ? tmpDay : ''}`}
                                </>
                            </p>
                            {iti.title !== undefined && iti.title !== "" &&
                                <p className="i-title"> {iti.title}</p>
                            }
                            <div className="mb40 font-16-rubik">
                                {ReactHtmlParser(iti.value || "")}
                            </div>
                            {tmpMeal.length > 0 &&

                                <p className="i-meals">
                                    <span className="i-meals-main">Meals Included:</span> &nbsp;
                                    <span className="i-meals-sub">{tmpMeal.join(', ')}</span>
                                </p>
                            }
                            {iti.accommodation !== undefined && iti.accommodation !== "" &&
                                <p className="i-meals">
                                    <span className="i-meals-main">Accommodation:</span> &nbsp;
                                    <span className="i-meals-sub">{iti.accommodation}</span>
                                </p>
                            }
                            {iti.activityName !== undefined && iti.activityName !== "" &&
                                <p className="i-meals">
                                    <span className="i-meals-main">Activity:</span> &nbsp;
                                    <span className="i-meals-sub">{iti.activityName}</span>
                                </p>
                            }
                            {item.itenary.length !== (index + 1) &&
                                <div className="bb"></div>
                            }
                        </div>
                    )
                }
                ))
        } else {
            return (<div className="not-exist-text">{`No Itinerary exists for this ${type}.`}</div>)
        }
    }

    const loadAccomodations = () => {
        if (typeof item.accomodations !== "undefined" && item.accomodations.length > 0) {
            return (
                item.accomodations.map((acco, index) => {
                    return (
                        <div key={index} className="">
                            <div className="mb20 not-exist-text">
                                {ReactHtmlParser(notFoundData('accommodation', acco.description || "", type))}
                            </div>
                            {item.accomodations.length !== (index + 1) &&
                                <div className="bb"></div>
                            }
                        </div>
                    )
                }
                ))
        } else {
            return (<div className="not-exist-text">{`No Accommodation exists for this ${type}.`}</div>)
        }
    }

    const loadReviews = () => {

        if (typeof review !== "undefined") {
            if (typeof review.reviewData !== "undefined" && review.reviewData.length > 0) {
                return (
                    <Row className="review-detail-page" >
                        {review.reviewData.map((item, index) => {
                            let Name = "";
                            if (item.fullName !== "") {
                                Name = item.fullName;
                            } else {
                                Name = item.enthu.firstName ? item.enthu.firstName : "";
                                Name += item.enthu.lastName ? " " + item.enthu.lastName : "";
                            }
                            let defaultRating = parseFloat(item.rating);
                            return (
                                <>
                                    <Row className={`review-grid ${(review.reviewData.length - 1) !== index ? 'border-bottom' : ''}`} key={index}>
                                        <Col xs={24} sm={24} md={24} lg={24} xl={24} className="review-parent-right">
                                            <div className="review-right">
                                                <div className="review-expert">
                                                    <div className="review-parent-left__title">
                                                        <div>
                                                            <p className="review-parent-left__secondLine">{Name}</p>
                                                            <p>
                                                                <span className="review-parent-left__secondLine date">
                                                                    {getDateFormat(item.attendDate)}
                                                                </span>
                                                            </p>

                                                        </div>
                                                        <div className="rr-child rr-child-rate">
                                                            <Rate disabled allowHalf defaultValue={returnTheRating(defaultRating)} style={{ color: "#FFBC00" }} /></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Col>
                                        <Col xs={24} sm={24} md={24} lg={24} xl={24} className="review-parent-left">
                                            <p className="review-parent-left__subtitle">
                                                {/* <ReadMoreReact text={typeof item.review !== "undefined" ? item.review : ""}
                                                    min={0}
                                                    ideal={760}
                                                    max={760}
                                                    readMoreText={"View More"} /> */}
                                                <ShowMoreText
                                                    lines={5}
                                                    more="View more"
                                                    less="View less"
                                                    className="content-css"
                                                    anchorClass="my-anchor-css-class"
                                                    expanded={false}
                                                    truncatedEndingComponent={"... "}
                                                >
                                                    {typeof item.review !== "undefined" ? item.review : ""}
                                                </ShowMoreText>
                                            </p>
                                        </Col>
                                    </Row>
                                </>
                            )
                        })}

                        {review.reviewData.length === 0 &&
                            <div className="text-center py20 loader-absolute-class min-height">
                                No Review
                            </div>
                        }

                    </Row>

                )
            } else {
                return <>{`No review for this ${type}.`}</>
            }
        } else {
            return <>{`No review for this ${type}.`}</>
        }


    }

    const getDateFormat = (date) => {
        if (moment(date).format("MMM Do YY") === moment().format("MMM Do YY")) {
            return moment(date).format("MMMM YYYY");
        }
        return moment(date).format("MMMM YYYY");
    };

    const getOverAllRating = () => {
        let defaultRating = 0;
        if (review !== undefined && review.reviewRating !== undefined) {
            defaultRating = parseFloat(review.reviewRating);
        }
        return (
            <div className="collaps-rate">
                <Rate disabled allowHalf defaultValue={returnTheRating(defaultRating)} style={{ color: "#111111" }} />
            </div>
        )
    };


    return (
        <div className="additional-info-section" id='scoll-to-here'>
            <Row>
                <Col xs={24} sm={24} md={17} lg={17} xl={17}>
                    <Collapse
                        bordered={false}
                        // expandIcon={({ isActive }) => <Icon type={isActive ? 'minus' : 'plus'} style={{ fontWeight: "600", fontSize: "16px" }} />}
                        expandIcon={({ isActive }) => isActive ? <img className="collaps-icon" src={minusIcon} alt="minus icon" /> : <img className="collaps-icon" src={plusIcon} alt="plus icon" />}
                        expandIconPosition={'right'}
                        defaultActiveKey={router.query.review === "true" ? [9] : [0]}
                    >

                        {/* Itinerary */}
                        {(type === "trip" || (type === "workshop" && item.workshopMedium !== "online")) &&
                            <Panel header={<IconAndTitle position={0} />} key="1" className="odd-panel bt itinerary-collaps">
                                {loadItiniary()}
                            </Panel>
                        }

                        {/* Accommodation */}
                        {(type === "trip" || (type === "workshop" && item.workshopMedium !== "online")) &&
                            <Panel header={<IconAndTitle position={1} />} key="2" className="even-panel accomodation-collaps">

                                <div>
                                    {loadAccomodations()}
                                    {/* {ReactHtmlParser(notFoundData('accommodation', item.accomodation || "", type))} */}
                                </div>
                                {isEdit &&
                                    <Col span={24} style={{ textAlign: "right" }}>
                                        <div className='fix_top_pad text-right edt_btn_sec adit_fix mb20' onClick={addAccommoPopUp}>
                                            <i className='fas fa-pencil-alt'></i> Add/Edit Photos
                                        </div>
                                    </Col>
                                }
                                {typeof item.accomodationPhotos !== "undefiend" && item.accomodationPhotos.length > 0 &&
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} className="pb30 pt20">
                                        <OwlCarousel {...sliderOptions}>
                                            {item.accomodationPhotos.map((t, index) => (
                                                <Col xs={24} sm={24} md={24} lg={24} xl={24} key={index} className="gutter-row">
                                                    <Card
                                                        hoverable
                                                        cover={<img onClick={() => onAccommoImageClick(index, "accomo")} alt="acco img" src={t} />} >
                                                    </Card>
                                                </Col>
                                            ))}
                                        </OwlCarousel>
                                    </Col>
                                }
                            </Panel>
                        }
                        {/* Logistic/Transfers */}
                        {(type === "trip" || (type === "workshop" && item.workshopMedium !== "online")) &&
                            <Panel header={<IconAndTitle position={2} />} key="3" className="odd-panel">
                                <div>
                                    <p className="ie-title"> Getting There</p>
                                    <div className="not-exist-text">{ReactHtmlParser(notFoundData('logisticGettingThere', item.gettingThere || "", type))}</div>
                                    <div className="bb"></div>
                                    <p className="ie-title"> Transfer</p>
                                    <div className="not-exist-text">{ReactHtmlParser(notFoundData('logisticTransfer', item.transfer || "", type))}</div>
                                </div>
                            </Panel>
                        }


                        {/* Inclusion & Exclusion */}
                        < Panel header={<IconAndTitle position={3} />} key="4" className="even-panel">
                            <div className=''>
                                <p className="ie-title"> Inclusions</p>
                                <div className='ie-main-content inclusion-section'>
                                    {item.inclusion && item.inclusion.trim() !== 'null' && item.inclusion.trim() !== "" &&
                                        <>
                                            {ReactHtmlParser(item.inclusion)}
                                        </>
                                    }
                                    {item.inclusion.trim() === "" && <p className="no-bullet">{notFound('trip', 'inclusionExclusion')}</p>}
                                </div>
                                <div className="bb"></div>
                                <p className="ie-title">Exclusions</p>
                                <div className='ie-main-content exclusion-section'>
                                    {item.exclusion && item.exclusion.trim() !== 'null' && item.exclusion.trim() !== "" &&
                                        <>
                                            {ReactHtmlParser(item.exclusion)}
                                        </>
                                    }
                                    {item.exclusion.trim() === "" && <p className="no-bullet">{notFound(type, 'inclusionExclusion')}</p>}
                                </div>

                            </div>
                        </Panel>


                        {/* Equipments & Gears */}
                        < Panel header={<IconAndTitle position={4} />} key="5" className="odd-panel">
                            <div className="not-exist-text">{ReactHtmlParser(notFoundData('equipmentsGears', item.equipmentsGears || "", type))}</div>
                        </Panel>

                        {/* Additional Information */}
                        <Panel header={<IconAndTitle position={5} />} key="6" className="even-panel">
                            <div className="not-exist-text">{ReactHtmlParser(notFoundData('extras', item.extras || "", type))}</div>
                        </Panel>
                        {/* COVID Guidlines */}
                        {(type === "trip" || (type === "workshop" && item.workshopMedium !== "online")) &&
                            <Panel header={<IconAndTitle position={6} />} key="7" className="odd-panel">
                                <div className="not-exist-text">{ReactHtmlParser(notFoundData('covidGuidlines', item.covidGuidlines || "", type))}</div>
                            </Panel>
                        }

                        {/* Terms & Conditions */}
                        <Panel header={<IconAndTitle position={7} />} key="8" className={`${item.workshopMedium === "online" ? 'odd-panel' : 'even-panel'}`}>
                            <div className="not-exist-text">{ReactHtmlParser(notFoundData('cancellations', type === "workshop" ? item.cancellantion : item.cancellations || "", type))}</div>
                        </Panel>

                        <Panel header={<IconAndTitle position={8} review={review} />} key="9" className="odd-panel review-tab" extra={getOverAllRating()}>
                            <div className="not-exist-text">
                                {loadReviews()}
                            </div>
                        </Panel>
                    </Collapse>
                </Col>
            </Row>
        </div >
    )

}

export default compose(AdditionalDetails)
