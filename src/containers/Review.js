import React, { useState, useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { compose } from "redux";
import { Modal, Row, Col, Input, Rate, Icon, Button, Form, DatePicker } from 'antd';
import queryString from 'query-string';
import { getExpertAndAdventureData, postReview } from '../services/expert';
import moment from "moment";
import { ModalActions } from "../redux/models/events";
import AppLoader from "../components/Loader";

import { useRouter } from 'next/router';

const Placeholder = '/images/upload_image.png';
const Success = "/images/success_ic.png";

const { TextArea } = Input;

const Review = props => {
    const router = useRouter()
    const { getFieldDecorator, setFieldsValue } = props.form;
    const dispatch = useDispatch();
    const [reviewData, setReviewData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isReviewSubmit, setIsReviewSubmit] = useState(false);

    // const [defaultRating, setDefaultRating] = useState({ "clarityOfInstruction": 0, "engaging": 0, "helpfulExamples": 0, "expectationsMet": 0, "valueforMoney": 0 });
    const [review, setReview] = useState("");
    const [ecReview, setECReview] = useState("");
    const [notFound, setNotFound] = useState(false);
    const [adventureType, setAdventureType] = useState("");
    const [errorPopup, setErrorPopup] = useState(false);
    const { isLogin, role } = useSelector((state) => state.auth);
    const profile = useSelector(state => state.enthu);
    const [isExists, setIsExists] = useState(false);
    const [isRecommend, setIsRecommend] = useState('');
    const [successPopup, setSuccessPopup] = useState(false);
    const [defaultRate, setDefaultRate] = useState(0);
    const [fullName, setFullName] = useState("");
    const [date, setDate] = useState(moment(new Date(), 'YYYY-MM-DD'));
    const [rateError, setRateError] = useState(false);
    const [superAdmin, setSuperAdmin] = useState(false);
    const [tripOrWorkshop, setTripOrWorkshop] = useState("");

    const { openAuthModal } = ModalActions;

    useEffect(() => {
        if (!isLogin) {
            localStorage.setItem("referrer", window.location.pathname + window.location.search);
            dispatch(openAuthModal(true));
        }
    }, [isLogin])


    useEffect(() => {

        if (isLogin && !isLoading && notFound) {
            setErrorPopup(true);
        }



    }, [isLogin, notFound, isLoading])

    let urlParams = queryString.parse(window.location.search ? window.location.search : {});
    const token = useSelector(state => state.auth.accessToken);


    const getExpertAndTripData = useCallback(async () => {
        const result = await getExpertAndAdventureData(token, urlParams);
        if (result.status === 200) {
            if (result.data.data.length > 0) {
                setReviewData(result.data.data[0]);
                if (typeof result.data.data[0].learning === "undefined" && typeof result.data.data[0].trip === "undefined") {
                    setSuperAdmin(true);
                } else if (typeof result.data.data[0].learning !== "undefined") {
                    setAdventureType('learning');
                } else {
                    setAdventureType('trip');
                }
                setIsLoading(false);
            } else {
                setNotFound(true);
                setIsLoading(false);
            }
        } else {
            setNotFound(true);
            setIsLoading(false);
        }
    }, []);


    useEffect(() => {
        getExpertAndTripData();
    }, []);

    const getDateFormat = (date) => {
        if (moment(date).format("MMM Do YY") === moment().format("MMM Do YY")) {
            return moment(date).format("DD MMM YYYY");
        }
        return moment(date).format("DD MMM YYYY");
    };

    const handleCommentChange = (e) => {
        if (e.target.id === "Review") {
            setReview(e.target.value);
        } else if (e.target.id === "ECReview") {
            setECReview(e.target.value);
        }
    }

    const handleReviewSubmit = async () => {

        if (defaultRate === 0) {
            setRateError(true);
        } else {
            setRateError(false);

            let attendDate = moment(date).toISOString();
            if (typeof urlParams.admin !== "undefined" && urlParams.admin !== "") {
                attendDate = date ? moment(date).toISOString() : moment(new Date(), 'YYYY-MM-DD');
            } else {
                attendDate = reviewData.date;
            }

            let formData = {
                "fullName": fullName,
                "rating": defaultRate,
                "review": review,
                "ecReview": ecReview,
                "type": adventureType,
                "eid": urlParams.eid,
                "twid": urlParams.twid,
                "attendDate": attendDate,
                "isRecommend": isRecommend,
                "isReal": (typeof urlParams.admin !== "undefined" && urlParams.admin !== "") ? false : true,
                "isSuperAdmin": superAdmin,
                "tripWorkshopTitle": tripOrWorkshop
            }

            setIsReviewSubmit(true);
            const result = await postReview(token, formData);
            if (result.status === 200) {
                if (result.data.success === "SUCCESS") {
                    setSuccessPopup(true);
                    setIsExists(false);
                } else if (result.data.success === "EXISTS") {
                    setSuccessPopup(true);
                    setIsExists(true);
                }
            }
            setIsReviewSubmit(false);
        }


    }

    const redirectToHomePage = () => {
        router.push('/');
    }

    const loadForm = (
        <Col xs={15} sm={15} md={15} lg={15} xl={15}>
            <div className="review-container">
                <Row>
                    <div className="r-title">
                        <span >Leave a Review </span>
                    </div>
                </Row>
                <Row>
                    {typeof urlParams.admin !== "undefined" && urlParams.admin !== "" &&
                        <Row className="rr-section first-name-date" gutter={24}>
                            {superAdmin &&
                                <Col xs={24} sm={24} md={24} lg={24} xl={24} className="mb20">
                                    <Form.Item label="Trip/Workshop Name">
                                        {getFieldDecorator("twname", { rules: [{ required: false, message: 'Trip/Workshop Name' }] })
                                            (<Input placeholder="Enter Trip or Workshop Name" onChange={(e) => setTripOrWorkshop(e.target.value)} />)}
                                    </Form.Item>
                                </Col>
                            }
                            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                                <Form.Item label="Full Name">
                                    {getFieldDecorator("fullName", { rules: [{ required: false, message: 'Full Name is required' }] })
                                        (<Input placeholder="Enter First Name" onChange={(e) => setFullName(e.target.value)} />)}
                                </Form.Item>
                            </Col>

                            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                                <Form.Item label={"Review Date"}>
                                    {getFieldDecorator("date", {
                                        initialValue: moment(),
                                        rules: [
                                            { required: false, message: "Please Enter End Date" },
                                        ],
                                    })(
                                        <DatePicker

                                            onChange={(e) => setDate(e)}
                                            format="YYYY-MM-DD"
                                            className="fill-width"
                                            placeholder="YYYY-MM-DD"
                                        // disabled={startDate ? false : true}
                                        />
                                    )}
                                </Form.Item>
                            </Col>

                        </Row>
                    }

                    <Row className={`rr-section ${typeof urlParams.admin !== "undefined" && urlParams.admin !== "" ? '' : 'mt15'}`}>
                        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                            <p className="r-sub-title">How was your overall experience?</p>
                            <div className="rr-child rr-child-rate">
                                <Rate
                                    style={{ color: "#FFBC00" }}
                                    defaultValue={defaultRate}
                                    onChange={(e) => [setDefaultRate(e), setRateError(false)]}
                                />
                                {rateError && <p className="r-error">Please select the star.</p>}
                            </div>
                        </Col>
                    </Row>

                    <p className="r-sub-title">Describe Your Experience</p>
                    <TextArea
                        onChange={(e) => handleCommentChange(e)}
                        id="Review"
                        rows={4}
                        className="rr-section"
                        placeholder="Tell us about your Experience"
                    />


                    <Row className="rr-section">
                        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                            <p className="r-sub-title">Would you recommend this expert?</p>
                            <div className="like-dislike">
                                <span>
                                    <Icon onClick={() => setIsRecommend('yes')} type="like" theme={isRecommend === "yes" ? 'twoTone' : 'outlined'} />
                                    &nbsp;Yes
                                    </span> <span> &nbsp; &nbsp;
                                    <Icon type="dislike" onClick={() => setIsRecommend('no')} theme={isRecommend === "no" ? 'twoTone' : 'outlined'} /> &nbsp;No</span>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <p className="r-sub-title mt15">Is there anything you would like to tell EC about your experience?</p>
                        <TextArea
                            onChange={(e) => handleCommentChange(e)}
                            id="ECReview"
                            rows={4}
                            placeholder="Tell us about your Experience"
                        />
                    </Row>
                    <Button tmlType="submit" className="r_submit_btn mt20" disabled={((isLogin && !isLoading && notFound) || isReviewSubmit)} onClick={handleReviewSubmit} >
                        {isLogin && !isLoading && notFound ? <small> You are not eligible to give feedback</small> : "SUBMIT"}
                    </Button>
                    <div></div>
                </Row>
            </div>
        </Col>
    );

    return (
        <div className="review-page">
            {!isLoading && !notFound &&
                <Row>
                    <div>
                        <Col xs={9} sm={9} md={9} lg={9} xl={9} className={`review-parent ${superAdmin ? 'hight-max' : ''}`}>
                            <div className="review-img">
                                <div className="review-img-overlay">
                                </div>
                                <div className="review-expert">
                                    <p className="review-expert__name">{reviewData.expert.name}</p>
                                    <p className="review-expert__title">{typeof reviewData.learning !== "undefined" ? reviewData.learning.title : typeof reviewData.trip !== "undefined" ? reviewData.trip.title : ""} </p>
                                    <p className="review-expert__date">Attended on {getDateFormat(reviewData.date)}</p>
                                    <div>
                                        <img src={reviewData.expert.profile}/>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </div>
                    {loadForm}
                </Row>
            }
            {isLoading &&
                <div className="text-center mt200 py20 loader-absolute-class">
                    <AppLoader />
                </div>
            }
            {!isLoading && notFound &&
                <Row>
                    <div>
                        <Col xs={9} sm={9} md={9} lg={9} xl={9} className="review-parent">
                            <div className="review-img">
                                <div className="review-img-overlay">
                                </div>
                                <div className="review-expert">
                                    <p>Please Logged In</p>
                                    <p>Please Logged In </p>
                                    <p>Attended on Please Logged In</p>
                                    <div>
                                        <img src={Placeholder}/>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </div>
                    {loadForm}

                </Row>
            }

            {errorPopup &&
                <Modal
                    centered
                    className="auth-modal success-modal"
                    width={380}
                    closable={false}
                    maskClosable={false}
                    visible={errorPopup}
                >
                    <div className="text-center">
                        <img src={Success} alt="" />
                        <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
                            Invalid Booking
               </h1>
                        <p className="an-18 mb20 regular-text">
                            you are not eligible to give feedback
               </p>

                        <Button type="primary" className="ex__primary_btn" onClick={() => setErrorPopup(false)}>
                            Ok
                            </Button>

                    </div>
                </Modal>
            }

            {successPopup &&
                <Modal
                    centered
                    className="auth-modal success-modal"
                    width={380}
                    closable={false}
                    maskClosable={false}
                    visible={successPopup}
                >
                    <div className="text-center">
                        <img src={Success} alt="" />
                        <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
                            {isExists ? "Thank You" : "Successful"}
                        </h1>
                        <p className="an-18 mb20 regular-text">
                            {isExists ? "You have already submitted the feedback" : "your feedback has been recorded"}

                        </p>

                        <Button type="primary" className="ex__primary_btn" onClick={() => redirectToHomePage()}>
                            Ok
                            </Button>

                    </div>
                </Modal>
            }

        </div>
    )
}


const TmpReview = Form.create({ name: "createReview" })(Review);

export default compose(TmpReview);