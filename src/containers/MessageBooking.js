import React, { useState, useEffect, useCallback } from 'react';
import {
  Icon,
  Input,
  Button,
  Row,
  Col,
  Collapse,
  Avatar,
  Select,
  Dropdown,
  Menu,
  Pagination,
} from 'antd';
import { useMediaQuery } from 'react-responsive';
import { CaretRightOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';




import {
  getExpertBookings,
  getBookingDetails,
  starBooking,
  hideBooking,
  deleteBooking,
} from '../services/expert';
import { CaptalizeFirst, formatPhoneNumber } from '../helpers/methods';
import { useRouter } from 'next/router';

const ThreeDot = '/images/dot.svg';
const SearchIc = '/images/search.svg';
const tripMenuImag = '/images/list_view_trip.svg';
const tripMenuImagActive = '/images/view_comfy_active.svg';

const { Search } = Input;
const suffix = <img src={SearchIc} alt='' />;

const { Panel } = Collapse;

const MessageMenu = (props) => {
  return (
    <Menu>
      <Menu.Item>
        <Button type='link' onClick={() => props.hide()}>
          Hide
        </Button>
      </Menu.Item>
      <Menu.Item>
        <Button type='link' onClick={() => props.delete()}>
          Delete
        </Button>
      </Menu.Item>
    </Menu>
  );
};

const Message = (props) => {
  const router = useRouter()
  const isTabletOrMobileDevice = useMediaQuery({
    query: '(max-device-width: 767px)',
  });
  const { id } = useSelector((state) => state.expert);
  const token = useSelector((state) => state.auth.accessToken);
  const [bookings, setBookings] = useState([]);
  const [loader, setLoader] = useState(true)
  const [selectedBooking, setSelectedBooking] = useState();
  const [bookingsData, setBookingsData] = useState([]);
  const [search, setSearch] = useState({ name: '', status: 'active' });
  const [paginate, setPaginate] = useState({
    current: 1,
    pageMinValue: 0,
    pageOffset: 0,
    pageLimit: 10,
  });

  const getBooking = useCallback(
    async (id, selected) => {
      try {
        const result = await getExpertBookings(token, id, search);
        setLoader(false);
        if (result.status === 200) {
          setBookings(result.data.data);
          if (result.data.data.length > 0) {
            if (!isTabletOrMobileDevice) {
              if(!selected) {
                setSelectedBooking(result.data.data[0]);
                const d =
                  result.data.data[0].learning || result.data.data[0].trip;
                  getBookingDetail(d._id);
              }
            }
          }
        }
      } catch (error) {}
    },
    [token, search],
  );

  const getBookingDetail = useCallback(
    async (id) => {
      try {
        const result = await getBookingDetails(token, id);
        setBookingsData(result.data.data);
      } catch (error) {}
    },
    [token],
  );

  useEffect(() => {
    if (id) {
      setLoader(true);
      getBooking(id, selectedBooking);
    }
  }, [getBooking, id]);

  const handleSearch = (obj) => {
    setSearch({ ...search, ...obj });
    getBooking(id);
  };

  const handleChange = (value) => {
    const newPaginate = paginate;
    newPaginate.current = value;
    newPaginate.pageOffset = value * 10 - 10;
    newPaginate.pageLimit = value * 10;
    setPaginate(newPaginate);
  };

  const getActiveClass = (chat) => {
    if (selectedBooking && chat.id === selectedBooking.id) {
      return 'active';
    }
    return '';
  };

  return (
    <div className='container booking-message'>
      <Row className='main-message-box'>
        <Col xs={24} sm={24} md={8} lg={8} xl={7}>
          <div className='resposive-message-div'>
            <div
              className='message'
              onClick={() => {
                setSelectedBooking('');
              }}
            >
              <h3>
                Message{' '}
                {/* <span className='' active_count>
                  2
                </span>{' '} */}
              </h3>
            </div>
            <div className='message-btn-res'>
              <Button
                type='primary'
                size='default'
                shape='round'
                className='ml-5'
                onClick={() => {
                  router.push('/messages');
                }}
              >
                All Message
              </Button>
              <Button
                type='primary'
                size='default'
                shape='round'
                className='active_massage'
                onClick={() => {
                  setSelectedBooking('');
                }}
              >
                Booking Details
              </Button>
            </div>
          </div>
          <div
            className={`message-left-box ${
              isTabletOrMobileDevice && selectedBooking ? 'mobile-messge' : ''
            }`}
          >
            <div className='serach-bar'>
              <Search
                placeholder='input search text'
                onSearch={(value) => handleSearch({ name: value })}
                enterButton
                suffix={suffix}
              />
              <div className='filter-button'>
                <Select
                  defaultValue='All'
                  style={{ width: '100%' }}
                  onChange={(value) => {
                    handleSearch({ status: value });
                  }}
                >
                  <Select.Option value='active'>All</Select.Option>
                  <Select.Option value='hidden'>Hidden</Select.Option>
                  <Select.Option value='fav'>Favourite</Select.Option>
                </Select>
              </div>
            </div>
            <div className={`chat-person-aera`}>
              {bookings.length === 0 && !loader && (
                <p style={{ textAlign: 'center', paddingTop: '15px' }}>
                  No bookings available!
                </p>
              )}
              {/* {loader && <Loader /> }  */}
              {bookings.length > 0 &&
                bookings
                  .slice(paginate.pageOffset, paginate.pageLimit)
                  .map((booking, i) => {
                    const bookmarkClass = booking.bookmark ? 'fillted' : '';
                    const tour = booking.learning || booking.trip;
                    let style = {background: '#fff'};
                    if (tour && tour.dateType === 1) {
                      for(let i = 0; i < tour.dateTime.length; i++){
                        const d = tour.dateTime[i];
                        if(moment(booking.date).isAfter(d.toTime)) {
                          style = { background: '#fff' };
                          break;
                        } else {
                          style = { background: 'rgba(242, 245, 245, 0.8)' };
                          break;
                        }
                      }
                    }
                    return (
                      <div className={`profile ${getActiveClass(booking)}`} style={style} key={i}>
                        <div
                          className='profile-cotnet'
                          onClick={() => {
                            setSelectedBooking(booking);
                            getBookingDetail(
                              booking.learning
                                ? booking.learning._id
                                : booking.trip._id,
                            );
                          }}
                        >
                          <div className='profile-photo'>
                            <Avatar src={booking.image} alt='' />
                          </div>
                          <div className='message-details'>
                            {/* <div className='person-name'>
                          <h5>{CaptalizeFirst(booking.user.name)}</h5>
                        </div> */}
                            <div className='message-name'>
                              <h5>
                                {CaptalizeFirst(
                                  booking.trip
                                    ? booking.trip.title
                                    : booking.learning.title,
                                )}
                              </h5>
                            </div>
                            <div
                              onClick={(event) => {
                                event.preventDefault();
                                event.stopPropagation();
                                const star = booking.bookmark ? false : true;
                                if(_.isEqual(selectedBooking.id, booking.id)){
                                  const newObj = selectedBooking;
                                  newObj.bookmark = star;
                                  setSelectedBooking(newObj);
                                }
                                starBooking(
                                  token,
                                  booking.trip ? booking.trip._id : booking.learning._id,
                                  star
                                ).then(() => {
                                  getBooking(id, selectedBooking);
                                });
                              }}
                              className={`bookmark ${bookmarkClass}`}
                            >
                              {' '}
                              <Icon type='star' theme='filled' />{' '}
                            </div>
                            <Dropdown
                              overlay={() => (
                                <MessageMenu
                                  msg={booking}
                                  hide={() => {
                                    hideBooking(
                                      token,
                                      booking.trip ? booking.trip._id : booking.learning._id,
                                    ).then(() => getBooking(id));
                                  }}
                                  delete={() => {
                                    deleteBooking(
                                      token,
                                      booking.trip ? booking.trip._id : booking.learning._id,
                                    ).then(() => getBooking(id));
                                  }}
                                />
                              )}
                              placement='bottomCenter'
                            >
                              <img
                                src={ThreeDot}
                                alt='#'
                                className='threeDot'
                              />
                            </Dropdown>
                          </div>
                        </div>
                        {/* <div className='booking-date'>
                      <div className='notification-dot'></div>
                      <div className='date'>
                        <h5>
                          Booking Date:{' '}
                          {moment(booking.createdAt).format('Do MMM YY')}
                        </h5>
                      </div>
                    </div> */}
                      </div>
                    );
                  })}
            </div>
            <div className='pagination-div'>
              <Pagination
                defaultPageSize={10}
                current={paginate.current}
                size='small'
                total={bookings.length}
                onChange={handleChange}
              />
            </div>
          </div>
        </Col>
        <Col
          xs={24}
          sm={24}
          md={16}
          lg={16}
          xl={17}
          className={`chat-person-aera ${
            isTabletOrMobileDevice && !selectedBooking ? 'mobile-messge' : ''
          }`}
        >
          {selectedBooking && (
            <div className='container fix_container_width'>
              <Col xs={24} sm={24} md={24} lg={24} xl={22}>
                <div className='trips_massage_name fix_top_padding'>
                  <div className='trips_massage_name_title'>
                    <h2>
                      {selectedBooking.trip
                        ? selectedBooking.trip.title
                        : selectedBooking.learning.title}
                      <div
                        className={`bookmark ${
                          selectedBooking.bookmark ? 'fillted' : ''
                        }`}
                        style={{ display: 'inline', fontSize: '15px', cursor: 'pointer' }}
                        onClick={(event) => {
                          event.preventDefault();
                          event.stopPropagation();
                          const star = selectedBooking.bookmark ? false : true;
                          const newObj = selectedBooking;
                          newObj.bookmark = star;
                          setSelectedBooking(newObj);
                          starBooking(
                            token,
                            selectedBooking.trip ? selectedBooking.trip._id : selectedBooking.learning._id,
                            star
                          ).then(() => {
                           getBooking(id, selectedBooking);
                          });
                        }}
                      >
                        {' '}
                        <Icon type='star' theme='filled' />{' '}
                      </div>
                    </h2>
                    <div className='menu_profile_right'>
                      <div className='side_profile_menu'>
                        <img alt='' className='grid_view' src={tripMenuImag} />
                      </div>
                      <div className='side_profile_menu'>
                        <img
                          className='grid_view grid_active'
                          src={tripMenuImagActive}
                          alt=''
                        />
                      </div>
                    </div>
                  </div>
                  {bookingsData.length > 0 &&
                    bookingsData
                      .sort((a, b) => a.slot - b.slot)
                      .map((book, index) => {
                        const tour =
                          selectedBooking.trip || selectedBooking.learning;
                        return (
                          <div className='trip_accordian' key={index}>
                            <Collapse
                              accordion
                              expandIconPosition={'right'}
                              expandIcon={({ isActive }) => (
                                <CaretRightOutlined
                                  rotate={isActive ? 90 : 0}
                                />
                              )}
                            >
                              <Panel
                                extra={
                                  <div className='trip_collapse'>
                                    <div className='trip_collapse_heding'>
                                      <p className='slot_01'>
                                        Slot {book.slot + 1}{' '}
                                      </p>
                                      {tour && tour.dateType === 1 && (
                                        <p className='date_time'>
                                          {tour.dateTime[book.slot] && moment(
                                            tour.dateTime[book.slot].fromDate,
                                          ).format('M/D/YY')}{' '}
                                          -{' '}
                                          {tour.dateTime[book.slot] && moment(
                                            tour.dateTime[book.slot].toDate,
                                          ).format('M/D/YY')}
                                        </p>
                                      )}
                                    </div>
                                    <p className='persone'>
                                      {book.peoples} : Persons
                                    </p>
                                  </div>
                                }
                                key='1'
                              >
                                <Row gutter={40} className='boxes_mani'>
                                  {book.data.map((b, i) => (
                                    <Col
                                      xs={6}
                                      sm={6}
                                      md={6}
                                      lg={6}
                                      xl={6}
                                      key={i}
                                      className='trip_collapse_box'
                                    >
                                      {console.log(b)}
                                      <div className='trip_profile_detai_name'>
                                        <div className='trip_name_profile_images'>
                                          <img
                                            className='img-fluid'
                                            src={b.user.profile}
                                            alt=''
                                          />
                                        </div>
                                        <h3>{CaptalizeFirst(b.user.name)}</h3>
                                        <p className='call_number'>
                                          {formatPhoneNumber(b.phone)}
                                        </p>
                                        <p className='participants'>
                                          Participants: {b.people}
                                        </p>
                                        <div className='trip_collaspe_button'>
                                          <Button
                                            onClick={() =>
                                              router.push(
                                                `/messages?id=${
                                                  b?.trip?._id || b.learning._id
                                                }&user=${b.user.id}`,
                                              )
                                            }
                                            className='view-more-trips'
                                          >
                                            Message
                                          </Button>
                                        </div>
                                      </div>
                                    </Col>
                                  ))}
                                </Row>
                              </Panel>
                            </Collapse>
                          </div>
                        );
                      })}
                </div>
              </Col>
            </div>
          )}

          {/* {Object.keys(selectedBooking).length > 0 && (
            <div className='container fix_container_width'>
              <Col xs={24} sm={24} md={24} lg={24} xl={22}>
                <div className='trips_massage_name fix_top_padding'>
                  <div className='trips_massage_name_title'>
                    <h2>
                      {selectedBooking.trip
                        ? selectedBooking.trip.title
                        : selectedBooking.learning.title}
                    </h2>
                    <div className='menu_profile_right'>
                      <div className='side_profile_menu'>
                        <img className='grid_view' src={tripMenuImag} />
                      </div>
                      <div className='side_profile_menu'>
                        <img
                          className='grid_view grid_active'
                          src={tripMenuImagActive}
                        />
                      </div>
                    </div>
                  </div>
                  <div className='trip_accordian'>
                    <Collapse
                      accordion
                      expandIconPosition={'right'}
                      expandIcon={({ isActive }) => (
                        <CaretRightOutlined rotate={isActive ? 90 : 0} />
                      )}
                    >
                      <Panel
                        extra={
                          <div className='trip_collapse'>
                            <div className='trip_collapse_heding'>
                              <p className='slot_01'>Slot 01 </p>
                              <p className='date_time'>
                                {selectedBooking.trip
                                  ? moment(
                                      selectedBooking.trip.createdAt,
                                    ).format('Do MMM YY')
                                  : moment(
                                      selectedBooking.learning.createdAt,
                                    ).format('Do MMM YY')}
                              </p>
                            </div>
                            <p className='persone'>
                              {selectedBooking.people} : Persons
                            </p>
                          </div>
                        }
                        key='1'
                      >
                        <Row gutter={40} className='boxes_mani'>
                          <Col
                            xs={12}
                            sm={12}
                            md={12}
                            lg={12}
                            xl={6}
                            className='trip_collapse_box'
                          >
                            <div className='trip_profile_detai_name'>
                              <div className='trip_name_profile_images'>
                                <img
                                  className='img-fluid'
                                  src={selectedBooking.user.profile}
                                />
                              </div>
                              <h3>
                                {CaptalizeFirst(selectedBooking.user.name)}
                              </h3>
                              <p className='call_number'>
                                {selectedBooking.phone}
                              </p>
                              <p className='participants'>
                                Participants: {selectedBooking.people}
                              </p>
                              <div className='trip_collaspe_button'>
                                <Button className='view-more-trips '>
                                  Message
                                </Button>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </Panel>
                    </Collapse>
                  </div>
                </div>
              </Col>
            </div>
          )} */}
        </Col>
      </Row>
    </div>
  );
};

export default Message;
