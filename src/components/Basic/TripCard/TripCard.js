import React, {useState} from "react";
import tripCardStyle from "./TripCard.module.scss";
import { compose } from "redux";
import { Card, Col, Dropdown, Icon, Popover, Rate, Row } from "antd";
import {
  CaptalizeFirst,
  DayorDaysNightOrNights,
  displayDifficultyText,
  getColorLogoURL,
  getCurrencySymbol,
} from "../../helpers/methods";
import shareOption from "containers/commonContainer/shareOption";
import { addorRemoveLikeForTrip } from "../../../services/expert";
import { useRouter } from 'next/router';

const Share = "/images/share_ic.png";
const getLikeStatus = (userId, likes) => {
  likes.forEach(like => {
    if(like.userId === userId){
      return true;
    }
  });
  return false;
}

const TripCard = (props) => {
  const router = useRouter();
  const [like, setLike] = useState(getLikeStatus(props.userId, props.likes));

  const onLikeButtonClick = async (event, id, view) => {
    event.stopPropagation();
    setLike(!like);
    const response = await addorRemoveLikeForTrip(props.token, {id});
    // console.log(response);
    // TODO: Handle create like failure here
  }
  
  const displayCommaSeprate = (lang) => {
    let resLang = "";
    if (lang.length > 0) {
      lang.map((a, index) => {
        let addCooma = "";
        if (lang.length !== index + 1) {
          addCooma = ", ";
        }
        resLang += CaptalizeFirst(a) + addCooma;
      });
    }

    return resLang;
  };

  return (
    <Card
      className="price_sec"
      hoverable
      onClick={() => router.push(`/trips-details/${props.id}`)}
      cover={<img alt="example" className="card_cover" src={props.cover} />}
    >
      {typeof props.medium !== undefined && props.medium === "online" && (
        <span className="online_tag an-10 medium-text">
          {CaptalizeFirst(props.medium)}{" "}
        </span>
      )}
      <div className="price_sec card_details">
        <div className="mt5 mb10 an-13 card-main-des">
          <span>
            {props.duration ? props.duration : ""}{" "}
            {DayorDaysNightOrNights("t", props.duration ? props.duration : "")}
            &nbsp;&nbsp;
          </span>
          <span className="secondLine">
            &nbsp;
            {props.country
              ? CaptalizeFirst(props.country)
              : CaptalizeFirst(props.medium)}{" "}
            &nbsp;<span className="secondLine"></span>{" "}
          </span>
          <span>
            {" "}
            &nbsp;
            {props.activity && displayCommaSeprate(props.activity)}
          </span>
        </div>

        <p className="mb10 an-15 medium-text card-main-title">
          {CaptalizeFirst(props.title ? props.title : "")}
        </p>
        <Row className="price_line">
          <Col xs={18} sm={18} md={18} lg={18} xl={18}>
            <h3 className="medium-text an-16 price-tag">
              <span className="spance_between_dollar">
                {getCurrencySymbol(props.priceCurrency)}
              </span>
              <span className="spance_between_txt">
                {props.price ? props.price : ""}
              </span>
            </h3>
          </Col>
          <Col xs={6} sm={6} md={6} lg={6} xl={6}>
            <div className="text-right">
              <Popover
                placement="bottom"
                content={`${displayDifficultyText(
                  props._doc ? props._doc.difficulty : props.difficulty
                )}`}
                trigger="hover"
              >
                <img
                  src={getColorLogoURL(
                    "trip",
                    props._doc ? props._doc.difficulty : props.difficulty
                  )}
                  alt="Skil level"
                />
              </Popover>
            </div>
          </Col>
        </Row>
      </div>
      <Row>
        <Col xs={16} sm={16} md={16} lg={16} xl={16}>
          <Rate allowHalf defaultValue={0} className="an-14 pt5" />
        </Col>
        <Col offset={4} xs={4} sm={4} md={4} lg={4} xl={4}>
          <div className="heart_fill trip_set_icon">
            <input type="checkbox" id="like1" />
            <div className="flex-x space-around">
              <Dropdown overlay={shareOption} placement="bottomCenter">
                <img
                  src={Share}
                  alt="Social Share"
                  className="share_icn"
                  style={{ width: "1.25rem" }}
                />
              </Dropdown>
              {/* <label htmlFor="like1"><svg viewBox="0 0 24 24"><path d="M12 21.35l-1.45-1.32c-5.15-4.67-8.55-7.75-8.55-11.53 0-3.08 2.42-5.5 5.5-5.5 1.74 0 3.41.81 4.5 2.09 1.09-1.28 2.76-2.09 4.5-2.09 3.08 0 5.5 2.42 5.5 5.5 0 3.78-3.4 6.86-8.55 11.54l-1.45 1.31z"></path></svg></label> */}
              <Icon
                type="heart"
                theme="filled"
                style={{
                  fontSize: "1.25rem",
                  color: `${like ? "#ff3c3c" : "#e8e8e8"}`,
                }}
                onClick={(event) => onLikeButtonClick(event, props.id)}
              />
            </div>
          </div>
        </Col>
      </Row>
    </Card>
  );
};

export default compose(TripCard);
