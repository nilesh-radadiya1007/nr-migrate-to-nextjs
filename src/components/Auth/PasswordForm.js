import React, { useState, useEffect } from "react";
import { Form, Input, Button, Divider, Modal } from "antd";

const PasswordForm = (props) => {
  const [formDate, setFormData] = useState({
    password: "",
  });

  const [wrongPassword, setWrongPassword] = useState(false);

  const [isVisible, setIsVisible] = useState(false);

  const { password } = formDate;

  const pass = localStorage.getItem("password");

  const onChange = (e) =>
    setFormData({ ...formDate, [e.target.name]: e.target.value });

  const removePassword = () => {
    localStorage.removeItem("password");
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (password) {
      if (password == process.env.NEXT_PUBLIC_PASSWORD) {
        localStorage.setItem("password", password);
        setIsVisible(false);
        setTimeout(removePassword, 10800000);
        window.location.reload();
      } else {
        setWrongPassword(true);
      }
    }
  };

  useEffect(() => {
    if (!pass) {
      setIsVisible(true);
    }
  }, [pass]);

  return (
    <div>
      <Modal visible={false} footer={null} closable={false}>
        <div>
          <p>Enter Password Below</p>
        </div>
        <Form onSubmit={(e) => onSubmit(e)}>
          <Form.Item hasFeedback label="Password">
            <Input.Password
              name="password"
              value={password}
              onChange={(e) => onChange(e)}
              placeholder="Password"
            />
          </Form.Item>
          {wrongPassword && (
            <p style={{ color: "red", fontSize: "16px" }}>
              The password you entered is incorrect
            </p>
          )}
          <Form.Item className="mb0">
            <div className="pt15 pb10">
              <Button
                type="primary"
                htmlType="submit"
                className="ex__primary_btn login-btn"
              >
                Login
              </Button>
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default PasswordForm;
