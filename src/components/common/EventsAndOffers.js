import React from "react";
import Link from "next/link";
import { compose } from "redux";
import { Row, Col } from "antd";

// import adventure1 from "assets/images/home/landingpage/cutnoise.png";
// import adventure2 from "assets/images/home/landingpage/destination1.jpg";
// import adventure5 from "assets/images/home/landingpage/SpendPTO1.jpg";
// import adventure6 from "assets/images/home/landingpage/adventure6.jpg";
// import adventure7 from "assets/images/home/landingpage/Bigthings2.jpg";
// import adventure8 from "assets/images/home/landingpage/bestof.png";
// import adventure9 from "assets/images/home/landingpage/ConnectNature1.jpg";

// import adventure33 from "assets/images/home/landingpage/Lastminute3.jpg";
// import adventure44 from "assets/images/home/landingpage/Weekend1.jpg";



import { useMediaQuery } from "react-responsive";

// import Slider from "react-slick";
// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const emailBox = "/images/home/landingpage/emailbox.svg";
const icon2 = "/images/home/landingpage/icon2.png";
const icon3 = "/images/home/landingpage/icon3.png";
const icon4 = "/images/home/landingpage/icon4.png";
const icon5 = "/images/home/landingpage/icon1.png";
const icon6 = "/images/home/landingpage/icon2.png";
const icon7 = "/images/home/landingpage/icon3.png";
const icon9 = "/images/home/landingpage/icon1.png";
const icon8 = "/images/home/landingpage/icon1.png";
const Lastminute3 = "/images/landingpage/Lastminute3.svg"
const SpendPTO1 = "/images/landingpage/SpendPTO1.svg"
const Weekend1 = "/images/landingpage/Weekend1.svg"
const Bigthings2 = "/images/landingpage/Bigthings2.svg"
const ConnectNature1 = "/images/landingpage/ConnectNature1.svg"
const adventureschoolimageimg = "/images/landingpage/adventureschoolimageimg.svg"
const bestof = "/images/landingpage/bestof.svg"
// const bestof = "/images/landingpage/bestof.png"
const destination1 = "/images/landingpage/destination1.svg"
const Cutcovid1 = "/images/landingpage/Cutcovid1.svg"

// const settings = {
//     dots: true,
//     rows: 3,
//     arrows: false,
//     speed: 500,
//     slidesToShow: 4,
//     slidesToScroll: 4,
//     responsive: [
//         {
//             breakpoint: 1200,
//             settings: {
//                 slidesToShow: 3,
//                 slidesToScroll: 3,
//             }
//         },
//         {
//             breakpoint: 768,
//             settings: {
//                 slidesToShow: 2,
//                 slidesToScroll: 2,
//             }
//         },
//         {
//             breakpoint: 576,
//             settings: {
//                 slidesToShow: 1,
//                 slidesToScroll: 1,
//             }
//         },
//     ]
// };

const EventAndAdventures = (props) => {
    const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });

    // const daysInMonth = (month, year) => {
    //     if (month < 12) {
    //         return new Date(year, month, 0).getDate();
    //     } else {
    //         return "31";
    //     }
    // }
    // const getMonth = (plus) => {
    //     let month = new Date().getMonth() + plus;
    //     if (month < 10) {
    //         return "0" + month;
    //     } else {
    //         return month;
    //     }
    // }


    let adventureData = [
        {
            "id": "1",
            "title": "Last chance to book for 2021",
            "subtitle": "Empty trails and seasonal discounts, what’s not to love?",
            "link": `/all-adventure?year=${new Date().getFullYear()}&isFlexible=false`,
            // "img": adventure33,
            "img": Lastminute3,
            "icon": icon2,
            'colMd': 12
        },
        {
            "id": "5",
            "title": "Spend that PTO",
            "subtitle": "Treat yourself to the experience of a lifetime. When a weekend is not enough.",
            "link": "/all-adventure?longAdventure=true",
            // "img": adventure5,
            // "img": 'https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/landingpage/SpendPTO1.jpg',
            "img": SpendPTO1,
            "icon": icon3,
            'colMd': 6
        },
        {
            "id": "4",
            "title": "Weekend Warriors",
            "subtitle": "Saturday and Sunday have never looked better",
            "link": "/all-adventure?weekend=true",
            // "img": adventure44,
            "img": Weekend1,
            "icon": icon6,
            'colMd': 6
        },
        {
            "id": "2",
            "title": "Big things on the horizon",
            "subtitle": "Book your 2022 expedition with peace of mind knowing our flexible booking guarantee has your back",
            "link": `/all-adventure?year=${new Date().getFullYear() + 1}&isFlexible=false`,
            // "img": adventure7,
            "img": Bigthings2,
            "icon": icon8,
            'colMd': 12
        },
        {
            "id": "3",
            "title": "Connect with nature",
            "subtitle": "Guided expeditions and workshops with conservation and field experts.",
            "link": `/all-adventure?activity=[%22Wildlife%20Photography%20and%20Filmmaking%22,%22Landscape%20Photography%22,%22Macro%20Photography%22,%22Birding%22,%22Wildlife%20Safari%22,%22Animal%20Watching%22,%22Nature%20Discovery%22,%22Nature%20Walks%22,%22Cycling%22,%22Mountain%20Biking%22,%22Camping%22,%22Hiking%22]&isFlexible=true`,
            // "img": adventure9,
            "img": ConnectNature1,
            "icon": icon5,
            'colMd': 6
        },
        {
            "id": "6",
            "title": "Adventure School",
            "subtitle": "Get certified and level up with the best courses and workshops",
            "link": '/all-adventure?adventure=workshop&&isFlexible=true',
            // "img": adventure6,
            "img": adventureschoolimageimg,
            "icon": icon4,
            'colMd': 6
        },
        {
            "id": "7",
            "title": "Best of Expeditions: UK",
            "subtitle": "Explore dozens of hand-selected adventures that you can book right now.",
            "link": '/all-adventure?country=United%20Kingdom&isFlexible=true',
            // "img": adventure8,
            "img": bestof,
            "icon": icon7,
            'colMd': 6
        },
        {
            "id": "8",
            "title": "Destionation: Europe",
            "subtitle": "Disconver Europe's best expeditions curated by top adventure professionals.",
            "link": `/all-adventure?country=group|Europe&isFlexible=true`,
            // "img": adventure2,
            "img": destination1,
            "icon": icon5,
            'colMd': 6
        },
        {
            "id": "9",
            "title": "Cut Through the noise of COVID",
            "subtitle": "Itching for an adventure, but not sure where to start? Let us help you find and navigate international trips you can confidently book right now.",
            "link": "/contact?type=covid",
            // "img": adventure1,
            "img": Cutcovid1,
            "icon": icon9,
            'colMd': 12
        },

    ]

    return (
        <>
            {isMaxWidth768 &&
                <div>
                    <Row gutter={[25, 25]} className="adventure_section_silder">
                        <Col xs={24} sm={24} md={6} lg={6} xl={6} className="pt5 pb5 accomo_thumb">
                            <div className="cover-bg">
                                <Link href={adventureData[0].link}>
                                    <a>
                                        <img src={adventureData[0].img} alt="Img" />
                                        <div className="adventure_section_txt">
                                            <p className="title">{adventureData[0].title}</p>
                                            <p className="sub-title">{adventureData[0].subtitle}</p>
                                        </div>
                                    </a>
                                </Link>
                            </div>
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6} xl={6} className="pt5 pb5 accomo_thumb">
                            <div className="cover-bg">
                                <Link href={adventureData[1].link}>
                                    <a>
                                        <img src={adventureData[1].img} alt="Img" />
                                        <div className="adventure_section_txt">
                                            <p className="title">{adventureData[1].title}</p>
                                            <p className="sub-title">{adventureData[1].subtitle}</p>
                                        </div>
                                    </a>
                                </Link>
                            </div>
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6} xl={6} className="pt5 pb5 accomo_thumb">
                            <div className="cover-bg">
                                <Link href={adventureData[2].link}>
                                    <a>
                                        <img src={adventureData[2].img} alt="Img" />
                                        <div className="adventure_section_txt">
                                            <p className="title">{adventureData[2].title}</p>
                                            <p className="sub-title">{adventureData[2].subtitle}</p>
                                        </div>
                                    </a>
                                </Link>
                            </div>
                        </Col>
                    </Row>
                    <Row gutter={[25, 25]} className="adventure_section_silder mobile">
                        {adventureData.slice(3).map((item, index) => {
                            return (
                                <Col xs={24} sm={24} md={6} lg={6} xl={6} className="pt5 pb5 accomo_thumb" key={index}>
                                    <div className="cover-bg">
                                        <Link href={item.link}>
                                            <a>
                                                <img src={item.img} alt="Img" />
                                                <div className="adventure_section_txt">
                                                    <p className="title">{item.title}</p>
                                                    <p className="sub-title">{item.subtitle}</p>
                                                </div>
                                            </a>
                                        </Link>
                                    </div>
                                </Col>
                            );
                        })}
                    </Row>
                </div>

            }
            {!isMaxWidth768 &&
                <Row gutter={12} className="adventure_section_silder">
                    {adventureData.map((item, index) => {
                        return (
                            <Col xs={24} sm={24} md={item.colMd} lg={item.colMd} xl={item.colMd} className="pt5 pb5 accomo_thumb" key={index}>
                                <div className="cover-bg">
                                    <Link href={item.link}>
                                        <a>
                                            {item.id === "9" && <img className="child-img" src={emailBox} alt="Img" />}
                                            <img src={item.img} alt="Img" />
                                            <div className="adventure_section_txt">
                                                <p className="title">{item.title}</p>
                                                <p className="sub-title">{item.subtitle}</p>
                                            </div>
                                        </a>
                                    </Link>
                                </div>
                            </Col>
                        );
                    })}
                </Row>
            }
        </>
    );
};

export default compose(EventAndAdventures);
