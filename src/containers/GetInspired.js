import React, { useState, useEffect, useCallback } from "react";
import { Row, Col, Select, Badge, Radio, Checkbox, Button, Popover, Menu, Icon, Input } from "antd";
import { compose } from "redux";
import axios from 'axios';
import { useRouter } from "next/router";

/**
 * App Imports
 */
import { DISPLAY_PER_PAGE, expertiseList, ActivityList } from "../helpers/constants";
import countries from "../helpers/countries";
import { getAllExperts } from "../services/expert";
import AppLoader from "../components/Loader";
import langs from "../helpers/langauges";
import { useMediaQuery } from "react-responsive";
// import mapIcon from "../../public/images/map.svg";
// import menuIcon from "../../public/images/menu.svg";
// import LangIcon from "../../public/images/newicon/language.svg";
// import AdventurerIcon from "../../public/images/adventurer.svg";
// import PinIcon from "../../public/images/Pin.svg";
import AdventuresMap from "../shared/map/map";
import SingleCardForExpert from "../components/common/SingleCardForExpert";
import Newsletter from '../components/common/NewsLetter';
import ExpeditionsConnect from "../components/common/ExpeditionsConnect";
// import Image from 'next/image';

const ApiKey = process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY;
// console.log(ApiKey);
const { Option } = Select;
let tempExperts = [];
let tmpSkillsData = [];
let tmpActivityData = [];


function GetInspired(props) {

    const router = useRouter();

    let urlParams = router.query;
    const [experts, setExpert] = useState([]);
    const [mapExperts, setMapExperts] = useState([])

    const [loader, setLoader] = useState(false);
    const [initialLoader, setInitialLoader] = useState(true);

    const [country, setCountry] = useState(urlParams.cnty || "");
    const [sortBy, setSortBy] = useState(urlParams.sortOrder || "1");
    const [currentPage, setCurrentPage] = useState(1);

    const [langauge, setLangauge] = useState([]);

    const [totalRecords, setTotalRecords] = useState(0);
    const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
    const [perPage, setPerPage] = useState(isMaxWidth768 ? DISPLAY_PER_PAGE : DISPLAY_PER_PAGE);
    const [currentView, setCurrentView] = useState("grid");
    const [hoveredCardId, setHoverCardId] = useState("");
    const [languageText, setLanguageText] = useState("Select Language");
    const [expertiseText, setExpertiseText] = useState("Select Expertise");
    const [rotateLangArrow, setRotateLangArrow] = useState(false);
    const [rotateExpertiseArrow, setRotateExpertiseArrow] = useState(false);

    // Below two list for langauge and expertise
    const [langaugeList, setLangaugeList] = useState(langs);
    const [expertiseFilter, setExpertiseFilter] = useState(expertiseList);
    const [, setDisplayActivityFeatured] = useState("");

    const [countryCoordinates, setCountryCoordinates] = useState();
    const [centerLatLong, setCenterLatLong] = useState();
    const [tmpvar, setTmpvar] = useState(false);
    const [isLoaded, setisLoaded] = useState(true);


    const getAllExpertsHandler = useCallback(async (value) => {
        try {
            setLoader(true);
            const result = await getAllExperts(value);
            if (result.status === 200) {
                setTmpvar(true);
                tempExperts = [...tempExperts, ...result.data.data];
                setExpert(tempExperts);
                setMapExperts(tempExperts)
                setTotalRecords(result.data.count);
                setInitialLoader(false);
                setLoader(false);
            }
            setLoader(false);
            setTmpvar(false);
        } catch (err) {
            console.log(err);
            setLoader(false);
        }
    }, []);


    // should be replaced with equivalent
    useEffect(() => {
        // return history.listen(location => {
        //     if (history.action === "POP") {
        //         let urlParams = router.query;
        //         filterFromURLParams(urlParams);
        //     }
        // });

        if (Object.keys(router.query).length > 0) {
            filterFromURLParams(urlParams);
        }
    }, []);

    const filterFromURLParams = (urlParams) => {
        setExpert([]);
        tempExperts = [];
        let tmpActivity = [], tmpLanguages = [], tmpSkills = [];
        setisLoaded(false);

        if (urlParams.activity !== undefined && urlParams.activity.length !== 0 && urlParams.activity !== "") {
            tmpActivity = urlParams.activity.replace(/'/g, '"');
            tmpActivity = JSON.parse(tmpActivity);
            onChangeExpertise(tmpActivity || []);
            tmpActivityData = tmpActivity || [];
        } else {
            onChangeExpertise(tmpActivity || []);
            tmpActivityData = tmpActivity || [];

        }

        if (urlParams.langauge !== undefined && urlParams.langauge.length !== 0 && urlParams.langauge !== "") {
            tmpLanguages = urlParams.langauge.replace(/'/g, '"');
            tmpLanguages = JSON.parse(tmpLanguages);
            onChangeLanguage(tmpLanguages || [])
            setLangauge(tmpLanguages || []);
        } else {
            onChangeLanguage(tmpLanguages || [])
            setLangauge(tmpLanguages || []);
        }

        if (urlParams.skills !== undefined && urlParams.skills.length !== 0 && urlParams.skills !== "") {
            tmpSkills = urlParams.skills.replace(/'/g, '"');
            tmpSkills = JSON.parse(tmpSkills);
            tmpSkillsData = tmpSkills;
        }

        setSortBy(urlParams.sortOrder || "1");
        setCountry(urlParams.cnty || "")

        const data = {
            country: urlParams.cnty || "",
            activity: tmpActivityData,
            langauge: tmpLanguages,
            sortOrder: urlParams.sortOrder || 1,
            perPage: perPage,
            currentPage: currentPage,
            skills: tmpSkillsData
        };

        getAllExpertsHandler(data);
        setisLoaded(true);
    }

    const refreshResult = (sortByData = 1) => {
        setMapExperts([]);
        const data = { country, activity: tmpActivityData, langauge, sortOrder: sortByData, perPage: perPage, currentPage: currentPage, skills: tmpSkillsData };
        getAllExpertsHandler(data);
    }

    useEffect(() => {
        if (Object.keys(router.query).length > 0) {
            let urlParams = router.query;
            displayActivity();
            filterFromURLParams(urlParams);
        } else {
            refreshResult(sortBy);
            displayActivity();
        }
    }, []);

    useEffect(() => {
        tempExperts = [];
    }, [currentView]);

    const onChangeLanguage = (checkedValues) => {
        setLangauge(checkedValues);
        if (checkedValues.length === 1) {
            setLanguageText(checkedValues[0]);
        } else if (checkedValues.length >= 2) {
            setLanguageText(checkedValues[0] + " +" + (checkedValues.length - 1));
        } else {
            setLanguageText('Select Language');
        }
    }

    const onSearchChange = (e, type) => {
        if (e.target.value !== "") {
            if (type === "expertise") {
                let filterExpertise = expertiseList.filter((expert) => {
                    return expert.name.toLowerCase().includes(e.target.value.toLowerCase())
                });
                setExpertiseFilter(filterExpertise);
            } else if (type === "language") {
                let filterLang = langs.filter((lang) => {
                    return lang.name.toLowerCase().includes(e.target.value.toLowerCase())
                });
                setLangaugeList(filterLang);
            }
        } else {
            setLangaugeList(langs);
            setExpertiseFilter(expertiseList);
        }
    }

    const languageMenu = (
        <>
            {isLoaded &&
                <div>
                    <div className="search-dropdown"><Input placeholder="Search langauge" allowClear onChange={(e) => onSearchChange(e, 'language')} /></div>
                    <Checkbox.Group onChange={onChangeLanguage} defaultValue={langauge}>
                        {langaugeList.map((exp, i) => (
                            <Menu key={i}>
                                <Menu.Item>
                                    <Checkbox key={i} value={exp.name}>{exp.name}</Checkbox>
                                </Menu.Item>

                            </Menu>
                        ))}
                    </Checkbox.Group>
                </div>
            }
        </>
    )

    const handleSearch = async (e) => {
        tempExperts = [];
        changeURL(sortBy);
        setInitialLoader(true);
        setExpert([]);
        setCurrentPage(1);
        refreshResult(sortBy);
        const res = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${country}&key=${ApiKey}`)
        if (res.data.results) {
            setCountryCoordinates(res.data?.results?.[0]?.geometry.location)
        }
    }

    const changeURL = (sortByData = "") => {

        let act = tmpActivityData.length ? JSON.stringify(tmpActivityData) : "";
        let skill = tmpSkillsData !== undefined && tmpSkillsData.length ? JSON.stringify(tmpSkillsData) : "";
        let lan = langauge.length ? JSON.stringify(langauge) : "";
        router.push(`/experts?cnty=${country}&langauge=${lan}&activity=${act}&skills=${skill}&sortOrder=${sortByData}&currentPage=${currentPage}&perPage=${perPage}`);

    }

    const setCardMarkerHover = id => setHoverCardId(id);

    const resetCardMarkerHover = () => setHoverCardId("");

    const onSortOrderChanged = (e) => {
        setSortBy(e);
        changeURL(e);
        tempExperts = [];
        setInitialLoader(true);
        setExpert([]);
        refreshResult(e || sortBy);
    }

    const onFeaturedSkillsChange = (e, addOrRemove) => {
        setInitialLoader(true);
        setExpert([]);
        tempExperts = [];
        if (addOrRemove === "add") {
            tmpSkillsData = [...tmpSkillsData, e];
        } else if (addOrRemove === "remove") {
            tmpSkillsData = tmpSkillsData.filter((el) => el !== e);
        }
        changeURL();
        refreshResult(sortBy);
        displayActivity();
    }


    const displayActivity = () => {
        // Activity List = skills field in database
        let tmpExpertise = ActivityList.map((item) => {
            if (typeof item.isFeatured !== "undefined" && item.isFeatured) {
                return (
                    <div className={`${tmpSkillsData.includes(item.name) ? 'active' : ''} extra-parent`}>
                        <div className={`extra-child`} onClick={() => onFeaturedSkillsChange(item.name, tmpSkillsData.includes(item.name) ? 'remove' : 'add')}>
                            <span>
                                <img src={'/images/newicon/expertise/' + item.icon + '.svg'} alt="" />
                                {/* <Image layout="fill" src={typeof item.icon !== "undefined" ? [item.icon] : "defaultIcon"} alt="Map Icon" layout="fill" /> */}
                            </span>
                            {item.name} </div>
                    </div>
                )
            }
        });
        setDisplayActivityFeatured(tmpExpertise);

    }

    //Update trips according to the marker positions
    const setExpertsOnMapChange = (boundedExperts) => {
        try {
            let expertsData = []
            if (boundedExperts && boundedExperts.length > 0) {
                boundedExperts.map((expert, index) => {
                    if (expert) {
                        const tempExperts = experts.find(x => x.id === expert)
                        if (tempExperts)
                            expertsData.push(tempExperts);
                    }
                    return true
                })
            }
            setMapExperts(expertsData);
        } catch (err) {

        }
    }

    // const findNearMe = async (key) => {
    //     tempExperts = [];
    //     if (country !== "") {
    //         setCountry("");
    //         const data = { country: "", activity: tmpActivityData, langauge, sortOrder: sortBy, perPage: perPage, currentPage: currentPage, skills: tmpSkillsData };
    //         getAllExpertsHandler(data);

    //     }
    //     setCountryCoordinates();
    //     setCenterLatLong();

    //     if ("geolocation" in navigator) {
    //         window.navigator.geolocation.getCurrentPosition(async function (position) {
    //             setCenterLatLong({ "lat": position.coords.latitude, "lng": position.coords.longitude });
    //         });
    //     } else {
    //         alert('Please allow location for the map')
    //     }
    //     setPerPage(1000);
    //     setCurrentView('map');
    // }

    const onChangeExpertise = (checkedValues) => {
        tmpActivityData = checkedValues;
        if (checkedValues.length === 1) {
            setExpertiseText(checkedValues[0]);
        } else if (checkedValues.length >= 2) {
            setExpertiseText(checkedValues[0] + " +" + (checkedValues.length - 1));
        } else {
            setExpertiseText('Select Expertise');
        }
    }

    const expertiseMenu = (
        <>
            {isLoaded &&
                <div>
                    <div className="search-dropdown"><Input placeholder="Search expertise" allowClear onChange={(e) => onSearchChange(e, 'expertise')} /></div>
                    <Checkbox.Group onChange={onChangeExpertise} defaultValue={tmpActivityData} >
                        {expertiseFilter.map((exp, i) => (
                            <Menu key={i}>
                                <Menu.Item>
                                    <Checkbox key={i} value={exp.name}>{exp.name}</Checkbox>
                                </Menu.Item>

                            </Menu>
                        ))}
                    </Checkbox.Group>
                </div>
            }
        </>
    )

    return (
        <div className="header-container getinspired-page">
            <div className="expert_head_bg expert_head_bg_color">
                <div className="expert_header_info">
                    <div className="bg-title">
                        <span className="first-txt">Be</span><span className="second-txt">INSPIRED</span>
                    </div>
                    <div className="bg-sub-title">
                        <span>Once in a Lifetime Experiences, led by Experts.</span>
                    </div>
                    <div className="filter-section">
                        <div className="filter-section-child">
                            <Row gutter={0} >
                                <Col xs={24} sm={24} md={7} lg={7} xl={7} className="types right-border">
                                    <div className="filter-type-img">
                                        <img src={"/images/adventurer.svg"} alt="Filter Type" className="group" />
                                    </div>
                                    <div className="filter-type-right">

                                        <div className="filter-sub-heading">Expertise</div>
                                        {isLoaded &&
                                            <Popover placement={`${isMaxWidth768 ? "bottom" : "bottomLeft"}`} content={expertiseMenu} overlayClassName="language-dropdown" trigger="click" onVisibleChange={() => setRotateExpertiseArrow(!rotateExpertiseArrow)}>
                                                <div className="filter-heading activity-filter-txt" style={{ cursor: "pointer" }}>
                                                    {expertiseText}

                                                    <span className={`${rotateExpertiseArrow ? 'rotate activity-drop-arrow' : 'activity-drop-arrow'}`} style={{ float: "right" }}>
                                                        <svg viewBox="0 0 1024 1024" focusable="false" className="" data-icon="caret-down" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M840.4 300H183.6c-19.7 0-30.7 20.8-18.5 35l328.4 380.8c9.4 10.9 27.5 10.9 37 0L858.9 335c12.2-14.2 1.2-35-18.5-35z"></path></svg>
                                                    </span>

                                                </div>
                                            </Popover>
                                        }
                                    </div>

                                </Col>
                                <Col xs={24} sm={24} md={7} lg={7} xl={7} className="types right-border">
                                    <div className="filter-type-img a_type">
                                        <img src={"/images/newicon/language.svg"} alt="Filter Type" className="type" />
                                    </div>
                                    <div className="filter-type-right">

                                        <div className="filter-sub-heading">Languages</div>
                                        {isLoaded &&
                                            <Popover placement={`${isMaxWidth768 ? "bottom" : "bottomLeft"}`} content={languageMenu} overlayClassName="language-dropdown" trigger="click" onVisibleChange={() => setRotateLangArrow(!rotateLangArrow)}>
                                                <div className="filter-heading activity-filter-txt" style={{ cursor: "pointer" }}>
                                                    {languageText}

                                                    <span className={`${rotateLangArrow ? 'rotate activity-drop-arrow' : 'activity-drop-arrow'}`} style={{ float: "right" }}>
                                                        <svg viewBox="0 0 1024 1024" focusable="false" className="" data-icon="caret-down" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M840.4 300H183.6c-19.7 0-30.7 20.8-18.5 35l328.4 380.8c9.4 10.9 27.5 10.9 37 0L858.9 335c12.2-14.2 1.2-35-18.5-35z"></path></svg>
                                                    </span>

                                                </div>
                                            </Popover>
                                        }
                                    </div>
                                </Col>
                                <Col xs={24} sm={24} md={7} lg={7} xl={7} className="types">
                                    <div className="filter-type-img">
                                        <img src={"/images/Pin.svg"} alt="Filter Type" className="pin" />
                                    </div>
                                    <div className="filter-type-right">
                                        <div className="filter-sub-heading">
                                            Country
                                        </div>
                                        <div className="filter-heading">
                                            {isLoaded &&
                                                <Select
                                                    showSearch
                                                    showArrow={true}
                                                    placeholder="Select Country"
                                                    onChange={(e) => setCountry(e)}
                                                    defaultValue={country}
                                                    suffixIcon={<Icon type="caret-down" style={{ fontSize: '17px', color: '#000000' }} />}
                                                >
                                                    <Option value="">All</Option>
                                                    {countries.map((exp, i) => (
                                                        <Option key={i} value={exp.name}>
                                                            {exp.name}
                                                        </Option>
                                                    ))}
                                                </Select>
                                            }
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={24} sm={24} md={3} lg={3} xl={3} className="search-parent">
                                    <Button className="search-btn" id="expert_search" onClick={() => handleSearch()}>Search</Button>
                                </Col>
                            </Row>
                        </div>
                    </div>
                    <Row gutter={0} >
                        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                            <div className="extra-filters">
                                {/* <div className="extra-parent active">
                                    <div className={`extra-child`} onClick={findNearMe} > <span ><Image layout="fill" layout="fill" src={locatorIconYellow} alt="Map Icon" /></span>  Experts Near Me </div>
                                </div> */}
                                {/* {displayActivityFeatured} */}

                            </div>
                        </Col>
                    </Row>



                </div>

            </div>

            <div className="container-fluid">
                <div className="learning_sec">
                    <Row gutter={0} className="border-bottom-1 mt25 pb20">
                        <Col xs={24} sm={24} md={6} lg={6} xl={6} className="pt25 pr0 text-left sorted_by_select">
                            <div className="total_records">{currentView === 'grid' ? totalRecords : mapExperts.length} Results</div>
                        </Col>


                        <Col xs={24} sm={24} md={18} lg={18} xl={18} className="pt25 pr0 text-right sorted_by_select">
                            <div className="filter-view-section">
                                <div className={`filter-view-child grid ${currentView === "grid" ? 'active' : ''}`} onClick={() => { setPerPage(isMaxWidth768 ? DISPLAY_PER_PAGE : DISPLAY_PER_PAGE); setCurrentView('grid'); }}><img src={"/images/menu.svg"} alt="Menu Icon" />Grid</div>
                                <div className={`filter-view-child mapview ${currentView === 'map' ? 'active' : ''}`} onClick={() => { setTotalRecords(0); setCurrentView('map'); setPerPage(1000); }}> <img src={'/images/map.svg'} alt="Map Icon" />Map</div>
                                {/* <div className={`filter-view-child mapview ${currentView === 'map' ? 'active' : ''}`} > <Image layout="fill" src={mapIcon} alt="Map Icon" layout="fill" />Map</div> */}
                                <div className="filter-view-child sort-By">SORT BY:
                                    {isLoaded &&
                                        <Select
                                            defaultValue={sortBy}
                                            onChange={(e) => onSortOrderChanged(e)}
                                            className="pr0"
                                        >
                                            <Option value="1">A-Z</Option>
                                            <Option value="-1">Z-A</Option>
                                        </Select>
                                    }
                                </div>
                            </div>
                        </Col>

                    </Row>

                    <Row gutter={0}>
                        <Col xs={24} sm={24} md={24} lg={24} xl={24} className="web_grid">
                            <div className="" id="scroll-here">
                                <Row gutter={[40, 25]} style={{ marginTop: "10px" }}>
                                    {initialLoader ? (
                                        <div className="text-center py20 loader-absolute-class mb20 min-height">
                                            <AppLoader />
                                        </div>
                                    ) : (<Col xs={24} sm={24} md={24} lg={24} xl={24} className="padding-zero">
                                        <div>
                                            {currentView === 'grid' && experts.map((t, index) => {
                                                return (
                                                    <Col xs={24} sm={24} md={12} lg={8} xl={8} key={index} className="gutter-row">
                                                        <SingleCardForExpert
                                                            expert={t}
                                                            index={index}
                                                            type='directoryCard'
                                                        />
                                                    </Col>
                                                )
                                            })}
                                            {currentView === "map" &&
                                                <>
                                                    <Row gutter={[20, 25]} className="map-view">
                                                        <Col xs={24} sm={24} md={8} lg={8} xl={8} className={`${isMaxWidth768 ? "hide map-view-card" : 'map-view-card'}`} style={{ paddingTop: "0px" }} style={{ paddingTop: "0px" }}>
                                                            {loader ? (
                                                                <div className="text-center">
                                                                    <AppLoader />
                                                                </div>) : mapExperts.length === 0 && <div className="mb10 an-14 medium-text text-center mt20">
                                                                    <h4>Sorry, no results found for your selection. Please search another location on map or adjust the filters.</h4>
                                                                </div>}

                                                            {mapExperts ? mapExperts.map((t, index) => {
                                                                return (
                                                                    <Col xs={24} sm={24} md={12} lg={24} xl={24} key={index} className={`${index === 0 ? 'ptop0 gutter-row' : 'gutter-row'}`}>
                                                                        <SingleCardForExpert
                                                                            expert={t}
                                                                            index={index}
                                                                            onMouseEnter={(id) => setCardMarkerHover(id)}
                                                                            onMouseLeave={(id) => resetCardMarkerHover(id)}
                                                                            type='mapCard'
                                                                        />
                                                                    </Col>
                                                                )
                                                            }) : experts.map((t, index) => {
                                                                return (
                                                                    <Col xs={24} sm={24} md={12} lg={24} xl={24} key={index} className={`${index === 0 ? 'ptop0 gutter-row' : 'gutter-row'}`}>
                                                                        <SingleCardForExpert
                                                                            expert={t}
                                                                            index={index}
                                                                            onMouseEnter={(id) => setCardMarkerHover(id)}
                                                                            onMouseLeave={(id) => resetCardMarkerHover(id)}
                                                                            type='mapCard'
                                                                        />
                                                                    </Col>
                                                                )
                                                            })}
                                                        </Col>
                                                        <Col xs={24} sm={24} md={16} lg={16} xl={16} className="ptop0" style={{ paddingLeft: "0px" }}>
                                                            {experts.length > 0 &&
                                                                <>
                                                                    <div className="close-map">  <Icon type="close-circle" onClick={() => setCurrentView('grid')} /></div>
                                                                    <AdventuresMap
                                                                        country={country}
                                                                        multiple
                                                                        locations={experts}
                                                                        zoom={5}
                                                                        ApiKey={ApiKey}
                                                                        height={650}
                                                                        hoveredCardId={hoveredCardId}
                                                                        myLocation={centerLatLong}
                                                                        setTripsOnMapChange={(boundedTrips) => setExpertsOnMapChange(boundedTrips)}
                                                                        countryCoordinates={countryCoordinates}
                                                                        map={'expertMap'}
                                                                    />

                                                                </>
                                                            }
                                                        </Col>
                                                    </Row>

                                                </>}

                                        </div>
                                    </Col>)}
                                </Row>

                            </div>
                        </Col>
                    </Row>

                    {!initialLoader && currentView !== "map" &&
                        <Row gutter={0} className="mt25 mb25">

                            <Row>
                                {loader ? (
                                    <div className="text-center py20 loader-absolute-class">
                                        <AppLoader />
                                    </div>
                                ) : experts.length === 0 ? (
                                    <div className="mb100 an-14 medium-text">
                                        <h4>Sorry, no results found</h4>
                                    </div>
                                ) : (
                                    <>
                                        {experts.length >= totalRecords ?
                                            <div className="show-more no-more-result">
                                                SHOW MORE
                                            </div>
                                            : <div className="show-more" onClick={() => setCurrentPage(currentPage + 1)}>
                                                SHOW MORE
                                            </div>}

                                    </>
                                )}
                            </Row>
                        </Row>
                    }

                    {/* {!initialLoader && currentView === "map" &&
                        <Row gutter={[56, 10]} className="mt10 mb20">
                            <Col xs={24} sm={24} md={8} lg={8} xl={8} className="show-more-parent">
                                {loader ? (
                                    <div className="text-center py20 loader-absolute-class">
                                        <AppLoader />
                                    </div>
                                ) : experts.length === 0 ? (
                                    <div className="mb10 an-14 medium-text text-center mt50 mb50">
                                        <h4>No results are found</h4>
                                    </div>
                                ) : (
                                            <>
                                                {experts.length >= totalRecords ?
                                                    <div className="show-more no-more-result">
                                                        SHOW MORE
                                                            </div>
                                                    : <div className="show-more" onClick={() => setCurrentPage(currentPage + 1)}>
                                                        SHOW MORE
                                                        </div>}

                                            </>
                                        )}
                            </Col>
                        </Row>
                    } */}
                </div>

            </div>
            <div className="why-choose-us-section">
                <ExpeditionsConnect className="pt60" />
            </div>

            <Newsletter page="get_inspired" />
        </div >
    );
};

export default compose(GetInspired);