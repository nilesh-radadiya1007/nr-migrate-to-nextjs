import React from "react";
import PropTypes from "prop-types";
import GoogleMapReact from "google-map-react";
import Marker from "../../components/Trips/MarkerAnimated.js";

const createMapOptions = (map) => {
  return {
    fullscreenControl: false,
    mapTypeControl: false,
    panControl: false,
    streetViewControl: false,
    zoomControl: "true",
    gestureHandling: "greedy",
    zoomControlOptions: {
      position: map.ControlPosition.TOP_RIGHT,
      style: map.ZoomControlStyle.SMALL
    },
  };
};

class MapAndMarkers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPageID: "",
    };
  }

  static defaultProps = {
    center: {
      lat: 51,
      lng: 10
    },
    zoom: 0
  };


  displayMarker = (id) => {
    this.setState({ currentPageID: id })
  }

  // check markers id's available on the bounded maps
  checkCoordinates = (point, bounds, id) => {
    let inLong = point[1] > bounds.sw.lng && point[1] < bounds.se.lng;
    let inLat = point[0] > bounds.sw.lat && point[0] < bounds.ne.lat;
    if (inLat && inLong) {
      return id;
    } else {
      return null;
    }
  }

  //returns markers id's visible on the map
  _onBoundsChange = (center) => {
    let mapMarkersId = []
    mapMarkersId = this.props.locations.map((location, index) => {
      return this.checkCoordinates([location.location.coordinates[1], location.location.coordinates[0]], center.bounds, location.id)
    })
    this.props.setTripsOnMapChange(mapMarkersId);
  }

  render() {
    const { locations, hoveredCardId, map } = this.props;
    for (var i = 0; i < locations.length - 1; i++) {
      for (var j = i + 1; j < locations.length; j++) {
        if (locations[i].location.coordinates[0] === locations[j].location.coordinates[0]) {
          locations[i].location.coordinates[0] += .05;
        }
      }
    }
    let MapMarkers = locations.map((location, index) => {
      return (
        <Marker
          key={index}
          lat={location.location.coordinates[1]}
          lng={location.location.coordinates[0]}
          data={location}
          pageId={location.id}
          hoveredCardId={hoveredCardId}
          currentPageID={this.state.currentPageID}
          displayMarker={(id) => this.displayMarker(id)}
          map={map}
        />
      );
    });

    return (
      <div className="all-border">
        <GoogleMapReact
          bootstrapURLKeys={{
            key: "AIzaSyDRbkCf-zFbUjLsY62KXua-1p-cVmrj6v0",
            v: "3.31"
          }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          hoverDistance={20 / 2}
          onChange={this._onBoundsChange}
          options={createMapOptions}
          center={this.props.countryCoordinates ? this.props.countryCoordinates : this.props.myLocation ? this.props.myLocation : this.props.center}
        >
          {MapMarkers}

        </GoogleMapReact>
      </div>
    );
  }
}

MapAndMarkers.propTypes = {
  classes: PropTypes.object.isRequired
};

export default MapAndMarkers;