import React, { useState, useEffect } from "react";
import { Modal, Row, Col, Form, Input, Button, Drawer, Select, Checkbox } from "antd";
import { withRouter } from "next/router";
import { compose } from "redux";
import { useSelector } from 'react-redux';
import { useMediaQuery } from "react-responsive";
import countries from '../../helpers/countries';
import Link from "next/link";

import { Contact } from '../../services/expert';

const { TextArea } = Input;
const { Option } = Select;

const Interested = (props) => {
  const { getFieldDecorator } = props.form;
  const [loader, setLoader] = useState(false);
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  // console.log('-------------------------');
  // console.log('props-->', props);
  // console.log('-------------------------');

  const onFormSubmit = async (e) => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        if (values.newsletterAllow !== undefined && values.newsletterAllow) {
          values.newsletterAllow = 'yes';
        } else {
          values.newsletterAllow = 'no';
        }
        values.type = props.type;
        values.twid = props.id;
        values.tripWorkshopName = props.trip; //name of the current trip or workshop name
        values.from = props.trip; //name of the current trip or workshop name
        setLoader(true);
        const result = await Contact(values);
        if (result.status === 200) {
          setLoader(false);
          props.onCloseClick(true);
        }
      }
    })
  }


  return (
    <>
      <Drawer
        title={<><h6 className="main_sub_title">Inquiry For</h6><h4 className="sub_title">{props.trip}</h4></>}
        width={isMaxWidth768 ? 338 : 720}
        onClose={() => props.onCloseClick(false)}
        visible={props.visible}
        className="get-in-touch-drawer"
      >
        <Form className="ant-advanced-search-form pt20" onSubmit={onFormSubmit}>
          <div className="reserve_frm_bg">
            {/* <p className="trip-workshop">{props.trip}</p> */}
            <Row gutter={[20]}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="mb10">
                <Form.Item label="Name" className="mb0 flex-1">
                  {
                    getFieldDecorator("name", { rules: [{ required: true, message: "Please enter name" }] })
                      (<Input placeholder="Enter Name" />)
                  }
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Email">
                  {getFieldDecorator('email', {
                    rules: [
                      {
                        type: 'email',
                        message: 'Please enter valid email address.',
                      },
                      {
                        required: true,
                        message: 'Please enter email address.',
                      },
                    ],
                  })(<Input placeholder="Enter Email" />)}
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Form.Item label="Country">
                  {getFieldDecorator("country", { rules: [{ required: false, message: "Please select your country!" }] })
                    (<Select showSearch placeholder="Select Your Country">
                      {countries.map((con, i) => <Option key={i} value={con.name}>{con.name}</Option>)}
                    </Select>)}
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Form.Item label="Phone number" className="mb0 flex-1">
                  {
                    getFieldDecorator("phone", { rules: [{ required: false, message: "Please enter Phone number" }] })
                      (<Input placeholder="Enter Phone number" />)
                  }
                </Form.Item>
              </Col>

            </Row>
            <Row gutter={[20]}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row message-box">
                <Form.Item label="Message" className="mb0 flex-1">
                  {
                    getFieldDecorator("message", { rules: [{ required: true, message: "Please enter message" }] })
                      (<TextArea rows={6} placeholder="Message" />)
                  }
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="gutter-row checkbox-design">
                <Form.Item>
                  {
                    getFieldDecorator("newsletterAllow", { valuePropName: "checked", initialValue: false, rules: [{ required: false, message: "Checked Terms & Conditions" }] })
                      (<Checkbox className="term_condition_txt" >
                        Hook me up with relevant adventure travel stories and tips, expedition drops, and expert curated content!
                      </Checkbox>)
                  }
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                <div className="text-left mt65">
                  <Button
                    type="primary"
                    htmlType="submit"
                    style={{ width: isMaxWidth768 ? "80%" : "30%" }}
                    loading={loader}
                    className="ex__primary_btn">
                    Send
                  </Button>
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="mt25">
                <Form.Item >
                  <p className="disclaimer">
                    {`By Clicking 'Send' button you agree to our`}
                    <Link href="/terms-and-conditions">
                      <a target="_blank"> Terms of Service </a>
                    </Link>
                    and
                    <Link href="/privacy-policy">
                      <a target="_blank"> Privacy Policy </a>
                    </Link>.
                  </p>
                </Form.Item>
              </Col>
            </Row>
          </div>
        </Form>
      </Drawer >

      {/* <SuccessModal isSuccess={isSuccess} /> */}

    </>
  );
};

const WrappedReservation = Form.create({ name: "Interested" })(Interested);
export default compose(withRouter)(WrappedReservation);
