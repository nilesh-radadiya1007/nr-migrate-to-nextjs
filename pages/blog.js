import React from 'react';
import Blog from '../src/containers/Blog';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const BlogInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Blog />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default BlogInApp;