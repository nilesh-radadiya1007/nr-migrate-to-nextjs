import React, { Fragment } from 'react';
import { compose } from "redux";
import { Rate, Card, Popover, Icon } from 'antd';

/**
 * App Imports
 */
import { CaptalizeFirst, commaSepratorString, getCityFromLocation, commaSepratorStringCaps, commaSepratorStringSingle } from "../../helpers/methods";
// import languageIcon from '../../../public/images/speaks_ic_2x.png';
// import followersIcon from '../../../public/images/newicon/followers_icon.png';
// import locationIcon from "../../../public/images/newicon/location_icon.png";
// import Image from 'next/image'


function SingleCard(props) {
    const { expert, type, onMouseEnter, onMouseLeave, closeCard } = props;

    const onCardClick = (id) => window.open(`/expert-profile/${id}`, "_blank");
    const onReviewClick = (e, id) => {
        e.stopPropagation();
        window.open(`/expert-profile/${id}?review`, "_blank")
    };

    return (
        <Fragment>
            <Card
                hoverable
                onClick={() => onCardClick(expert.id)}
                onMouseEnter={() => type == 'mapCard' ? onMouseEnter(expert.id) : ''}
                onMouseLeave={() => type == 'mapCard' ? onMouseLeave(expert.id) : ''}
                cover={<img alt="example" src={expert.profile} />}
            >

                {type === "googleMapCard" &&
                    <span className="card_close_tag an-10 medium-text">
                        <Icon type="close-circle" onClick={() => closeCard()} />
                    </span>
                }
                <div className="">

                    <div className="expert_card_details">
                        <p className="mb10 expert-title">{`${CaptalizeFirst(expert.firstName)} ${CaptalizeFirst(expert.lastName)}`}</p>

                        <p className="mb20 expert_card_title">
                            <Popover placement="bottomLeft" content={`${commaSepratorString(expert.experties)}`} trigger="hover" overlayClassName="card-title-hover">
                                {`${commaSepratorStringSingle(expert.experties)}`}
                            </Popover>

                        </p>
                        <p className="mb10 expert_card_loc">
                            <img src={'/images/newicon/location_icon.png'} alt="location" className="location_icon" />{" "}
                            {getCityFromLocation(expert.city)}{CaptalizeFirst(expert.country)}
                        </p>
                        <p className="mb10 expert_card_lang">
                            <img src={'/images/speaks_ic_2x.png'} alt='language' className='languageIcon' id='languageIcon' />
                            {commaSepratorStringCaps(expert.speaks)}

                        </p>
                        <p className="mb10 expert_card_follow">
                            <img src={'/images/newicon/followers_icon.png'} alt='followerIcon' className='followerIcon' />
                            No Followers
                        </p>
                        <div className="mb10 expert_review-text" onClick={(e) => onReviewClick(e, expert.id)}>
                            <Rate
                                allowHalf
                                disabled
                                defaultValue={expert.review !== undefined && expert.review.reviewRating !== undefined ? Number(expert.review.reviewRating) : 0}
                                className="an-14"
                                style={{ color: "#FFBC00" }}
                            /><span className="review-text-expert">
                                {(expert.review !== undefined && expert.review.reviewLength !== undefined && expert.review.reviewLength) === 0 ? `No Review` : expert.review.reviewLength === 1 ? `${expert.review.reviewLength + ' Review'}` : `${expert.review.reviewLength} Reviews`}
                                {/* {(expert.review !== undefined && expert.review.reviewLength !== undefined) ? expert.review.reviewLength + ' Reviews' : 'No Review'} */}
                            </span>
                        </div>

                        {/* <span className="blk-color fnt-rubik">0 Followers &nbsp;</span> */}

                    </div>
                </div>

            </Card>
        </Fragment >
    )

}

export default compose(SingleCard)
