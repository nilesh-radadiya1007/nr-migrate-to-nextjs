import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';


const UpdateTrip = dynamic(
    () => import("../../src/containers/UpdateTrip"),
    { ssr: false }
);

const UpdateTripInApp = (props) => {
    return (   
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <UpdateTrip />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

// export async function getServerSideProps({ params }) {
//     return { props: { id: params.id } }
// }

export default UpdateTripInApp;