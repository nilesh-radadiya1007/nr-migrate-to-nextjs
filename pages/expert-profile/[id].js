import React from 'react';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';
import dynamic from 'next/dynamic';

const ExpertPublicView = dynamic(
    () => import("../../src/containers/ExpertPublicView"),
    { ssr: false }
);

const ExpertProfileInApp = (props) => {
    // console.log(props);
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <ExpertPublicView />
            </BasicPageWrapper>
        </BasicWrapperProvider>
    )
}

// export async function getServerSideProps({ params }) {
//     return { props: { id: params.id } }
// }

export default ExpertProfileInApp;