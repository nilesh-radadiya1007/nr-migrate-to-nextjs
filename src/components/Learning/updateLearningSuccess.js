import React from "react";
import { Modal, Button } from "antd";
// import Link from "next/link";
import {useDispatch, useSelector} from 'react-redux';
import { compose } from "redux";
// import Image from 'next/image';
import { useRouter } from 'next/router';
/**
 * Image Import
 */

/**
 * App Imports
 */
import { ModalActions } from '../../redux/models/events';
const Success = "/images/success_ic.png";

const CreateLearningSuccess = (props) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const learning=useSelector(state => state.learning);
  const { closeUpdateModal } = ModalActions;

  const handleClick = () => {
    dispatch(closeUpdateModal());
    router.push('/learning-edit/' + learning.id);
  }

  return (
    <Modal
      centered
      className="auth-modal success-modal"
      width={380}
      closable={false}
      maskClosable={false}
      visible={props.visible}
    >
      <div className="text-center">
        <img src={Success} alt="" />
        <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
          Successfully Updated
        </h1>
        <p className="an-18 mb20 regular-text">
          Your workshop is updated
        </p>
        <Button type="primary" className="done_btn medium-text an-16 ex__primary_btn" onClick={handleClick}>
          View
        </Button>
        {/* <Link href="" className="done_btn medium-text an-16">
        </Link> */}
      </div>
    </Modal>
  );
};

export default compose(CreateLearningSuccess);
