import axios from 'axios';
import { setHeadersWithAccessToken } from './index';
import { getApiUrl } from '../helpers/methods';

const API_BASE = getApiUrl();
// console.log(API_BASE);

export const UserAuth = (type, params) => {
  return axios
    .post(`${API_BASE}/auth/${type}`, params)
    .then(e => e)
    .catch(e => e);
}

export const getProfile = (token, role) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/${role}`)
    .then(e => e)
    .catch(e => e);
};

export const VerifyProfile = token => {
  return axios
    .post(`${API_BASE}/auth/verify-profile/${token}`)
    .then(e => e)
    .catch(e => e);
};

export const changePassword = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/auth/change-password`, data)
    .then(e => e)
    .catch(e => e);
};

export const confirmEmail = (data) => {
  return axios
    .post(`${API_BASE}/auth/confirm-email`, data)
    .then(e => e)
    .catch(e => e);
};

export const updatePreferredCurrency = (data) => {
  return axios
    .post(`${API_BASE}/auth/preferred-currency/${data}`)
    .then(e => e)
    .catch(e => e);
};

export const confirmPassword = (data) => {
  return axios
    .post(`${API_BASE}/auth/confirm-password`, data)
    .then(e => e)
    .catch(e => e);
};

export const changeEmail = (data) => {
  if (data) {
    return axios
      .post(`${API_BASE}/auth/change-email`, data)
      .then(e => e)
      .catch(e => e);
  }
};

export const userForgotPassword = params => {
  return axios
    .post(`${API_BASE}/auth/forgot-password`, params)
    .then(e => e)
    .catch(e => e);
};

export const checkResetToken = token => {
  return axios
    .get(`${API_BASE}/auth/reset-password/${token}`)
    .then(e => e)
    .catch(e => e);
};

export const userResetPassword = (token, params) => {
  return axios
    .post(`${API_BASE}/auth/reset-password/${token}`, params)
    .then(e => e)
    .catch(e => e);
};

export const updateActivity = (token) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/auth/activity`)
    .then(e => e)
    .catch(e => e);
}

export const getMessageUreadCount = (token, role) => {
  setHeadersWithAccessToken(token);
  return axios
    .get(`${API_BASE}/${role}/messages/count`)
    .then(e => e)
    .catch(e => e);
};

export const updateUserRole = (token, data) => {
  setHeadersWithAccessToken(token);
  return axios
    .post(`${API_BASE}/auth/updateRole`, { role: data })
    .then(e => e)
    .catch(e => e);
};
