import Head from 'next/head';
import dynamic from 'next/dynamic';
import { Provider } from "react-redux";
// import BaseLayout from '../src/components/BaseLayout';
import { store, persistor } from '../src/redux/store';
import { PersistGate } from "redux-persist/integration/react";


const BaseLayout= dynamic(
  () => import("../src/components/BaseLayout"),
  { ssr: false }
);


export default function SEO() {
  return (
    <div>
      <Head>
        <title>Expeditions Connect</title>
        <meta charSet="utf-8" />
        {/* <link rel="icon" href="%PUBLIC_URL%/favicon.ico" /> */}
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta name="description" content="Join Expeditions Connect for free. Explore the best adventure courses, workshops and expeditions directly crafted by professionals." />
        <meta property="og:title" content="Connect. Learn. Impact | Expeditions Connect" />
        <meta property="og:description" content="Join Expeditions Connect for free. Explore the best adventure courses, workshops and expeditions directly crafted by professionals." />
        <meta property="og:image:type" content="image/ong" />
        <meta property="og:image" content="https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/Logos/expeditionLohgo.png" />
        <meta property="og:url" content="http://expeditionsconnect.com/" />
        <meta name="facebook-domain-verification" content="hy2r6qalzpbjg8i3wdiwfp4s8yta9l" />
        {/* <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
        <link rel="manifest" href="%PUBLIC_URL%/manifest.json" /> */}
      </Head>
        
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BaseLayout />
        </PersistGate>
      </Provider>
    </div>
  )
}
