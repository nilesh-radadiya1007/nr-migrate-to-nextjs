import React, { useState, useCallback } from 'react';
import { Form, Input, Row, Col, Button, message, Popover, Icon } from "antd";
import { useRouter } from 'next/router';
import { compose } from "redux";
import { useMediaQuery } from "react-responsive";

import { userResetPassword } from '../../services/auth';

const ResetPassword = (props) => {
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const router = useRouter();
  const [confirmDirty, setConfirmDirty] = useState(false);
  const [loader, setLoader] = useState(false);
  const [passwordError, setPasswordError] = useState("Please input your password");
  const [isLower, setIsLower] = useState(false);
  const [isUpper, setIsUpper] = useState(false);
  const [isNumber, setIsNumber] = useState(false);
  const [isSpecial, setIsSpecial] = useState(false);
  const [isMinimum, setIsMinumum] = useState(false);
  const [isMsgShow, setIsMsgShow] = useState(false);
  const [isEmpty, setIsEmpty] = useState(false);

  const { getFieldDecorator } = props.form;
  const { token } = props

  const resetPassword = useCallback(async (password) => {
    setLoader(true);
    try {
      const result = await userResetPassword(token, password);
      if (result.status === 200) {
        setLoader(false);
        message.success('Password reset success !!');
        router.push('/')
      } else {
        message.error('Error setting new password !!');
        router.push('/')
      }
    } catch (err) {
      message.error(err.response.data.message);
      router.push('/');
    }
  }, [router, token])

  const handleFormSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setIsMsgShow(false);
        resetPassword({ password: values.password })
      }
    });
  };

  const handleConfirmBlur = e => {
    const { value } = e.target;
    setConfirmDirty(confirmDirty || !!value);
  };

  const compareToFirstPassword = (rule, value, callback) => {
    const { form } = props;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter are inconsistent!");
    } else {
      callback();
    }
  };

  const validateToNextPassword = (rule, value, callback) => {
    const { form } = props;

    var patt = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{8,}$/);

    setIsLower(/[a-z]/.test(value) ? true : false);
    setIsUpper(/[A-Z]/.test(value) ? true : false);
    setIsNumber(/[0-9]/.test(value) ? true : false);
    setIsSpecial(/[@#$!%*?&]/.test(value) ? true : false);
    setIsMinumum((value !== undefined && value.length >= 8) ? true : false)

    var res = patt.test(value);
    setPasswordError('');
    if (value === "" || value === undefined) {
      callback();
    } else {
      if (!res) {
        setIsMsgShow(true);
        setIsEmpty(false);
        callback("Enter a password according to the policy.");
      } else {
        if (value && confirmDirty) {
          form.validateFields(["confirm"], { force: true });
        }
        setIsMsgShow(false);
        callback();
      }
    }

  };

  const content = (
    <div className="password-popup-main">
      {/* <p className="password-popup-title">Password must meet the following requierments:</p> */}
      <p className={`${isEmpty ? 'gray' : isMinimum ? 'green' : 'red'}`}><Icon type={`${isMinimum ? 'check' : 'close'}`} /> &nbsp;At least 8 characters</p>
      <p className={`${isEmpty ? 'gray' : isLower ? 'green' : 'red'}`}><Icon type={`${isLower ? 'check' : 'close'}`} /> &nbsp;At least one small letter</p>
      <p className={`${isEmpty ? 'gray' : isUpper ? 'green' : 'red'}`}><Icon type={`${isUpper ? 'check' : 'close'}`} /> &nbsp;At least one capital letter</p>
      <p className={`${isEmpty ? 'gray' : isNumber ? 'green' : 'red'}`}><Icon type={`${isNumber ? 'check' : 'close'}`} /> &nbsp;At least one number</p>
      <p className={`${isEmpty ? 'gray' : isSpecial ? 'green' : 'red'}`}><Icon type={`${isSpecial ? 'check' : 'close'}`} /> &nbsp;At least one special characters (!, @, #, $, %, &, *)</p>
    </div >
  );

  const onPasswordFocus = (e) => {
    if(e.target.value == ""){
      setIsEmpty(true);
    }else{
      setIsEmpty(false);
    }
    setIsMsgShow(true);
  }

  const onPasswordBlur = (e) => {
    // setIsMsgShow(false);
  }

  return (
    <div className="step-1-expert-form">
      <div className="an-20 medium-text success--text step-title">
        RESET PASSWORD
      </div>
      <Form className="ant-advanced-search-form" onSubmit={handleFormSubmit}>
        <div className="form-profile-container">
          <div className="pt10">
            <Row gutter={24}>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="New Password">
                  {/* <Popover placement="top" overlayClassName="password-tooltip" content={content} trigger={'focus'}> */}
                    {getFieldDecorator("password", {
                      rules: [
                        { required: true, message: passwordError },
                        { validator: validateToNextPassword }
                      ]
                    })
                      (<Input.Password placeholder="New password" onFocus={onPasswordFocus} onBlur={onPasswordBlur} />)}
                  {/* </Popover> */}
                </Form.Item>
                {isMsgShow && content}
              </Col>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Confirm New Password">
                  {getFieldDecorator("confirm", {
                    rules: [
                      { required: true, message: 'Please confirm your password' },
                      { validator: compareToFirstPassword }
                    ]
                  })
                    (<Input.Password placeholder="Confirm new Password" onBlur={handleConfirmBlur} />)}
                </Form.Item>
              </Col>
            </Row>
          </div>
        </div>
        <Form.Item className="mb0 pt25">
          <Button type="primary"
            loading={loader}
            htmlType="submit"
            className="ex__primary_btn">
            Next
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

const WrappedResetPassword = Form.create({ name: "createProfile" })(ResetPassword);

export default compose(WrappedResetPassword);
