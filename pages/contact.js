import React from 'react';
import ContactUs from '../src/containers/ContactUs';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const ContactUsInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <ContactUs />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default ContactUsInApp;