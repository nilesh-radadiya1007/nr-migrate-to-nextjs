import React, { useState } from 'react';
import { Form, Input, Button, Popover, Icon, Radio, Row, Col } from "antd";
import { useMediaQuery } from "react-responsive";
// import expertLogo from '../../../public/images/login_expert_logo.png';
// import enthuLogo from '../../../public/images/login_enthu_logo.png';
// import Image from 'next/image';
import Link from 'next/link';

/**
 * App Import
 */
import { REGISTER } from '../../helpers/constants';

const RegisterForm = (props) => {
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });

  const [passwordError, setPasswordError] = useState("Please input your password");
  const [isLower, setIsLower] = useState(false);
  const [isUpper, setIsUpper] = useState(false);
  const [isNumber, setIsNumber] = useState(false);
  const [isSpecial, setIsSpecial] = useState(false);
  const [isMinimum, setIsMinumum] = useState(false);
  const [isMsgShow, setIsMsgShow] = useState(false);
  const [isEmpty, setIsEmpty] = useState(false);
  const [tmpuserRole, setTmpUserRole] = useState('enthusiasts');

  const { getFieldDecorator } = props.form;

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setIsMsgShow(false);
        props.handleLogin({ ...values, type: REGISTER });
      }
    });
  };

  const validatePassword = (rule, value, callback) => {
    var patt = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{8,}$/);

    setIsLower(/[a-z]/.test(value) ? true : false);
    setIsUpper(/[A-Z]/.test(value) ? true : false);
    setIsNumber(/[0-9]/.test(value) ? true : false);
    setIsSpecial(/[@#$!%*?&]/.test(value) ? true : false);
    setIsMinumum((value !== undefined && value.length >= 8) ? true : false)

    var res = patt.test(value);

    setPasswordError('');
    if (value === "" || value === undefined) {
      callback();
    } else {
      if (!res) {
        setIsMsgShow(true);
        setIsEmpty(false);
        callback("Enter a password according to the policy.");
      } else {
        setIsMsgShow(false);
        callback();
      }
    }

  };

  const onPasswordChange = (e) => {
    if (e.target.value === "") {
      setPasswordError('');
    }
  }

  const onPasswordFocus = (e) => {
    if (e.target.value == "") {
      setIsEmpty(true);
    } else {
      setIsEmpty(false);
    }
    setIsMsgShow(true);
  }

  const onPasswordBlur = (e) => {
    // setIsMsgShow(false);
  }

  const content = (
    <div className="password-popup-main">
      {/* <p className="password-popup-title">Password must meet the following requierments:</p> */}
      <p className={`${isEmpty ? 'gray' : isMinimum ? 'green' : 'red'}`}><Icon type={`${isMinimum ? 'check' : 'close'}`} /> &nbsp;At least 8 characters</p>
      <p className={`${isEmpty ? 'gray' : isLower ? 'green' : 'red'}`}><Icon type={`${isLower ? 'check' : 'close'}`} /> &nbsp;At least one small letter</p>
      <p className={`${isEmpty ? 'gray' : isUpper ? 'green' : 'red'}`}><Icon type={`${isUpper ? 'check' : 'close'}`} /> &nbsp;At least one capital letter</p>
      <p className={`${isEmpty ? 'gray' : isNumber ? 'green' : 'red'}`}><Icon type={`${isNumber ? 'check' : 'close'}`} /> &nbsp;At least one number</p>
      <p className={`${isEmpty ? 'gray' : isSpecial ? 'green' : 'red'}`}><Icon type={`${isSpecial ? 'check' : 'close'}`} /> &nbsp;At least one special characters (!, @, #, $, %, &, *)</p>
    </div >
  );

  return (
    <>
      <div className="custom-login-txt signup-page"><p>Signup in Expeditions Connect Account</p></div>
      <Form onSubmit={handleSubmit} className="ex__form" autoComplete="new-password">
        <div className="expert-enthu-logos">
          <div className="txt-center enth">
            <img src={'/images/login_enthu_logo.png'} alt="Enthu Logo" />
          </div>
          <div className="txt-center expert">
            <img src={'/images/login_expert_logo.png'} alt={"Expert Logo"} />
          </div>
        </div>
        <Form.Item className="expert-enthu-logos-parent" >
          {getFieldDecorator('tmpRole', {
            initialValue: tmpuserRole,
          })(
            <Radio.Group initialValue={tmpuserRole} onChange={(e) => [props.upldateTmpRole(e.target.value), setTmpUserRole(e.target.value)]}>
              <Radio value="enthusiasts">Adventure Seeker</Radio>
              <Radio value="expert" className="ad-provider">Adventure Provider</Radio>
            </Radio.Group>
          )}
        </Form.Item>
        <Form.Item label="Email" className="registration-email">
          {getFieldDecorator("email", {
            rules: [
              {
                type: "email",
                message: "Invalid email address!"
              },
              { required: true, message: "Please input your email" }
            ]
          })(<Input className="email-input" type="email" placeholder="Email" />)}
        </Form.Item>

        <Form.Item hasFeedback label="Password" className="mb10 registration-password">
          {/* <Popover placement={`${isMaxWidth768 ? 'top' : 'topRight'}`} overlayClassName="password-tooltip" content={content} trigger={`${isMaxWidth768 ? 'focus' : 'focus'}`}> */}
          {getFieldDecorator("password", {
            rules: [
              { required: true, message: passwordError },
              { validator: validatePassword }
            ]
          })(<Input.Password placeholder="Password" onChange={onPasswordChange} onFocus={onPasswordFocus} onBlur={onPasswordBlur} />)}

          {/* </Popover> */}
        </Form.Item>
        {isMsgShow && content}
        <div className="flex-x pt5">
          <div className="flex-1">
            {getFieldDecorator("remember", {
              valuePropName: "checked",
              initialValue: true
            })(
              <div className="an-14 regular-text dark--text">
                By signing up, you agree to the
                <Link href="/terms-and-conditions">
                  <a target="_blank"> Terms of Service </a>
                </Link>  and
                <Link href="/privacy-policy">
                  <a target="_blank"> Privacy Policy </a>
                </Link>
                of expeditions connect
              </div>
            )}
          </div>

        </div>

        <Form.Item className="mb0">
          <div className="pt30 pb10">
            <Button
              loading={props.loader}
              type="primary"
              htmlType="submit"
              className="ex__primary_btn signup-btn"
            >
              Register
            </Button>
          </div>
        </Form.Item>
      </Form>
    </>
  )
}

const WrappedNormalRegisterForm = Form.create({ name: "RegisterForm" })(RegisterForm);

export default WrappedNormalRegisterForm;
