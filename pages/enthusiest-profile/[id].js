import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';


const EnthusiestProfile = dynamic(
    () => import("../../src/containers/EnthusiestProfile"),
    {
        ssr: false
    }
);

const EnthusiestProfileInApp = (props) => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <EnthusiestProfile />
            </BasicPageWrapper>
        </BasicWrapperProvider>
    )
}

// export async function getServerSideProps({ params }) {
//     return { props: { id: params.id } }
// }

export default EnthusiestProfileInApp;