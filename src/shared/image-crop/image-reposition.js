import React, { useState, useCallback, useEffect } from "react";
import Cropper from "react-easy-crop";
import { Button, Modal } from "antd";
import getCroppedImg from "./crop-image";
import { FullscreenOutlined, LoadingOutlined } from "@ant-design/icons";
import RangeSlider from 'react-bootstrap-range-slider';
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';
import useStateWithCallback from 'use-state-with-callback';
import { getBase64 } from "../../helpers/methods";


const ImageReposition = ({
  originalCover,
  page,
  saveCropperdImage,
  setReposition,
  cropSize
}) => { 
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(page ? 0.5 : 1.2);
  const [showDrag, setDragValue] = useState(false);
  const [showLoader, setShowLoader] = useStateWithCallback(false, showLoader => {
    if (showLoader) {
      setCroppedImage();
    }
  });
  const [showImageLoading, setImageLoading] = useState(true);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
  const [showModal, setShowModal] = useState(false)
  const [urlImageSource, setImageSource] = useState("");
  const [showInfo, seShowInfo] = useState(true)
  //on completing reposition
  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels);
  }, []);

  useEffect(()=>{
    if(originalCover && !showModal){
      setShowModal(true);
      createImage(originalCover)
    }
  },[originalCover])

  useEffect(()=>{
    if(showInfo)
    setTimeout(function(){ seShowInfo(false); }, 5000);
  },[showInfo])

  const createImage = url =>
  new Promise((resolve, reject) => {
    const image = new Image()
    image.addEventListener('load', () => {resolve(image); setImageSource(image)})
    image.addEventListener('error', error => {reject(error);setImageSource(null)})
    image.setAttribute('crossOrigin', null)
    image.src = url
  })

  //set the cropped image
  const setCroppedImage = useCallback(async () => {
    try {
      const croppedImage = await getCroppedImg(
        urlImageSource,
        originalCover,
        croppedAreaPixels
      );
      getBase64(croppedImage, (imageUrl) => {
        setShowLoader(false);
        saveCropperdImage(imageUrl);
      });
    } catch (e) {
      setShowLoader(false);
      console.error(e);
    }
  }, [croppedAreaPixels]);

  const setOnDragValue =()=>{
    setDragValue(!showDrag)
  }
  const handleSaveClick=()=>{
    setDragValue(!showDrag)
    setShowLoader(true);
  }

  return (
    <React.Fragment>
    <Modal width='80%' 
        style={{ textAlign: 'center' }}
        footer={[]}
        closable={false} 
        visible={showModal} className="croper-adjuster-modal">
      <div className="image-crp-repostion">
       
        {!showImageLoading && <div className="save-res-btns">    
        <div className="imagezoom">
        <button onClick={()=> { if(!page){
                      if (zoom > 1) {
                        setZoom(zoom - 0.1);
                      }
                    }else{
                      if (zoom > 0.5) {
                        setZoom(zoom - 0.1);
                      }
                    }
                    
                  }} >-</button>
          <RangeSlider
            min={page ? 0.4 : 1}
            max={5}
            step={0.1}
            value={zoom}
            variant='warning'
            tooltip={"off"}
            onChange={changeEvent => { setZoom(changeEvent.target.value)}}
          />
          {showInfo && <span className="show-info-zoom">Adjust zoom</span>}
          <button onClick={()=>setZoom(zoom+0.1)}>+</button>
        </div>   
        <div>
          <Button
            type="primary"
            onClick={() => setReposition(false)}
            className="save-reposition an-14 medium-text btn-cancel"
          >
            Cancel
          </Button>
          <Button
            type="primary"
            onClick={()=>{handleSaveClick()}}
            className="save-reposition an-14 medium-text save-reposition1"
          >
            Save
          </Button>
          </div>
        </div>}
        {showDrag && (
          <p className="drag-info">
            <FullscreenOutlined /> Drag to reposition
          </p>
        )}
        {showLoader && (
          <p className="drag-info">
            <LoadingOutlined /> Repositioning Image...
          </p>
        )}
        {showImageLoading && (
          <p className="drag-info">
            <LoadingOutlined /> Loading...
          </p>
        )}
        <Cropper
          image={originalCover}
          crop={crop}
          zoom={zoom}
          minZoom={page ? .4 : 1}
          maxZoom={5}
          aspect={4 / 3}
          cropSize={cropSize} 
          onCropChange={setCrop}
          onCropComplete={
            onCropComplete
          }
          showGrid={false}
          onZoomChange={setZoom}
          disableAutomaticStylesInjection={false}
          onInteractionStart={() => setOnDragValue()}
          onInteractionEnd={() => setOnDragValue()}
          onMediaLoaded={()=>{setImageLoading(false); setDragValue(true); setShowModal(true)}}
        />
      </div>
      </Modal>
      </React.Fragment>
  );
};

export default ImageReposition;
