import React from 'react'
import Adventure from '../src/containers/Adventure';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

function AllAdventure() {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Adventure />
            </BasicPageWrapper>
        </BasicWrapperProvider>
    )
}

export default AllAdventure;
