import { createStore, applyMiddleware, compose } from 'redux';
// import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from "react-router-redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import Cookies from 'universal-cookie';
import Router from 'next/router';

/**
 * App Imports
 */
import createReducer from "./reducers";
import rootSaga from "./sagas";


const history = Router;
const persistConfig = {
  key: "root",
  storage,
  whitelist: ["auth"]
};

const cookies = new Cookies();

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware, routerMiddleware(history)];
const composeEnhancer = compose;
const persistedReducer = persistReducer(persistConfig, createReducer());


const store = createStore(persistedReducer, composeEnhancer(applyMiddleware(...middlewares)));

store.dispatch({ type: "RESET_AUTH_LOADER" });
sagaMiddleware.run(rootSaga);

const persistor = persistStore(store, null, function () {
  store.dispatch({ type: "SET_PREFERRED_CURRENCY", preferredCurrency: cookies.get('preferredCurrency') });
});

export { store, history, persistor };