import React from 'react';
// import { withRouter } from "react-router-dom";
import { compose } from "redux";
// import covidInfo from "../../../public/images/covid/covid_info.svg";
import Link from 'next/link';
const CovidBanner = (props) => {

    return (
        <div className="covid-banner">
            <div className="mb15">
                {/* <Image layout="fill" src={covidInfo} alt={"covid information"} /> */}
                {/* <span className="title-yellow">COVID-19 : </span> */}
                <span className="book-with-confidence">Book With Confidence</span>
            </div>
            <div className="sub-title">
                Get Peace of mind with our flexible and risk-free booking.
                <span>
                    <Link href="/covid"><a className="covid-link">Learn More</a></Link>
                </span>
            </div>
        </div >
    )

}

export default compose(CovidBanner)
