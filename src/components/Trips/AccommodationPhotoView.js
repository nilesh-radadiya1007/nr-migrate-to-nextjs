/* eslint-disable react/display-name */
import React, { useRef, useEffect, useState } from 'react';
import { useSelector } from "react-redux";
import { Card, Form, Modal, Row, Col, Carousel, Dropdown, Input } from 'antd';
import OwlCarousel from "react-owl-carousel";
import ReactPlayer from 'react-player';


const ArrowImage = '/images/arrow_imag.png';

const expeditionsOptions = {
  items: 3,
  nav: true,
  loop: false,
  responsiveClass: true,
  responsive: {
    0: {
      items: 1,
      nav: true,
      dots: false,
    },
    768: {
      items: 2,
      nav: true,
      dots: false,
    },
    991: {
      items: 3,
      nav: true,
      dots: false,
    },
  },
};

const AccommodationPhotoView = React.memo((props) => {

  const token = useSelector((state) => state.auth.accessToken);
  const slider = useRef();
  const { visible, id, onCloseClick, allPhotos } = props;
  const [slide, setSlide] = useState(0);

  const next = (e) => {
    setSlide(e.target.alt);
    if (e.target.alt++ <= allPhotos.length) {
      slider.current.goTo(e.target.alt++);
    }
  }


  const onAccommoImageClick = (index) => {
    setSlide(index);
    slider.current.goTo(index);
  };


  const prev = (e) => {
    setSlide(e.target.alt);
    if (slide >= 0) {
      slider.current.goTo(--e.target.alt);
    }
  }

  return (
    <Modal
      visible={visible}
      onCancel={onCloseClick}
      footer={false}
      className="accommodation_popup header_text album_select_detail_main"
    >
      <div className="album_select_detail">
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <Carousel
              autoplay
              className="album_carousel"
              ref={ref => { slider.current = ref; }}
            >
              {allPhotos.map((img, index) => (
                <div key={index}>
                  {img.search(".mp4") != -1 ?
                    (<ReactPlayer className="album_carousel" playing={false} controls={true} url={img} />) :
                    <div className="div_image_fix" style={{ backgroundImage: `url(${img})` }}> </div>
                  }

                  {allPhotos.length >= 2 &&
                    <div className="costome_btton">
                      <div className="left_arrow">
                        <img src={ArrowImage} alt={index} onClick={e => prev(e)} />
                      </div>
                      <div className="right_arrow">
                        <img src={ArrowImage} alt={index} onClick={e => next(e)} />
                      </div>
                    </div>
                  }
                </div>
              ))
              }
            </Carousel>

          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24} className="pt25 pb25 accomo_thumb">

            <OwlCarousel
              className="owl-theme accommodation_img_view"
              {...expeditionsOptions}
              margin={40}
            >
              {allPhotos.map((img, index) => (

                <Col
                  xs={24}
                  sm={24}
                  md={24}
                  lg={24}
                  xl={24}
                  key={index}
                  className="gutter-row"
                >
                  <Card
                    hoverable
                    cover={
                      img.search(".mp4") != -1 ?
                        (<ReactPlayer className="album_carousel" playing={false} controls={true} url={img} />) :
                        <img
                          onClick={() => onAccommoImageClick(index)}
                          alt="example"
                          src={img} />
                    }
                  >
                  </Card>
                </Col>
              ))}
            </OwlCarousel>
          </Col>
        </Row>
      </div>
    </Modal >
  );
});

export default Form.create({ name: 'album' })(AccommodationPhotoView);
