import React, { useEffect, useState } from 'react';
import { useSelector } from "react-redux";
import { Button, Form, Input, Modal, Dropdown, Menu, Upload, Spin, message } from 'antd';
import PlacesAutocomplete, { getLatLng, geocodeByAddress } from 'react-places-autocomplete';
import moment from 'moment';
import { DownOutlined } from '@ant-design/icons';


import { getBase64, beforeUpload } from '../../helpers/methods';
import { CreateAlbums, updateAlbums } from '../../services/expert';
import { FILE_SIZE } from '../../helpers/constants';
// import Image from 'next/image'
const UploadImage = '/images/cover_upload.png';
const VideoImage = '/images/video-icon.jpg';
const LocationImage = '/images/location.png';

const uploadButton = (
  <div>
    <img
      src={UploadImage}
      alt='cover'
      style={{
        width: '100%',
        height: '100%',
        objectFit: 'fill',
      }}
    />
  </div>
);

const { TextArea } = Input;

const Popup = React.memo((props) => {

  const token = useSelector((state) => state.auth.accessToken);
  const { loading, visible, title, handleOk, handleCancel, isPublicView, selectedAlbum, isMultiple } = props;
  const { getFieldDecorator, setFieldsValue } = props.form;

  const [imageUrl, setImageUrl] = useState('');
  const [location, setLocation] = useState('');
  const [country, setCountry] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [address, setAddress] = useState('');
  const [year, setYear] = useState('YYYY');
  const [month, setMonth] = useState('MM');
  const [monthNumber, setMonthNumber] = useState('01');
  const [date, setDate] = useState('DD');
  const [albumName, setAlbumName] = useState('Select album');
  const [albumList, setAlbumList] = useState([]);
  const [latLng, setLatLng] = useState({});
  const [loding, setLoding] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [isVideo, setIsVideo] = useState(false);
  const [isShowVideoIcon, setIsShowVideoIcon] = useState(false);
  const [yearList, setYearList] = useState([]);
  const [fileList, setFileList] = useState([]);
  const monthList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

  useEffect(() => {
    setFieldsValue({
      fileList,
      name,
      description,
      date,
      location,
    });
    if (isEdit === false) {
      if (Object.keys(selectedAlbum).length > 0) {
        setImageUrl(selectedAlbum.images[0]);
        setName(selectedAlbum.name);
        setCountry(selectedAlbum.country);
        setDescription(selectedAlbum.description)
        let finalData = [];
        selectedAlbum.images.map((img, index) => {
          finalData.push({
            uid: index,
            name: img.split("albums/")[1],
            status: 'done',
            url: img,
          })
          if (selectedAlbum.images.length === index + 1) {
            setFileList(finalData)
          }
        })
        setAddress(selectedAlbum.address);
        selectedAlbum.date && setYear(selectedAlbum.date.split("-")[0]);
        selectedAlbum.date && setMonth(monthList[parseInt(selectedAlbum.date.split("-")[1])]);
        selectedAlbum.date && setDate(selectedAlbum.date.split("-")[2].split("T")[0]);
        setIsEdit(true);
      }
    }
    YEARS();
  }, [setFieldsValue, fileList, name, description, location, selectedAlbum]);

  const YEARS = () => {
    const years = []
    const dateStart = moment("01-01-1950", "DD-MM-YYYY")
    const dateEnd = moment().add(10, 'y')
    while (dateEnd.diff(dateStart, 'years') >= 0) {
      years.push(dateStart.format('YYYY'))
      dateStart.add(1, 'year')
    }
    setYearList(years);
    return years
  }

  const onSelect = address => {
    setCountry(address.split(", ").slice(-1)[0]);
    setAddress(address);
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(ll => setLatLng(ll))
      .catch(error => console.error('Error', error));
  }

  const onChange = (address) => setAddress(address);

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoding(true);

    if (fileList.length > 0) {
      let totalSize = 0;
      fileList.map((file) => {
        if (file.size !== undefined) {
          totalSize = totalSize + file.size;
        }
      });

      const isLt2M = totalSize / 1024 / 1024 < FILE_SIZE.ALBUM;
      if (!isLt2M) {
        setLoding(false);
        message.error(FILE_SIZE.ERR_ALBUM);
        return;
      }
    }

    props.form.validateFields(async (err, values) => {
      if (!err) {
        const formData = new FormData();
        if (Object.keys(selectedAlbum).length > 0) {
          values.fileList.map((pic) => {
            formData.append("pictures", pic.originFileObj);
          });
          formData.append("photoList", JSON.stringify(fileList.map(dd => { return `albums/${dd.name}` })));
        } else {
          values.fileList.map((pic) => {
            formData.append("pictures", pic.originFileObj);
          });
        }
        formData.append('name', values.name);
        formData.append('description', values.description);
        if (year !== "YYYY" && monthNumber !== "MM" && date !== "DD") {
          formData.append('date', moment(`${year}-${monthNumber}-${date}`).format('YYYY-MM-DD[T00:00:00.000Z]'));
        }
        if (Object.keys(latLng).length > 0 || selectedAlbum.location !== undefined) {
          formData.append('location', JSON.stringify({ coordinates: [latLng.lng ? latLng.lng : selectedAlbum.location.coordinates[0], latLng.lat ? latLng.lat : selectedAlbum.location.coordinates[1]] }));
          formData.append('coordinates', JSON.stringify([latLng.lng ? latLng.lng : selectedAlbum.location.coordinates[0], latLng.lat ? latLng.lat : selectedAlbum.location.coordinates[1]]));
        }
        formData.append('country', country.toLowerCase());
        formData.append('address', address);
        if (isVideo === true || selectedAlbum.isVideo === true) {
          formData.append('isVideo', true);
        } else {
          formData.append('isVideo', false);
        }
        if (Object.keys(selectedAlbum).length > 0) {
          formData.append('id', selectedAlbum.id);
          const result = await updateAlbums(token, formData, selectedAlbum.id);
          if (result) {
            setLoding(false);
            handleOk();
            setImageUrl('');
            setFileList([]);
            setAddress('');
            setFieldsValue({
              fileList: [],
              name: "",
              description: "",
              date,
              location: "",
            });
            setIsVideo(false);
          }
        } else {
          const result = await CreateAlbums(token, formData);
          if (result) {
            setLoding(false);
            handleOk();
            setImageUrl('');
            setFileList([]);
            setAddress('');
            setFieldsValue({
              fileList: [],
              name: "",
              description: "",
              date,
              location: "",
            });
            setIsVideo(false);
          }
        }
      } else {
        setLoding(false);
      }
    });
  };

  const removeByAttr = function (arr, attr, value) {
    var i = arr.length;
    while (i--) {
      if (arr[i]
        && arr[i].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[i][attr] === value)) {

        arr.splice(i, 1);

      }
    }
    setFileList([...arr]);
    if (arr.length > 0) {
      if (arr[0].originFileObj) {
        if (arr[0].type.search("video") != -1) {
          setIsShowVideoIcon(true);
        } else {
          setIsShowVideoIcon(false);
          getBase64(arr[0].originFileObj, (imageUrl) => {
            setImageUrl(imageUrl);
          });
        }
      } else {
        setImageUrl(arr[0].url)
      }
    } else {
      setImageUrl('');
    }
    return arr;
  }

  const handleYear = (e) => {
    setYear(e.item.props.children);
  }

  const handleMonth = (e) => {
    setMonthNumber(e.key);
    setMonth(e.item.props.children);
  }

  const handleDay = (e) => {
    setDate(e.item.props.children);
  }

  const yearMenu = (
    <Menu className="Dropdown_set" onClick={(e) => handleYear(e)}>
      {yearList.map((year, index) => (
        <Menu.Item key={index}>
          {year}
        </Menu.Item>
      ))}
    </Menu>
  );

  const monthMenu = (
    <Menu className="Dropdown_set" onClick={(e) => handleMonth(e)}>
      {monthList.map((year, index) => (
        <Menu.Item key={index}>
          {year}
        </Menu.Item>
      ))}
    </Menu>
  );

  const dayMenu = (
    <Menu className="Dropdown_set" onClick={(e) => handleDay(e)}>
      {["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"].
        map((year, index) => (
          <Menu.Item key={index}>
            {year}
          </Menu.Item>
        ))}
    </Menu>
  );

  const albumMenu = (
    <Menu className="Dropdown_set" onClick={(e) => handleYear(e)}>
      {albumList.map((year, index) => (
        <Menu.Item key={index}>
          {year}
        </Menu.Item>
      ))}
    </Menu>
  );

  const onChangeImg = ({ fileList: newFileList }) => {
    if (newFileList[0].originFileObj) {
      if (isMultiple === false || selectedAlbum.images && selectedAlbum.images.length === 1) {
        if (newFileList[0].type.search("video") !== -1) {
          setIsVideo(true);
        }
      }
      if (newFileList[0].type.search("video") != -1) {
        setIsShowVideoIcon(true);
      } else {
        setIsShowVideoIcon(false);
        getBase64(newFileList[0].originFileObj, (imageUrl) => {
          setImageUrl(imageUrl);
        });
      }
    }
    setFileList(newFileList);
  };

  const onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  return (
    <Modal
      visible={visible}
      title={title}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={false}
      className="header_text section-set"
    >
      <div className={`step-1-expert-form height_sec_enthu learn_sec ${fileList.length === 0 && "img-set-icon"}`}>
        <Form onSubmit={handleSubmit}>
          <div className='form-profile-container pt20'>
            {isMultiple === false &&
              <Form.Item className="date_select" label='Album list'>
                {getFieldDecorator('selectAlbum', {
                  rules: [{ required: false, message: 'Please select album.' }],
                })(<>
                  <Dropdown trigger={"click"} overlay={albumMenu}>
                    <Button>
                      {albumName} <DownOutlined />
                    </Button>
                  </Dropdown>
                </>
                )}
              </Form.Item>
            }
            <Form.Item label='' className='mb20'>
              {getFieldDecorator('fileList', {
                rules: [{ required: true, message: 'Please Upload Images!' }],
              })(
                <>
                  {isVideo === false && fileList.length > 0 &&
                    <div className='flex-y center'>
                      <img
                        src={isEdit ? imageUrl.search(".mp4") != -1 ? VideoImage : imageUrl : isShowVideoIcon ? VideoImage : imageUrl}
                        alt='cover'
                        style={{
                          width: imageUrl ? '100%' : '',
                          height: imageUrl ? '100%' : '',
                          objectFit: 'fill',
                        }}
                      />
                      {!imageUrl && (
                        <span className='an-14 regular-text mt20'>
                          {isMultiple ? "Drag & Drop/Click here to upload multiple Image"
                            : "Drag & Drop/Click here to add photos/videos in an Album"}
                        </span>
                      )}
                    </div>
                  }
                  <Upload
                    accept={'image/*,video/*'}
                    listType={'picture-card'}
                    multiple={isMultiple ? true : false}
                    showUploadList={{ showPreviewIcon: false, showDownloadIcon: false }}
                    fileList={fileList}
                    onChange={onChangeImg}
                    onPreview={onPreview}
                    beforeUpload={(file) => beforeUpload(file, FILE_SIZE.ALBUM, FILE_SIZE.ERR_ALBUM)}
                    customRequest={({ file, onSuccess }) => {
                      setTimeout(() => onSuccess('ok'), 0)
                    }}
                    className="uplord_img"
                    onRemove={(d) => removeByAttr(fileList, 'uid', d.uid)}
                  >
                    {fileList.length === 0 &&
                      <div className='flex-y center zero-img'>
                        <img
                          src={UploadImage}
                          alt='cover'
                          style={{
                            width: '100%',
                            height: '100%',
                            objectFit: 'fill',
                          }}
                        />
                        <span className='an-14 regular-text mt20'>
                          {isMultiple ? "Drag & Drop/Click here to upload multiple Image"
                            : "Drag & Drop/Click here to add photos/videos in an Album"}
                        </span>
                      </div>}
                    {fileList.length > 0 && uploadButton}
                  </Upload>
                </>
              )}
            </Form.Item>
            <Form.Item label='Album title'>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please Enter Album name.' }],
              })(<Input type='text' value={name} onChange={e => setFieldsValue({ name: e.target.value })} min={1} placeholder='Write album name..' />)}
            </Form.Item>
            <Form.Item label='Album description'>
              {getFieldDecorator('description', {
                rules: [{ required: false, message: 'Please Enter description.' }],
              })(<TextArea value={description} onChange={(e) => setFieldsValue({ description: e.target.value })} placeholder='Write album descriptio...' />)}
            </Form.Item>
            <Form.Item label='Album locations'>
              <PlacesAutocomplete value={address} onChange={onChange} onSelect={onSelect} >
                {
                  ({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div>
                      <Input
                        prefix={<img src={LocationImage} />}
                        {...getInputProps({
                          placeholder: 'Search Locations ...',
                          className: 'location-search-input',
                        })}
                      />
                      <div className="autocomplete-dropdown-container">
                        {loading && <div style={{ marginTop: 20 }}>Loading...</div>}
                        {suggestions.map(suggestion => {
                          const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                          return (
                            <div {...getSuggestionItemProps(suggestion, { className })}>
                              <span>{suggestion.description}</span><br />
                            </div>
                          )
                        })}
                      </div>
                    </div>
                  )
                }
              </PlacesAutocomplete>
            </Form.Item>
            <Form.Item className="date_select" label='Album date'>
              {getFieldDecorator('date', {
                rules: [{ required: false, message: 'Please Enter Date.' }],
              })(
                // <DatePicker format="DD/MM/YYYY" placeholder="DD/MM/YYYY" disabledDate={(current) => current && current.valueOf() < Date.now()} />
                // <DatePicker format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                <>
                  <Dropdown trigger={"click"} overlay={yearMenu}>
                    <Button>
                      {year} <DownOutlined />
                    </Button>
                  </Dropdown>
                  <Dropdown trigger={"click"} overlay={monthMenu}>
                    <Button>
                      {month} <DownOutlined />
                    </Button>
                  </Dropdown>
                  <Dropdown trigger={"click"} overlay={dayMenu}>
                    <Button>
                      {date} <DownOutlined />
                    </Button>
                  </Dropdown>
                </>
              )}
            </Form.Item>
            {/* <Row gutter={24} className="album_date">
              <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              </Col>
              <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              </Col>
            </Row> */}
            <Form.Item className="uplord_button text-right">
              {loding ? <Spin tip="Loading..." /> :
                <Button
                  htmlType='submit'
                  type='primary'
                  className='ant-btn ex__primary_btn text-upper ant-btn-primary'
                  // loading={loading}
                  disabled={loading ? true : false}
                >
                  Upload
              </Button>
              }
            </Form.Item>
          </div>
        </Form>
      </div>
    </Modal>
  );
});

export default Form.create({ name: 'album' })(Popup);
