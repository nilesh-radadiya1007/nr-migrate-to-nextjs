import React from "react";
import { Modal, Button } from "antd";
import { compose } from "redux";
// import Image from 'next/image'
import Link from 'next/link';
const Success = "/images/success_ic.png";

const CreateTripSuccess = (props) => {
  return (
    <Modal
      centered
      className="auth-modal success-modal"
      width={380}
      closable={false}
      maskClosable={false}
      visible={props.visible}
    >
      <div className="text-center">
        <img src={Success} alt="" />
        <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
          Verification successful
        </h1>
        <p className="an-18 mb20 regular-text">
          Your email has successfully verified. Now you can login and start
          using Expeditions Connect
        </p>
        <Link href="/" className="done_btn medium-text an-16" passHref>
          <a className="done_btn medium-text an-16"><Button
            type="primary"
            className="ex__primary_btn"
            onClick={props.handleClick}
          >
            Done
          </Button>
          </a>
        </Link>
      </div>
    </Modal>
  );
};

export default compose(CreateTripSuccess);
