import { useRouter } from "next/router";
import React, { useState } from "react";
import { compose } from "redux";
import HomePageSection2 from '../containers/Home/HomePageSection2.js';
import LandingPageHeader from "../containers/Home/LandingPageHeader.js";

const LandingPage = (props) => {
	const router = useRouter()
	const [activityName, setActivityName] = useState("");
	
	const handleSearch = (e) => {
        e.preventDefault();
        let searchData = [];
        if (activityName !== "") {
            searchData.push(activityName);
        }
        let act = searchData.length ? JSON.stringify(searchData) : "";
        if (activityName !== "") {
            router.push(`/all-adventure?activity=${act}&isFlexible=true`);
        }
    }

	return (
		<div className="mainhomepage">
			<LandingPageHeader handleSearch={handleSearch} setActivityName={setActivityName} {...props}/>

			<HomePageSection2 {...props} />
		</div >
	);
};
export default compose(LandingPage);
