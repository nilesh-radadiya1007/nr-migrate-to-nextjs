import React from "react";
import Link from "next/link";
import { compose } from "redux";
import { Row, Col, Card } from "antd";
import Slider from "react-slick";
// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const epic1 = "/images/home/epic/1_climbing.svg";
const epic2 = "/images/home/epic/2_hiking.svg";
const epic3 = "/images/home/epic/3_skiing.svg";
const epic4 = "/images/home/epic/4_bird.svg";
// const epic5 = "/images/home/epic/5_wildlife.png";
const epic5 = "/images/home/whatFacinatesYouImage2.svg";
const epic6 = "/images/home/epic/6_nature.svg";
const epic7 = "/images/home/epic/7_kayaking.svg";
const epic8 = "/images/home/epic/8_scuba.svg";
// const epic1 = "/images/home/epic/1_climbing.png";
// const epic2 = "/images/home/epic/2_hiking.png";
// const epic3 = "/images/home/epic/3_skiing.png";
// const epic4 = "/images/home/epic/4_bird.png";
// // const epic5 = "/images/home/epic/5_wildlife.png";
// const epic5 = "/images/home/whatFacinatesYouImage2.png";
// const epic6 = "/images/home/epic/6_nature.png";
// const epic7 = "/images/home/epic/7_kayaking.png";
// const epic8 = "/images/home/epic/8_scuba.png";

const settings = {
    dots: true,
    rows: 2,
    arrows: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            }
        },
        {
            breakpoint: 576,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
    ]
};

const EpicAdventure = (props) => {

    let adventureData = [
        {
            "id": "1",
            "title": "Climbing & Trekking",
            "link": `/all-adventure?activity=[%22Rock%20Climbing%22,%22Abseiling%22,%22Trekking%22,%22Via%20Ferrata%22,%22Bouldering%22,%22Indoor%20Climbing%22,%22Scrambling%22,%22Navigation%22,%22Hill%20Walking%22,%22Canyoning%22,%22Gorge%20Walking%22,%22Ice%20Climbing%22,%22Winter%20Climbing%22,%22Camping%22,%22Backpacking%22,%22Caving%22,%22Mountain%20Biking%22,%22Avalanche%20Education%22]&isFlexible=true`,
            "img": epic1,
            'colMd': 6,
        },
        {
            "id": "2",
            "title": "Hillwalking & Hiking",
            "link": `/all-adventure?activity=[%22Navigation%22,%22Scrambling%22,%22Hill%20Walking%22,%22Canyoning%22,%22Gorge%20Walking%22,%22Camping%22,%22Hiking%22,%22Backpacking%22,%22Caving%22,%22Mountain%20Biking%22,%22Avalanche%20Education%22]&isFlexible=true`,
            "img": epic2,
            'colMd': 6,
        },
        {
            "id": "3",
            "title": "Skiing",
            "link": `/all-adventure?activity=[%22Snowboarding%22,%22Skiing%22,%22Off%20Piste%20Skiing%22,%22Ski%20Touring%22,%22Back%20Country%20Skiing%22,%22Cross%20Country%20Skiing%22,%22Avalanche%20Education%22]&isFlexible=true`,
            "img": epic3,
            'colMd': 6,
        },
        {
            "id": "4",
            "title": "Birdwatching",
            "link": `/all-adventure?activity=["Birdwatching"]&isFlexible=true`,
            "img": epic4,
            'colMd': 6,
        },
        {
            "id": "5",
            "title": "Wildlife Photography",
            "link": `/all-adventure?activity=[%22Wildlife%20Photography%20and%20Filmmaking%22,%22Landscape%20Photography%22,%22Macro%20Photography%22,%22Birding%22,%22Wildlife%20Safari%22,%22Animal%20Watching%22,%22Nature%20Discovery%22,%22Nature%20Walks%22,%22Cycling%22]&isFlexible=true`,
            "img": epic5,
            'colMd': 6,
        },
        {
            "id": "6",
            "title": "Nature & Discovery",
            "link": `/all-adventure?activity=[%22Wildlife%20Photography%20and%20Filmmaking%22,%22Landscape%20Photography%22,%22Macro%20Photography%22,%22Birding%22,%22Wildlife%20Safari%22,%22Animal%20Watching%22,%22Nature%20Discovery%22,%22Nature%20Walks%22,%22Cycling%22,%22Mountain%20Biking%22,%22Camping%22,%22Hiking%22]&isFlexible=true`,
            "img": epic6,
            'colMd': 6,
        },
        {
            "id": "7",
            "title": "Kayaking",
            "link": `/all-adventure?activity=["Kayaking"]&isFlexible=true`,
            "img": epic7,
            'colMd': 6,
        },
        {
            "id": "8",
            "title": "Scuba Diving",
            "link": `/all-adventure?&activity=["Scuba-Diving"]&isFlexible=true`,
            "img": epic8,
            'colMd': 6
        }
    ];

    return (
        <Row gutter={[25, 25]} className="epic-adventure_card">
            <Slider {...settings}>
                {adventureData.map((item, index) => {
                    return (
                        <Col xs={24} sm={24} md={item.colMd} lg={item.colMd} xl={item.colMd} className="" key={index}>
                            <Card>
                                <Link href={item.link}>
                                    <a>
                                        <div className="card">
                                            <div className="card_img"><img src={item.img} alt="Img"></img></div>
                                            <div className="card_title">
                                                <p className="title">{item.title}</p>
                                                <p className="count">View More</p>
                                            </div>
                                        </div>
                                    </a>
                                </Link>
                            </Card>
                        </Col>
                    );
                })}
            </Slider>

        </Row>
    );
};

export default compose(EpicAdventure);
