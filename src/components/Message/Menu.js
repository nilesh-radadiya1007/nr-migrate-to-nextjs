import React from 'react';
import { Button, Menu } from 'antd';
import { useSelector } from 'react-redux';

const MessageMenu = (props) => {
  const { accessToken: token, role } = useSelector((state) => state.auth);
  const obj = {};
  obj.receiver_id = props.msg.user.id;

  if(props.msg.type === 'trip') {
    obj.type = 'trip';
    obj.trip_id = props.msg.trip._id;
  }
  if(props.msg.type === 'learning') {
    obj.type = 'learning';
    obj.learning = props.msg.learning._id;
  }
  return (
    <Menu>
      {/* <Menu.Item>
         <Button
          type='link'
          onClick={() => {
            props.unread(token, role, {
              trip_id: props.msg.trip._id,
              receiver_id: props.msg.user.id,
            });
            props.setChat('');
          }}
        >
          Unread email
        </Button>
      </Menu.Item> */}
      <Menu.Item>
        <Button
          type='link'
          onClick={() =>
            props.hide(token, role, obj)
          }
        >
          Hide email
        </Button>
      </Menu.Item>
      <Menu.Item>
        <Button
          type='link'
          onClick={() =>
            props.delete(token, role, obj)
          }
        >
          Delete
        </Button>
      </Menu.Item>
      {/* <Menu.Item>
        <Button
          type='link'
          onClick={() =>
            props.star(token, role, {
              trip_id: props.msg.trip._id,
              receiver_id: props.msg.user.id,
            })
          }
        >
          Bookmark/Star
        </Button>
      </Menu.Item> */}
    </Menu>
  );
};

export default MessageMenu;
