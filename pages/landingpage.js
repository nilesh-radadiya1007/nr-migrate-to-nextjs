import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';


const Landingpage = dynamic(
    () => import("../src/containers/Landingpage"),
    { ssr: false }
);

const LandingPageInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Landingpage />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default LandingPageInApp;