import {
  LOGIN_REQUEST, LOGIN_SUCCESS, LOGOUT,
  FORGOT_PASSWORD_REQUEST, FORGOT_PASSWORD_SUCCESS,
  AUTH_ERROR,
  CHANGE_ROLE,
  CHANGE_TMP_ROLE,
  CHANGE_PASSWORD_REQUEST,
  CONFIRM_PASSWORD_SUCCESS,
  CONFIRM_PASSWORD_REQUEST,
  PROFILE_COMPLETED
} from "../../helpers/actions";

const initState = {
  isLogin: false,
  accessToken: null,
  isError: null,
  message: null,
  email: null,
  isActive: false,
  isCompleted: false,
  loader: false,
  isApproved: false,
  preferredCurrency: "USD",
  role: null,
  isProfileCompleted: false,
  tmpRole: "user"
};

function auth (state = initState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        loader: true,
      }
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLogin: true,
        accessToken: action.token,
        userId: action.userId,
        email: action.email,
        isActive: action.isActive,
        isCompleted: action.isCompleted,
        role: action.role,
        isApproved: action.isApproved,
        preferredCurrency: action.preferredCurrency,
        loader: false,
        isProfileCompleted: action.isCompleted,
        tmpRole: (typeof action.tmpRole !== "undefined" && action.tmpRole !== "user") ? action.tmpRole : "user",
      }
    case LOGOUT:
      return {
        ...state,
        isLogin: false,
        accessToken: null,
        isActive: false,
        isCompleted: false,
        isApproved: false,
        role: null,
        isProfileCompleted: false,
        tmpRole: "user"
      }
    case FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        loader: true
      }
    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        loader: false
      }
    case CHANGE_ROLE:
      return {
        ...state,
        role: action.data,
      }
    case CHANGE_TMP_ROLE:
      return {
        ...state,
        tmpRole: action.data,
      }
    case AUTH_ERROR:
      return {
        ...state,
        loader: false,
        isError: true
      }
    case CHANGE_PASSWORD_REQUEST:
      return {
        ...state,
        loader: true,
      }
    case CONFIRM_PASSWORD_REQUEST:
      return {
        ...state,
        loader: true,
      }
    case CONFIRM_PASSWORD_SUCCESS:
      return {
        ...state,
        loader: false,
      }
    case 'RESET_AUTH_LOADER':
      return {
        ...state,
        loader: false,
      }
    case 'SET_PREFERRED_CURRENCY':
      return {
        ...state,
        preferredCurrency: action.preferredCurrency,
        loader: false,
      }
    case PROFILE_COMPLETED:
      return {
        ...state,
        isProfileCompleted: true,
        loader: false,
      }
    default:
      return state;
  }
}

export default auth