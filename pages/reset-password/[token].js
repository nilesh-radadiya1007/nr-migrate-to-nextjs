import React from 'react';
import ResetPassword from '../../src/containers/ResetPassword';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';

const ResetPasswordInApp = (props) => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <ResetPassword />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}


export default ResetPasswordInApp;