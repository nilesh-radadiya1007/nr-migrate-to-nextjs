import React, { useState } from "react";
import { Modal, Form, Icon, Input, Button, Checkbox, Row, Col, Select } from "antd";
import Cookies from "universal-cookie";
import currencies from "../../helpers/currencies";
import { getCurrencySymbol } from "../../helpers/methods";
const { Option } = Select;
const { TextArea } = Input;

const PaymentDetailsModal = (props) => {
  const { getFieldDecorator } = props.form;
  const [visible, setVisible] = useState(false);
  const [paymentType, setPaymentType] = useState("");

  const cookies = new Cookies();

  const preferredCurrency = cookies.get("preferredCurrency") || "USD";

  const showModal = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const handleSubmit = () => {
    props.form.validateFields((err, values) => {
      if (!err) {
        props.sendPaymentDetails({ ...values, preferredCurrency: getCurrencySymbol(preferredCurrency) });
        props.form.resetFields();
        setVisible(false);
      }
    });
   
  };

  const onPaymentChange = (type) => {
    setPaymentType(type)
  }

  return (
    <>
      <Button onClick={showModal} className="payment-details__button">
        Payment Details
      </Button>
      <Modal
        centered
        title="Payment Details"
        width={"600px"}
        footer={null}
        closable={true}
        onCancel={handleCancel}
        visible={visible}
      >
        <div className="payment-details-container">
          <Form className="login-form">
            <Row gutter={24}>
              <Col>
                <Form.Item label="Name" className="mb0 flex-1">
                  {getFieldDecorator("accountHolderName", {
                    rules: [
                      {
                        required: true,
                        message: "Please enter name",
                      },
                    ],
                  })(<Input placeholder="Name" />)}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24} className="mt15">
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Payment Method" className="mb0 flex-1">
                  {getFieldDecorator("paymentMethod", {
                    rules: [{ required: true, message: "Select Payment Method" }],
                  })(
                    <Select
                      showSearch
                      placeholder="Select Payment Method"
                      optionFilterProp="children"
                      onChange={onPaymentChange}

                    >
                      <Option value="Bank details">Bank Transfer</Option>
                      <Option value="Card details">Card Payment</Option>
                      <Option value="Paypal details">Paypal</Option>
                      <Option value="Payment details">Other</Option>
                    </Select>

                  )}
                </Form.Item>
              </Col>
            </Row>

            {paymentType !== "" &&
              <Row gutter={24} className="mt15">

                <Col>
                  <Form.Item label={`${paymentType}`} className="mb0 flex-1 payment-details">
                    {getFieldDecorator("paymentDetails", {
                      rules: [
                        {
                          required: true,
                          message: "Please enter payment details",
                        },
                      ],
                    })(<TextArea placeholder="Enter Payment Details" />)}
                  </Form.Item>
                </Col>
                {/* <Col>
                  <div className="payment-type-details">
                    {paymentType !== "other" ? "Enter" : ""} {paymentType} details</div>
                </Col> */}
              </Row>
            }

            <Row gutter={24} className="mt15">
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Amount" >
                  {getFieldDecorator("paymentAmount", {
                    rules: [
                      {
                        required: true,
                        message: "Please enter Amount",
                      },
                    ],
                  })(
                    <Input
                      addonBefore={getCurrencySymbol(preferredCurrency)}
                      type="number"
                      min={1}
                      placeholder="Enter Amount"
                    />
                  )}
                </Form.Item>
              </Col>
              {/* <Col xs={24} sm={8} md={8} lg={8} xl={8}>
                <Form.Item>
                  <Button
                    type="primary"
                    className="login-form-button"
                    onClick={handleSubmit}
                  >
                    Send Payment Details
                  </Button>
                </Form.Item>
              </Col> */}
            </Row>
            <Row>
              <Col>
                <div className="text-right mt40">
                  <Button
                    type="primary"
                    className="ex_grey_btn mr20 mb10"
                    onClick={handleCancel}>
                    Cancel
                </Button>
                  <Button
                    type="primary"
                    htmlType="submit"
                    onClick={handleSubmit}
                    className="ex__primary_btn">
                    Send Payment Details
                </Button>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
      </Modal>
    </>
  );
};

const WrappedPaymentDetailsModal = Form.create({ name: "paymentDetails" })(
  PaymentDetailsModal
);

export default WrappedPaymentDetailsModal;
