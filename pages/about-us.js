import React from 'react';
import AboutUs from '../src/containers/AboutUs';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const AboutUsInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <AboutUs />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default AboutUsInApp;