import React from 'react'
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const CreateLearnings = dynamic(
    () => import("../src/containers/CreateLearning"),
    { ssr: false }
);

function CreateLearningsInApp (){
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <CreateLearnings />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default CreateLearningsInApp;
