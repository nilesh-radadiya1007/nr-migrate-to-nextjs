import React from 'react';
import { Avatar, Button, Icon } from 'antd';
import moment from 'moment';
import { useSelector } from 'react-redux';

import { CaptalizeFirst } from '../../helpers/methods';

const Profile = ({ profile, ...rest }) => {
  const { accessToken: token, role } = useSelector((state) => state.auth);

  let title = '';
  if (profile.type === 'trip') {
    title = 'Trip Request';
  } else {
    title = 'Workshop Request';
  }

  const bookmarkClass = profile.bookmark ? 'fillted' : '';
  return (
    <div className='right-card-align'>
      <div className='up-details'>
        <div className='profile-photo'>
          <Avatar src={profile.user.profile} alt={profile.user.name} />
        </div>
        <div className='profile-details'>
          <h2>
            {CaptalizeFirst(profile.user.name)}{' '}
            <div
              className={`bookmark ${bookmarkClass}`}
              onClick={() => {
                let obj = {};
                obj.receiver_id = profile.user.id;
                if (profile.type === 'trip') {
                  obj.type = 'trip';
                  obj.trip_id = profile.trip._id;
                }
                if (profile.type === 'learning') {
                  obj.type = 'learning';
                  obj.learning = profile.learning._id;
                }
                obj.star = profile.bookmark ? false : true;
                profile.bookmark = obj.star;
                rest.star(token, role, obj);
              }}
            >
              {' '}
              <Icon type='star' theme='filled' />{' '}
            </div>
          </h2>
          <h6>
            {moment().year() === moment(profile.createdAt).year()
              ? moment(profile.createdAt).format('MMMM DD, h:mm a')
              : moment(profile.createdAt).format('LLL')}
            , {CaptalizeFirst(profile.user.country)}
          </h6>
        </div>
      </div>
      <div className='booking-req'>
        <h5>{title}</h5> &nbsp;&nbsp;{'>'}
        <h2>{profile.trip ? profile.trip.title : profile.learning.title}</h2>
        {/* <div className='Confirm_Booking_mobile'>
          <Button onClick={rest.booking} className='view-more-trips '>
            Confirm {`${role === 'expert' ? 'Booking' : 'Payment'}`}
          </Button>
        </div> */}
      </div>
    </div>
  );
};

export default Profile;
