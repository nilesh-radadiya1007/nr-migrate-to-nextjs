import React, { useState, useEffect } from "react";
import SingleCard from "../common/SingleCard";
// import GPSIcon from "../../../public/images/newicon/location_black_svg.svg";
import SingleCardForExpert from "../../components/common/SingleCardForExpert";
// import Image from 'next/image'

const MarkerAnimated = props => {
    const { hoveredCardId, data, pageId, displayMarker, currentPageID, map } = props;
    const [showCard, setShowCard] = useState(false);

    const markerClick = () => displayMarker(pageId);

    useEffect(() => {
        setShowCard(currentPageID === pageId ? true : false)
    }, [currentPageID])

    return (
        <>
            <div className="marker-main-div" onClick={(e) => markerClick()} onMouseOver={(e) => markerClick()} onMouseLeave={(e) =>[displayMarker(''), setShowCard(false)]}>
                <img src={'/images/newicon/location_black_svg.svg'} className={`${pageId == hoveredCardId ? 'map-icon-animate marker' : 'marker'}`}/>
            </div>
            {showCard &&
                <div className="google-map-card" >
                    {map === 'adventureMap' ? (<SingleCard
                        t={data}
                        index={pageId}
                        type='googleMapCard'
                        closeCard={() => [displayMarker(''), setShowCard(false)]}
                    />) : (<SingleCardForExpert
                        expert={data}
                        index={pageId}
                        type='googleMapCard'
                        closeCard={() => [displayMarker(''), setShowCard(false)]}
                    />)}
                </div>
            }
        </>
    );

}

export default MarkerAnimated;