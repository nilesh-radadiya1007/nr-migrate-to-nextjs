import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';


const UpdateLearning = dynamic(
    () => import("../../src/containers/UpdateLearning"),
    { ssr: false }
);

const UpdateLearningInApp = (props) => {
    return (   
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <UpdateLearning />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

// export async function getServerSideProps({ params }) {
//     return { props: { id: params.id } }
// }

export default UpdateLearningInApp;