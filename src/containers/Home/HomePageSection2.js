/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect, useCallback } from "react";
import Link from "next/link";
import { compose } from "redux";
import { useSelector } from "react-redux";
import { Button, Col, Row, Select } from "antd";
// import beMoreConnectedImage from "../../../public/images/home/expert_ss.png";
// import getinspierd from "../../../public/images/home/getinspierd.png";
// import leavePositiveImpact from "../../../public/images/home/leave_positive_impact.png";
// import beMoreInspiredImage from "../../../public/images/home/beMoreInspiredImage.png";
// import LeftArrow from "../../../public/images/newicon/left_arrow.svg";
// import LearningDetails from "../../../public/images/home/learnig_details.png";
// import Cource1 from "../../../public/images/newicon/cource1.png";
// import Why1 from "../../../public/images/newicon/why1.svg";
// import Why2 from "../../../public/images/newicon/why2.svg";
// import Why3 from "../../../public/images/newicon/why3.svg";
// import Why4 from "../../../public/images/newicon/why4.svg";

import ActivityCardSliderNew from "../../components/Basic/ActivityCardSlider/ActivityCardSliderNew";
import ExpertCardForHomePage from "../../components/Basic/ActivityCardSlider/ExpertCardForHomePage";
import ExpeditionsConnect from "../../components/common/ExpeditionsConnect";
import { getAllExperts } from '../../services/expert';
import Newsletter from '../../components/common/NewsLetter';
import { getLeraningPlusTripNew } from "../../services/expert";
import AppLoader from "../../components/Loader";
// import Image from 'next/image'


// import adventure1 from "../../../public/images/home/landingpage/adventure1.png";
// import adventure2 from "../../../public/images/home/landingpage/adventure2.png";

/**
 * Auth Modal
 */
import AuthModal from "../../components/Auth/AuthModal";
import { useMediaQuery } from "react-responsive";
import { ActivityList } from "../../helpers/constants";
import EpicAdventure from "../../components/common/EpicAdventure";
import EventsAndOffersBigCard from "../../components/common/EventsAndOffersBigCard";
import EventsAndOffersSmallCard from "../../components/common/EventsAndOffers";
import { useRouter } from "next/router";

const BlogImg1 = "/images/blog/blog1Latest.svg";
const Cource2 = "/images/newicon/cource2.svg";
const Cource3 = "/images/newicon/cource3.svg";
const Cource4 = "/images/newicon/cource4.svg";
const Cource1Icon = "/images/newicon/cource1_icon.svg";
const Cource2Icon = "/images/newicon/cource2_icon.svg";
const Cource3Icon = "/images/newicon/cource3_icon.svg";
const Cource4Icon = "/images/newicon/cource4_icon.svg";
const joinTheExpeditionBackground = "/images/home/joinTheExpeditionBackground.svg";
const joinTheExpedition_compas = "/images/home/joinTheExpedition_compas.svg";
const joinTheExpedition_flashlight = "/images/home/joinTheExpedition_flashlight.svg";
const joinTheExpedition_Hummer = "/images/home/joinTheExpedition_Hummer.svg";
const quoteCloseIcon = "/images/home/quoteCloseIcon.svg";
const quoteOpenIcon = "/images/home/quoteOpenIcon.svg";
// const BlogImg1 = "/images/blog/blog1Latest.jpg";
// const Cource2 = "/images/newicon/cource2.png";
// const Cource3 = "/images/newicon/cource3.png";
// const Cource4 = "/images/newicon/cource4.png";
// const Cource1Icon = "/images/newicon/cource1_icon.svg";
// const Cource2Icon = "/images/newicon/cource2_icon.svg";
// const Cource3Icon = "/images/newicon/cource3_icon.svg";
// const Cource4Icon = "/images/newicon/cource4_icon.svg";
// const joinTheExpeditionBackground = "/images/home/joinTheExpeditionBackground.svg";
// const joinTheExpedition_compas = "/images/home/joinTheExpedition_compas.png";
// const joinTheExpedition_flashlight = "/images/home/joinTheExpedition_flashlight.png";
// const joinTheExpedition_Hummer = "/images/home/joinTheExpedition_Hummer.png";
// const quoteCloseIcon = "/images/home/quoteCloseIcon.svg";
// const quoteOpenIcon = "/images/home/quoteOpenIcon.svg";


const Home = (props) => {
	const router = useRouter()
	const [showLogin, setShowLogin] = useState(false);
	const openSignup = () => setShowLogin(true);

	const [isShowMoreCategories, setIsShowMoreCategories] = useState(false);

	const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });

	const [adventure] = useState('group');
	const [tripAndAdventure, setTripAndAdventure] = useState([]);
	const [loader, setLoader] = useState(false);
	const [pageNo] = useState(1);
	const [perPage] = useState(1000);
	const { isLogin } = useSelector((state) => state.auth);

	let blogJson = [
		{
			"id": "123465",
			"title": "Olly Sanders: Champion of Land and Sea",
			"description": "Meet Olly Sanders, a British sea kayaker and mountain climber who turned his passion for adventure into a fulfilling career.",
			"date": "February 4th, 2021",
			"link": "https://blog.expeditionsconnect.com/olly-sanders-champion-of-land-and-sea/",
			"img": BlogImg1
		},
		{
			"id": "123465",
			"title": "Soaring Peaks and Ocean Depths: Introducing Manuel Bustelo",
			"description": "Manuel Bustelo was born in the beautiful Argentinian city of Mendoza, at the foothills of the soaring Andes Mountains. As a youngster, he suffered from asthma, so on his doctor’s advice, his parents.",
			"date": "March 7th, 2021",
			"link": "https://blog.expeditionsconnect.com/soaring-peaks-and-ocean-depths-introducing-manuel-bustelo/",
			"img": "https://blog.expeditionsconnect.com/wp-content/uploads/elementor/thumbs/92459145_1329609590563989_4787196365170016256_o-p3vvguva4l4n9gktliv9g9e6ts214bysw8rya3z0c0.jpg"
		},
		{
			"id": "123465",
			"title": "Nick Dale: Wildlife Photographer",
			"description": "Nick Dale was 15 when he first set his sights on his dream career as a wildlife photographer. To practice his skills. ",
			"date": "February 3rd, 2021",
			"link": "https://blog.expeditionsconnect.com/meet-the-outdoor-adventure-consultant-mark-brown/",
			"img": 'https://blog.expeditionsconnect.com/wp-content/uploads/2021/02/62526055_2047835448859016_7324238451769868288_n.jpg'
		},
		{
			"id": "123465",
			"title": "6 Climbing Techniques from Beginner to Expert",
			"description": "It’s easy to forget that even the world’s top mountaineering experts started out as beginners. They had to learn the basics of the sport before they could progress to the next level, and the same is true for all of us. ",
			"date": "April 15th, 2021",
			"link": "https://blog.expeditionsconnect.com/mountaineering-101-6-tips-to-get-you-started/",
			"img": "https://blog.expeditionsconnect.com/wp-content/uploads/2021/03/Moutain-Path-home_clients_2048x800.jpg"
		},
	]
	const [experts, setExpert] = useState([]);
	const [expertLoader, setExpertLoader] = useState(true);

	const getAllTripsAndWorkshops = useCallback(async (value) => {
		try {
			setLoader(true);
			const result = await getLeraningPlusTripNew(value, true);
			if (result.status === 200) {
				setTripAndAdventure(result.data.data.data);
				setLoader(false);
			} else {
				setLoader(false);
			}
		} catch (err) {
			setLoader(false);
		}
	}, []);


	const getExpertsHandler = useCallback(async (value) => {
		try {
			setExpertLoader(true);
			const result = await getAllExperts(value);
			if (result.status === 200) {
				setExpert(result.data.data);
				setExpertLoader(false);
			}
			setExpertLoader(false);
		} catch (err) {
			console.log(err);
			setExpertLoader(false);
		}
	}, []);

	useEffect(() => {
		refreshTripAdventureResult();
		refreshExpertResults();
	}, []);

	const refreshTripAdventureResult = (sortBy = "") => {
		const allSearch = { pageNo, perPage, adventure, isFeatured: true };
		getAllTripsAndWorkshops(allSearch);
	}

	const refreshExpertResults = (sortBy = "") => {
		const allSearch = { country: "", activity: [], langauge: [], sortOrder: "created_at", perPage: 1000, pageNo, skills: [], isFeatured: true };
		getExpertsHandler(allSearch);
	}


	return (
		<div className="">
			<div className="section2 epic-adventure">
				<div className="adventure_section container-fluid">
					<h2 className="rubik-54-medium header title-case">
						{isMaxWidth768 ? (<><span className="color-yellow">Epic Adventures</span> <br></br><span >Right Around the Corner</span></>) :
							(<><span>Epic Adventures</span> <span className="color-yellow">Right Around the Corner</span></>)}
					</h2>
					<div className="border-bottom-yellow"></div>
					<div className="mb55 section-2-subtitle">Find the best guides and expeditions in your backyard.</div>
					{/* <div className="mb45 section-2-subtitle">Find the best guides and expeditions in your backyard.</div> */}

					<div className="">
						<Row gutter={[5, 5]}>
							<EpicAdventure
								type="directoryPage"
							/>
						</Row>
						{/* <button
							id="see_all_adventures_from_epic"
							style={{ marginTop: "70px" }}
							className="ex__primary_btn home-page-adventure-btn"
							onClick={() => props.history.push("/all-adventure")}
						>
							VIEW ALL ADVENTURES
        					</button> */}
					</div>

				</div>
			</div>
			<div className="section2 adventure-page">
				<div className="adventure_section container-fluid mb100">
					<h2 className="rubik-54-medium header title-case">Find Your <span className="color-yellow">Adventure</span></h2>
					<div className="border-bottom-yellow"></div>
					<div className="mb45 section-2-subtitle">Explore the best expeditions, workshops, and courses from qualified experts around the world. Where will your hobbies take you next? {isMaxWidth768 ? <><br></br><br></br></> : <><br></br></>}At Expeditions Connect, we help you discover what excites you for bigger and better adventures.</div>

					<div className="">
						<Row gutter={[5, 5]}>
							<EventsAndOffersSmallCard
								type="directoryPage"
							/>
						</Row>
					</div>

				</div>

			</div>

			<div className="section2 adventure-page bg-white ">
				{!loader &&
					<>
						<h2 className="rubik-54-medium header title-case">Learn what <span className="color-yellow">fascinates you</span></h2>
						<div className="border-bottom-yellow"></div>
						{!isMaxWidth768 ? (<div className="mb45 section-2-subtitle">Whatever it is that fascinates you, you’ll find a course or expedition to broaden your horizons and ignite your wanderlust.<br /> Discover a new passion with Expeditions Connect.</div>) : <div className="mb45 section-2-subtitle">Whatever it is that fascinates you, you’ll find a course or expedition to broaden your horizons and ignite your wanderlust. Discover a new passion with Expeditions Connect.</div>}

						<div className="categories trip_exprt_navigation">
							<div className="other-categories">
								<div className="first-row">
									<div className="category active">All categories</div>
									<div className="category ">
										<Link href='/all-adventure?activity=["Rock%20Climbing","Mountaineering","Trekking","Via%20Ferrata","Alpinism","Ice%20climbing"]&isFlexible=true'>
											<a>Climbing and Trekking</a>
										</Link>
									</div>
									<div className="category ">
										<Link href='/all-adventure?activity=["Wildlife%20Photography","Wildelife%20Filmmaking","Safari","Conservation","Nature%20and%20Wildlife"]&isFlexible=true'>
											<a>Nature and Wildlife</a>
										</Link>
									</div>
									<div className="category ">
										<Link href='/all-adventure?activity=["Scuba-Diving","Snorkelling"]&isFlexible=true'>
											<a>Scuba-Diving and Snorkelling</a>
										</Link>
									</div>
									<div className="category extra-category">
										<Button
											className="category"
											onClick={() => {
												setIsShowMoreCategories(!isShowMoreCategories);
											}}
										>
											....
										</Button>
									</div>
								</div>
								{isShowMoreCategories &&
									<div className="extra-activity">
										<Link href='/all-adventure?activity=["Skiing","Skiing,%20Snowboarding"]'>
											<a className="category">Skiing and Snowboarding</a>
										</Link>
									</div>
								}
							</div>
						</div>
						<div className="container-fluid learning_sec mt20 new-card-design" style={{ paddingLeft: "5%", paddingRight: "5%" }}>
							<ActivityCardSliderNew
								items={tripAndAdventure}
								type="directoryPage"
							/>
							<button
								id="see_all_adventures"
								className="ex__primary_btn home-page-adventure-btn mb40"
								onClick={() => router.push("/all-adventure")}
							>
								SEE ALL ADVENTURES
							</button>
						</div>
					</>
				}
			</div>

			{/* Join the Expedition */}
			<div className="join-the-expedition">
				{/* <div className="expedition-bgImage"> */}
				<img src={joinTheExpeditionBackground} alt="" className="bgImage" />
				{/* </div> */}
				<div className="join-the-expedition-content">
					<h3 className="header rubik-54-medium title-case">Join the Expedition</h3>
					<div className="our-aim">
						{/* Images */}
						<div className="our-aim-images">
							{/* <div className="flash-light-div"> */}
							<img
								src={joinTheExpedition_flashlight}
								alt="join expedition"
								className="flashlight"
							/>
							{/* </div> */}
							{/* <div className="hammer-div"> */}
							<img src={joinTheExpedition_Hummer} alt="" className="hammer" />
							{/* </div> */}
						</div>

						{/* Text */}
						<p className="our-aim-text">
							{/* <div className="compass-image-div"> */}
							<img
								src={joinTheExpedition_compas}
								alt="compas"
								className="compassImage"
							/>
							{/* </div> */}
							{/* <div className="qoute-open-div"> */}
							<img src={quoteOpenIcon} alt="quote open" className="quoteOpen" />
							{/* </div>
							<div className="qoute-closed-div"> */}
							<img src={quoteCloseIcon} alt="quote close" className="quoteClose" />
							{/* </div> */}
							<b>Our aim</b> is to connect enthusiasts with expert adventurers.
							<br />
							<br />
							We are on a mission to create a sustainable future for our planet
							by creating a community of like-minded, passionate explorers - a
							place where those seeking their wanderlust can learn from
							experienced professionals.
						</p>
					</div>
				</div>
			</div>

			{/* Be more sections */}
			<div className="be-more-sections">
				{/* Be more connected */}
				<div className="be-more-section">
					<div className="be-more-section-body">
						<div className="be-more-content">
							<div>
								<div className="be-more-summary title-case">
									<div>Discover & connect with <br />Experts Worldwide</div>
								</div>
								<div className="border-bottom-yellow"></div>
								<div className="be-more-description">
									As part of the community, you’ll be able to connect with adventure
									professionals anywhere in the world. You can also share your passion
									for adventure with fellow explorers from across the globe.
								</div>
								<button
									id="browse_experts"
									className="ex__primary_btn be-more-button"
									onClick={() => router.push("/experts")}
									style={{ display: 'flex', alignItems: 'center' }}
								>
									BROWSE EXPERTS <span>
										<img src={'/images/newicon/left_arrow.svg'} className="left-arrow" alt="left arrow" />
									</span>
								</button>
							</div>
						</div>
						<div className="be-more-image-container">
							<img src={'/images/home/expert_ss.svg'} alt="be more connect" />
						</div>
					</div>
					<div className="be-more-section-footer">
						<div className="dash" />
						<div className="text">
							Ignite your wanderlust and learn from passionate and experienced professionals
							in your area of interest. Whatever your level or ability, they’ll help you reach your adventure goals through personal experiences.
						</div>
					</div>
				</div>

				{/* Be more Inspired */}
				<div className="be-more-section be-more-inspired">
					<div className="be-more-section-body">
						<div className="be-more-image-container-left">
							<img src={'/images/home/getinspierd.svg'} alt="get inspierd" />
						</div>

						<div className="be-more-content">
							<div className="ml100">
								<div className="be-more-summary title-case">
									{!isMaxWidth768 ? (<div>Once-in-a-lifetime <br />adventures led by true field experts</div>) : (<div>Once-in-a-lifetime adventures led by true field experts</div>)}

								</div>
								<div className="border-bottom-yellow"></div>
								<div className="be-more-description">
									Discover amazing experiences for all budgets and abilities, designed and led by adventure professionals who know how to keep you safe. Learn about the natural world, leave a positive impact and make memories to last a lifetime.
								</div>
								<button
									id="browse_adventures"
									className="ex__primary_btn be-more-button"
									onClick={() => router.push("/all-adventure")}
								>
									BROWSE ADVENTURES
									<span>
										<img src={'/images/newicon/left_arrow.svg'} className="left-arrow" alt="left arrow" />
									</span>
								</button>
							</div>
						</div>
					</div>
					<div className="be-more-section-footer adventure">
						<div className="dash" />
						<div className="text">
							Find qualified experts to guide you on your next expedition. Bypass mass-market tours and activities sold by agents. Discover adventures you can’t find anywhere else.
						</div>
					</div>
				</div>

				{/* Be more Sustainable */}
				<div className="be-more-section be-more-sustainable">
					<div className="be-more-section-body">
						<div className="be-more-content">
							<div>
								<div className="be-more-summary title-case">
									<div>Learn from the most <br /> experienced professionals</div>
								</div>
								<div className="border-bottom-yellow"></div>
								<div className="be-more-description">
									Whatever your area of interest may be, with Expeditions Connect, you’ll find online and on-the-ground courses, workshops and many other learning materials to broaden your horizons. You can experience real-life expeditions in incredible locations across the globe.<br /><br /> You can even sign up for globally recognised adventure accreditations to kick start a career. Every single one of our experiences is carefully crafted and thought-out by expert adventurers.
								</div>
								<div className="be-more-experience">
									<div className="child">
										<Link href="/all-adventure?adventure=workshop">
											<a>
												{/* <div className="main-img-wrapper"> */}
													<img className="main-img" src={'/images/newicon/cource1.svg'} alt="" />
												{/* </div> */}
											</a>
										</Link>
										<div className="main-child-img">
											<img className="main-img-above" src={Cource1Icon} alt="cource img" />
										</div>
										<div className="main-img-txt">Courses</div>
									</div>
									<div className="child">
										<Link href="/all-adventure?adventure=workshop">
											<a>
												{/* <div className="main-img-wrapper"> */}
													<img className="main-img" src={Cource2} alt="" />
												{/* </div> */}
											</a>
										</Link>
										<div className="main-child-img">
											<img className="main-img-above" src={Cource2Icon} alt="cource img" />
										</div>
										<div className="main-img-txt">Workshops</div>
									</div>
									<div className="child">
										<Link href="/all-adventure?adventure=trip">
											<a>
												{/* <div className="main-img-wrapper"> */}
													<img className="main-img" src={Cource3} alt="" />
												{/* </div> */}
											</a>
										</Link>
										<div className="main-child-img">
											<img className="main-img-above" src={Cource3Icon} alt="cource" />
										</div>
										<div className="main-img-txt">Expeditions</div>
									</div>
									<div className="child">
										{/* <div className="main-img-wrapper"> */}
											<img className="main-img" src={Cource4} alt="" />
										{/* </div> */}
										<div className="main-child-img">
											<img className="main-img-above" src={Cource4Icon} alt="cource" />
										</div>
										<div className="main-img-txt">Webinars</div>
									</div>

								</div>
							</div>
						</div>
						<div className="be-more-image-container">
							<img src={'/images/home/learnig_details.svg'} alt="cource" />
						</div>
					</div>
				</div>

				{/* Explore with experts */}
				<div className="explore-with-experts getinspired-page">
					{expertLoader && (<div className="text-center py20 loader-position-relative">
						<AppLoader />
					</div>)}
					{!expertLoader &&
						<>
							<h1 className="rubik-54-medium title-case">Meet our Experts</h1>
							{isMaxWidth768 && (<div className="yellow-border-parent">
								<div className="border-bottom-yellow"></div>
							</div>)}
							< p className="description">
								Our adventure experts are leaders in their fields and they’re always keen to share their knowledge and experience with you. Whatever fascinates you, from mountaineering to scuba diving, there’s a world-class expert on Expeditions Connect who is eager to share their skills and passion.
							</p>

							<div className="container-fluid learning_sec">
								<ExpertCardForHomePage
									experts={experts}
								/>

								<button
									id="see_all_experts"
									className="ex__primary_btn home-page-adventure-btn"
									onClick={() => router.push("/experts")}
								>
									SEE ALL EXPERTS
								</button>
							</div>
						</>
					}

				</div>

				{/* Calling All Experts  */}
				<div className="expert_calling">
					<div className="expert_calling_info">
						<div className="bg-title">
							<span className="first-txt">Calling All Experts</span>
						</div>
						<div className="border-bottom-yellow"></div>
						<div className="bg-sub-title">
							<span>Share your knowledge, skills and expertise with people from around the globe. Inspire passionate explorers to discover the wonders of the natural world through travel, adventure and education. Are you offering a trip, running a course, event or workshop, or publishing new content? Share it with adventure enthusiasts all over the world.</span>
						</div>
						<button
							id="become_an_expert"
							className="ex__primary_btn be-an-expert"
							onClick={() => {
								if (isLogin) {
									router.push('/profile-expert');
								} else {
									openSignup()
								}
							}}>
							BECOME AN EXPERT <span>
								<img src={'/images/newicon/left_arrow.svg'} className="left-arrow" alt="left arrow" />
							</span>
						</button>


					</div>

				</div>

				{/*Leave Positive Impact */}
				<div className="be-more-section be-more-inspired leave-positive-impact mb90 ">
					<div className="be-more-section-body">
						{/* <div className='positive-impract-img-div'> */}
						<img
							src={'/images/home/leave_positive_impact.svg'}
							alt="get inspierd"
							className="positive-impract-img"
						/>
						{/* </div> */}

						<div className="be-more-content">
							<div className="ml80">
								<div className="be-more-summary">
									<div>Leave a positive impact on <br />the planet</div>
								</div>
								<div className="border-bottom-yellow"></div>
								<div className="be-more-description">
									Learning and education are core principles of the Expeditions Connect community. Through education, we aim to bring like-minded adventurers closer to the environment, empowering them to create positive change. Together we can protect our precious environment in the face of an uncertain future. <br /><br />Through Expeditions Connect, you can enjoy extraordinary learning experiences while contributing towards a brighter, more sustainable future.
								</div>
								<div className="btn-center">
									<button
										id="read_our_story"
										className="ex__primary_btn be-more-button"
										onClick={() => router.push("/about-us")}
									>
										READ OUR STORY
										<span>
											<img src={'/images/newicon/left_arrow.svg'} className="left-arrow" alt="left arrow" />
										</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="blog-section">
					<Row gutter={[24, 24]}>
						<Col span={24}>
							<Col xs={24} sm={24} md={24} lg={18} xl={18}>
								<div className="blog-main-title" >Latest Adventures Tips, Tricks, Stories and Inspiration</div>
								<div className="blog-sub-title">The world is your oyster. Check out our blog packed with stories, guides, tips, and tricks, to get inspired and create more adventure in your life.</div>
							</Col>
							<Col xs={24} sm={24} md={24} lg={6} xl={6} className="read-more-blog">
								<button
									id="read_more_blog"
									className="ex__primary_btn be-more-button"
									onClick={() => window.location.href = "https://blog.expeditionsconnect.com"}
								>
									READ MORE
									{/* <span> */}
									<img src={'/images/newicon/left_arrow.svg'} className="left-arrow" alt="left arrow" />
									{/* </span> */}
								</button>
							</Col>
						</Col>
						<Col xs={24} sm={24} md={24} lg={12} xl={12} className="first-blog">
							<div className="blog_number1" onClick={() => window.location.href = blogJson[0].link}>
								<img src={blogJson[0].img} alt="" />
								<div className="blog-img-text" >
									<p className="blog-date">{blogJson[0].date}</p>
									<p className="blog-title">{blogJson[0].title}</p>
									<p className="blog-description">{blogJson[0].description}</p>
								</div>
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} lg={12} xl={12} className="all-padding-zero padding-top-zero">
							<Col xs={24} sm={24} md={24} lg={24} xl={24}>
								<div className="blog_number2" onClick={() => window.location.href = blogJson[1].link}>
									<img src={blogJson[1].img} alt="" />
									<div className="blog-img-text">
										<p className="blog-date">{blogJson[1].date}</p>
										<p className="blog-title">{blogJson[1].title}</p>
										<p className="blog-description">{blogJson[1].description}</p>
									</div>
								</div>
							</Col>
							<Col xs={24} sm={24} md={24} lg={12} xl={12}>
								<div className="blog_number3" onClick={() => window.location.href = blogJson[2].link} >
									<img src={blogJson[2].img} alt="" />
									<div className="blog-img-text">
										<p className="blog-date">{blogJson[2].date}</p>
										<p className="blog-title">{blogJson[2].title}</p>
										<p className="blog-description">{blogJson[2].description}</p>
									</div>
								</div>
							</Col>
							<Col xs={24} sm={24} md={24} lg={12} xl={12}>
								<div className="blog_number4" onClick={() => window.location.href = blogJson[3].link}>
									<img src={blogJson[3].img} alt="" />
									<div className="blog-img-text">
										<p className="blog-date">{blogJson[3].date}</p>
										<p className="blog-title">{blogJson[3].title}</p>
										<p className="blog-description">{blogJson[3].description}</p>
									</div>
								</div>
							</Col>
						</Col>
					</Row>
				</div>
				<br></br>
				<br></br>
				<br></br>
				<br></br>

				{/*Why Choose */}
				<ExpeditionsConnect />

			</div>
			{
				showLogin && (
					<AuthModal isLoginShow={'signup'} visible={showLogin} onCancel={() => setShowLogin(false)} />
				)
			}

			<Newsletter page="homepage_2" />
		</div >
	);
};
export default compose(Home);
