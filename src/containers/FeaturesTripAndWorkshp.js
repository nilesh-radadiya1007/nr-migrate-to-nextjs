import React, { useState, useEffect, useCallback } from "react";
import {
	Row,
	Col,
	Select,
	Collapse,
	Form,
	Card,
	Rate,
	Radio,
	Badge,
	Checkbox,
	Dropdown
} from "antd";

import { compose } from "redux";
// import Grid from "../../public/images/expeditions_directory/grid_disable_ic.png";
// import Pin from "../../public/images/expeditions_directory/pin_disable.png";

/**
 * App Imports
 */
// import { expertiseList } from "../helpers/constants";
// import countries from "../helpers/countries";
// import langauges from "../helpers/langauges";
// import AppLoader from "../../../components/Loader";

import shareOption from "../containers/commonContainer/shareOption";

// import Location_Img from "../../public/images/country_ic.png";
// import Card_1 from "../../public/images/expeditions_directory/card_1.png";
// import { getAllExperts, getAllRecentTrips } from "../services/expert";
import { CaptalizeFirst } from "../../../helpers/methods";
import OwlCarousel from "react-owl-carousel";
import { useRouter } from "next/router";

const Share = "/images/share_ic.png";
const SkillRed = "/images/skill_red.svg";
const SkillGreen = "/images/skill_green.svg";
const SkillOrange = "/images/skill_orange.svg";

const sliderOptions = {
	margin: 10,
	nav: true,
	responsive: {
		0: {
			items: 1,
			nav: true,
			dotsEach: 3,
		},
		768: {
			items: 2,
			nav: true,
		},
		991: {
			items: 3,
			nav: true,
		},
	},
};


const { Option } = Select;
const { Panel } = Collapse;

const FeaturesTripAndWorkshp = (props) => {
	const { trips, view } = props;
	const router = useRouter();
	const onCardClick = (id) => router.push(`/expert-profile/${id}`);

	const onTripClick = (type, id) => {
		if (type == "trip") {
			router.push(`/trips-details/${id}`);
			return;
		} else {
			router.push(`/learning-details/${id}`);
		}
	}

	const displayLang = (lang) => {
		let resLang = ""
		if (lang.length > 0) {
			lang.map((a, index) => {
				let addCooma = "";
				if (lang.length !== index + 1) {
					addCooma = ", "
				}
				resLang += CaptalizeFirst(a) + addCooma;
			});

		}
		return resLang;
	}


	return (
		<Row gutter={10} className="pb40 workshop_similar slider_prev_next">
			<OwlCarousel
				className="owl-theme"
				{...sliderOptions}
				dots={true}
			>
				{!tripsLoader &&
					trips.map((l, index) => {
						if (index <= 5) {
							return (
								<Col xs={24} sm={24} md={24} lg={24} xl={24} key={l.id} className="gutter-row pb25 expidition_bg expendition">
									<Card
										className="price_sec"
										hoverable
										cover={<img alt="example" className="card_cover" src={l.cover} />}
									>
										{typeof l.medium !== undefined && l.medium === "online" && (
											<span className="online_tag an-10 medium-text">
												{CaptalizeFirst(typeof l.medium !== "undefined" ? l.medium : l.medium)}{" "}
											</span>
										)}
										<div className="price_sec card_details">
											<div className="mt5 mb10 an-13 card-main-des">
												{l.duration ? l.duration : ""} Days {((typeof l.country !== "undefined" && l.country !== "undefined") && l.country !== "") ? "- " + CaptalizeFirst(l.country) : ""} -{" "}
												{typeof l.activity !== "undefined" && l.activity.length > 0 ? displayLang(l.activity) : ""}
											</div>
											<p className="mb10 an-15 medium-text card-main-title">
												{CaptalizeFirst(l._doc ? l._doc.title : l.title)}
											</p>
											<Row className="price_line">
												<Col xs={18} sm={18} md={18} lg={18} xl={18}>
													<h3 className="medium-text an-16 price-tag">
														From $ {l._doc ? l._doc.price : l.price}
													</h3>
												</Col>
												<Col xs={6} sm={6} md={6} lg={6} xl={6}>
													<div className="text-right">
														<img
															src={
																parseInt(l._doc ? l._doc.skill : l.skill) < 50
																	? SkillGreen
																	: parseInt(l._doc ? l._doc.skill : l.skill) >= 50 &&
																		parseInt(l._doc ? l._doc.skill : l.skill) < 100
																		? SkillOrange
																		: SkillRed
															}
															alt="Skil level"
														/>
													</div>
												</Col>
											</Row>
										</div>
										<Row>
											<Col xs={16} sm={16} md={16} lg={16} xl={16}>
												<Rate
													allowHalf
													defaultValue={0}
													className="an-14"
												/>
											</Col>
											<Col xs={8} sm={8} md={8} lg={8} xl={8}>
												<div className="heart_fill trip_set_icon">
													<input type="checkbox" id="like1" />
													<div className="text-right">
														<Dropdown overlay={shareOption} placement="bottomCenter">
															<img
																src={Share}
																alt="Social Share"
																className="share_icn"
															/>
														</Dropdown>
														<label htmlFor="like1"><svg viewBox="0 0 24 24"><path d="M12 21.35l-1.45-1.32c-5.15-4.67-8.55-7.75-8.55-11.53 0-3.08 2.42-5.5 5.5-5.5 1.74 0 3.41.81 4.5 2.09 1.09-1.28 2.76-2.09 4.5-2.09 3.08 0 5.5 2.42 5.5 5.5 0 3.78-3.4 6.86-8.55 11.54l-1.45 1.31z"></path></svg></label>
													</div>

												</div>
											</Col>
										</Row>
									</Card>
								</Col>
							);
						} else {
							return null;
						}

					})}
			</OwlCarousel>
		</Row>
	);
};

export default compose(FeaturesTripAndWorkshp);
