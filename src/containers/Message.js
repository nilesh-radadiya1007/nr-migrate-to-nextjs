import React, {
  useState,
  useEffect,
  useCallback,
  useRef,
} from 'react';
import { Row, Col, Button, message } from 'antd';
import { useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import { useRouter } from 'next/router'
import { Sidebar, Mainarea } from '../components/Message';
import Loader from '../components/Loader';

import { updateActivity } from '../services/auth';

import {
  getEnthuTripMessages,
  getEnthuMessasgesSession,
  sendEnthuTripMessages,
  getExpertMessasgesSession,
  getExpertTripMessages,
  sendExpertTripMessages,
  readMessages,
  hideMessages,
  deleteMessages,
  starMessages,
  unReadSideMessages,
  editMessage,
  deleteMessage,
  confirmBooking,
} from '../services/expert';

const Message = (props) => {
  const router = useRouter()
  const isTabletOrMobileDevice = useMediaQuery({
    query: '(max-device-width: 767px)',
  });
  const { accessToken: token, role } = useSelector((state) => state.auth);
  const [sidebar, setSidebar] = useState([]);
  const [search, setSearch] = useState({ name: '', status: 'active' });
  const [isnorec, setIsnorec] = useState(false);
  const [messages, setMessages] = useState([]);
  const [chat, setChat] = useState();
  const [repText, setRepText] = useState('');
  const [isEdit, setIsEdit] = useState(false);
  const [isSearch, setIsSearch] = useState(false);
  const [media, setMedia] = useState([]);
  const [loader, setLoader] = useState(false);
  const [paginate, setPaginate] = useState({
    current: 1,
    pageMinValue: 0,
    pageOffset: 0,
    pageLimit: 10,
  });
  const prevMessageRef = useRef();
  const prevSidebarRef = useRef();

  const handleChange = (value) => {
    const newPaginate = paginate;
    newPaginate.current = value;
    newPaginate.pageOffset = value * 10 - 10;
    newPaginate.pageLimit = value * 10;
    setPaginate(newPaginate);
  };

  const getSidebar = useCallback(
    async (data) => {
      try {
        let result = {};

        if (role === 'enthusiasts') {
          result = await getEnthuMessasgesSession(token, data);
        }
        if (role === 'expert') {
          result = await getExpertMessasgesSession(token, data);
        }
        
        setSidebar(result.data.data);
        if (result.data.data.length === 0) {
          setIsnorec(true);
        } else {
          setIsnorec(false);
        }
        if (chat) {
          const rs = result.data.data.find((c) => c.id === chat.id);
          if (rs >= 0) setChat(rs);
        }
      } catch (error) {}
    },
    [chat, role, token],
  );

  const getMessage = useCallback(
    async (msg, update = false, data) => {
      const params = {};
      if (msg.type === 'learning') {
        params.type = 'learning';
        params.learning = msg.learning._id;
      } else {
        params.type = 'trip';
        params.trip = msg.trip._id;
      }
      //params.status = data.status;

      try {
        setChat(msg);
        let result = {};
        if (role === 'enthusiasts') {
          params.expert = msg.user.id;
          result = await getEnthuTripMessages(token, params);
        }
        if (role === 'expert') {
          params.enthuId = msg.user.id;
          result = await getExpertTripMessages(token, params);
        }
        if (
          update ||
          result.data.data.length !== prevMessageRef.current.length
        ) {
          setMessages(result.data.data);
          if (!update && document) {
            document.querySelector('.inner-main-box').scrollTo({
              top: document.querySelector('.inner-main-box').scrollHeight + 500,
              left: 100,
              behavior: 'smooth',
            });
          }
        }
      } catch (error) {}
    },
    [token, role],
  );

  useEffect(() => {
    prevMessageRef.current = messages;
    prevSidebarRef.current = sidebar;
    const interval = setInterval(() => {
      if (!isSearch) {
        if (chat) {
          getMessage(chat, false, search);
        }
        getSidebar(search);
      }
    }, 1000);
    if (!chat && sidebar.length > 0 && !isTabletOrMobileDevice) {
      setChat(sidebar[0]);
    }
    const query = router.pathname;
    // if (query.get('user') && query.get('id')) {
    //   const findIndex = sidebar.findIndex((s) => {
    //     const tour = s.trip || s.learning;
    //     return s.user.id === query.get('user') && tour._id === query.get('id');
    //   });
    //   if (findIndex !== -1) {
    //     setChat(sidebar[findIndex]);
    //     window.router.pushState('', '', '/messages');
    //   }
    // }
    return () => clearInterval(interval);
  }, [chat, sidebar, search, isSearch]);

  const addReplyHandler = async (text='') => {
    // if(typeof text)
    if(typeof text === 'object') {
      text = '';
    }
    const param = {
      message: repText || text,
      expertId: chat.user.id,
      media,
      status: search.status,
    };
    if (chat.type === 'trip') {
      param.tripId = chat.trip._id;
      param.type = 'trip';
    }
    if (chat.type === 'learning') {
      param.learning = chat.learning._id;
      param.type = 'learning';
    }
    try {
      let result;
      if (role === 'enthusiasts') {
        if (!isEdit) {
          result = await sendEnthuTripMessages(token, param);
        } else {
          result = await editMessage(token, role, {
            message: repText,
            id: isEdit,
          });
        }
      }
      if (role === 'expert') {
        if (!isEdit) {
          result = await sendExpertTripMessages(token, param);
        } else {
          result = await editMessage(token, role, {
            message: repText,
            id: isEdit,
          });
        }
      }
      // if (result.status === 200) {
      getMessage(chat, true);
      setLoader(false);
      setRepText('');
      setMedia([]);
      setIsEdit(false);
      // }
    } catch (error) {
      setRepText('');
      message.error('Something went wrong. Please try again!');
      console.log(error);
      setLoader(false);
    }
  };

  const messageRead = (id, action) => {
    readMessages(token, role, { id, action });
  };

  const handleOnActive = (e) => {
    updateActivity(token);
  };

  const handleEdit = (msg) => {
    setRepText(msg.message);
    setIsEdit(msg.id);
  };

  const handleSearch = (data) => {
    setIsSearch(true);
    setIsnorec(false);
    setSearch({ ...search, ...data });
    setChat('');
    setMessages([]);
    setSidebar([]);
    setIsSearch(false);
  };

  // const { getRemainingTime, getLastActiveTime } = useIdleTimer({
  //   timeout: 7200000,
  //   onIdle: () => {},
  //   onActive: handleOnActive,
  //   onAction: handleOnActive,
  //   debounce: 500,
  // });

  return (
    <div className='container'>
      <Row className='main-message-box'>
        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
          <div className='resposive-message-div'>
            <div
              onClick={() => {
                setChat('');
              }}
              className='message'
            >
              <h3>
                Messages{' '}
                {/* <span className='' active_count>
                  2
                </span>{' '} */}
              </h3>
            </div>
            <div className='message-btn-res'>
              <Button
                type='primary'
                size='default'
                shape='round'
                className='active_massage'
                onClick={() => {
                  setChat('');
                }}
              >
                All Messages
              </Button>
              <Button
                type='primary'
                size='default'
                shape='round'
                className='ml-5'
                onClick={() => {
                  router.push('/bookings');
                }}
              >
                Booking Details
              </Button>
            </div>
          </div>
          <div
            className={`${
              isTabletOrMobileDevice && chat ? 'mobile-messge' : ''
            }`}
          >
            <Sidebar
              data={sidebar}
              current={chat}
              getMessage={getMessage}
              delete={deleteMessages}
              hide={hideMessages}
              star={(token, role, obj) => {
                const chatNew = chat;
                chatNew.bookmark = obj.star;
                setChat(chatNew); 
                starMessages(token, role, obj);
              }}
              unread={unReadSideMessages}
              setChat={setChat}
              handleSearch={handleSearch}
              handlePage={handleChange}
              paginate={paginate}
              total={sidebar.length}
              isnorec={isnorec}
            />
          </div>
        </Col>

        <Col
          xs={24}
          sm={24}
          md={24}
          lg={16}
          xl={16}
          className={`${
            isTabletOrMobileDevice && !chat ? 'mobile-messge' : ''
          }`}
        >
          {loader && <Loader />}
          <Mainarea
            chat={chat}
            messages={messages}
            addReplyHandler={addReplyHandler}
            setRepText={setRepText}
            repText={repText}
            read={messageRead}
            delete={deleteMessages}
            hide={hideMessages}
            star={starMessages}
            media={media}
            setMedia={setMedia}
            edit={handleEdit}
            deleteSingle={(id) => deleteMessage(token, role, { id: id })}
            booking={() => {
              setLoader(true);
              const param = {};
              if (chat.type === 'trip') {
                param.tripId = chat.trip._id;
                param.type = 'trip';
              }
              if (chat.type === 'learning') {
                param.learning = chat.learning._id;
                param.type = 'learning';
              }
              param.user = chat.user.id;

              confirmBooking(token, role, param)
                .then((response) => {
                  if (response.status === 200) {
                    if (role === 'expert') {
                      const m = "This booking has been confirmed by the expert."
                      addReplyHandler(m);
                      message.success(m);
                    } else {
                      const m = "Payment has been completed for this booking."
                      addReplyHandler(m);
                      message.success(m);
                    }
                  } else {
                    message.error(response.response.data.message);
                  }
                  setLoader(false);
                })
                .catch((e) => {
                  console.log(e);
                  setLoader(false);
                });
            }}
          />
        </Col>
      </Row>
    </div>
  );
};

export default Message;
