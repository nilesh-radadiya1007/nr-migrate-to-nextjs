import React, { useState, useEffect, useCallback } from "react";
import {
  Row,
  Col,
  Collapse,
  Form,
  Select,
  Radio,
  Checkbox,
  Pagination
} from "antd";
import { compose } from "redux";
import { ActivityList, PAGINATION_OPTIONS, DISPLAY_PER_PAGE_MOBILE, DISPLAY_PER_PAGE } from "../helpers/constants";
import countries from "../helpers/countries";
import { getAllTrips, GetLearnings, getLeraningPlusTrip, getUpcommingLearning, getUpcommingTrip } from "../services/expert";
import AppLoader from "../components/Loader";
import TripWorkshopAllCards from "../components/common/TripWorkshopAllCards";
import langs from "../helpers/langauges";
import { CaptalizeFirst } from "../helpers/methods";
import { useMediaQuery } from "react-responsive";
import TripWorkshopAllCardsCarousel from "../components/common/TripWorkshopAllCardsCarousel";
import { useRouter } from 'next/router'

const Grid = "/images/expeditions_directory/grid_disable_ic.png";
const Pin = "/images/expeditions_directory/pin_disable.png";

const { Option } = Select;
const { Panel } = Collapse;

const Trips = (props) => {
  const router = useRouter()
  const [trips, setTrips] = useState([]);
  const [loader, setLoader] = useState(false);
  const [upcommingLoader, setupcommingLoader] = useState(false);

  const [activity, setActivity] = useState([]);
  const [country, setCountry] = useState("");
  const [type, setType] = useState([]);
  const [month, setMonth] = useState("");
  const [price, setPrice] = useState("");
  const [suitable, setSuitable] = useState([]);
  const [difficulty, setDifficulty] = useState("");
  const [sortBy, setSortBy] = useState(-1);

  const [currentPage, setCurrentPage] = useState(router.pathname === "/learning" ? "learning" : "expeditions");

  const [langauges, setLangauges] = useState([]);
  const [skill, setSkill] = useState("");
  const [workshopType, setWorkshopType] = useState([]);
  const [workshopMedium, setWorkshopMedium] = useState([]);
  const [filterShow, setFilterShow] = useState(false);
  const [upcommingWorkshops, setUpcommingWorkshops] = useState([]);

  const [pageNo, setPageNo] = useState(1);
  const [totalRecords, setTotalRecords] = useState(0);
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const [perPage, setPerPage] = useState(isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE);

  const sliderOptions = {
    margin: 10,
    nav: true,
    responsive: {
      0: {
        items: 1,
        nav: true,
        dotsEach: 3,
      },
      768: {
        items: 2,
        nav: true,
      },
      991: {
        items: 3,
        nav: true,
      },
    },
  };

  const getAllTripsHandler = useCallback(async (value) => {
    try {

      setLoader(true);
      const result = await getAllTrips(value);
      if (result.status === 200) {
        setTrips(result.data.data);
        setTotalRecords(result.data.count);
        setTimeout(() => {
          setLoader(false);
        }, 300);
      } else {
        setLoader(false);
      }

    } catch (err) {
      setLoader(false);
    }


  }, []);

  const getLearningHandler = useCallback(async (value) => {
    try {
      setLoader(true);
      const result = await GetLearnings(value);
      if (result.status === 200) {
        setTrips(result.data.data.data);
        setTotalRecords(result.data.data.count);

        setTimeout(() => {
          setLoader(false);
        }, 300);
      } else {
        setLoader(false);
      }

    } catch (err) {
      setLoader(false);
    }
  }, []);

  const getUpcommingLearningHandler = useCallback(async (value) => {
    try {
      setupcommingLoader(true);
      const result = await getUpcommingLearning(value);

      if (result.status === 200) {
        setUpcommingData(result.data.data.data);
      } else {
        setupcommingLoader(false);
      }
    } catch (err) {
      setupcommingLoader(false);
    }
  }, []);

  const getUpcommingTripHandler = useCallback(async (value) => {
    try {
      setupcommingLoader(true);
      const result = await getUpcommingTrip(value);
      if (result.status === 200) {
        setUpcommingData(result.data.data);
      } else {
        setupcommingLoader(false);
      }
    } catch (err) {
      setupcommingLoader(false);
    }
  }, []);

  const getAllTripsHandlerForAll = useCallback(async (value) => {
    try {
      setLoader(true);
      const result = await getLeraningPlusTrip(value);
      if (result.status === 200) {
        setTrips(result.data.data.data);
        setTotalRecords(result.data.data.count);
        setTimeout(() => {
          setLoader(false);
        }, 300);
      } else {
        setLoader(false);
      }

    } catch (err) {
      setLoader(false);
    }
  }, []);


  const setUpcommingData = (currentData) => {

    let finalWorkshop = [];
    if (currentData.length > 0) {
      currentData.forEach((trips, index) => {
        if (trips.dateTime.length > 0) {
          if (
            new Date(trips.dateTime[0].fromDate) > new Date() ||
            trips.dateType === 2
          ) {
            finalWorkshop.push(trips);
          }
        }
        if (trips.dateType === 2) {
          finalWorkshop.push(trips);
        }
      });
    }
    setUpcommingWorkshops(finalWorkshop);

    setTimeout(() => {
      setupcommingLoader(false);
    }, 500);

  }
  useEffect(() => {

    if (currentPage === "learning") {
      const llearningData = { activity, country, month, price, langauges, sortBy, skill, difficulty, workshopType, workshopMedium, suitable, pageNo, perPage };
      getLearningHandler(llearningData);
    } else if (currentPage === "expeditions") {
      const tripData = { activity, country, type, month, price, suitable, difficulty, sortBy, pageNo, perPage };
      getAllTripsHandler(tripData);
    } else {
      const allSearch = { activity, country, month, price, langauges, suitable, difficulty, sortBy, skill, workshopType, workshopMedium, pageNo, perPage };

      getAllTripsHandlerForAll(allSearch);
    }

  }, [activity, country, difficulty, month, price, suitable, type, getAllTripsHandler, getLearningHandler, sortBy, langauges, skill, workshopType, workshopMedium, currentPage, pageNo, perPage]);

  useEffect(() => {
    if (window !== undefined) {
      setCurrentPage(window.location.pathname.split("/").reverse()[0])
    }
  }, []);


  useEffect(() => {

    if (currentPage === "learning") {
      const llearningData = { activity, country, month, price, langauges, sortBy, skill, difficulty, workshopType, workshopMedium, suitable };
      getUpcommingLearningHandler(llearningData);
    } else if (currentPage === "expeditions") {
      const tripData = { activity, country, type, month, price, suitable, difficulty, sortBy };
      getUpcommingTripHandler(tripData);
    } else {
      const allSearch = { activity, country, month, price, langauges, suitable, difficulty, sortBy, skill, workshopType, workshopMedium };

      // getAllTripsHandlerForAll(allSearch);
    }

  }, [activity, country, difficulty, month, price, suitable, type, getUpcommingTripHandler, getUpcommingLearningHandler, sortBy, langauges, skill, workshopType, workshopMedium, currentPage]);


  const displayLang = (lang) => {
    let resLang = ""
    if (lang.length > 0) {
      lang.map((a, index) => {
        let addCooma = "";
        if (lang.length !== index + 1) {
          addCooma = ", "
        }
        resLang += CaptalizeFirst(a) + addCooma;
      });

    }

    return resLang;
  }

  const onSortOrderChanged = (e) => setSortBy(e);

  const handlePageChange = (e, page) => {
    setCurrentPage(page)
  }

  const onTripClick = (type, id) => {
    if (type == "trip") {
      router.push(`/trips-details/${id}`);
      return;
    } else {
      router.push(`/learning-details/${id}`);
    }

  };

  const onChange = (page, pageSize) => {
    if (window !== undefined) {
      window.scrollTo({ top: document.getElementById('scroll-here'), behavior: 'smooth' });
    }
    setPageNo(page)
    setPerPage(pageSize)
  }

  const onShowSizeChange = (page, pageSize) => {
    if (window !== undefined) {
      window.scrollTo({ top: document.getElementById('scroll-here'), behavior: 'smooth' });
    }
    setPageNo(page);
    setPerPage(pageSize);
  }

  const itemRender = (current, type, originalElement) => {
    if (!isMaxWidth768) {
      if (type === 'prev') {
        return <a>Prev</a>;
      }
      if (type === 'next') {
        return <a>Next</a>;
      }
    }
    return originalElement;
  }


  return (
    <div className="header-container expedition_header_img expedition_header_img_mobile">
      <div className={"expedition_head_bg " + (currentPage === "learning" ? "learning_bannner_img" : "expedition_banner_img")}>
        <h1 className="an-40 medium-text">
          <span className="cover-main-title">Once in a Lifetime Experiences,</span>
          <br />
          <span className="sub-title">LED BY EXPERTS</span>

        </h1>
      </div>

      <div className="categories trip_exprt_navigation" style={{ display: "none" }}>
        <div className="other-categories">
          <div className="first-row">
            <div className={"category " + (currentPage === "learning" ? "active" : "")} onClick={(e) => handlePageChange(e, "learning")}>
              Workshops
            </div>
            <div className={"category " + (currentPage === "expeditions" ? "active" : "")} onClick={(e) => handlePageChange(e, "expeditions")}>
              Trips
            </div>
            <div className={"category " + (currentPage === "all-trips" ? "active" : "")} onClick={(e) => handlePageChange(e, "all-trips")}>
              All
            </div>
          </div>

        </div>
      </div>


      <div className="container-fluid align-center">
        <div className="filter_sec_bg learning_sec">
          <Row gutter={40} className="web_view_sorting_options mt25">
            <Col xs={24} sm={24} md={24} lg={25} xl={25}>
              <Col xs={12} sm={12} md={18} lg={18} xl={17}></Col>
              <Col xs={6} sm={6} md={4} lg={4} xl={4} className="pt25 pr0 text-right sorted_by_select">
                <Select
                  defaultValue="Sort by"
                  onChange={onSortOrderChanged}
                  style={{ width: 175 }}
                  className="pr0"
                >
                  <Option value="1">A-Z</Option>
                  <Option value="-1">Z-A</Option>
                </Select>
              </Col>
              <Col xs={6} sm={6} md={2} lg={2} xl={3} className="pt25 text-right pr15 list_view_web">
                <Radio.Group defaultValue="a" buttonStyle="solid">
                  <Radio.Button value="a">
                    <img src={Grid} alt="grid" />
                  </Radio.Button>
                  <Radio.Button value="b">
                    <img src={Pin} alt="pin" />
                  </Radio.Button>
                </Radio.Group>
              </Col>
            </Col>
          </Row>
          <Row gutter={0} className="mobile_view_sorting_options mt25">
            <Col xs={24} sm={24} md={24} lg={25} xl={25}>
              <Col xs={14} sm={14} md={14} lg={14} xl={14} className="pt25 pr0 text-left main-title">
                {currentPage === "learning" ? "Learning Workshops" : currentPage === "expeditions" ? "Expeditions" : "Learning Workshops and Expeditions"}
              </Col>
              <Col xs={10} sm={10} md={10} lg={10} xl={10} className="pt25 text-right list_view">
                <Radio.Group defaultValue="a" buttonStyle="solid">
                  <Radio.Button value="a">
                    <img src={Grid} alt="grid" />
                  </Radio.Button>
                  <Radio.Button value="b">
                    <img src={Pin} alt="pin" />
                  </Radio.Button>
                </Radio.Group>
              </Col>

              <Col xs={12} sm={12} md={12} lg={12} xl={12} className="pt25 pr10 text-left sorted_by_select">
                <div className="filter_div" onClick={() => setFilterShow(!filterShow)}>FILTERS</div>
              </Col>


              <Col xs={12} sm={12} md={12} lg={12} xl={12} className="pt25 pr0 text-right sorted_by_select">
                <Select
                  defaultValue="Sort by"
                  onChange={onSortOrderChanged}
                  style={{ width: 175 }}
                  className="mobile"
                >
                  <Option value="1">A-Z</Option>
                  <Option value="-1">Z-A</Option>
                </Select>
              </Col>

              <Col xs={24} sm={24} md={24} lg={24} xl={24} className="pt25 pr10 text-left sorted_by_select">
                {filterShow &&
                  <Form>
                    <Collapse>
                      <Panel header="Activity" key="1">
                        <div>
                          <Select
                            mode="multiple"
                            onChange={(e) => [setActivity(e), setPageNo(1)]}
                            style={{ width: "100%" }}
                            placeholder="Activity"
                          >
                            {ActivityList.map((exp, i) => (
                              <Option key={i} value={exp.name}>
                                {exp.name}
                              </Option>
                            ))}
                          </Select>
                        </div>
                      </Panel>
                      <Panel header="Languages" key="2">
                        <div>
                          <Select
                            mode="multiple"
                            onChange={(e) => [setLangauges(e), setPageNo(1)]}
                            style={{ width: "100%" }}
                            placeholder="Select Language"
                          >
                            {langs.map((exp, i) => (
                              <Option key={i} value={exp.name}>
                                {exp.name}
                              </Option>
                            ))}
                          </Select>
                        </div>
                      </Panel>
                      <Panel header="Country" key="3">
                        <div>
                          <Select
                            showSearch
                            style={{ width: "100%" }}
                            onChange={(e) => [setCountry(e), setPageNo(1)]}
                            placeholder="Select Country"
                          >
                            <Option value="">All</Option>
                            {countries.map((exp, i) => (
                              <Option key={i} value={exp.name}>
                                {exp.name}
                              </Option>
                            ))}
                          </Select>
                        </div>
                      </Panel>
                      {currentPage !== "expeditions" &&

                        <Panel header="Workshop Type" key="4">
                          <div className="checkbox_bg">
                            <Checkbox.Group style={{ width: '100%' }}
                              onChange={(e) => [setWorkshopType(e), setPageNo(1)]}
                            >
                              <Row>
                                <Col span={24}>
                                  <Checkbox value="all">All</Checkbox>
                                </Col>
                                <Col span={24}>
                                  <Checkbox value="one-to-one">1-1</Checkbox>
                                </Col>
                                <Col span={24}>
                                  <Checkbox value="group">Group</Checkbox>
                                </Col>
                                <Col span={24}>
                                  <Checkbox value="customized">Customized</Checkbox>
                                </Col>
                              </Row>
                            </Checkbox.Group>
                          </div>
                        </Panel>
                      }
                      {currentPage !== "expeditions" &&
                        <Panel header="Workshop Medium" key="5">
                          <div className="checkbox_bg setWorkshopMedium">
                            <Checkbox.Group style={{ width: '100%' }} onChange={(e) => [setWorkshopMedium(e), setPageNo(1)]}>
                              <Row>
                                <Col span={24}>
                                  <Checkbox value="all">All</Checkbox>
                                </Col>
                                <Col span={24}>
                                  <Checkbox value="online">Online</Checkbox>
                                </Col>
                                <Col span={24}>
                                  <Checkbox value="classroom">Classroom</Checkbox>
                                </Col>
                                <Col span={24}>
                                  <Checkbox value="onsite">Onsite</Checkbox>
                                </Col>
                              </Row>
                            </Checkbox.Group>
                          </div>
                        </Panel>

                      }
                      <Panel header="Months" key="6">
                        <Radio.Group
                          className="months_div"
                          defaultValue={`${new Date().getFullYear()}-01-01`}
                          onChange={(e) => [setMonth(e.target.value), setPageNo(1)]}
                          buttonStyle="solid"
                        >
                          <Radio.Button value="">All</Radio.Button>
                          <Radio.Button
                            value={`31-${new Date().getFullYear()}-01-01`}
                          >
                            Jan
                          </Radio.Button>
                          <Radio.Button
                            value={`28-${new Date().getFullYear()}-02-01`}
                          >
                            Feb
                          </Radio.Button>
                          <Radio.Button
                            value={`31-${new Date().getFullYear()}-03-01`}
                          >
                            Mar
                          </Radio.Button>
                          <Radio.Button
                            value={`30-${new Date().getFullYear()}-04-01`}
                          >
                            Apr
                          </Radio.Button>
                          <Radio.Button
                            value={`31-${new Date().getFullYear()}-05-01`}
                          >
                            May
                          </Radio.Button>
                          <Radio.Button
                            value={`30-${new Date().getFullYear()}-06-01`}
                          >
                            Jun
                          </Radio.Button>
                          <Radio.Button
                            value={`31-${new Date().getFullYear()}-07-01`}
                          >
                            Jul
                          </Radio.Button>
                          <Radio.Button
                            value={`31-${new Date().getFullYear()}-08-01`}
                          >
                            Aug
                          </Radio.Button>
                          <Radio.Button
                            value={`30-${new Date().getFullYear()}-09-01`}
                          >
                            Sep
                          </Radio.Button>
                          <Radio.Button
                            value={`31-${new Date().getFullYear()}-10-01`}
                          >
                            Oct
                          </Radio.Button>
                          <Radio.Button
                            value={`30-${new Date().getFullYear()}-11-01`}
                          >
                            Nov
                          </Radio.Button>
                          <Radio.Button
                            value={`31-${new Date().getFullYear()}-12-01`}
                          >
                            Dec
                          </Radio.Button>
                        </Radio.Group>
                      </Panel>
                      <Panel header="Price Range" key="7">
                        <div className="checkbox_bg">
                          <Radio.Group
                            defaultValue=""
                            onChange={(e) => [setPrice(e.target.value), setPageNo(1)]}
                          >
                            <Radio value="">Any</Radio>
                            <Radio value="0-1000">$0 - $1000</Radio>
                            <Radio value="1000-5000">$1000 - $5000</Radio>
                            <Radio value="5000-10000">$5000 - $10000</Radio>
                            <Radio value="10000-10000000">
                              {" "}
                              More Than $10000{" "}
                            </Radio>
                          </Radio.Group>
                        </div>
                      </Panel>
                      {(currentPage === "expeditions" || currentPage === "all-trips") &&
                        <Panel header="Difficulty level" key="8">
                          <div className="checkbox_bg">
                            <Radio.Group
                              defaultValue=""
                              onChange={(e) => [setDifficulty(e.target.value), setPageNo(1)]}
                            >
                              <Radio value="">All</Radio>
                              <Radio value="0-32">Light</Radio>
                              <Radio value="33-66">Moderate</Radio>
                              <Radio value="67-99">Difficult</Radio>
                              <Radio value="100-101">Tough</Radio>
                            </Radio.Group>
                          </div>
                        </Panel>
                      }
                      {(currentPage === "learning" || currentPage === "all-trips") &&
                        <Panel header="Skill Level" key="9">
                          <div className="checkbox_bg">
                            <Radio.Group
                              defaultValue="0-1000"
                              onChange={(e) => [setSkill(e.target.value), setPageNo(1)]}
                            >
                              <Radio value="">All</Radio>
                              <Radio value="0-49">Beginner</Radio>
                              <Radio value="50-99">Medium</Radio>
                              <Radio value="100-10000">Advanced</Radio>
                            </Radio.Group>
                          </div>
                        </Panel>
                      }
                      <Panel header="Suitable For" key="10">
                        <div className="checkbox_bg">
                          <Checkbox.Group style={{ width: '100%' }} onChange={(e) => [setSuitable(e), setPageNo(1)]} >
                            <Row>
                              <Col span={24}>
                                <Checkbox value="all">All</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="individual"></Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="groups">Group</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="couples">Couples</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="families">Family</Checkbox>
                              </Col>
                            </Row>
                          </Checkbox.Group>
                        </div>
                      </Panel>
                    </Collapse>
                  </Form>
                }
              </Col>

            </Col>
          </Row>

          <Row gutter={20} className="mt20">
            <Col xs={24} sm={24} md={5} lg={5} xl={5} className="web_filters">
              <div className="filter_left">
                <h4 className="sub_title filters_text an-14">FILTERS</h4>
                <Form>
                  <Collapse defaultActiveKey={["1", "2"]}>
                    <Panel header="Activity" key="1">
                      <div>
                        <Select
                          mode="multiple"
                          onChange={(e) => [setActivity(e), setPageNo(1)]}
                          style={{ width: "100%" }}
                          placeholder="Activity"
                        >
                          {ActivityList.map((exp, i) => (
                            <Option key={i} value={exp.name}>
                              {exp.name}
                            </Option>
                          ))}
                        </Select>
                      </div>
                    </Panel>
                    <Panel header="Languages" key="2">
                      <div>
                        <Select
                          mode="multiple"
                          onChange={(e) => [setLangauges(e), setPageNo(1)]}
                          style={{ width: "100%" }}
                          placeholder="Select Language"
                        >
                          {langs.map((exp, i) => (
                            <Option key={i} value={exp.name}>
                              {exp.name}
                            </Option>
                          ))}
                        </Select>
                      </div>
                    </Panel>
                    <Panel header="Country" key="3">
                      <div>
                        <Select
                          showSearch
                          style={{ width: "100%" }}
                          onChange={(e) => [setCountry(e), setPageNo(1)]}
                          placeholder="Select Country"
                        >
                          <Option value="">All</Option>
                          {countries.map((exp, i) => (
                            <Option key={i} value={exp.name}>
                              {exp.name}
                            </Option>
                          ))}
                        </Select>
                      </div>
                    </Panel>
                    {currentPage !== "expeditions" &&
                      <Panel header="Workshop Type" key="4">
                        <div className="checkbox_bg">
                          <Checkbox.Group style={{ width: '100%' }} onChange={(e) => [setWorkshopType(e), setPageNo(1)]}>
                            <Row>
                              <Col span={24}>
                                <Checkbox value="all">All</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="one-one">1-1</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="group">Group</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="customized">Customized</Checkbox>
                              </Col>
                            </Row>
                          </Checkbox.Group>
                        </div>
                      </Panel>

                    }
                    {currentPage !== "expeditions" &&
                      <Panel header="Workshop Medium" key="5">
                        <div className="checkbox_bg setWorkshopMedium">
                          <Checkbox.Group style={{ width: '100%' }} onChange={(e) => [setWorkshopMedium(e), setPageNo(1)]} >
                            <Row>
                              <Col span={24}>
                                <Checkbox value="all">All</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="online">Online</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="classroom">Classroom</Checkbox>
                              </Col>
                              <Col span={24}>
                                <Checkbox value="onsite">Onsite</Checkbox>
                              </Col>
                            </Row>
                          </Checkbox.Group>
                        </div>
                        {/* <div className="checkbox_bg setWorkshopMedium">
                          <Radio.Group
                            defaultValue=""
                            onChange={(e) => setWorkshopMedium(e.target.value)}
                          >
                            <Radio value="">All</Radio>
                            <Radio value="online">Online</Radio>
                            <Radio value="classroom">Classroom</Radio>
                            <Radio value="onsite">Onsite</Radio>
                          </Radio.Group>
                        </div> */}
                      </Panel>

                    }
                    <Panel header="Months" key="6">
                      <Radio.Group
                        defaultValue={`${new Date().getFullYear()}-01-01`}
                        onChange={(e) => [setMonth(e.target.value), setPageNo(1)]}
                        buttonStyle="solid"
                        className="months_div"
                      >
                        <Radio.Button value="">All</Radio.Button>
                        <Radio.Button
                          value={`31-${new Date().getFullYear()}-01-01`}
                        >
                          Jan
                        </Radio.Button>
                        <Radio.Button
                          value={`28-${new Date().getFullYear()}-02-01`}
                        >
                          Feb
                        </Radio.Button>
                        <Radio.Button
                          value={`31-${new Date().getFullYear()}-03-01`}
                        >
                          Mar
                        </Radio.Button>
                        <Radio.Button
                          value={`30-${new Date().getFullYear()}-04-01`}
                        >
                          Apr
                        </Radio.Button>
                        <Radio.Button
                          value={`31-${new Date().getFullYear()}-05-01`}
                        >
                          May
                        </Radio.Button>
                        <Radio.Button
                          value={`30-${new Date().getFullYear()}-06-01`}
                        >
                          Jun
                        </Radio.Button>
                        <Radio.Button
                          value={`31-${new Date().getFullYear()}-07-01`}
                        >
                          Jul
                        </Radio.Button>
                        <Radio.Button
                          value={`31-${new Date().getFullYear()}-08-01`}
                        >
                          Aug
                        </Radio.Button>
                        <Radio.Button
                          value={`30-${new Date().getFullYear()}-09-01`}
                        >
                          Sep
                        </Radio.Button>
                        <Radio.Button
                          value={`31-${new Date().getFullYear()}-10-01`}
                        >
                          Oct
                        </Radio.Button>
                        <Radio.Button
                          value={`30-${new Date().getFullYear()}-11-01`}
                        >
                          Nov
                        </Radio.Button>
                        <Radio.Button
                          value={`31-${new Date().getFullYear()}-12-01`}
                        >
                          Dec
                        </Radio.Button>
                      </Radio.Group>
                    </Panel>
                    <Panel header="Price Range" key="7">
                      <div className="checkbox_bg">
                        <Radio.Group
                          defaultValue=""
                          onChange={(e) => [setPrice(e.target.value), setPageNo(1)]}
                        >
                          <Radio value="">Any</Radio>
                          <Radio value="0-1000">$0 - $1000</Radio>
                          <Radio value="1000-5000">$1000 - $5000</Radio>
                          <Radio value="5000-10000">$5000 - $10000</Radio>
                          <Radio value="10000-10000000">
                            {" "}
                            More Than $10000{" "}
                          </Radio>
                        </Radio.Group>
                      </div>
                    </Panel>
                    {(currentPage === "expeditions" || currentPage === "all-trips") &&
                      <Panel header="Difficulty level" key="8">
                        <div className="checkbox_bg">
                          <Radio.Group
                            defaultValue=""
                            onChange={(e) => [setDifficulty(e.target.value), setPageNo(1)]}
                          >
                            <Radio value="">All</Radio>
                            <Radio value="0-32">Light</Radio>
                            <Radio value="33-65">Moderate</Radio>
                            <Radio value="66-99">Difficult</Radio>
                            <Radio value="100-101">Tough</Radio>
                          </Radio.Group>
                        </div>
                      </Panel>
                    }
                    {(currentPage === "learning" || currentPage === "all-trips") &&
                      <Panel header="Skill Level" key="9">
                        <div className="checkbox_bg">
                          <Radio.Group
                            defaultValue="0-1000"
                            onChange={(e) => [setSkill(e.target.value), setPageNo(1)]}
                          >
                            <Radio value="">All</Radio>
                            <Radio value="0-49">Beginner</Radio>
                            <Radio value="50-99">Medium</Radio>
                            <Radio value="100-10000">Advanced</Radio>
                          </Radio.Group>
                        </div>
                      </Panel>
                    }
                    <Panel header="Suitable For" key="10">
                      <div className="checkbox_bg">
                        <Checkbox.Group style={{ width: '100%' }} onChange={(e) => [setSuitable(e), setPageNo(1)]} >
                          <Row>
                            <Col span={24}>
                              <Checkbox value="all">All</Checkbox>
                            </Col>
                            <Col span={24}>
                              <Checkbox value="individual">Solo</Checkbox>
                            </Col>
                            <Col span={24}>
                              <Checkbox value="groups">Group</Checkbox>
                            </Col>
                            <Col span={24}>
                              <Checkbox value="couples">Couples</Checkbox>
                            </Col>
                            <Col span={24}>
                              <Checkbox value="families">Family</Checkbox>
                            </Col>
                          </Row>
                        </Checkbox.Group>
                      </div>
                    </Panel>
                  </Collapse>
                </Form>
              </div>
            </Col>
            <Col xs={24} sm={24} md={24} lg={19} xl={19} className="web_grid">
              <div className="expidition_bg" id="scroll-here">
                <Row>
                  {loader ? (
                    <div className="text-center py20 loader-absolute-class">
                      <AppLoader />
                    </div>
                  ) : trips.length === 0 ? (
                    <div className="mb10 an-14 medium-text text-center mt100">
                      <h4>No results are found</h4>
                    </div>
                  ) : (
                    <TripWorkshopAllCards
                      items={trips}
                      view={currentPage}
                    />
                  )}
                </Row>
              </div>
              {!loader && trips.length !== 0 &&
                <div className='pagination_trip pagination_design'>
                  <Pagination
                    showSizeChanger
                    defaultPageSize={perPage}
                    responsive={true}
                    onChange={onChange}
                    total={totalRecords}
                    onShowSizeChange={onShowSizeChange}
                    pageSizeOptions={PAGINATION_OPTIONS}
                    itemRender={itemRender}
                    defaultCurrent={pageNo}
                    size={`${isMaxWidth768 ? 'small' : ''}`}
                  />

                </div>
              }
              {/* {!loader && trips.length > 15 &&
                <div className="view_more_position">
                  <Button className="view-more-trips an-20 mt10 mb0 button_set">
                    Load More
                </Button>
                </div>
              } */}

              {!upcommingLoader && upcommingWorkshops.length > 0 && upcommingWorkshops.length >= 3 &&
                <Row className="upcomming_section mt40">
                  <Col xs={24} sm={24} md={24} lg={24} xl={24} className="top_boreder">

                  </Col>
                  <Col xs={24} sm={24} md={24} lg={24} xl={24} className="web_grid">
                    <Row className="">
                      <Col span={24}>
                        <h4 className="sub_title">
                          {currentPage === "expeditions" ? "Upcoming Trips" : currentPage === "learning" ? "Upcoming Workshops" : "Upcoming trips & workshops"}
                        </h4>
                      </Col>
                    </Row>

                    <Row gutter={10} className="pb40 slider_prev_next">
                      {!upcommingLoader &&
                        <TripWorkshopAllCardsCarousel
                          items={upcommingWorkshops}
                        />
                      }
                    </Row>

                  </Col>
                </Row>
              }


            </Col>

          </Row>
        </div>
      </div>
    </div>
  );
};

export default compose(Trips);
