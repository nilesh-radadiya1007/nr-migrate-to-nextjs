import React, { useState, Fragment, useEffect } from 'react';
import { Button, Divider, Tabs, Modal } from "antd";
import { GoogleLogin } from "react-google-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { useSelector, useDispatch } from 'react-redux';

/**
 * Images import
 */
// import fullfill from '../../../public/images/fullfill.png';
// import singleLogo2 from '../../../public/images/single-logo2.png';
// import appLogo from '../../../public/images/logo.png';
// import googleIc from '../../../public/images/google_ic.png';
// import fbIc from '../../../public/images/fb_ic.png';

/**
 * App Imports
 */
import LoginForm from './LoginForm';
import RegisterForm from './RegisterForm';
import { AuthActions } from '../../redux/auth/events';
import { FACEBOOK, GOOGLE } from '../../helpers/constants';
import ForgotPasswordForm from './ForgotPasswordForm';
// import Image from 'next/image'

// Antd Destructring
const { TabPane } = Tabs;

const Auth = (props) => {

  const [activeTab, setActivetab] = useState(props.isLoginShow ? "signup" : "login");
  const [showForm, setShowForm] = useState(false);
  const [showGoogle, setShowGoogle] = useState(true);
  const dispatch = useDispatch();
  const loader = useSelector(state => state.auth.loader);
  const [tmpuserRole, setTmpUserRole] = useState('enthusiasts');

  const { login, forgotPassword } = AuthActions;
  const { visible, onCancel } = props;

  const responseGoogle = response => {
    if (response.error) return;
    const obj = {
      id: response.profileObj.googleId,
      email: response.profileObj.email,
      provider: "google",
      type: GOOGLE,
      tmpRole: tmpuserRole
    };
    dispatch(login(obj));
  };

  useEffect(() => {
    setTimeout(() => {
      setShowGoogle(false)
      setShowGoogle(true)
    }, 1000);

  }, [])

  const responseFacebook = response => {
    if (response.status === 'unknown') return;
    const obj = {
      id: response.userID,
      email: response.email,
      provider: "facebook",
      type: FACEBOOK,
      tmpRole: tmpuserRole
    };
    dispatch(login(obj));
  };

  const changeForm = (form) => setShowForm(form)

  const handleLogin = data => dispatch(login(data));

  const handleForgotPassword = (data) => dispatch(forgotPassword(data))

  const upldateTmpRole = (e) => {
    setTmpUserRole(e);
  }

  return (
    <Modal
      centered
      className="auth-modal signin-modal"
      width={1000}
      closable={true}
      visible={visible}
      onCancel={onCancel}
    >
      <div className="login-container flex-x">
        <div className="login_banner flex-1">
          <div className="login_overlay">
            <div>
              <img src={'/images/single-logo2.png'} alt="" />
            </div>
            <div className="fullfill-text">
              <div className="login-main-title">Join Expeditions Connect for free</div>
              <div className="login-sub-title">Explore the best adventure courses,
              workshops and expeditions directly crafted by professionals</div>
              {/* <Image layout="fill" width="150" src={fullfill} alt="fullfill" /> */}
            </div>
          </div>
        </div>
        <div className="auth_form flex-1">
          {/* <div className="auth_app_logo">
            <Image layout="fill" src={appLogo} alt="Expeditions Connect" />
          </div> */}
          {
            showForm ?
              (<ForgotPasswordForm changeForm={changeForm} loader={loader} handleForgotPassword={handleForgotPassword} />) :
              (<Fragment>
                <div className="auth_form_tabs">
                  <Tabs defaultActiveKey={activeTab} onChange={t => setActivetab(t)}>
                    <TabPane tab={<span className="an-16 medium-text fw-500">LOGIN</span>} key="login">
                      <div>
                        <LoginForm handleLogin={handleLogin} loader={loader} changeForm={changeForm} />
                      </div>
                    </TabPane>
                    <TabPane tab={<span className="an-16 medium-text fw-500">SIGNUP</span>} key="signup">
                      <div>
                        <RegisterForm handleLogin={handleLogin} loader={loader} upldateTmpRole={upldateTmpRole} />
                      </div>
                    </TabPane>
                  </Tabs>
                </div>
                <Divider
                  className={`an-14 regular-text dark-text mb25 ${activeTab === 'login' ? '' : ''}`} orientation="left" >
                  {activeTab === "login" ? "Or Login With" : "Or Signup With"}
                </Divider>
                <div className="">
                  <div className="row pb15">
                    {showGoogle &&
                      <GoogleLogin
                        clientId={process.env.NEXT_PUBLIC_GOOGLE_ID}
                        buttonText="Login"
                        onSuccess={responseGoogle}
                        onFailure={responseGoogle}
                        cookiePolicy={"single_host_origin"}
                        render={renderProps => (
                          <Button
                            className="fill-width google-button"
                            onClick={renderProps.onClick}
                            disabled={renderProps.disabled}
                          >
                            <img src={'/images/google_ic.png'} alt="g" className="mr20" />
                            {activeTab === 'login' ? "Log in with Google" : "Signup with Google"}
                          </Button>
                        )}
                      />
                    }
                  </div>
                  <div className="row">
                    <FacebookLogin
                      appId={process.env.NEXT_PUBLIC_FACEBOOK_ID}
                      autoLoad={false}
                      fields="name,email,picture"
                      callback={responseFacebook}
                      cssClass="my-facebook-button-class"
                      render={renderProps => (
                        <Button
                          className="fill-width facebook-button"
                          onClick={renderProps.onClick}>
                          <img src={'/images/fb_ic.png'} alt="fb" className="mr20" />
                          {activeTab === 'login' ? "Log in with Facebook" : "Signup with Facebook"}
                        </Button>
                      )}
                    />
                  </div>
                </div>
              </Fragment>
              )}
        </div>
      </div>
    </Modal>
  )
}

export default Auth;
