import React, { useState } from "react";
import { Modal, Row, Col, Form, Button, Input } from "antd";
// import fbIcon from '../../../public/images/home/fb_icon.png';
// import twitterIcon from '../../../public/images/home/twitter_icon.png';
// import instaIcon from '../../../public/images/home/instagram_icon.png';
// import linkedIcon from '../../../public/images/home/linkedin_icon.png';
import { SocialURL } from '../../helpers/constants';
import { Subscribe } from '../../services/expert';
import Cookies from 'universal-cookie';
// import Image from 'next/image'


const cookies = new Cookies();

const NewsletterModal = (props) => {

  const { getFieldDecorator, setFields } = props.form;
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      // values.email = emailData.nemail;
      if (!err) {
        setIsLoading(true);
        values = { ...values, subscribe: true, subscribeFrom: 'newsletter-popup' }
        const result = await Subscribe(values);
        if (result.data.status === "SUCCESS") {
          //Set expire date from day to 5 days
          
          cookies.set('newsLetterEmail', { "email": values.email }, { path: '/', expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 5) });

          props.isSuccessClose();
          props.onClose();
        } else if (result.data.status === "FAIL") {
          setFields({
            'email': {
              errors: [{
                "message": `${values.email} email is already exists.`,
                "field": "email"
              }]
            }
          });
        }
        setIsLoading(false);
      }
    });
  }

  return (
    <>
      <Modal
        centered
        className="news-letter-modal auth-modal"
        width={925}
        maskClosable={false}
        visible={props.visible}
        onCancel={() => props.onClose()}
      >
        <div className="news-letter-container">
          <Row>
            <Col xs={24} sm={24} md={15} lg={15} xl={15} className="newsletter-form">
              <p className="title">Feeling adventurous?</p>
              <p className="sub-title">Subscribe to our newsletter and stay up-to-date on the best adventures, exclusive offers, tips and tricks, stories and inspirations.</p>
              <Form onSubmit={handleSubmit} className="ex__form" autoComplete="new-password">
                <Form.Item label="Email" className="registration-email">
                  {getFieldDecorator("email", {
                    rules: [
                      {
                        type: "email",
                        message: "Please enter valid email address."
                      },
                      { required: true, message: "Please input your email" }
                    ]
                  })(<Input className="email-input" type="email" placeholder="Email" />)}
                </Form.Item>

                <Form.Item label="Name" className="registration-email">
                  {getFieldDecorator("name")(<Input className="name-input" type="name" placeholder="Name" />)}
                </Form.Item>

                <Form.Item label="Country" className="registration-email">
                  {getFieldDecorator("country")(<Input className="country-input" type="text" placeholder="Country" />)}
                </Form.Item>
                <Form.Item className="mb5">
                  <div className="pt10 pb15">
                    {isLoading ?
                      <Button type="primary" htmlType="submit" className="news-letter-submit" loading >KEEP ME UPDATED</Button>
                      :
                      <Button type="primary" htmlType="submit" className="news-letter-submit" >KEEP ME UPDATED</Button>
                    }

                  </div>
                </Form.Item>
                <p className="text-after-button">You can unsubscribe at any time by clicking the link in our emails.</p>
                <div className="social-share-section mt25">
                  <div className="follow-us-text">Follow US</div>
                  <div className="social-share-icons-wrapper">
                    <img className="share-icon" src={'/images/home/fb_icon.png'} onClick={() => window.open(SocialURL.facebook, "_blank")} />
                    <img className="share-icon" src={'/images/home/twitter_icon.png'} onClick={() => window.open(SocialURL.twitter, "_blank")} />
                    <img className="share-icon" src={'/images/home/instagram_icon.png'} onClick={() => window.open(SocialURL.instagram, "_blank")} />
                    <img className="share-icon" src={'/images/home/linkedin_icon.png'} onClick={() => window.open(SocialURL.linkedIn, "_blank")} />
                  </div>
                </div>
              </Form>
            </Col>
            <Col xs={24} sm={24} md={9} lg={9} xl={9} className="newsletter-right-img">
              {/* <Image layout="fill" src={newsLetterImg} className="newsletter-right-img"/> */}
            </Col>
          </Row>
        </div>
      </Modal>
    </>
  );
};

const WrappedNormalRegisterForm = Form.create({ name: "RegisterForm" })(NewsletterModal);

export default WrappedNormalRegisterForm;

