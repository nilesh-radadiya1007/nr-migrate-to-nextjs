import React from 'react';
import VerifyProfile from '../../src/containers/VerifyProfile';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';

const VerifyEmailInApp = (props) => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <VerifyProfile />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default VerifyEmailInApp;