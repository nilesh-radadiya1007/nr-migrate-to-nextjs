import { all, takeEvery, put, fork, select } from "redux-saga/effects";
import { message } from 'antd';
import {
  EXPERT_STEP_PERSOAL_INFO, SAVE_EXPERT_STEP_PERSOAL_INFO,
  EXPERT_STEP_BUSINESS_DETAILS,
  // SAVE_EXPERT_STEP_BUSINESS_DETAILS, 
  GO_TO_TAB_REQ, GO_TO_TAB,
  ERROR_SAVE_EXPERT, SUCCESS_SAVE_EXPERT,
  OPEN_SUCCESS_MODAL,
  UPDATE_EXPERT_PROFILE,
  OPEN_UPDATE_MODAL, EXPERT_STEP_AWARDS_AND_BIO, SAVE_EXPERT_STEP_AWARDS_AND_BIO, PROFILE_COMPLETED
} from "../../helpers/actions";

import { getToken } from './selector';

import { CreateExpertProfile, UpdateExpertProfile } from '../../services/expert';
import { setPreferredCurrency } from "../../helpers/methods";

export function* saveStepPersonalInfo() {
  yield takeEvery(EXPERT_STEP_PERSOAL_INFO, function* ({ data }) {
    const { firstName, lastName, speaks, country, experties, picture, city, skills,location,originalPicture } = data;
    yield put({
      type: SAVE_EXPERT_STEP_PERSOAL_INFO,
      firstName, lastName,
      speaks, country, experties, picture, city, skills,location,originalPicture
    });
  });
}

export function* saveStepBusinessDetails() {
  yield takeEvery(EXPERT_STEP_BUSINESS_DETAILS, function* ({ data }) {
    // const { companyname, address, phone, website, businessEmail } = data;
    // yield put({ type: SAVE_EXPERT_STEP_BUSINESS_DETAILS, companyname, website, phone, address, businessEmail });
    try {
      const token = yield select(getToken)
      const res = yield CreateExpertProfile(token, data);
      if (res.status === 200 || res.status === 201) {
        const { data } = res.data;
        yield put({
          type: SUCCESS_SAVE_EXPERT,
          profile: data.profile,
          originalProfile:data.originalProfile,
          lastName: data.lastName,
          firstName: data.firstName,
          country: data.country,
          city: data.city,
          speaks: data.speaks,
          experties: data.experties,
          skills: data.skills,
          bio: data.bio,
          awards: data.awards,
          professionalExperience: data.professionalExperience,
          professionalQualifications: data.professionalQualifications,
          socialContributions: data.socialContributions,
          booksAndPublications: data.booksAndPublications,
          companyname: data.company.name,
          website: data.company.website,
          phone: data.company.phone,
          address: data.company.address,
          businessEmail: data.company.businessEmail,
          approved: data.approved,
          active: data.active,
          cover: data.cover,
          originalCover:data.originalCover,
          id: data.id,
          stepPersonalInfo: false,
          stepBusinessDetails: false,
          stepAwardsAndBio: false,
        });
        setPreferredCurrency(data.preferredCurrency);
        yield put({ type: "SET_PREFERRED_CURRENCY", preferredCurrency: data.preferredCurrency });
        yield put({ type: PROFILE_COMPLETED, isProfileCompleted: true, });
        yield put({ type: OPEN_SUCCESS_MODAL });
      } else {
        throw res;
      }
    } catch (err) {
      message.error(err.response.data.message);
      yield put({ type: ERROR_SAVE_EXPERT });
    }
  });
}

export function* saveStepAwardsAndBio() {
  yield takeEvery(EXPERT_STEP_AWARDS_AND_BIO, function* ({ data }) {
    const {
      bio,
      professionalExperience,
      professionalQualifications,
      awards,
      socialContributions,
      booksAndPublications,
    } = data;
    yield put({ type: SAVE_EXPERT_STEP_AWARDS_AND_BIO, bio, professionalQualifications, awards, socialContributions, booksAndPublications, professionalExperience });
    // try {
    //   const token = yield select(getToken)
    //   const res = yield CreateExpertProfile(token, data);
    //   if (res.status === 200 || res.status === 201) {
    //     const { data } = res.data;
    //     yield put({ 
    //       type: SUCCESS_SAVE_EXPERT,
    //       profile: data.profile,
    //       lastName: data.lastName,
    //       firstName: data.firstName,
    //       initiatives: data.initiatives,
    //       country: data.country,
    //       speaks: data.speaks,
    //       experties: data.experties,
    //       bio: data.bio,
    //       awards: data.awards,
    //       companyname: data.company.name,
    //       website: data.company.website,
    //       phone: data.company.phone,
    //       address: data.company.address.line1,
    //       address2: data.company.address.line2,
    //       companycountry: data.company.country,
    //       approved: data.approved,
    //       active: data.active,
    //       cover: data.cover,
    //       id: data.id,
    //       stepPersonalInfo: false,
    //       stepBusinessDetails: false,
    //       stepAwardsAndBio: false,
    //     });
    //     yield put({ type: OPEN_SUCCESS_MODAL });
    //   } else {
    //     throw res;
    //   }
    // } catch (err) {
    //   message.error(err.response.data.message);
    //   yield put({ type: ERROR_SAVE_EXPERT });
    // }
  });
}

export function* updateExpert() {
  yield takeEvery(UPDATE_EXPERT_PROFILE, function* ({ data }) {
    try {
      const token = yield select(getToken)
      
      const res = yield UpdateExpertProfile(token, data);
      if (res.status === 200 || res.status === 201) {
        const { data } = res.data;
        
        yield put({
          type: SUCCESS_SAVE_EXPERT,
          profile: data.profile,
          originalProfile:data.originalProfile,
          lastName: data.lastName,
          firstName: data.firstName,
          country: data.country,
          city: data.city,
          speaks: data.speaks,
          experties: data.experties,
          skills: data.skills,
          bio: data.bio,
          awards: data.awards,
          professionalExperience: data.professionalExperience,
          professionalQualifications: data.professionalQualifications,
          socialContributions: data.socialContributions,
          booksAndPublications: data.booksAndPublications,
          companyname: data.company.name,
          website: data.company.website,
          phone: data.company.phone,
          address: data.company.address,
          businessEmail: data.company.businessEmail,
          approved: data.approved,
          active: data.active,
          cover: data.cover,
          originalCover:data.originalCover,
          id: data.id,
          stepPersonalInfo: true,
          stepBusinessDetails: true,
          stepAwardsAndBio: true,
        });
        yield put({ type: OPEN_UPDATE_MODAL });
        yield put({ type: PROFILE_COMPLETED, isProfileCompleted: true, });
      } else {
        throw res;
      }
    } catch (err) {
      message.error(err.response.data.message);
      yield put({ type: ERROR_SAVE_EXPERT });
    }
  });
}

export function* goToTab() {
  yield takeEvery(GO_TO_TAB_REQ, function* ({ data }) {
    yield put({ type: GO_TO_TAB, tab: data });
  });
}

export default function* AuthSaga() {
  yield all([
    fork(saveStepPersonalInfo),
    fork(saveStepBusinessDetails),
    fork(saveStepAwardsAndBio),
    fork(goToTab),
    fork(updateExpert),
  ]);
}