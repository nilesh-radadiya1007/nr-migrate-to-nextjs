import React, { useState } from "react";
import { Form, Button, Input } from "antd";
// import mailIcon from "../.././../public/images/home/mailIcon.svg";
import { Subscribe } from '../../services/expert';
import SubscribeSuccessModal from '../../components/common/SubscribeSuccessModal';
import Cookies from 'universal-cookie';
import { useMediaQuery } from "react-responsive";
// import Image from 'next/image'


const cookies = new Cookies();

const Newsletter = (props) => {

    const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
    const { getFieldDecorator, setFields, setFieldsValue } = props.form;
    const [isLoading, setIsLoading] = useState(false);
    const [isSubscribeSuccess, setIsSubscribeSuccess] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields(async (err, values) => {
            // values.email = emailData.nemail;

            if (!err) {
                setIsLoading(true);
                values = { ...values, subscribe: true }
                const result = await Subscribe(values);
                if (result.data.status === "SUCCESS") {
                    //Set expire date from day to 5 days
                    cookies.set('newsLetterEmail', { "email": values.email }, { path: '/', expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 5) });
                    setIsSubscribeSuccess(true);
                    setFieldsValue({ email: "" });
                } else if (result.data.status === "FAIL") {
                    setFields({
                        'email': {
                            errors: [{
                                "message": `${values.email} email is already exists.`,
                                "field": "email"
                            }]
                        }
                    });
                }
                setIsLoading(false);
            }
        });
    }

    return (
        <>
            <div className="community-section">
                <div className="join-our-community">
                    <h4 className=" text-center">Join Our Adventure Community</h4>
                    <p className="community-subtext">You’ll be the first to hear of our latest news, tips, tricks and updates.</p>
                    <Form onSubmit={handleSubmit} className="ex__form" autoComplete="new-password">
                        <Form.Item className="registration-email">
                            {getFieldDecorator("email", {
                                rules: [
                                    {
                                        type: "email",
                                        message: "Please enter valid email address."
                                    },
                                    { required: true, message: "Please input your email" }
                                ]
                            })(
                                <Input
                                    type="email"
                                    placeholder="Enter your email address"
                                    addonBefore={
                                        <div className="mail-icon-wrapper">
                                            <img src={'/images/home/mailIcon.svg'} alt="Mail icon" />
                                        </div>
                                    }

                                    addonAfter={
                                        !isMaxWidth768 ? isLoading ?
                                            (<Button className="ex__primary_btn" loading>SUBSCRIBE</Button>) :
                                            (<Button className="ex__primary_btn" id={`${typeof props.page !== "undefined" ? props.page + '_subscribe' : "subscribe_button"}`} onClick={handleSubmit}>SUBSCRIBE</Button>) : ''

                                    }
                                    className="email-input-box"
                                    id="email-input-box"

                                />
                            )}
                        </Form.Item>
                        {isMaxWidth768 &&
                            (<Form.Item className="mb5">
                                <div className="pt20 pb15">
                                    {isLoading ?
                                        (<Button className="ex__primary_btn" loading>SUBSCRIBE</Button>) :
                                        (<Button className="ex__primary_btn" onClick={handleSubmit}>SUBSCRIBE</Button>)}
                                </div>
                            </Form.Item>)
                        }
                    </Form>
                </div>
                {
                    isSubscribeSuccess &&
                    <SubscribeSuccessModal
                        visible={isSubscribeSuccess}
                        onClose={() => setIsSubscribeSuccess(false)}
                    />
                }
            </div>
        </>
    );
};

const WrappedNormalRegisterForm = Form.create({ name: "RegisterForm" })(Newsletter);

export default WrappedNormalRegisterForm;

