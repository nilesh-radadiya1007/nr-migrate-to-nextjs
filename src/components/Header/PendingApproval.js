import React from "react";
import { Modal, Button } from "antd";
import Link from "next/link";
import { compose } from "redux";
// import Image from 'next/image'

/**
 * Image Import
 */
const Success = "/images/success_ic.png";

const PendingApproval = (props) => {

  return (
    <Modal
      centered
      className="auth-modal success-modal"
      width={380}
      closable={false}
      maskClosable={false}
      visible={props.visible}
    >
      <div className="text-center">
        <img src={Success} alt="" />
        <h1 className="text-upper medium-text an-26 text- mb15 mt15">
          APPROVAL PENDING
        </h1>
        <p className="an-18 mb20 regular-text">
          Your profile is under review. Once profile is approved you will be able to create {props.viewType === "trip" ? "trips" : "workshops"}.
        </p>
        <Link href="/">
          <a className="done_btn medium-text an-16">
            <Button type="primary" className="ex__primary_btn" onClick={props.onCancel}>
              Done
            </Button>
          </a>
        </Link>
      </div>
    </Modal>
  );
};

export default compose(PendingApproval);
