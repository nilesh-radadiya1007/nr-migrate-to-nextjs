import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';


const LandingpageV2 = dynamic(
    () => import("../src/containers/Landingpage_v2"),
    { ssr: false }
);

const LandingPageInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <LandingpageV2 />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default LandingPageInApp;