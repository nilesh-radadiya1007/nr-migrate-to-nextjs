import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const Bookings = dynamic(() => import('../src/containers/MessageBooking'), { ssr: false });

const BookingsInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Bookings />
            </BasicPageWrapper>
        </BasicWrapperProvider>
    )
}

export default BookingsInApp;