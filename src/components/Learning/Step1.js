/* eslint-disable react/display-name */
import React, { useState, useEffect, useCallback } from "react";
import {
  Form,
  Row,
  Col,
  Input,
  Select,
  Button,
  Breadcrumb,
  message,
  Tooltip,
  Icon,
  Checkbox
} from "antd";
import { useDispatch, useSelector } from "react-redux";
import Cookies from "universal-cookie";
// import Image from 'next/image';

/**
 * App Imports
 */
import { ActivityList, SkillsList, PRICE_TYPE, GROUP_TYPE, SuitableFor, DISCOUNT_FIELDS } from "../../helpers/constants";
import { getBase64, } from "../../helpers/methods";
import langauges from "../../helpers/langauges";
import { LearningEvents } from "../../redux/learning/events";
import ImageUplaoderAndCropper from "../../shared/image-cropper/image-cropper";
import { UpdateLearningCoverAndOriginalCover } from "../../services/expert";
import { LoadingOutlined } from "@ant-design/icons";

const { Option } = Select;

const Step1 = React.memo((props) => {
  const [imageUrl, setImageUrl] = useState();
  const { getFieldDecorator, setFieldsValue } = props.form;
  const dispatch = useDispatch();
  let {
    cover,
    originalCover,
    title,
    price,
    skill,
    activity,
    duration,
    langauge,
    workshopType,
    workshopMedium,
    participants,
    durationType,
    priceType,
    groupType,
    discount,
    season,
    suitable,
    discountType,
    certification
  } = useSelector((state) => state.learning);

  const cookies = new Cookies();
  const [loader, setLoader] = useState(false);
  const [imageCover, setImageCover] = useState(originalCover);
  let preferredCurrency = cookies.get("preferredCurrency");
  const token = useSelector((state) => state.auth.accessToken);
  const id = useSelector((state) => state.learning.id);
  const [currency] = useState(preferredCurrency || "USD");
  const [durationAdd, setDurationAdd] = useState(durationType || "days");
  const [imageCoverObject, setImageCoverObject] = useState(undefined);
  const [imageOriginalCoverObject, setImageOriginalCoverObject] = useState(undefined);
  const [refreshFieldLoader, setrefreshFieldLoader] = useState(false);
  const [priceTypeNew, setPriceTypeNew] = useState(priceType || "3");
  const [groupTypeNew, setGroupTypeNew] = useState(groupType || "1");
  const [discountTypeNew, setDiscountTypeNew] = useState(discountType || 1);

  const { step1 } = LearningEvents;
  const [isImageChanged, setIsImageChanged] = useState(false);
  useEffect(() => {
    if (originalCover !== null) {
      setImageCover(originalCover);
    }
  }, originalCover);
  const onAddonChange = (e) => {
    setDurationAdd(e);
  };

  useEffect(() => {
    setPriceTypeNew(priceType);
  }, [priceType]);

  useEffect(() => {
    setGroupTypeNew(groupType);
  }, [groupType]);

  useEffect(() => {
    setDiscountTypeNew(discountType);
  }, [discountType]);

  const selectBefore = (
    <Select defaultValue={durationAdd} onChange={onAddonChange} style={{ width: 125 }}>
      <Option value="days">Days</Option>
      <Option value="hours">Hours</Option>
      <Option value="weeks">Weeks</Option>
    </Select>
  );

  const selectAfterPriceType = (
    <Select defaultValue={priceTypeNew} onChange={(e) => setPriceTypeNew(e)} style={{ width: 125 }}>
      {PRICE_TYPE.filter((item) => item.value !== 4).map((type, i) => {
        return (
          <Option key={i} value={type.value}>{type.name}</Option>
        )
      })}
    </Select>
  );

  const selectBeforeDiscount = (
    <Select defaultValue={discountTypeNew} onChange={(e) => setDiscountTypeNew(e)} style={{ width: 125 }}>
      {DISCOUNT_FIELDS.map((type, i) => {
        return (
          <Option key={i} value={type.value}>{type.name}</Option>
        )
      })}
    </Select>
  );

  const selectAfterGroupType = (
    <Select defaultValue={groupTypeNew} onChange={(e) => setGroupTypeNew(e)} style={{ width: 125 }}>
      {GROUP_TYPE.map((type, i) => {
        return (
          <Option key={i} value={type.value}>{type.name}</Option>
        )
      })}
    </Select>
  );

  const setPicture = useCallback((picture) => {
    if (picture !== null) {
      if (typeof picture === "string") {
        setImageUrl(picture);
      } else {
        getBase64(picture, (imageUrl) => {
          setImageUrl(imageUrl);
        });
      }
    }
  }, []);

  useEffect(() => {

    setPicture(cover);
    setrefreshFieldLoader(true);
    setDurationAdd(durationType);
    setPriceTypeNew(priceType);
    setGroupTypeNew(groupType);
    setDiscountTypeNew(discountType);
    if (typeof skill === 'string') {
      if (skill !== "") {
        skill = skill.split(',')
      } else {
        skill = [];
      }

    }

    if (typeof suitable === "string") {
      suitable = suitable.split(",");
    }
    setFieldsValue({
      cover,
      originalCover,
      title,
      price,
      skill,
      workshopType,
      workshopMedium,
      participants,
      duration,
      langauge: typeof langauge === "string" ? JSON.parse(langauge) : [],
      activity: typeof activity === "string" ? JSON.parse(activity) : [],
      discount: typeof discount !== "undefined" ? discount : "",
      season: typeof season !== "undefined" ? season : "",
      suitable,
      certification,
    });
    setTimeout(() => {
      setrefreshFieldLoader(false);
      setFieldsValue({ certification })
    }, 100);

  }, [
    activity,
    cover,
    originalCover,
    duration,
    langauge,
    participants,
    price,
    setFieldsValue,
    setPicture,
    skill,
    title,
    workshopMedium,
    workshopType,
    durationType,
    priceType,
    groupType,
    discount,
    season,
    suitable,
    discountType,
    certification
  ]);

  const uploadOriginalCover = useCallback(
    async (data) => {
      try {
        if (data !== "") {
          const formData = new FormData();
          formData.append("cover", data);
          if (imageOriginalCoverObject)
            formData.append("originalCover", imageOriginalCoverObject);
          await UpdateLearningCoverAndOriginalCover(id, token, formData);
        } else {
          const formData = new FormData();
          formData.append("cover", "");
          formData.append("originalCover", "");
          await UpdateLearningCoverAndOriginalCover(id, token, formData);
        }
      } catch (err) {
      }
    },
    [id, token, imageOriginalCoverObject]
  );

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {

        setLoader(true);
        if (isImageChanged) {
          if (id) {
            if (imageCoverObject) {
              await uploadOriginalCover(imageCoverObject);
            } else {
              await uploadOriginalCover("");
            }
          }
          if (imageOriginalCoverObject) {
            values = { ...values, originalCover: imageCoverObject }
          } else if (!id) {
            values = { ...values, originalCover: imageCover }
          }
          if (imageCoverObject) {
            values = { ...values, cover: imageCoverObject }
          }
        } else {
          if (!id) {
            values = { ...values, originalCover: imageCover }
          }
        }

        if (!values.skill) {
          setLoader(false)
          return message.error("Please select the skills level");
        }
        dispatch(step1({
          ...values, priceCurrency: currency,
          durationType: durationAdd,
          priceType: priceTypeNew,
          groupType: groupTypeNew,
          discountType: discountTypeNew
        }));
        setLoader(false);
        setIsImageChanged(false);
      }
    });
  };


  const setCoverImage = (coverFile, originalFile) => {
    setIsImageChanged(true);
    if (coverFile && coverFile !== "") {
      if (originalFile) {
        setImageOriginalCoverObject(originalFile);
      }
      setImageCoverObject(coverFile);
    } else {
      setImageOriginalCoverObject("");
      setImageCoverObject("");
    }
  };

  const onImageError = () => {
    setFieldsValue({
      cover: null
    });
  }

  return (
    <div className="step-1-expert-form learn_sec">
      <div className="an-20 medium-text  step-title">
        <Breadcrumb separator=">">
          <Breadcrumb.Item className="an-18 medium-text clr_green success--text">
            Create Workshop
          </Breadcrumb.Item>
          <Breadcrumb.Item className="an-16 regular-text pt10">
            Basic Information
          </Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <Form className="ant-advanced-search-form" onSubmit={handleSubmit}>
        <div className="form-profile-container pt20">
          <Form.Item label="" className="mb20">
            {
              getFieldDecorator("cover", {
                rules: [
                  { required: true, message: "Please Upload Cover photo!" },
                ],
              })(
                <div className={`step1-img step1-profile`}>
                  <ImageUplaoderAndCropper onImageError={() => onImageError()} section="workshop" setCoverImage={(evt, check) => setCoverImage(evt, check)} imageUrl={imageUrl} originalCover={originalCover} />
                </div>
              )
            }
          </Form.Item>

          <div>
            <Row gutter={24}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Workshop Name">
                  {getFieldDecorator("title", {
                    rules: [
                      { required: true, message: "Please Enter Trip Name" },
                    ],
                  })(<Input placeholder="Enter Workshop Name" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Select Activity">
                  {getFieldDecorator("activity", {
                    rules: [
                      { required: true, message: "Please Select Activity" },
                    ],
                  })(
                    <Select
                      mode="multiple"
                      showSearch
                      placeholder="Select Activity"
                    >
                      {ActivityList.map((exp, i) => (
                        <Option key={i} value={exp.name}>
                          {exp.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col
                className="base_price_trip_expert" xs={24} sm={12} md={12} lg={12} xl={12} >
                <Tooltip
                  placement="left"
                  title="Please go to currency selection field in the header if you want to change the currency selection for your account"
                >
                  <Form.Item label="Starting Price">
                    {getFieldDecorator("price", {
                      rules: [
                        { required: true, message: "Please Enter Price" },
                      ],
                    })(
                      !refreshFieldLoader ? <Input
                        addonAfter={selectAfterPriceType}
                        addonBefore={currency}
                        type="number"
                        min={1}
                        placeholder="Enter Price"
                      /> : <Input
                          addonBefore={currency}
                          type="number"
                          min={1}
                          placeholder="Enter Price"
                        />
                    )}
                  </Form.Item>
                </Tooltip>
              </Col>
              <Col
                className="base_price_trip_expert discount_field" xs={24} sm={12} md={12} lg={12} xl={12} >
                <Form.Item label="Special Offer (%)">
                  {getFieldDecorator("discount")
                    (
                      !refreshFieldLoader ? <Input
                        addonBefore={selectBeforeDiscount}
                        type="number"
                        min={0}
                        placeholder="Discount"
                      /> : <Input
                          type="number"
                          min={0}
                          placeholder="Discount"
                        />
                    )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col
                className="base_price_trip_expert" xs={24} sm={12} md={12} lg={12} xl={12} >
                <Form.Item label="Duration">
                  {getFieldDecorator("duration", {
                    rules: [
                      { required: true, message: "Enter Duration" },
                    ],
                  })(
                    !refreshFieldLoader ? <Input
                      addonAfter={selectBefore}
                      type="number"
                      min={1}
                      placeholder="Duration"
                    /> : <Input
                        type="number"
                        min={1}
                        placeholder="Duration"
                      />

                  )}
                </Form.Item>
              </Col>
              <Col className="base_price_trip_expert" xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label={
                  <span>
                    Season&nbsp;&nbsp;
                    <Tooltip title={<span>Please enter seasons in below format: <br /> JAN,FEB  <br /> JAN-MAR</span>}>
                      <Icon type="question-circle" theme="filled" />
                    </Tooltip>
                  </span>
                }
                >
                  {getFieldDecorator("season", {
                    rules: [
                      {
                        required: false,
                        message: "Enter Months as JAN,FEB or JAN-MAY",
                        pattern: new RegExp("^[a-zA-Z-, ]*$"),
                        message: "Only alphabet, space, hyphen(-) and comma(,) are allowed"
                      },
                    ],
                  })(
                    <Input
                      placeholder="Enter Months as JAN,FEB or JAN-MAY"
                    />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Group Size" className="group-size-field">
                  {getFieldDecorator("participants", {
                    rules: [
                      {
                        required: true,
                        message: "Please select your Group Size!",
                      },
                    ],
                  })(
                    !refreshFieldLoader ? <Input
                      type="number"
                      min={1}
                      addonAfter={selectAfterGroupType}
                      placeholder="Group Size"
                    /> : <Input
                        type="number"
                        min={1}
                        placeholder="Group Size"
                      />

                  )}
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Languages">
                  {getFieldDecorator("langauge", {
                    rules: [
                      {
                        required: true,
                        message: "Please select your Languages!",
                      },
                    ],
                  })(
                    <Select
                      mode="multiple"
                      showSearch
                      placeholder="Select Your Languages"
                      className="languages"
                    >
                      {langauges.map((lang, i) => (
                        <Option key={i} value={lang.name}>
                          {lang.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Skills Level">
                  {
                    getFieldDecorator("skill", { rules: [{ required: true, message: "Please Select Skill Level" }] })
                      (
                        <Select className="skill" mode="multiple" showSearch placeholder="Select Skill Level" >
                          {SkillsList.map((exp, i) => <Option key={i} value={exp.value}>{exp.name}</Option>)}
                        </Select>
                      )
                  }
                </Form.Item>
              </Col>
              <Col className="" xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Suitable for">
                  {getFieldDecorator("suitable", {
                    rules: [
                      { required: true, message: "Please Select Suitable for" },
                    ],
                  })(
                    <Select
                      showSearch
                      placeholder="Select Suitable"
                      mode="multiple"
                    >
                      {SuitableFor.map((exp, i) => (
                        <Option key={i} value={exp.value}>
                          {exp.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Workshop Type">
                  {getFieldDecorator("workshopType", {
                    rules: [
                      {
                        required: true,
                        message: "Please select Workshop Type!",
                      },
                    ],
                  })(
                    <Select placeholder="Select Workshop Type">
                      <Option value="One-to-One">One-to-One</Option>
                      <Option value="Group">Group</Option>
                      <Option value="Customized">Customized</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Workshop Medium">
                  {getFieldDecorator("workshopMedium", {
                    rules: [
                      {
                        required: true,
                        message: "Please select Workshop Medium!",
                      },
                    ],
                  })(
                    <Select placeholder="Select Workshop Medium">
                      <Option value="online">Online</Option>
                      <Option value="classroom">Classroom</Option>
                      <Option value="onsite">Onsite</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24} className="certification-parent">
              <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                {refreshFieldLoader &&
                  <Form.Item>
                    {getFieldDecorator(`certification`)(
                      <Checkbox defaultChecked={certification}><span className="certification-text">This workshop has certification?</span></Checkbox>
                    )}
                  </Form.Item>
                }
                {!refreshFieldLoader &&
                  <Form.Item>
                    {getFieldDecorator(`certification`)(
                      <Checkbox defaultChecked={certification}><span className="certification-text">This workshop has certification?</span></Checkbox>
                    )}
                  </Form.Item>
                }
              </Col>
            </Row>
          </div>
        </div>
        <Form.Item className="mb0 pt25">
          <Button type="primary" htmlType="submit" className="ex__primary_btn">
            Next {loader && <LoadingOutlined />}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
});

const WrappedCreateStep1 = Form.create({ name: "createTrips" })(Step1);

export default WrappedCreateStep1;
