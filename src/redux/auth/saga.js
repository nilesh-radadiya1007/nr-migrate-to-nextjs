import { all, takeEvery, put, fork, select } from "redux-saga/effects";
import { message } from "antd";
import { push } from "react-router-redux";
import Cookies from 'universal-cookie';

/**
 * App Import
 */
import {
  LOGIN_REQUEST,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  OPEN_CHOOSE_PROFILE,
  CLOSE_AUTH_MODEL,
  GET_PROFILE,
  CONFIRM_PASSWORD_REQUEST,
  LOGOUT,
  SUCCESS_SAVE_EXPERT,
  CHANGE_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_REQUEST,
  CONFIRM_PASSWORD_SUCCESS
} from '../../helpers/actions';
import {
  ACCOUNT_SUSPENDED, VERIFY_EMAIL,
  GOOGLE, FACEBOOK, LOGIN, REGISTER,
} from '../../helpers/constants';
import { UserAuth, getProfile, changePassword, userForgotPassword, confirmPassword } from '../../services/auth';
import { getRole, getToken } from './selector';
import { setPreferredCurrency } from "../../helpers/methods";
import Router from "next/router";

const cookies = new Cookies();

export function* LoginRequest() {
  yield takeEvery(LOGIN_REQUEST, function* ({ data }) {
    try {
      const { email, password, id, provider, type, tmpRole } = data;
      let Obj = {}
      if (type === LOGIN || type === REGISTER) {
        Obj = { email, password, tmpRole }
      } else if (type === FACEBOOK || type === GOOGLE) {
        Obj = { id, email, provider, tmpRole }
      }
      const res = yield UserAuth(type, Obj);
      if (res.status === 200 || res.status === 201) {
        const { data } = res.data;
        if (!data.user.isVerified) {
          message.success(VERIFY_EMAIL);
          yield put({ type: AUTH_ERROR });
        } else if (data.user.isVerified && !data.user.active) {
          message.success(ACCOUNT_SUSPENDED);
          yield put({ type: AUTH_ERROR });
        } else if (data.user.isVerified && data.user.active && !data.user.isCompleted) {
          yield put({
            type: LOGIN_SUCCESS,
            token: data.token,
            isActive: data.user.active,
            email: data.user.email,
            isCompleted: data.user.isCompleted,
            role: data.user.role,
            isApproved: data.user.isApproved,
            preferredCurrency: data.user.preferredCurrency,
            tmpRole: typeof data.user.tmpRole !== "undefined" ? data.user.tmpRole : "user",
            userId: data.user.id
          });
          setPreferredCurrency(data.user.preferredCurrency);
          yield put({ type: "SET_PREFERRED_CURRENCY", preferredCurrency: data.user.preferredCurrency });
          // cookies.set('preferredCurrency', data.user.preferredCurrency);
          if (typeof data.user.tmpRole !== "undefined" && (data.user.tmpRole === "expert" || data.user.tmpRole === "enthusiasts")) {
            yield put({ type: CLOSE_AUTH_MODEL });
            if (data.user.tmpRole.toLowerCase() === "expert") {
              yield put(Router.push({ pathname: "/create-expert-profile" }));
            } else {
              yield put(push(Router.push({ pathname: "/create-enthusiest-profile" })))
            }
          } else {
            yield put({ type: OPEN_CHOOSE_PROFILE });
          }
        } else if (data.user.isVerified && data.user.isCompleted && data.user.active) {
          yield put({
            type: LOGIN_SUCCESS,
            token: data.token,
            isActive: data.user.active,
            email: data.user.email,
            isCompleted: data.user.isCompleted,
            role: data.user.role,
            isApproved: data.user.isApproved,
            preferredCurrency: data.user.preferredCurrency,
            tmpRole: typeof data.user.tmpRole !== "undefined" ? data.user.tmpRole : "user",
            userId: data.user.id
          });
          setPreferredCurrency(data.user.preferredCurrency);
          yield put({ type: "SET_PREFERRED_CURRENCY", preferredCurrency: data.user.preferredCurrency });
          // cookies.set('preferredCurrency', data.user.preferredCurrency);
          yield put({ type: CLOSE_AUTH_MODEL });
          //localStorage.getItem('referrer')  Will check that Request is comming from which page and based on that it will redirect
          if (data.user.role.toLowerCase() === "expert") {
            yield put(push(localStorage.getItem('referrer') !== null ? localStorage.getItem('referrer') : "/"));
            localStorage.removeItem('referrer');
          } else if (data.user.role.toLowerCase() === "enthusiasts") {
            yield put(Router.push({ pathname: localStorage.getItem('referrer') !== null ? localStorage.getItem('referrer') : "/" }))
            localStorage.removeItem('referrer');
          } else {
            yield put(Router.push({ pathname: "/" }))
          }
        }
      } else {
        throw res;
      }
    } catch (err) {
      message.error(err.response.data.message);
      yield put({
        type: AUTH_ERROR,
      });
    }
  });
}

export function* GetProfile() {
  yield takeEvery(GET_PROFILE, function* () {
    try {
      const token = yield select(getToken);
      const role = yield select(getRole);
      const res = yield getProfile(token, role);
      if (res.status === 200 || res.status === 201) {
        const { data } = res.data;
        if (role === 'expert') {
          yield put({
            type: SUCCESS_SAVE_EXPERT,
            id: data.id,
            profile: data.profile,
            originalProfile: data.originalProfile,
            firstName: data.firstName,
            lastName: data.lastName,
            country: data.country,
            city: data.city,
            coordinates: data.location ? data.location.coordinates : [] || [],
            speaks: data.speaks,
            skills: data.skills,
            experties: data.experties,
            bio: data.bio,
            awards: data.awards,
            professionalExperience: data.professionalExperience !== undefined ? data.professionalExperience : [],
            professionalQualifications: data.professionalQualifications,
            socialContributions: data.socialContributions,
            booksAndPublications: data.booksAndPublications,
            companyname: data.company.name,
            website: data.company.website,
            phone: data.company.phone,
            address: data.company.address,
            businessEmail: data.company.businessEmail,
            approved: data.approved,
            active: data.active,
            cover: data.cover,
            originalCover: data.originalCover,
            stepPersonalInfo: true,
            stepBusinessDetails: true,
            stepAwardsAndBio: true,
          });
        } else {
          yield put({ type: "SAVE_ENTHU_PROFILE", ...data })
        }
      } else {
        throw res;
      }
    } catch (err) {
      if (err && err.response && err.response.status === 401) {
        yield put({ type: LOGOUT });
        yield put(Router.push({ pathname: "/" }));
      } else {
        message.error(err.response ? err.response.data.message : "Error fetching data");
      }
    }
  });
}

export function* changePasswordRequest() {
  yield takeEvery(CHANGE_PASSWORD_REQUEST, function* ({ data }) {
    try {
      const token = yield select(getToken);
      let response = yield changePassword(token, data);
      if (response.status === 200) {
        message.success("Password successfully changed");
        yield put({ type: LOGOUT });
        // if (response && response.data && response.data.data &&
        //   response.data.data.role.toLowerCase() === 'enthusiasts') {
        //   yield put(push("/enthusiest-profile"));
        // } else {
        //   yield put(push("/profile-expert"));
        // }
      } else {
        throw response;
      }
    } catch (e) {
      message.error(e.response.data.message);
      yield put({ type: AUTH_ERROR });
    }
  });
}

export function* confirmPasswordRequest() {
  yield takeEvery(CONFIRM_PASSWORD_REQUEST, function* ({ data }) {
    try {
      let response = yield confirmPassword(data);
      if (response.status === 200) {
        yield put({
          type: CONFIRM_PASSWORD_SUCCESS
        });
        message.success(`We’ve sent a confirmation to ${data.email}. Please verify your email to change your email ID.`);
        if (response && response.data && response.data.data &&
          response.data.data.role.toLowerCase() === 'enthusiasts') {
          yield put(Router.push({ pathname: "/enthusiest-profile" }));
        } else {
          yield put(Router.push({ pathname: "/profile-expert" }));
        }
      } else {
        throw response;
      }
    } catch (e) {
      message.error(e.response.data.message);
      yield put({ type: AUTH_ERROR });
    }
  });
}

export function* resetPasswordRequest() {
  yield takeEvery(FORGOT_PASSWORD_REQUEST, function* ({ data }) {
    try {
      let response = yield userForgotPassword(data);
      if (response.status === 200) {
        yield put({
          type: FORGOT_PASSWORD_SUCCESS
        });
        message.success("We have sent instructions to reset password on your email");
      } else {
        throw response;
      }
    } catch (e) {
      message.error(e.response.data.message);
      yield put({ type: AUTH_ERROR });
    }
  });
}

export default function* AuthSaga() {
  yield all([
    fork(LoginRequest),
    fork(GetProfile),
    fork(changePasswordRequest),
    fork(confirmPasswordRequest),
    fork(resetPasswordRequest),
  ]);
}
