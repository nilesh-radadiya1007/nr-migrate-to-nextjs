import React, { Fragment, useState, useEffect, useCallback } from 'react';
import {
  Form,
  Row,
  Col,
  Input,
  Button,
  DatePicker,
  // TimePicker,
  Icon,
  message,
  Select,
  Breadcrumb,
  Radio,
  Tooltip,
  Checkbox
} from 'antd';
import PlacesAutocomplete, {
  getLatLng,
  geocodeByAddress,
} from 'react-places-autocomplete';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

/**
 * App Imports
 */
import { TripsEvents } from '../../redux/trips/events';
import countries from '../../helpers/countries';
import ReactQuill from 'react-quill';
import { quillFormats, quillModules, DISCOUNT_FIELDS } from '../../helpers/constants';
import { getPriceType } from '../../helpers/methods';


import 'react-quill/dist/quill.snow.css';

const content = (
  <div>
    <p style={{ marginBottom: "0px" }}>By selecting flexi dates,</p>
    <p>you allow customers to contact you regarding suitable trip dates.</p>
  </div>
);


const { Option } = Select;
const InputGroup = Input.Group;

const Step2 = React.memo((props) => {
  const [description, setDescription] = useState('');
  const [whatLearn, setWhatWillLearn] = useState('');
  const [attend, setWhoAttend] = useState('');
  const [address1, setAddress1] = useState('');
  const [latLng, setLatLng] = useState([]);
  const [showDate, setShowDate] = useState(true);
  const [, setStartDate] = useState(false);
  const { getFieldDecorator, getFieldValue, setFieldsValue } = props.form;
  const { step2, changeTab } = TripsEvents;

  const dispatch = useDispatch();
  const {
    dateTime,
    accommodation,
    meetingPoint,
    description: dec,
    whatLearn: wl,
    attend: an,
    dateType,
    country,
    address,
    coordinates,
    price,
    priceCurrency,
    priceType,
    discount,
    discountType
  } = useSelector((state) => state.trips);

  const setEditors = useCallback((dec, wl, an) => {
    setWhoAttend(an);
    setDescription(dec);
    setWhatWillLearn(wl);
  }, []);

  useEffect(() => {
    setEditors(dec, wl, an);
    if (dateType === 2) {
      setShowDate(false);
      setFieldsValue({ dates: [] });
    }
    setFieldsValue({
      accommodation,
      meetingPoint,
      dates:
        dateTime.length !== 0
          ? JSON.parse(dateTime)
          : dateType === 1
            ? [{}]
            : [],
      dateType: dateType,
      country,
    });
    setLatLng(coordinates);
    setAddress1(address);
  }, [dec, wl, an, accommodation, dateTime, meetingPoint, setFieldsValue, setEditors, dateType, country, coordinates, address]);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        if (latLng.length === 0) {
          return message.error('Please fill all required fields');
        }
        if (!address1) {
          return message.error("Please fill all required fields");
        }
        if (!description || description === '<p><br></p>') {
          return message.error('Please fill out Description field');
        }
        const dateAndTime = [];
        values.dates.map((val, i) => {
          dateAndTime.push({
            fromDate: values[`from-date-${i}`],
            fromTime: values[`from-time-${i}`],
            toDate: values[`to-date-${i}`],
            toTime: values[`to-time-${i}`],
            price: values[`price-${i}`],
            discount: values[`discount-${i}`],
            discountType: values[`discount-type-${i}`],
            priceCurrency: priceCurrency,
            isWeekend: values[`isWeekend-${i}`],
          });
          return null;
        });

        const Obj = {
          country: values.country,
          address: address1,
          dateTime: dateAndTime,
          meetingPoint: values.meetingPoint,
          description,
          whatLearn,
          attend,
          dateType: values.dateType,
          coordinates: [
            latLng.lng ? latLng.lng : latLng[0],
            latLng.lat ? latLng.lat : latLng[1],
          ],
        };
        console.log(Obj);
        dispatch(step2(Obj));
      }
    });
  };

  const onSelect = (address) => {
    if (props.form.getFieldsValue().country === null) {
      return message.error('Please select country');
    }
    setAddress1(address);
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((ll) => setLatLng(ll))
      .catch((error) => console.error('Error', error));
  };

  const onChange = (address) => {
    setAddress1(address);
  };

  const addSlots = () => {
    const keys = getFieldValue('dates');
    const nextKeys = keys.concat(new Date().getTime());
    setFieldsValue({ dates: nextKeys });
  };

  const removeSlots = (k) => {
    const keys = getFieldValue('dates');
    setFieldsValue({ dates: keys.filter((key) => key !== k) });
  };

  const handleDescriptionChange = (textQuillBubble) => {
    setDescription(textQuillBubble);
  };

  const handleWhatLearnChange = (textQuillBubble) => {
    setWhatWillLearn(textQuillBubble);
  };


  const handleWhatAttend = (textQuillBubble) => {
    setWhoAttend(textQuillBubble);
  };

  const onDateTypeChanged = (e) => {
    if (e.target.value === 1) {
      setShowDate(true);
      setStartDate(false);
      setFieldsValue({ dates: [{}] });
    } else {
      setShowDate(false);
      setStartDate(true);
      setFieldsValue({ dates: [] });
    }
  };


  getFieldDecorator('dates', { initialValue: [] });
  const kdates = getFieldValue('dates');
  const datesItems = kdates.map((key, i) => {
    console.log(key);
    return (<Fragment key={i}>
      <div className='border_sec'>
        <Col xs={24} sm={24} md={24} lg={24} xl={24} className="pl-zero">
          <h4 className="an-14 medium-text date-slot-title">Date Slot {i + 1}</h4>
        </Col>
        <Row gutter={20}>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item label='Start date and time'>
              <Fragment>
                <Row gutter={4}>
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <Form.Item>
                      {getFieldDecorator(`from-date-${i}`, {
                        initialValue: key.fromDate
                          ? moment(key.fromDate)
                          : null,
                        rules: [
                          {
                            required: true,
                            message: 'Please Enter Start Date',
                          },
                        ],
                      })(
                        <DatePicker
                          format='YYYY-MM-DD'
                          className='fill-width'
                          placeholder='DD/MM/YY'
                          onChange={() => setStartDate(true)}
                        />,
                      )}
                    </Form.Item>
                  </Col>
                  {/* <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <Form.Item>
                      {getFieldDecorator(`from-time-${i}`, {
                        initialValue: key.fromTime
                          ? moment(key.fromTime)
                          : null,
                        rules: [
                          {
                            required: true,
                            message: 'Please Enter Start Time',
                          },
                        ],
                      })(
                        <TimePicker
                          className='fill-width'
                          placeholder='HH:MM'
                          format='HH:mm'
                          defaultOpenValue={moment('00:00', 'HH:mm')}
                        />,
                      )}
                    </Form.Item>
                  </Col> */}
                </Row>
              </Fragment>
            </Form.Item>
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item label='End date and time'>
              <Fragment>
                <Row gutter={4}>
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <Form.Item>
                      {getFieldDecorator(`to-date-${i}`, {
                        initialValue: key.toDate ? moment(key.toDate) : null,
                        rules: [
                          { required: true, message: 'Please Enter End Date' },
                        ],
                      })(
                        <DatePicker
                          format='YYYY-MM-DD'
                          className='fill-width'
                          placeholder='DD/MM/YY'
                        // disabled={startDate ? false : true}
                        />,
                      )}
                    </Form.Item>
                  </Col>
                  {/* <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <Form.Item>
                      {getFieldDecorator(`to-time-${i}`, {
                        initialValue: key.toTime ? moment(key.toTime) : null,
                        rules: [
                          { required: true, message: 'Please Enter End Time' },
                        ],
                      })(
                        <TimePicker
                          className='fill-width'
                          placeholder='HH:MM'
                          format='HH:mm'
                          defaultOpenValue={moment('00:00', 'HH:mm')}
                        // disabled={startDate ? false : true}
                        />,
                      )}
                    </Form.Item>
                  </Col> */}
                </Row>
              </Fragment>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={20}>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <Form.Item >
              {getFieldDecorator(`isWeekend-${i}`, {
                initialValue: key.isWeekend
              })(
                <Checkbox defaultChecked={key.isWeekend}><span className="certification-text">This is weekend trip?</span></Checkbox>
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={20}>
          <Col xs={12} sm={12} md={12} lg={12} xl={12}>
            <Form.Item label='Price'>
              {getFieldDecorator(`price-${i}`, {
                initialValue: key.price ? key.price : price,
                rules: [{ required: true, message: 'Please Enter Price' }],
              })(
                <Input
                  addonBefore={priceCurrency}
                  addonAfter={getPriceType(priceType)}
                  type='number'
                  min={1}
                  placeholder='Enter Price'
                />,
              )}
            </Form.Item>
          </Col>
          <Col xs={12} sm={12} md={12} lg={12} xl={12} className="discount_field">
            <Form.Item label="Special Offer (%)">
              <InputGroup compact>
                {getFieldDecorator(`discount-type-${i}`, {
                  initialValue: typeof key.discountType !== "undefined" && key.discountType !== "" && key.discountType !== null ? key.discountType : discountType,
                  rules: [{ required: false, message: "Enter Discount" }],
                })(
                  <Select style={{ width: 125 }}>
                    {DISCOUNT_FIELDS.map((type, i) => {
                      return (
                        <Option key={i} value={type.value}>{type.name}</Option>
                      )
                    })}
                  </Select>
                )}
                {getFieldDecorator(`discount-${i}`, {
                  initialValue: key.discount ? key.discount : "",
                  rules: [{ required: false, message: "Enter Discount" }],
                })(
                  <Input
                    defaultValue={discount}
                    style={{ width: '50%' }}
                    placeholder="Discount"
                  />
                )}
              </InputGroup>
            </Form.Item>
          </Col>
        </Row>
        {kdates.length !== 1 && (
          <Icon
            className='delete-icon date_icn'
            type='close'
            onClick={() => removeSlots(key)}
          />
        )}
      </div>
    </Fragment>)
  });

  return (
    <div className='step-1-expert-form step-2 ln_sec learn_sec'>
      <Breadcrumb separator='>' style={{ marginLeft: '-10px' }}>
        <Breadcrumb.Item className='an-20 medium-text success--text step-title'>
          Create Trip
        </Breadcrumb.Item>
        <Breadcrumb.Item className='an-16 regular-text pt10'>
          Trip Details
        </Breadcrumb.Item>
      </Breadcrumb>
      <Form
        className='ant-advanced-search-form creacte_rip_section_step_2'
        onSubmit={handleSubmit}
      >
        <div className='form-profile-container pt20'>
          <Row gutter={24}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24} style={{ padding: 0 }}>

              <Form.Item label='Description' className='flex-1 ant-form-item-required desc-min-height' >
                <ReactQuill
                  theme='snow'
                  modules={{
                    toolbar: false,
                  }}
                  value={description && description.trim() !== 'null' ? description : ''}
                  onChange={handleDescriptionChange}
                />
              </Form.Item>

              {/* {getFieldDecorator(`description`, {
                  rules: [
                    { required: true, message: 'Please fill description' },
                  ],
                })(
                  <TextArea
                    placeholder={`Description`}
                    value={removeLinebr(description)}
                    onChange={(e) => handleChageDescription(e)}
                  />,
                )}
              </Form.Item> */}
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24} style={{ padding: 0 }}>
              {/* <Form.Item label="What will you learn?" className="ant-form-item-required"> */}
              <Form.Item label={<span>What will you learn?&nbsp;&nbsp;&nbsp;(*optional)</span>}>
                <ReactQuill
                  theme='snow'
                  value={whatLearn && whatLearn.trim() !== 'null' ? whatLearn : ''}
                  modules={quillModules}
                  formats={quillFormats}
                  onChange={handleWhatLearnChange}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24} style={{ padding: 0 }}>
              <Form.Item label={<span>Who should attend?&nbsp;&nbsp;&nbsp;(*optional)</span>}>
                <ReactQuill
                  theme='snow'
                  value={attend && attend.trim() !== 'null' ? attend : ''}
                  modules={quillModules}
                  formats={quillFormats}
                  onChange={handleWhatAttend}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <div className='an-14 flex-x space-between date-field'>
              <Form.Item label="Dates and Prices"></Form.Item>
              {showDate && (
                <div>
                  <Button
                    shape='round'
                    icon='plus'
                    className='award-add-btn mb-24'
                    onClick={addSlots}
                  >
                    Add
                  </Button>
                </div>
              )}
            </div>
            <Col
              xs={24}
              sm={24}
              md={24}
              lg={24}
              xl={24}
              className="step2_date_and_price"
              style={{ padding: '0', marginBottom: '20px' }}
            >
              <Form.Item>
                {getFieldDecorator('dateType')(
                  <Radio.Group className='ml10' onChange={onDateTypeChanged}>
                    <Radio value={1} className='label'>
                      Fixed Date
                    </Radio>
                    <Radio value={2} className='label'>
                      Flexible Date{' '}
                      <Tooltip title={content}>
                        <Icon type="question-circle" theme="filled" />
                      </Tooltip>
                    </Radio>
                  </Radio.Group>,
                )}
              </Form.Item>
            </Col>
            {showDate && (
              <Col
                xs={24}
                sm={24}
                md={24}
                lg={24}
                xl={24}
                style={{ padding: '0' }}
              >
                {datesItems}
              </Col>
            )}
          </Row>
          <Row gutter={24}>
            <Col
              className='Country_trip'
              xs={24}
              md={12}
              lg={12}
              xl={12}
              style={{ paddingLeft: 0 }}
            >
              <Form.Item label='Country'>
                {getFieldDecorator('country', {
                  rules: [{ required: true, message: 'Please Select country' }],
                })(
                  <Select showSearch placeholder='Select trip Country'>
                    {countries.map((con, i) => (
                      <Option key={i} value={con.name}>
                        {con.name}
                      </Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
            </Col>
            <Col
              className='Location_trip'
              xs={24}
              md={12}
              lg={12}
              xl={12}
              style={{ paddingRight: 0, paddingLeft: 0 }}
            >
              <Form.Item
                label='City, Location'
                className='ant-form-item-required'
              >
                <PlacesAutocomplete
                  value={address1}
                  onChange={onChange}
                  onSelect={onSelect}
                >
                  {({
                    getInputProps,
                    suggestions,
                    getSuggestionItemProps,
                    loading,
                  }) => (
                    <div>
                      <Input
                        {...getInputProps({
                          placeholder: 'Search Places ...',
                          className: 'location-search-input',
                        })}
                      />
                      <div className='autocomplete-dropdown-container'>
                        {loading && (
                          <div style={{ marginTop: 20 }}>Loading...</div>
                        )}
                        {suggestions.map((suggestion, i) => {
                          const className = suggestion.active
                            ? 'suggestion-item--active'
                            : 'suggestion-item';
                          return (
                            <div key={i}
                              {...getSuggestionItemProps(suggestion, {
                                className,
                              })}
                            >
                              <span>{suggestion.description}</span>
                              <br />
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  )}
                </PlacesAutocomplete>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col
              className='Meeting_point_trip'
              xs={24}
              sm={24}
              md={24}
              lg={24}
              xl={24}
              style={{ padding: 0 }}
            >
              <Form.Item label='Meeting Point'>
                {getFieldDecorator('meetingPoint')(
                  <Input placeholder='Enter Meeting Point' />,
                )}
              </Form.Item>
            </Col>
          </Row>
        </div>
        <Form.Item className='mb0 pt40 btn-wrap'>
          <Button type='primary' htmlType='submit' className='ex__primary_btn'>
            Save & Next
          </Button>
          <Button
            type='primary'
            className='ex_grey_btn ml40'
            onClick={() => dispatch(changeTab(1))}
          >
            {' '}
            Back
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
});

const WrappedCreateStep2 = Form.create({ name: 'createTrips' })(Step2);

export default WrappedCreateStep2;
