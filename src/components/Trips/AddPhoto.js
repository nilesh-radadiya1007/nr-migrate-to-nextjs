import React, { useEffect, useState } from 'react';

import { Modal, Row, Col } from "antd";
import { useSelector } from "react-redux";
import Link from "next/link";
import { ArrowLeftOutlined } from '@ant-design/icons';
import { uploadPhotosAndVideo } from '../../services/expert';
import { CaptalizeFirst } from '../../helpers/methods';

import ReactPlayer from 'react-player';


const photoIcon = "/images/tick_round_ic.png"
const Close = "/images/close_ic.png"

const AddPhoto = (props) => {

  const { id, onCloseClick, visible, currentImages, type, getRefreshTrip } = props;

  const token = useSelector((state) => state.auth.accessToken);

  const [currentModal, setCurrentModal] = useState("albumList");
  const [albumImages, setAlbumImages] = useState([]);
  const [albumName, setAlbumName] = useState();
  const [gridSize, setgridSize] = useState(6);
  const [modalSize, setModalSize] = useState(640);


  const [selectedImages, setSelectedImages] = useState(typeof currentImages !== "undefined" ? currentImages : []);

  const openAlbum = (img, name) => {

    setModalStyle(img)
    setAlbumName(name)
    setAlbumImages(img)
    setCurrentModal('albumImages');
  }

  const onImageSelect = (img, isSelected) => {
    let sImg = [...selectedImages, img];

    if (isSelected) {
      sImg = sImg.filter(item => item !== img)
    }

    setSelectedImages(sImg);

  }

  const handleAddPhoto = async () => {

    const formData = new FormData();
    formData.append('id', id);
    formData.append("type", type);
    formData.append("images", JSON.stringify(selectedImages));

    const result = await uploadPhotosAndVideo(token, formData, id);
    if (result) {
      getRefreshTrip();
      setSelectedImages([]);
      onCloseClick();
    }

  }

  useEffect(() => {

    if (currentModal === "albumList") {
      setModalStyle(props.albumList)
    }
  }, [currentModal])

  const setModalStyle = (images) => {
    if (images.length === 1 || images.length === 2) {
      setgridSize(9)
      setModalSize(490)
    } else if (images.length === 3) {
      setgridSize(8)
      setModalSize(525)
    } else {
      setgridSize(6)
      setModalSize(670)
    }
  }

  return (
    <Modal
      centered
      className="auth-modal add_photo_images"
      width={modalSize}
      closable={false}
      maskClosable={false}
      onCancel={onCloseClick}
      visible={visible}>
      <div className="head_title pb10">

        <Row>
          <Col className="header_text_photo" span={22}>
            <h5 className="an-20 medium-text">{currentModal === "albumList" ? `Add photos and videos for your ${type === "trip" ? "trip" : "workshop"}` : `Add photos and videos for your ${type === "trip" ? "trip" : "workshop"}`}</h5>
            <p>{currentModal === "albumList" ? `Select an album to upload photos and videos` : "Please select photos and press add photo button"}</p>
          </Col>
          <Col span={2}>
            <div className="text-right">
              <img src={Close} alt="Delete" style={{ cursor: "pointer" }} onClick={props.onCloseClick} />
            </div>
          </Col>
        </Row>
      </div>

      <Row gutter={20} className="add_photo_section">

        {currentModal === "albumList" &&
          <>
            {props.albumList.length ? props.albumList.map((album, index) => {

              return (
                <Col className="add_photo_image" span={gridSize} key={index}>
                  {album.images[0].search(".mp4") !== -1 ?
                    (<ReactPlayer className="video" url={album.images[0]} onClick={() => openAlbum(album.images, album.name)} />)
                    : (<img alt="" src={album.images[0]} onClick={() => openAlbum(album.images, album.name)} />)
                  }
                  <div className="photos_video_album_name">
                    <h5 className='an-14 medium-text'>
                      {CaptalizeFirst(album.name)}
                    </h5>
                  </div>

                </Col>
              )
            }) :
              <div className="mb10 ml10" style={{ marginTop: "-15px" }}><p>No album or photo exist in your account, please go to gallary to upload photos to your account</p></div>
            }

          </>
        }

        {currentModal === "albumImages" &&
          <>
            <Row className="bold-txt">
              <Col span={12} className="pl10">
                <p >{CaptalizeFirst(albumName)}</p>
              </Col>
              <Col span={12}>
                <p onClick={() => setCurrentModal('albumList')} className="pb15 pull-right" ><ArrowLeftOutlined /> &nbsp;Back to albums</p>
              </Col>
            </Row>
            {albumImages.length > 0 && albumImages.map((album, index) => {


              let makeSelected = false;
              if (selectedImages.includes(album)) {
                makeSelected = true;
              }

              return (
                <Col className="add_photo_image" span={gridSize} key={index}>
                  {album.search(".mp4") !== -1 ?

                    (<ReactPlayer className="video" url={album} onClick={() => onImageSelect(album, makeSelected)} />)

                    : (<img alt="" src={album} onClick={() => onImageSelect(album, makeSelected)} />)

                  }


                  {makeSelected &&
                    (<div className="add_icone_div">
                      <img alt="" className="add_icon_photo_add" src={photoIcon} onClick={() => onImageSelect(album, makeSelected)} />
                    </div>)}
                </Col>
              )
            })}

          </>
        }

        <Col span={24} className="button_add_photo_images">
          <div className="button_trip_images" style={{ float: "right" }}>
            {/* <span className="yellow_hover_fix yellow_btn mr10 cursor-pointer ">
              <Link href={`/profile-expeditions?4&type=trip&id=${id}`} className="ex__primary_btn br5">
                Upload New Photo
              </Link>
            </span> */}
            {props.albumList.length === 0 ?
              (
                <>
                  <span className="add_photo_div yellow_hover_fix yellow_btn cursor-pointer">
                    <Link href={`/profile-expeditions?4`}>
                      <a>{currentModal === "albumList" ? "Gallery" : "Gallery"}</a>
                    </Link>

                  </span>
                  {/* <span className="add_photo_div yellow_hover_fix yellow_btn mr10 cursor-pointer upload-photo-btn">
                    <Link href={`/profile-expeditions?4`}>
                      {currentModal === "albumList" ? "Upload Photos/Videos" : "Upload Photos/Videos"}
                    </Link>
                  </span> */}

                  {/* <span className="add_photo_div yellow_hover_fix yellow_btn mr10 cursor-pointer" style={{ marginRight: "15px" }}>
                    {currentModal === "albumList" ? "Create Album" : "Create Album"}
                  </span>
                  <span className="add_photo_div yellow_hover_fix yellow_btn mr10 cursor-pointer">
                    {currentModal === "albumList" ? "Upload Photos/Videos" : "Upload Photos/Videos"}
                  </span> */}
                </>
              ) : (
                <span className="add_photo_div yellow_hover_fix yellow_btn cursor-pointer" onClick={() => handleAddPhoto()}>
                  {currentModal === "albumList" ? "Select Photos" : "Add Photos"}
                </span>
              )
            }

          </div>
        </Col>
      </Row>

    </Modal>
  );
};

export default AddPhoto;