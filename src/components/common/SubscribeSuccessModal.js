import React from "react";
import { Modal, Row, Col, Form, Button } from "antd";
// import fbIcon from '../../../public/images/home/fb_icon.png';
// import twitterIcon from '../../../public/images/home/twitter_icon.png';
// import instaIcon from '../../../public/images/home/instagram_icon.png';
// import linkedIcon from '../../../public/images/home/linkedin_icon.png';
// import thankImage from '../../../public/images/home/thanks_image.png';
import { SocialURL } from '../../helpers/constants';
import Link from 'next/link';
// import Image from 'next/image'


const SubscribeSuccessModal = (props) => {

  return (
    <>
      <Modal
        centered
        className="news-letter-modal auth-modal thanks-modal"
        width={965}
        maskClosable={true}
        visible={props.visible}
        onCancel={() => props.onClose()}
      >
        <div className="news-letter-container">
          <Row>
            <Col xs={24} sm={24} md={13} lg={13} xl={13} className="newsletter-form">
              <p className="title">Thank you for subscribing!</p>
              <p className="sub-title mb40">You’ll be the first to hear of our latest offers, tips and tricks, stories and updates.</p>
              <div className='thanks-redirect-btn'>
                <Link href='/all-adventure' onClick={() => props.onClose()}>
                  <a onClick={() => props.onClose()}><Button type="primary" htmlType="submit" className="news-letter-submit start-expor mb20" >Start Exploring</Button></a>
                </Link>
                {/* <Link href='/home'> */}
                <Button type="primary" htmlType="submit" className="news-letter-submit back-to-home" onClick={() => props.onClose()} >Back to Home</Button>
                {/* </Link> */}
              </div>

            </Col>
            <Col xs={24} sm={24} md={11} lg={11} xl={11} className="news-thank-img">
              <img src={'/images/home/thanks_image.png'} className="newsletter-thank-right-img" />
            </Col>

            <Col xs={24} sm={24} md={24} lg={24} xl={24} className="thanks-follow">
              <div className="social-share-section mt15">
                <div className="follow-us-text">Follow US</div>
                <img className="share-icon" src={'/images/home/fb_icon.png'} onClick={() => window.open(SocialURL.facebook, "_blank")} />
                <img className="share-icon" src={'/images/home/twitter_icon.png'} onClick={() => window.open(SocialURL.twitter, "_blank")} />
                <img className="share-icon" src={'/images/home/instagram_icon.png'} onClick={() => window.open(SocialURL.instagram, "_blank")} />
                <img className="share-icon" src={'/images/home/linkedin_icon.png'} onClick={() => window.open(SocialURL.linkedIn, "_blank")} />
              </div>
            </Col>
          </Row>
        </div>
      </Modal>
    </>
  );
};

const WrappedNormalRegisterForm = Form.create({ name: "RegisterForms" })(SubscribeSuccessModal);

export default WrappedNormalRegisterForm;

