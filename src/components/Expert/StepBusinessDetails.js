/* eslint-disable react/display-name */
import React, { useEffect, useState } from "react";
import { Form, Input, Row, Col, Select, Button } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import { useRouter } from "next/router";

import { ExpertEvents } from "../../redux/expert/events";
import {
  CompanyValidator,
  WebsiteValidator,
  PhoneValidator,
} from "../../helpers/validations";
import { RemoveHttpCom, setDefaultValueForHttp } from "../../helpers/methods";

const { Option } = Select;

const CreateExpertStepBusinessDetails = React.memo((props) => {
  const router = useRouter();
  // console.log(router);
  const { pathname } = router;

  const dispatch = useDispatch();
  const { companyname, website, phone, address, businessEmail } = useSelector(
    (state) => state.expert
  );
  const loader = useSelector((state) => state.expert.loader);
  const { stepBusinessDetails, changeTab, updateProfile } = ExpertEvents;
  const { getFieldDecorator, setFieldsValue } = props.form;
  const expert = useSelector((state) => state.expert);
  const [websiteHttp, setWebsiteHttp] = useState(expert.website ? setDefaultValueForHttp(expert.website) : "https://");

  useEffect(() => {
    setFieldsValue({
      companyname,
      website: RemoveHttpCom(website),
      phone,
      address,
      businessEmail,
    });
  }, [companyname, address, phone, setFieldsValue, website, businessEmail]);
  const selectBefore = (
    <Select
      onChange={(evt) => setWebsiteHttp(evt)}
      defaultValue={websiteHttp}
      className="select-before slect-before"
    >
      <Option value="http://">http://</Option>
      <Option value="https://">https://</Option>
    </Select>
  );
  const handleFormSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        //Check if direct update and remove _id from arrays of info
        if (
          expert.professionalQualifications &&
          expert.professionalQualifications.length > 0
        ) {
          let refinedQualification = expert.professionalQualifications.map(
            (qualifucation, index) => {
              if (qualifucation._id) delete qualifucation._id;
              return qualifucation;
            }
          );
          expert.professionalQualifications = refinedQualification;
        }
        if (
          expert.booksAndPublications &&
          expert.booksAndPublications.length > 0
        ) {
          let refinedPublication = expert.booksAndPublications.map(
            (publication, index) => {
              if (publication._id) delete publication._id;
              if (publication.cover) delete publication.cover;
              return publication;
            }
          );
          expert.booksAndPublications = refinedPublication;
        }
        if (expert.awards && expert.awards.length > 0) {
          let refinedAward = expert.awards.map((award, index) => {
            if (award._id) delete award._id;
            return award;
          });
          expert.awards = refinedAward;
        }
        if (
          expert.professionalExperience &&
          expert.professionalExperience.length > 0
        ) {
          let refinedexp = expert.professionalExperience.map(
            (experiance, index) => {
              if (experiance._id) delete experiance._id;
              return experiance;
            }
          );
          expert.professionalExperience = refinedexp;
        }
        if (
          expert.socialContributions &&
          expert.socialContributions.length > 0
        ) {
          let refinedSocial = expert.socialContributions.map(
            (social, index) => {
              if (social._id) delete social._id;
              return social;
            }
          );
          expert.socialContributions = refinedSocial;
        }

        const formDataObject = {
          picture: expert.picture, // 1
          originalPicture: expert.originalPicture,
          firstName: expert.firstName, // 1
          lastName: expert.lastName, // 1
          country: expert.country, // 1
          city: expert.city, // 1
          coordinates: JSON.stringify(expert.coordinates),
          experties: JSON.stringify(expert.experties), //1
          speaks: JSON.stringify(expert.speaks), // 1
          skills: JSON.stringify(expert.skills), // 1
          bio: expert.bio, // 2
          awards: JSON.stringify(expert.awards), // 2
          professionalExperience: JSON.stringify(expert.professionalExperience), //2
          professionalQualifications: JSON.stringify(
            expert.professionalQualifications
          ), //2
          socialContributions: JSON.stringify(expert.socialContributions), //2
          booksAndPublications: JSON.stringify(expert.booksAndPublications), //2
          companyname: values.companyname, //3
          website: websiteHttp + values.website, //3
          phone: values.phone, //3
          address: values.address, //3
          businessEmail: values.businessEmail, //3
        };
        let formData = new FormData();
        for (const property in formDataObject) {
          if (property === "picture") {
            if (typeof formDataObject[property] !== "string") {
              formData.append(
                property,
                formDataObject[property]
              );
            }
            else {
              formData.append(property, '')
            }
          }
          else if (property === "originalPicture") {
            if (typeof formDataObject[property] !== "string") {
              formData.append(
                property,
                formDataObject[property]
              );
            }
            else {
              formData.append(property, '')
            }
          }
          else {
            formData.append(property, formDataObject[property]);
          }
        }
        if (pathname === "/update-expert-profile") {
          dispatch(updateProfile(formData));
        } else {
          dispatch(stepBusinessDetails(formData));
        }
      }
    });
  };
  return (
    <div className="step-1-expert-form height_sec profile-stp3-info">
      <div className="an-20 medium-text success--text step-title">
        Create Expert Profile{" "}
        <span className="an-16">  Business Details </span>
      </div>
      <div className="an-16 regular-text pt10">
        Please fill in the details below to create your expert profile
      </div>
      <Form className="ant-advanced-search-form" onSubmit={handleFormSubmit}>
        <div className="form-profile-container pt20">
          <Row gutter={24}>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Form.Item label="Company Name">
                {getFieldDecorator(
                  "companyname",
                  CompanyValidator
                )(<Input placeholder="Enter Company Name" />)}
              </Form.Item>
            </Col>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Form.Item label="Company Address">
                {getFieldDecorator("address", {
                  rules: [
                    {
                      required: true,
                      message: "Company Address is required field",
                    },
                  ],
                })(<Input placeholder="Company Address" />)}
              </Form.Item>
            </Col>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Form.Item label="Website">
                {getFieldDecorator(
                  "website",
                  WebsiteValidator
                )(<Input addonBefore={selectBefore} placeholder="website" />)}
              </Form.Item>
            </Col>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Form.Item label="Phone Number">
                {getFieldDecorator(
                  "phone",
                  PhoneValidator
                )(<Input placeholder="Enter Phone Number" type="number" />)}
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <Form.Item label="Business Email">
                {getFieldDecorator("businessEmail", {
                  rules: [
                    { required: true, message: "Email is required field" },
                  ],
                })(<Input placeholder="Email" />)}
              </Form.Item>
            </Col>
          </Row>
        </div>
        <Form.Item className="mb0 pt40">
          <Button
            type="primary"
            className="ex_grey_btn"
            onClick={() => [
              window.scrollTo({
                top: document.getElementById("scroll-to-here"),
                behavior: "smooth",
              }),
              dispatch(changeTab(2)),
            ]}
          >
            Back
          </Button>
          <Button
            loading={loader}
            type="primary"
            htmlType="submit"
            className="ex__primary_btn ml40"
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
});

const WrappedCreateExpertStepBusinessDetails = Form.create({
  name: "createProfile",
})(CreateExpertStepBusinessDetails);

export default compose(WrappedCreateExpertStepBusinessDetails);
