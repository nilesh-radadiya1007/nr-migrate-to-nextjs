import React from 'react';
import { Icon } from 'antd';
import { getExtension, getFilename } from '../../helpers/methods';

const File = ({ media }) => {
  if (media.length === 0) {
    return '';
  }
  return (
    <React.Fragment>
      {media.map((m, i) => {
        if (!m.url) return '';
        const extension = getExtension(m.url).replace('.', '').trim().toLowerCase();
        const src =
          extension === 'jpg' ||
          extension === 'jpeg' ||
          extension === 'gif' ||
          extension === 'png' ? (
              <a target="_blank" rel="noreferrer noopener" href={m.url}>
                <span style={{ display: 'block' }}>{m.name}</span>
              <img width='250' src={m.url} alt='#' />
              </a>
          ) : (
              <a href={m.url} target="_blank" rel="noreferrer noopener">{m.name}</a>
          );
        return <div key={`media-${i}`}>{src}</div>;
      })}
    </React.Fragment>
  );
};

export default File;
