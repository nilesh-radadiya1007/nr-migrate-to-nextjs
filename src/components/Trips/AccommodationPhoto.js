/* eslint-disable react/display-name */
import React, { useEffect, useState } from 'react';
import { useSelector } from "react-redux";
import { Button, Form, Input, Modal, Upload, Spin } from 'antd';
import { getBase64 } from '../../helpers/methods';
import { accommoUpdate } from '../../services/expert';

const UploadImage = '/images/cover_upload.png';


const uploadButton = (
  <div>
    <img
      src={UploadImage}
      alt='cover'
      style={{
        width: '100%',
        height: '100%',
        objectFit: 'fill',
      }}
    />
  </div>
);

const { TextArea } = Input;

const AccommodationPhoto1 = React.memo((props) => {

  const token = useSelector((state) => state.auth.accessToken);
  const { loading, visible, selectedAlbum, isMultiple, id, onCloseClick, getRefreshTrip, type } = props;
  const { getFieldDecorator, setFieldsValue } = props.form;

  const [imageUrl, setImageUrl] = useState('');
  const [loding, setLoding] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [fileList, setFileList] = useState([]);

  const onCreateAlbum = () => {


  }

  useEffect(() => {
    setFieldsValue({
      fileList
    });
    if (isEdit === false) {
      if (Object.keys(selectedAlbum).length > 0) {
        setImageUrl(selectedAlbum[0]);
        let finalData = [];
        selectedAlbum.map((img, index) => {
          finalData.push({
            uid: index,
            name: img.split("photos/")[1],
            status: 'done',
            url: img,
          })
          if (selectedAlbum.length === index + 1) {
            setFileList(finalData)
          }
        })
        setIsEdit(true);
      }
    }
  }, [setFieldsValue, fileList, selectedAlbum]);

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoding(true);
    props.form.validateFields(async (err, values) => {
      if (!err) {
        const formData = new FormData();
        if (Object.keys(selectedAlbum).length > 0) {
          values.fileList.map((pic) => {
            formData.append("pictures", pic.originFileObj);
          });

          let fieldData = fileList.filter((dd) => {
            return dd.response !== "ok";
          });

          formData.append("photoList", JSON.stringify(fieldData.map(dd => { return `photos/${dd.name}` })));
        } else {
          values.fileList.map((pic) => {
            formData.append("pictures", pic.originFileObj);
          });
        }
        formData.append("type", type);


        formData.append('id', id);
        const result = await accommoUpdate(token, formData, id);
        if (result) {
          getRefreshTrip();
          setLoding(false);
          onCloseClick();
          setImageUrl('');
          setFileList([]);
          setFieldsValue({
            fileList: [],
          });

        }

      } else {
        setLoding(false);
      }
    });
  };

  const removeByAttr = function (arr, attr, value) {
    var i = arr.length;
    while (i--) {
      if (arr[i]
        && arr[i].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[i][attr] === value)) {

        arr.splice(i, 1);

      }
    }
    setFileList([...arr]);
    if (arr.length > 0) {
      if (arr[0].originFileObj) {
        if (arr[0].type.search("video") != -1) {

        } else {

          getBase64(arr[0].originFileObj, (imageUrl) => {
            setImageUrl(imageUrl);
          });
        }
      } else {
        setImageUrl(arr[0].url)
      }
    } else {
      setImageUrl('');
    }
    return arr;
  }


  const onChangeImg = ({ fileList: newFileList }) => {
    if (newFileList[0].originFileObj) {
      if (isMultiple === false || selectedAlbum && selectedAlbum.length === 1) {
        if (newFileList[0].type.search("video") !== -1) {

        }
      }
      if (newFileList[0].type.search("video") != -1) {

      } else {

        getBase64(newFileList[0].originFileObj, (imageUrl) => {
          setImageUrl(imageUrl);
        });
      }
    }
    setFileList(newFileList);
  };

  const onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  return (
    <Modal
      visible={visible}
      title={"Upload Accommmodation Photos"}
      onCancel={onCloseClick}
      footer={false}
      className="header_text section-set accommo_modal" 
    >
      <div className={`step-1-expert-form height_sec_enthu learn_sec ${fileList.length === 0 && "img-set-icon"}`}>
        <Form onSubmit={handleSubmit}>
          <div className='form-profile-container pt20'>

            <Form.Item label='' className='mb20'>
              {getFieldDecorator('fileList', {
                rules: [{ required: false, message: 'Please Upload Images!' }],
              })(
                <>
                  {fileList.length > 0 &&
                    <div className='flex-y center'>
                      <img
                        src={imageUrl}
                        alt='cover'
                        style={{
                          width: imageUrl ? '100%' : '',
                          height: imageUrl ? '100%' : '',
                          objectFit: 'fill',
                        }}
                      />
                      {!imageUrl && (
                        <span className='an-14 regular-text mt20'>
                          {isMultiple ? "Drag & Drop/Click here to upload multiple Image"
                            : "Drag & Drop/Click here to add photos/videos in an Album"}
                        </span>
                      )}
                    </div>
                  }
                  <Upload
                    accept={'image/*,video/*'}
                    listType={'picture-card'}
                    multiple={true}
                    showUploadList={{ showPreviewIcon: false, showDownloadIcon: false }}
                    fileList={fileList}
                    onChange={onChangeImg}
                    onPreview={onPreview}
                    customRequest={({ file, onSuccess }) => {
                      setTimeout(() => onSuccess('ok'), 0)
                    }}
                    className="uplord_img"
                    onRemove={(d) => removeByAttr(fileList, 'uid', d.uid)}
                  >
                    {fileList.length === 0 &&
                      <div className='flex-y center zero-img'>
                        <img
                          src={UploadImage}
                          alt='cover'
                          style={{
                            width: '100%',
                            height: '100%',
                            objectFit: 'fill',
                          }}
                        />
                        <span className='an-14 regular-text mt20'>
                          {isMultiple ? "Drag & Drop/Click here to upload multiple Image"
                            : "Drag & Drop/Click here to add photos/videos in an Album"}
                        </span>
                      </div>}
                    {fileList.length > 0 && uploadButton}
                  </Upload>
                </>
              )}
            </Form.Item>
            <Form.Item className="uplord_button text-right">
              {loding ? <Spin tip="Loading..." /> :
                <Button
                  htmlType='submit'
                  type='primary'
                  className='ant-btn ex__primary_btn text-upper ant-btn-primary'
                  // loading={loading}
                  disabled={loading ? true : false}
                >
                  Save
              </Button>
              }
            </Form.Item>
          </div>
        </Form>
      </div>
    </Modal>
  );
});

export default Form.create({ name: 'album' })(AccommodationPhoto1);
