import React, { useState } from "react";
import { Carousel, Form, Input, Button } from "antd";

import { Subscribe } from '../../services/expert';
// import Image from 'next/image';
import { useRouter } from "next/router";

const LeftArrow = "/images/newicon/left_arrow_black.svg";
const image2 = "/images/home/landingpage/landingpage4.png";
const mailIcon = "/images/home/mailIcon.svg";

const LandingPageHeader = (props) => {
    const router = useRouter()
    const { getFieldDecorator, setFields, setFieldsValue } = props.form;
    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields(async (err, values) => {
            if (!err) {
                setIsLoading(true);
                values = { ...values, subscribe: true, subscribeFrom: 'landingpage' }
                const result = await Subscribe(values);
                if (result.data.status === "SUCCESS") {
                    router.push("/all-adventure");
                } else if (result.data.status === "FAIL") {
                    setFields({
                        'email': {
                            errors: [{
                                "message": `${values.email} email is already subscribe.`,
                                "field": "email"
                            }]
                        }
                    });

                }
                setIsLoading(false);
            }
        });
    }

    return (
        <div className="landingpage-v2">
            <Carousel autoplay>
                <div className='landingpage-img-div'>
                    <div className="bg-layer">
                        <img className="" src={image2} alt="Banner-for-mobile" />
                    </div>
                </div>
            </Carousel>
            <div className="ex_landing_page">
                <p className="ex_landing_page__title">CONNECT. <span className="ex_landing_page__s-yellow">LEARN. </span> IMPACT</p>
                <div className="border-bottom-green"></div>
                <p className="ex_landing_page__subtitle">Connect with adventure professionals worldwide. Learn through experience. Help protect the planet.</p>
                <Form onSubmit={handleSubmit} autoComplete="new-password">
                    <div className="ex_landing_page__form">
                        <Form.Item className="registration-email">
                            {getFieldDecorator("email", {
                                rules: [
                                    {
                                        type: "email",
                                        message: "Please enter valid email address."
                                    },
                                    { required: true, message: "Please input your email" }
                                ]
                            })(
                                <Input
                                    type="email"
                                    placeholder="Enter your email"
                                    addonBefore={<img src={mailIcon} alt="Mail icon" />}
                                    className="email-input-box"
                                    id="email-input-box2"

                                />
                            )}
                        </Form.Item>

                        <Form.Item className="button-div">
                            <Button className="ex__primary_btn be-more-button" loading={isLoading ? true : false} onClick={handleSubmit}>
                                GET STARTED FOR FREE <span><img src={LeftArrow} className="left-arrow" alt="left arrow" /></span>
                            </Button>
                        </Form.Item>
                    </div>
                </Form>
            </div>
        </div >
    );
};
const LandingPage = Form.create({ name: "RegisterForm" })(LandingPageHeader);

export default LandingPage;
