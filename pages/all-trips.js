import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const Trips = dynamic(
    () => import("../src/containers/Trips"),
    { ssr: false }
);

const AllTrips = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Trips />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default AllTrips;