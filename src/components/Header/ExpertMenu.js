import React from 'react';
import Link from 'next/link';
import { Popover } from 'antd';
import { CaretDownOutlined } from '@ant-design/icons';
import CurrencySelectionModal from "./CurrencySelectionModal";

const disabledClass = "flex-x align-center medium-text text-upper an-16 header-nav-list another mr10 disabled-menu";
const enableClass = "flex-x align-center medium-text text-upper an-16 header-nav-list another mr10";

const ExpertMenu = (props) => {

  const { isProfileCompleted, tmpRole } = props;

  const handleClick = () => {
    props.onClose();
  };

  // const cookies = new Cookies();

  // const preferredCurrency = cookies.get('preferredCurrency') || "USD";

  // const onChangeCurrency = async (e) => {
  //   setPreferredCurrency(e);
  //   // cookies.set('preferredCurrency', e);
  //   await updatePreferredCurrency(e);
  //   window.location.reload();
  // }

  const expertContent = (
    <div className='profile_bg'>
      <div onClick={handleClick} className='primary--text py5 cursor-pointer'>
        {
          (!isProfileCompleted && tmpRole === "expert") ?
            <Link href="/create-expert-profile"><a>Home</a></Link>
            :
            (!isProfileCompleted && tmpRole === "enthusiasts") ?
              <Link href="/create-enthusiest-profile"><a>Home</a></Link>
              :
              <Link href="/"><a>Home</a></Link>
        }
      </div>
      <div onClick={handleClick} className='primary--text py5 cursor-pointer'>
        <Link href='/experts'><a>Experts</a></Link>
      </div>
      <div onClick={handleClick} className='primary--text py5 cursor-pointer'>
        <Link href='/all-adventure'><a>Adventures</a></Link>
      </div>
      <div onClick={handleClick} className='primary--text py5 cursor-pointer'>
        <Link href='/special-offers'><a>Special Offers</a></Link>
      </div>
      {/* <div onClick={handleClick} className='primary--text py5 cursor-pointer'>
        <Link href='/learning'>Learn</NavLink>
      </div> */}
    </div >
  );

  return (
    <div className='mobile_menu' style={{ display: 'flex' }}>
      <div className='flex-x align-center medium-text text-upper an-16 header-nav-list another mr20'>
        <Popover placement='topLeft' content={expertContent} trigger="click">
          <div><span style={{ color: '#000', fontFamily: 'rubik', fontSize: '18px' }}>Browse</span>
            <CaretDownOutlined />
          </div>
        </Popover>
      </div>
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <Link onClick={handleClick} href='/profile-expeditions?trip'>
          <a onClick={handleClick}><div>My Trips</div></a>
        </Link>
      </div>
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <Link onClick={handleClick} href='/profile-workshops?workshop'>
          <a onClick={handleClick}><div>My Workshops</div></a>
        </Link>
      </div>
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <Link onClick={handleClick} href='/messages'>
          <a onClick={handleClick}><div>My Messages ({props.count})</div></a>
        </Link>
      </div>
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <Link onClick={handleClick} href='/bookings'>
          <a onClick={handleClick}><div>Bookings</div></a>
        </Link>
      </div>
      <div className="flex-x align-center medium-text an-16 header-nav-list mr10 currency-selection exp">
        <CurrencySelectionModal></CurrencySelectionModal>
      </div>
    </div>
  );
};

export default ExpertMenu;
