import React, { useState, useEffect, useCallback, Fragment } from "react";
import { Form, Row, Col, Button, Breadcrumb, Input, Icon, message, Select, Tooltip } from "antd";
import { CloseCircleFilled } from '@ant-design/icons';
import { useSelector, useDispatch } from "react-redux";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

import { quillFormats, quillModules, MEALS } from "../../helpers/constants";
import { LearningEvents } from "../../redux/learning/events";

const { TextArea } = Input;
const { Option } = Select;

const Step3 = (props) => {

  const { getFieldDecorator, getFieldValue, setFieldsValue } = props.form;

  const [accomodation, setAccomodation] = useState("");
  const [inclusion, setInclusion] = useState("");
  const [exclusion, setExclusion] = useState("");
  const [extras, setExtras] = useState("");
  const [cancellantion, setCancellation] = useState("");
  const [gettingThere, setGettingThere] = useState('');
  const [transfer, setTransfer] = useState('');
  const [equipmentsGears, setEquipmentsGears] = useState('');
  const [covidGuidlines, setCovidGuidlines] = useState('');

  const loader = useSelector((state) => state.learning.loader);
  const learnings = useSelector(state => state.learning);

  const dispatch = useDispatch();
  const { step3, changeTab, update } = LearningEvents;
  const { editMode, workshopMedium } = learnings;

  const setEditors = useCallback((data) => {
    setAccomodation(data.accomodation || "");
    setInclusion(data.inclusion || "");
    setExtras(data.extras || "");
    setExclusion(data.exclusion || "");
    setCancellation(data.cancellantion || "");
    setGettingThere(data.gettingThere || '');
    setTransfer(data.transfer || '');
    setEquipmentsGears(data.equipmentsGears || '');
    setCovidGuidlines(data.covidGuidlines || '');
  }, [])

  useEffect(() => {
    setEditors(learnings);
    setFieldsValue({ itinerary: learnings.itenary ? JSON.parse(learnings.itenary) : [{}] });
    setFieldsValue({
      accomodations:
        learnings.accomodations && learnings.accomodations.length !== 0
          ? JSON.parse(learnings.accomodations)
          : [{}],
    });
  }, [learnings, setEditors, setFieldsValue])

  const addItinerary = () => {
    const keys = getFieldValue('itinerary');
    const nextKeys = keys.concat(new Date().getTime());
    setFieldsValue({ itinerary: nextKeys });
  }

  const removeItinerary = (k) => {
    const keys = getFieldValue('itinerary');
    setFieldsValue({ itinerary: keys.filter(key => key !== k) });
  }

  const addAccomodations = () => {
    const keys = getFieldValue('accomodations');
    const nextKeys = keys.concat(new Date().getTime());
    setFieldsValue({ accomodations: nextKeys });
  };

  const removeAccomodations = (k) => {
    const keys = getFieldValue('accomodations');
    setFieldsValue({ accomodations: keys.filter((key) => key !== k) });
  };

  getFieldDecorator('itinerary', { initialValue: [{}] });
  const itineraryFields = getFieldValue('itinerary');
  const itenaryItems = itineraryFields.map((key, i) => {

    let tmpItineraryVal = key.value;
    if (typeof tmpItineraryVal !== "undefined") {
      if (tmpItineraryVal === "<p><br></p>" || tmpItineraryVal === "") {
        tmpItineraryVal = "";
      }
    } else {
      tmpItineraryVal = "";
    }

    let meals = key.meals;
    if (typeof meals === 'string') {
      meals = JSON.parse(meals);
    }

    return (<Fragment key={i}>
      <div className='border_sec'>
        <div className=''>
          <Row gutter={20}>
            <Col xs={6} sm={6} md={6} lg={6} xl={6}>
              <Form.Item label={
                <span>
                  Day&nbsp;&nbsp;
                  <Tooltip title="Please enter a day either a number eg.1 or as a range 1-5 It will display as Day 1. or Day 1 - Day 5">
                    <Icon type="question-circle" theme="filled" />
                  </Tooltip>
                </span>
              }>
                <Fragment>
                  <Row gutter={4}>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>

                      {getFieldDecorator(`iti-day-${i}`, {
                        initialValue: key.dayRange ? key.dayRange : `${i + 1}`,
                        rules: [{ required: true, message: 'Enter day in format e.g 1 or 1-5', pattern: new RegExp("^([0-9]{1,2})|([-][0-9]{1,2})$"), }]
                      })(
                        <Input
                          addonBefore={'Day'}
                          placeholder={`${i + 1} - X`} />,
                      )}
                    </Col>
                  </Row>
                </Fragment>
              </Form.Item>
            </Col>
            <Col xs={18} sm={18} md={18} lg={18} xl={18}>
              <Form.Item label='Headline'>
                <Fragment>
                  <Row gutter={4}>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                      <Form.Item>
                        <Form.Item>
                          {getFieldDecorator(`iti-title-${i}`, {
                            initialValue: key.title || "",
                          })(
                            <Input placeholder='Enter Title' />,
                          )}
                        </Form.Item>
                      </Form.Item>
                    </Col>
                  </Row>
                </Fragment>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <Form.Item label='Description'>
                {getFieldDecorator(`iti-description-${i}`, {
                  initialValue: tmpItineraryVal,
                  rules: [{ required: workshopMedium !== 'online' ? true : false, message: 'Please enter description' }, { min: 12, message: "Min Description length 5 is required!" }],
                })(<ReactQuill
                  theme='snow'
                  className={"quill-min-height"}
                  modules={quillModules}
                  formats={quillFormats}
                  placeholder="Write here..."
                />)}
                {/* {getFieldDecorator(
                      `itinerary-${i}`, {
                      initialValue: key.value,
                      itineraryDescriptionValidation
                    }
                    )(<TextArea placeholder={`Day ${i + 1}`} />)} */}
              </Form.Item>
            </Col>

            <Col xs={11} sm={11} md={11} lg={11} xl={11}>
              <Form.Item label='Accommodation'>
                <Fragment>
                  <Row gutter={4}>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                      <Form.Item>
                        {getFieldDecorator(`iti-accommodation-${i}`, {
                          initialValue: key.accommodation || "",
                        })(
                          <Input placeholder='Accommodation name' />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Fragment>
              </Form.Item>
            </Col>

            <Col xs={13} sm={13} md={13} lg={13} xl={13}>
              <Form.Item label='Meals'>
                <Fragment>
                  <Row gutter={4}>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                      <Form.Item>
                        <Form.Item>
                          {getFieldDecorator(`iti-meals-${i}`, { initialValue: meals || [] })
                            (
                              <Select mode="multiple" placeholder="Select Meals" >
                                {MEALS.map((exp, i) => <Option key={i} value={exp.value}>{exp.name}</Option>)}
                              </Select>
                            )}
                        </Form.Item>
                      </Form.Item>
                    </Col>
                  </Row>
                </Fragment>
              </Form.Item>
            </Col>

            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <Form.Item label='Activity Name & Duration'>
                {getFieldDecorator(`iti-activityName-${i}`, {
                  initialValue: key.activityName || "",
                })(
                  <Input placeholder='Hiking: 25 minutes' />,
                )}
              </Form.Item>
            </Col>
          </Row>
        </div>
        <Icon
          className='delete-icon work_delete'
          type='close'
          onClick={() => removeItinerary(key)}
        />
      </div>
    </Fragment>)
  });

  getFieldDecorator('accomodations', { initialValue: [{}] });
  const accomodationsFields = getFieldValue('accomodations');
  const accommodationItems = accomodationsFields.map((key, i) => {

    let tmpAccommodationvalue = key.description;
    if (typeof tmpAccommodationvalue !== "undefined") {
      if (tmpAccommodationvalue === "<p><br></p>" || tmpAccommodationvalue === "") {
        tmpAccommodationvalue = "";
      }
    } else {
      tmpAccommodationvalue = "";
    }

    return (<Fragment key={i}>
      <div className=''>
        <div className=''>
          <Row gutter={20}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24} className="mb20">
              <Form.Item label={i === 0 ? ``:`Accommodation`}>
                {getFieldDecorator(`acco-description-${i}`, {
                  initialValue: tmpAccommodationvalue,
                  rules: [{ required: false, message: 'Please enter description' }, { min: 12, message: "Min Description length 5 is required!" }],
                })(<ReactQuill
                  theme='snow'
                  className={"quill-min-height"}
                  modules={quillModules}
                  formats={quillFormats}
                  placeholder="Write here..."
                />)}
              </Form.Item>
            </Col>
          </Row>
        </div>
        <Icon
          className='delete-icon work_delete accommodation'
          type='close'
          onClick={() => removeAccomodations(key)}
        />
      </div>
    </Fragment>)
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        if (!cancellantion || cancellantion == '<p><br></p>') {
          return message.error("Please fill out Terms & Conditions field");
        }
        const itinerary = [];
        values.itinerary.map((key, i) => {
          let itineraryObj = {
            dayRange: values[`iti-day-${i}`] || "",
            title: values[`iti-title-${i}`] || "",
            value: values[`iti-description-${i}`] || "",
            meals: JSON.stringify(typeof values[`iti-meals-${i}`] !== "undefined" ? values[`iti-meals-${i}`] : []),
            accommodation: values[`iti-accommodation-${i}`] || "",
            activityName: values[`iti-activityName-${i}`] || "",
            activityDuration: values[`iti-activityDuration-${i}`] || "",
          };
          itinerary.push(itineraryObj);
          return null;
        });

        const accomodations = [];
        values.accomodations.map((key, i) => {
          let accommoObj = {
            description: values[`acco-description-${i}`] || "",
          };
          accomodations.push(accommoObj);

          return null;
        });

        const Obj = {
          ...learnings,
          coordinates: JSON.stringify(learnings.coordinates),
          accomodation: accomodation,
          itenary: JSON.stringify(itinerary),
          inclusion: inclusion,
          exclusion: exclusion,
          extras: extras,
          cancellantion: cancellantion,
          active: true,
          gettingThere: gettingThere,
          transfer: transfer,
          equipmentsGears: equipmentsGears,
          covidGuidlines: covidGuidlines,
          accomodations: JSON.stringify(accomodations)
        };
        let formData = new FormData();
        for (const property in Obj) {
          if (property === 'cover') {
            if (typeof Obj[property] !== 'string') {
              formData.append(property, Obj[property]);
            }
            else {
              formData.append(property, '');
            }
          }
          else if (property === 'originalCover') {
            if (typeof Obj[property] !== 'string') {
              formData.append(
                property,
                Obj[property]
              );
            } else {
              formData.append(property, '');
            }
          } else {
            formData.append(property, Obj[property]);
          }
        }
        if (editMode) {
          dispatch(update(formData))
        } else {
          dispatch(step3(formData));
        }
      }
    });
  };

  return (
    <div className="step-1-expert-form step-2 learn_sec">
      <div className="an-20 medium-text step-title">
        <Breadcrumb separator=">">
          <Breadcrumb.Item className="an-18 medium-text clr_green success--text">
            Create Workshop
          </Breadcrumb.Item>
          <Breadcrumb.Item className="an-16 regular-text pt10">Additional Details</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <Form className="ant-advanced-search-form creacte_learning_section_step_3" onSubmit={handleSubmit}>
        <div className="form-profile-container pt20">
          {workshopMedium !== 'online' && (
            <Fragment>
              <Row gutter={24}>
                <Col className='trip_itinerary' xs={24} sm={24} md={24} lg={24} xl={24}>
                  <div className="step3_itinerary">
                    <div className='pt15 pb5 flex-x space-between pt24'>
                      <div className='medium-text step-title'>
                        <label className="itinerary_txt_trip">Accommodation</label>
                      </div>
                      <div>
                        <Button
                          shape='round'
                          icon='plus'
                          className='award-add-btn mb-24'
                          onClick={addAccomodations}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                    {accommodationItems}
                  </div>
                </Col>
              </Row>

              <Row gutter={24}>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <div className="step3_itinerary">
                    <div className="pt15 pb5 an-14 flex-x space-between pt24">
                      <div className="an-14 step-title" style={{ color: "#0f0f0f" }}>
                        <label className="itinerary_txt">Itinerary</label>
                      </div>
                      <div>
                        <Button
                          shape="round"
                          icon="plus"
                          className="award-add-btn mb-24"
                          onClick={addItinerary}>
                          Add
                    </Button>
                      </div>
                    </div>
                    {itenaryItems}
                  </div>
                </Col>
              </Row>

              <Row gutter={24}>
                <Col className='' xs={24} sm={24} md={24} lg={24} xl={24}>
                  <Form.Item label='Logistics & Transfers'>
                    <div className="logistic-transfer">
                      <Form.Item label='Getting There'>
                        <ReactQuill
                          theme='snow'
                          value={gettingThere || ''}
                          modules={quillModules}
                          formats={quillFormats}
                          onChange={(data) => setGettingThere(data)}
                          placeholder='Write here...'
                        />
                      </Form.Item>

                      <Form.Item label='Transfers'>
                        <ReactQuill
                          theme='snow'
                          value={transfer || ''}
                          modules={quillModules}
                          formats={quillFormats}
                          onChange={(data) => setTransfer(data)}
                          placeholder='Write here...'
                        />
                      </Form.Item>
                    </div>
                  </Form.Item>
                </Col>
              </Row>
            </Fragment>

          )}
          <Row gutter={24}>
            <Col className='' xs={24} sm={24} md={24} lg={24} xl={24}>
              <Form.Item label='Inclusions &  Exclusion'>
                <div className="logistic-transfer">
                  <Form.Item label="Inclusions">
                    <ReactQuill
                      theme="snow"
                      value={inclusion}
                      modules={quillModules}
                      formats={quillFormats}
                      onChange={(data) => setInclusion(data)}
                    />
                  </Form.Item>

                  <Form.Item label="Exclusions">
                    <ReactQuill
                      theme="snow"
                      value={exclusion}
                      modules={quillModules}
                      formats={quillFormats}
                      onChange={(data) => setExclusion(data)}
                    />
                  </Form.Item>
                </div>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={24}>
            <Col className='' xs={24} sm={24} md={24} lg={24} xl={24}>
              <Form.Item label='Equipments and Gears'>
                <ReactQuill
                  theme='snow'
                  value={equipmentsGears || ''}
                  modules={quillModules}
                  formats={quillFormats}
                  onChange={(data) => setEquipmentsGears(data)}
                  placeholder='Write here...'
                />
              </Form.Item>
            </Col>
          </Row>


          <Row gutter={24}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <Form.Item label="Additional Details">
                <ReactQuill
                  theme="snow"
                  value={extras}
                  modules={quillModules}
                  formats={quillFormats}
                  onChange={(data) => setExtras(data)}
                />
              </Form.Item>
            </Col>
          </Row>


          {workshopMedium !== 'online' && (
            <Row gutter={24}>
              <Col className='' xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label='COVID Guidlines'>
                  <ReactQuill
                    theme='snow'
                    value={covidGuidlines || ''}
                    modules={quillModules}
                    formats={quillFormats}
                    onChange={(data) => setCovidGuidlines(data)}
                    placeholder='Write here...'
                  />
                </Form.Item>
              </Col>
            </Row>
          )}

          <Row gutter={24}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <Form.Item label="Terms & Conditions" className="ant-form-item-required">
                <ReactQuill
                  theme="snow"
                  value={cancellantion}
                  modules={quillModules}
                  formats={quillFormats}
                  onChange={(data) => setCancellation(data)}
                />
              </Form.Item>
            </Col>
          </Row>

        </div>
        <Form.Item className="mb0 pt40">
          <Button
            loading={loader}
            type="primary"
            htmlType="submit"
            className="ex__primary_btn"
          >
            Submit
          </Button>
          <Button
            type="primary"
            className="ex_grey_btn ml40"
            onClick={() => dispatch(changeTab(2))}
          > Back
          </Button>
        </Form.Item>
      </Form>
    </div >
  );
};

const WrappedCreateStep3 = Form.create({ name: "createTrips" })(Step3);

export default WrappedCreateStep3;
