import React, { useEffect, useState, Fragment } from "react";
import Link from 'next/link'
import { useRouter } from 'next/router';
import { useSelector, useDispatch } from "react-redux";
import Cookies from "universal-cookie";
import { compose } from "redux";
import { Button, Popover, Avatar, Drawer, Select } from "antd";

/**
 * App Imports
 */
import Auth from "../Auth/AuthModal";
import ChooseProfile from "../Auth/ChooseProfileModal";
import PendingApproval from "./PendingApproval";
import { ModalActions } from "../../redux/models/events";
import { AuthActions } from "../../redux/auth/events";
import { ExpertEvents } from "../../redux/expert/events";
import { EnthuEvents } from "../../redux/enthu/events";
import { useMediaQuery } from "react-responsive";
import ActiveLink from '../ActiveLink';
import ExpertMenu from "./ExpertMenu";
import EnthusiastMenu from "./EnthusiastMenu";
import {
  getMessageUreadCount,
  updatePreferredCurrency,
  updateUserRole,
} from "../../services/auth";
import currencies from "../../helpers/currencies";
import { setPreferredCurrency } from "../../helpers/methods";
import CurrencySelection from "./CurrencySelectionModal";
import NewsletterModal from "../common/NewsletterModal";
import SubscribeSuccessModal from "../common/SubscribeSuccessModal";
import PasswordForm from "../Auth/PasswordForm";
// import PasswordForm from "../../Auth/PasswordForm";
import publicIp from 'public-ip';
import iplocate from "iplocation"
// const iplocate = require("node-iplocate");

const headerLogo = "/images/headerLogo.png";
const playOutlineIcon = "/images/playOutlineIcon.svg";
const hamburgerMenuIcon = "/images/hamburgerMenuIcon.png";


const { Option } = Select;
const disabledClass = "primary--text py5 cursor-pointer disabled-menu";
const enableClass = "primary--text py5 cursor-pointer";

function Header(props) {
  const history = useRouter();
  const dispatch = useDispatch();
  const showAuth = useSelector((state) => state?.modal?.authModal);
  const showChooseProfile = useSelector((state) => state?.modal?.chooseProfile);
  const showApprovalProfile = useSelector((state) => state?.modal?.approvalModal);
  const showApprovalProfileViewType = useSelector(
    (state) => state?.modal?.viewType
  );
  const {
    isLogin,
    role,
    accessToken,
    preferredCurrency = "USD",
    isCompleted,
    isProfileCompleted,
    tmpRole,
  } = useSelector((state) => state?.auth);
  const all = useSelector((state) => state?.auth);
  const { picture, approved } = useSelector((state) => state?.expert);

  const enthu = useSelector((state) => state?.enthu);

  const [successPopup, setSuccessPopup] = useState(false);
  const [isSubscribeSuccess, setIsSubscribeSuccess] = useState(false);
  // console.log(history);
  const { isLoginShow, isNewsLetterShow, isThankYouShow } = history.query;

  const [count, setCount] = useState(0);

  const { changeTmpRole } = AuthActions;
  const {
    openAuthModal,
    closeAuthModal,
    closeChooseProfileModal,
    openApprovalModal,
    closeApprovalModal,
    openChooseProfileModal,
  } = ModalActions;
  const { getProfile, logout, setPreferedCurrency } = AuthActions;
  const { nullExpert } = ExpertEvents;
  const { nullEnthu } = EnthuEvents;
  const isMaxWidth992 = useMediaQuery({ query: "(max-width: 992px)" });

  const cookies = new Cookies();

  useEffect(() => {
    if (isLogin) {
      if (isCompleted || isProfileCompleted) {
        localStorage.removeItem("userType");
      } else {
        if (tmpRole === "user" && tmpRole !== "") {
          dispatch(openChooseProfileModal());
        } else {
          if (tmpRole === "expert") {
            // return history.push("/create-expert-profile");
          } else if (tmpRole === "enthusiasts") {
            // return history.push("/create-enthusiest-profile");
          }
        }
      }

      if (role !== null && role !== "user") {
        dispatch(getProfile());
        if (accessToken) {
          const interval = setInterval(() => {
            getMessageUreadCount(accessToken, role)
              .then((resp) => {
                if (resp.status === 200) setCount(resp.data.data);
              })
              .catch((err) => console.log(err));
          }, 2000);
          return () => clearInterval(interval);
        }
      }
    }
  }, [dispatch, getProfile, isLogin, role, isCompleted]);

  useEffect(() => {
    //Open the newsletter popup after 20 seconds
    if (typeof cookies.get('newsLetter') !== "undefined") {

    } else {
      cookies.set('newsLetter', { "key": "value" }, { path: '/', expires: new Date(new Date().getTime() + 1000 * 60 * 30) });
      // cookies.set('newsLetter', { "key": "value" }, { path: '/', expires: new Date(new Date().getTime() + 1000 * 10) });
      if (!showAuth && !isLogin) {
        setTimeout(() => {
          if (typeof cookies.get('newsLetterEmail') === "undefined") {
            setSuccessPopup(true);
          }
        }, 25000);
      }

    }

  }, [props]);

  const onProfileChoose = (type) => {
    dispatch(closeChooseProfileModal());
    dispatch(closeAuthModal());
    if (type === 0) {
      dispatch(changeTmpRole("expert"));
      updateUserRole(accessToken, "expert");
      return history.push("/create-expert-profile");
    } else {
      dispatch(changeTmpRole('enthusiasts'));
      updateUserRole(accessToken, "enthusiasts");
      return history.push("/create-enthusiest-profile");
    }
  };

  const checkDisabedTrip = (e) => {
    if (!approved) {
      e.preventDefault();
      dispatch(openApprovalModal("trip"));
    }
  };

  const checkDisabedWorkshop = (e) => {
    if (!approved) {
      e.preventDefault();
      dispatch(openApprovalModal("workshop"));
    }
  };

  const logOut = () => {
    setSuccessPopup(false);
    dispatch(nullEnthu());
    dispatch(nullExpert());
    dispatch(logout());
    history.push("/");
  };

  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const content = (
    <div className="profile_bg">
      <div className="primary--text py5 cursor-pointer">
        {role === "expert" ? (
          <Link href="/profile-expert?about"><a className='disabled-link'>Profile</a></Link>
        ) : (
          !isProfileCompleted && tmpRole === "expert" ?
            <Link href="/create-expert-profile"><a>Profile</a></Link>
            :
            !isProfileCompleted && tmpRole === "enthusiasts" ?
              <Link href="/create-enthusiest-profile"><a>Profile</a></Link>
              :
              <Link href="/enthusiest-profile"><a>Profile</a></Link>

        )}
      </div>
      {role === "expert" && <div className="border_bottom"></div>}
      {role === "expert" && (
        <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
          <Link onClick={checkDisabedTrip} href="/create-trips">
            <a onClick={checkDisabedTrip}>Create Trip</a>
          </Link>
        </div>
      )}
      {role === "expert" && (
        <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
          <Link  href="/create-learnings">
            <a onClick={checkDisabedWorkshop}>Create Workshop</a>
          </Link>
        </div>
      )}
      {/*role === "expert" && (
        <div className="primary--text py5 cursor-pointer">
          <ActiveLink href="/profile-expert?tab=5">Messages</ActiveLink>
        </div>
      )*/}
      <div className={`${!isProfileCompleted ? disabledClass : enableClass}`}>
        <Link
          href="/settings"

        >
          <a className={`${!isProfileCompleted ? "disabled-menu" : ""}`}>Settings</a>
        </Link>
      </div>
      <div className="border_top"></div>
      <div className="primary--text py5 cursor-pointer" onClick={logOut}>
        Logout
      </div>
    </div>
  );

  let Menu = (
    <>
      <div className="flex-x align-center medium-text  an-16 header-nav-list mr10">
        <ActiveLink href="/" activeClassName="active" onClick={onClose}>
          <a onClick={onClose}><div>Home</div></a>
        </ActiveLink>
      </div>
      <div className="flex-x align-center medium-text  an-16 header-nav-list mr10">
        <ActiveLink href="/experts" activeClassName="active" onClick={onClose}>
          <a onClick={onClose}><div>Experts</div></a>
        </ActiveLink>
      </div>
      <div className="flex-x align-center medium-text an-16 header-nav-list mr10">
        <ActiveLink href="/all-adventure" activeClassName="active" onClick={onClose}>
          <a onClick={onClose}><div>Adventures</div></a>
        </ActiveLink>
      </div>
      <div className="flex-x align-center medium-text an-16 header-nav-list mr10">
        <ActiveLink href="/special-offers" activeClassName="active" onClick={onClose}>
          <a onClick={onClose}><div>Special Offers</div></a>
        </ActiveLink>
      </div>

      <div className="flex-x align-center medium-text an-16 header-nav-list mr5 currency-selection">
        <CurrencySelection></CurrencySelection>
      </div>
      {isLogin ? (
        <div className="flex-x align-center an-16 header-nav-list mr10">
          <Popover
            placement="topRight"
            content={content}
            trigger={`${isMaxWidth992 ? "click" : "hover"}`}
          >
            <Avatar
              shape="square"
              className="cursor-pointer"
              size={32}
              src={role === "expert" ? picture : enthu.picture}
            />
          </Popover>
        </div>
      ) : (
        <div className="flex-x align-center an-14 medium-text header-nav-list cursor-pointer">
          <div className="get-started-btn-container">
            <div className="line" />
            <Button
              id="get_started"
              type="primary"
              className="ex__primary_btn text-upper get-started-btn"
              onClick={() => {
                setSuccessPopup(false);
                dispatch(openAuthModal(true));
                onClose();
              }}
            >
              Get Started
              <img src={playOutlineIcon} alt="" />
            </Button>
            <div className="line" />
          </div>
        </div>
      )}
    </>
  );

  if (isLogin) {
    Menu =
      role === "enthusiasts" ? (
        <EnthusiastMenu
          onClose={onClose}
          count={count}
          isProfileCompleted={isProfileCompleted}
          tmpRole={tmpRole}
        />
      ) : (
        <ExpertMenu
          onClose={onClose}
          count={count}
          isProfileCompleted={isProfileCompleted}
          tmpRole={tmpRole}
        />
      );
  }

  useEffect(() => {
    if (!isLogin && isLoginShow) {
      dispatch(openAuthModal(true));
    }
    if (isNewsLetterShow) {
      setSuccessPopup(true);
    }
    if (isThankYouShow) {
      setIsSubscribeSuccess(true);
    }
  }, []);

  useEffect(() => {
    if (process.env.REACT_APP_HOST_ENV === "staging") {
      const pass = localStorage.getItem("password");

      if (!pass) {
        setSuccessPopup(false);
        setIsSubscribeSuccess(false);
      } else {
        if (!isLogin) {
          cookies.set(
            "newsLetter",
            { key: "value" },
            {
              path: "/",
              expires: new Date(new Date().getTime() + 1000 * 60 * 30),
            }
          );
          setTimeout(() => {
            if (typeof cookies.get("newsLetterEmail") === "undefined") {
              setSuccessPopup(true);
            }
          }, 25000);
          if (!isLogin && isLoginShow) {
            dispatch(openAuthModal(true));
          }
          if (isNewsLetterShow) {
            setSuccessPopup(true);
          }
          if (isThankYouShow) {
            setIsSubscribeSuccess(true);
          }
        }
      }
    }
  }, []);

  const MyDrawer = (
    <Drawer
      title={<img className="mb5" width="170" src={headerLogo} alt="logo" />}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={visible}
      className="mobile-drawer header-nav-list"
    >
      {Menu}
    </Drawer>
  );

  const callFunction = () => {
    setIsSubscribeSuccess(true);
  };

  // Get country name from the public IP

  useEffect(() => {
    const getResult  = async () => {
      
      if ("geolocation" in navigator) {
        window.navigator.geolocation.getCurrentPosition(function (position) {

          localStorage.setItem('lat', position.coords.latitude);
          localStorage.setItem('lng', position.coords.longitude);
        });
      }

      if (localStorage.getItem('lat') === null) {
        let ipaddress = await publicIp.v4();
        // console.log(ipaddress);
        if (localStorage.getItem('publicIp') === null || ipaddress !== localStorage.getItem('publicIp')) {
          localStorage.setItem('publicIp', ipaddress);
          await iplocate(ipaddress).then(function (results) {
            if (localStorage.getItem('lat') === null) {
              localStorage.setItem('lat', results.latitude);
              localStorage.setItem('lng', results.longitude);
            }
          }).catch((e) => console.log('Navigator not available'));
        }
      }
    }

    getResult();

  }, [])

  return (
    <Fragment>
      {process.env.REACT_APP_HOST_ENV === "staging" && <PasswordForm />}
      <div className="header-container1">
        <div className="container-fluid main-header-container flex-x align-center">
          <div className="header-logo pr10 flex-1">
            <Link
              href="/"
              onClick={() => {
                setTimeout(() => {
                  isProfileCompleted && window.location.reload();
                });
                onClose();
              }}
            >
              <a onClick={() => {
                setTimeout(() => {
                  isProfileCompleted && window.location.reload();
                });
                onClose();
              }}>
                <img
                  className="mb5 img-fluid"
                  width="170"
                  src={headerLogo}
                  alt="logo"
                />
              </a>
            </Link>
          </div>
          {!isMaxWidth992 && Menu}
          {isMaxWidth992 && (
            <>
              {!isLogin && (
                <Button
                  type="primary"
                  className="ex__primary_btn mobile-login-btn"
                  onClick={() => {
                    dispatch(openAuthModal(true));
                    onClose();
                  }}
                >
                  Login
                </Button>
              )}
              <Button onClick={showDrawer} className="toggle-drawer-btn">
                <img src={hamburgerMenuIcon} alt="" />
              </Button>
            </>
          )}
          {isLogin && (
            <>
              <div className="flex-x align-center an-16 header-nav-list mr10  profile_detail">
                <Popover
                  placement="topRight"
                  content={content}
                  trigger={`${isMaxWidth992 ? "click" : "hover"}`}
                >
                  <Avatar
                    shape="square"
                    className="cursor-pointer"
                    size={32}
                    src={role === "expert" ? picture : enthu.picture}
                  />
                </Popover>
              </div>
            </>
          )}
        </div>
        {MyDrawer}
        {showAuth && (
          <Auth
            visible={showAuth}
            onCancel={() => dispatch(closeAuthModal(false))}
            isLoginShow={isLoginShow}
          />
        )}
        {showChooseProfile && (
          <ChooseProfile
            visible={showChooseProfile}
            onChoose={onProfileChoose}
          />
        )}
        {showApprovalProfile && (
          <PendingApproval
            visible={showApprovalProfile}
            viewType={showApprovalProfileViewType}
            onCancel={() => dispatch(closeApprovalModal(false))}
          />
        )}
      </div>
      {MyDrawer}
      {showAuth && (
        <Auth
          visible={showAuth}
          onCancel={() => dispatch(closeAuthModal(false))}
          isLoginShow={isLoginShow}
        />
      )}
      {showChooseProfile && (
        <ChooseProfile visible={showChooseProfile} onChoose={onProfileChoose} />
      )}
      {showApprovalProfile && (
        <PendingApproval
          visible={showApprovalProfile}
          viewType={showApprovalProfileViewType}
          onCancel={() => dispatch(closeApprovalModal(false))}
        />
      )}

      {successPopup && !showAuth && !isLogin &&
        <NewsletterModal
          visible={successPopup}
          onClose={() => setSuccessPopup(false)}
          isSuccessClose={() => callFunction()}
        />
      }
      {isSubscribeSuccess &&
        <SubscribeSuccessModal
          visible={isSubscribeSuccess}
          onClose={() => setIsSubscribeSuccess(false)}
        />
      }
      {/* </div> */}
    </Fragment >
  );
};

export default compose(Header);
