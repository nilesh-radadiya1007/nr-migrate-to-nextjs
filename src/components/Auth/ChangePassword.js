import React, { useState } from 'react';
import { Button, Col, Form, Input, Row, Popover, Icon } from "antd";
import { useDispatch, useSelector } from 'react-redux';
import { AuthActions } from '../../redux/auth/events';

const ChangePassword = (props) => {
  const { getFieldDecorator } = props.form;

  const dispatch = useDispatch();
  const loader = useSelector(state => state.auth.loader);
  const { changePassword } = AuthActions;

  const [passwordError, setPasswordError] = useState("Please input new password");
  const [isLower, setIsLower] = useState(false);
  const [isUpper, setIsUpper] = useState(false);
  const [isNumber, setIsNumber] = useState(false);
  const [isSpecial, setIsSpecial] = useState(false);
  const [isMinimum, setIsMinumum] = useState(false);
  const [isMsgShow, setIsMsgShow] = useState(false);
  const [isEmpty, setIsEmpty] = useState(false);

  const handleFormSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setIsMsgShow(false);
        dispatch(changePassword(values));
      }
    });
  };

  const validatePassword = (rule, value, callback) => {
    var patt = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{8,}$/);

    setIsLower(/[a-z]/.test(value) ? true : false);
    setIsUpper(/[A-Z]/.test(value) ? true : false);
    setIsNumber(/[0-9]/.test(value) ? true : false);
    setIsSpecial(/[@#$!%*?&]/.test(value) ? true : false);
    setIsMinumum((value !== undefined && value.length >= 8) ? true : false)

    var res = patt.test(value);

    setPasswordError('');
    if (value === "" || value === undefined) {
      callback();
    } else {
      if (!res) {
        setIsMsgShow(true);
        setIsEmpty(false);
        callback("Enter a password according to the policy.");
      } else {
        setIsMsgShow(false);
        callback();
      }
    }

  };

  const onPasswordFocus = (e) => {
    if(e.target.value == ""){
      setIsEmpty(true);
    }else{
      setIsEmpty(false);
    }
    setIsMsgShow(true);
  }

  const onPasswordBlur = (e) => {
    // setIsMsgShow(false);
  }

  const onPasswordChange = (e) => {
    if (e.target.value === "") {
      setPasswordError('');
    }
  }

  const content = (
    <div className="password-popup-main">
      {/* <p className="password-popup-title">Password must meet the following requierments:</p> */}
      <p className={`${isEmpty ? 'gray' : isMinimum ? 'green' : 'red'}`}><Icon type={`${isMinimum ? 'check' : 'close'}`} /> &nbsp;At least 8 characters</p>
      <p className={`${isEmpty ? 'gray' : isLower ? 'green' : 'red'}`}><Icon type={`${isLower ? 'check' : 'close'}`} /> &nbsp;At least one small letter</p>
      <p className={`${isEmpty ? 'gray' : isUpper ? 'green' : 'red'}`}><Icon type={`${isUpper ? 'check' : 'close'}`} /> &nbsp;At least one capital letter</p>
      <p className={`${isEmpty ? 'gray' : isNumber ? 'green' : 'red'}`}><Icon type={`${isNumber ? 'check' : 'close'}`} /> &nbsp;At least one number</p>
      <p className={`${isEmpty ? 'gray' : isSpecial ? 'green' : 'red'}`}><Icon type={`${isSpecial ? 'check' : 'close'}`} /> &nbsp;At least one special characters (!, @, #, $, %, &, *)</p>
    </div >
  );

  return (
    <div className="step-1-expert-form">
      <div className="an-20 medium-text success--text step-title">
        CHANGE PASSWORD
      </div>
      <Form className="ant-advanced-search-form" onSubmit={handleFormSubmit}>
        <div className="form-profile-container">
          <div className="pt10">
            <Row gutter={24}>
              <Col className="phone-class-position device-position" xs={12} sm={12} md={12} lg={12} xl={12}>
                <Form.Item className="ant-form-item-change" label="Current Password">
                  {getFieldDecorator("opassword", { rules: [{ required: true, message: 'Current password is required' }] })
                    (<Input.Password placeholder="Current Password" />)}
                </Form.Item>
              </Col>
              <Col className="phone-class-position device-position" xs={12} sm={12} md={12} lg={12} xl={12}>
                <Form.Item className="ant-form-item-change" label="New Password">
                  {/* <Popover placement="top" overlayClassName="password-tooltip" content={content} trigger={'click'}> */}
                    {getFieldDecorator("npassword", {
                      rules: [
                        { required: true, message: passwordError },
                        { validator: validatePassword }
                      ]
                    })
                      (<Input.Password placeholder="New Password" onChange={onPasswordChange} onFocus={onPasswordFocus} onBlur={onPasswordBlur} />)}
                  {/* </Popover> */}

                </Form.Item>
                {isMsgShow && content}
              </Col>
            </Row>
          </div>
        </div>
        <Form.Item className="mb0 pt25 ant-form-item-change devicept25">
          <Button type="primary"
            loading={loader}
            htmlType="submit"
            className="ex__primary_btn">
            Next
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

const WrappedChangePassword = Form.create({ name: "createProfile" })(ChangePassword);

export default WrappedChangePassword;
