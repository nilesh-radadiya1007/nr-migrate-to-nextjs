import {
  EXPERT_STEP_PERSOAL_INFO,
  EXPERT_STEP_BUSINESS_DETAILS,
  GO_TO_TAB_REQ,
  EDIT_MODE,
  UPDATE_EXPERT_PROFILE,
  EXPERT_STEP_AWARDS_AND_BIO,
} from "../../helpers/actions";

export const ExpertEvents = {
  stepPersonalInfo: (data) => ({ type: EXPERT_STEP_PERSOAL_INFO, data }),
  stepBusinessDetails: (data) => ({ type: EXPERT_STEP_BUSINESS_DETAILS, data }),
  stepAwardsAndBio: (data) => ({ type: EXPERT_STEP_AWARDS_AND_BIO, data }),
  changeTab: (data) => ({ type: GO_TO_TAB_REQ, data }),
  isEditMode: () => ({ type: EDIT_MODE }),
  updateProfile: (data) =>({ type: UPDATE_EXPERT_PROFILE, data }),
  updateCover: (data) => ({ type: "UPDATE_EXPERT_COVER", cover: data.cover,originalCover:data.originalCover }),
  nullExpert: () => ({ type: "NULL_EXPERT" }),
};