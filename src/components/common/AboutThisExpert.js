import React from 'react';
import { compose } from "redux";
import Link from 'next/link';

import { CaptalizeFirst, commaSepratorString, getCityFromLocation } from "../../helpers/methods";

const Location_Img = "/images/country_ic.png";
const Lang = "/images/followers_ic.png";

const AboutThisExpert = (props) => {

    const displayLang = (lang) => {
        let resLang = ""
        if (lang.length > 0) {
            lang.map((a, index) => {
                let addCooma = "";
                if (lang.length !== index + 1) {
                    addCooma = ", "
                }
                resLang += CaptalizeFirst(a) + addCooma;
            });

        }

        return resLang;
    }

    const { expert, type } = props;
    return (
        <div className="br20 text-center pb40 about-this-expert">
            <h4 className="sub_title text-center">ABOUT THIS EXPERT</h4>
            <img
                src={expert.profile}
                alt="profile"
                className="br10 right_side_image"
            />
            <h5 className="an-18 pt20 mb20 expert-name">{`${CaptalizeFirst(
                expert.firstName
            )} ${CaptalizeFirst(expert.lastName)}`}</h5>
            <p className="grn_txt an-16">
                {commaSepratorString(expert.experties)}
            </p>
            <p className="pt5 mb5 blanck-Color">
                <img
                    src={Location_Img}
                    alt=""
                    className="pb10"
                    id="location_img"
                />
                {getCityFromLocation(expert.city)}{CaptalizeFirst(expert.country)}
            </p>
            <p className="pt5 pb10 blanck-Color">
                <img
                    src={Lang}
                    alt="Lang"
                    className="pb10"
                    id="location_img"
                />
                {commaSepratorString(expert.speaks)}
            </p>
            <p></p>
            <Link
                href={`/expert-profile/${expert._id}`}
                >
                <a className="get-in-tuch pt20 pb20 br5">VIEW PROFILE</a>
            </Link>
        </div>
    )

}

export default compose(AboutThisExpert)
