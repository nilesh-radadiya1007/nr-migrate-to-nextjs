import {
  SAVE_TRIP_STEP_1, SAVE_TRIP_STEP_2, GO_TO_TAB, TRIP_STEP_3,
  SUCCESS_CREATE_TRIP, ERROR_CREATE_TRIP, UPDATE_TRIP, UPDATE_TRIP_SUCCESS
} from "../../helpers/actions";

const initState = {
  cover: null,
  originalCover: null,
  title: "",
  duration: null,
  durationType: 'days',
  price: null,
  discount: "",
  priceType: 3,
  priceCurrency: null,
  type: [],
  imgList: [],
  suitable: [],
  activity: [],
  difficulty: [],
  itinerary: [],
  dates: [],
  accomodationPhotos: [],
  accomodations: [],
  description: "",
  accommodation: "",
  meetingPoint: "",
  step1: false,
  step2: false,
  step3: false,
  loader: false,
  tab: 1,
  dateType: 1,
  language: [],
  skill: [],
  participants: null,
  groupType: 1,
  whatLearn: null,
  attend: null,
  country: null,
  dateTime: [],
  inclusion: "",
  exclusion: "",
  cancellations: "",
  extras: "",
  gettingThere: "",
  transfer: "",
  equipmentsGears: "",
  covidGuidlines: "",
  season: '',
  discountType: 1,
  certification:'no'
}

function trips (state = initState, action) {
  switch (action.type) {
    case SAVE_TRIP_STEP_1:
      return {
        ...state,
        activity: JSON.stringify(action.activity),
        cover: action.cover,
        originalCover: action.originalCover,
        fileList: action.fileList,
        difficulty: action.difficulty,
        duration: action.duration,
        durationType: action.durationType,
        price: action.price,
        discount: action.discount,
        priceCurrency: action.priceCurrency,
        priceType: action.priceType,
        suitable: action.suitable,
        title: action.title,
        type: JSON.stringify(action.etype),
        step1: true,
        tab: 2,
        language: action.language,
        skill: action.skill,
        participants: action.participants,
        groupType: action.groupType,
        season: action.season,
        discountType: action.discountType,
      };
    case SAVE_TRIP_STEP_2:
      return {
        ...state,
        coordinates: action.data.coordinates,
        country: action.data.country,
        address: action.data.address,
        dateTime: JSON.stringify(action.data.dateTime),
        description: action.data.description,
        whatLearn: action.data.whatLearn,
        attend: action.data.attend,
        meetingPoint: action.data.meetingPoint,
        dateType: action.data.dateType,
        step2: true,
        tab: 3
      }
    case UPDATE_TRIP_SUCCESS:
      return {
        ...state,
        id: action.data.id
      }
    case TRIP_STEP_3:
      return {
        ...state,
        loader: true
      }

    case SUCCESS_CREATE_TRIP:
      return {
        ...state,
        loader: false,
      }
    case ERROR_CREATE_TRIP:
      return {
        ...state,
        loader: false,
      }

    case GO_TO_TAB:
      return {
        ...state,
        tab: action.tab
      }
    case UPDATE_TRIP: {
      return {
        ...state,
        step3: true,
        loader: true
      }
    }
    case "CLEAR_FIELDS":
      return {
        cover: null,
        originalCover: null,
        fileList: [],
        title: "",
        duration: null,
        durationType: 'days',
        priceType: 3,
        discount: "",
        groupType: 1,
        dateType: 1,
        price: null,
        type: [],
        suitable: [],
        activity: [],
        difficulty: [],
        itinerary: [],
        accomodationPhotos: [],
        accomodations: [],
        dates: [],
        description: "",
        accommodation: "",
        meetingPoint: "",
        step1: false,
        step2: false,
        step3: false,
        loader: false,
        tab: 1,
        language: [],
        skill: [],
        participants: null,
        whatLearn: null,
        attend: null,
        country: null,
        dateTime: [],
        gettingThere: "",
        transfer: "",
        equipmentsGears: "",
        covidGuidlines: "",
        season: "",
        discountType: 1
      }
    case 'SET_TRIP_UPDATE_DATA':
      return {
        id: action.data._id,
        cover: action.data.cover,
        originalCover: action.data.originalCover,
        imgList: action.data.images,
        title: action.data.title,
        duration: action.data.duration,
        durationType: (typeof action.data.durationType === "undefined" || action.data.durationType === "") ? 'days' : action.data.durationType,
        price: action.data.price,
        discount: (typeof action.data.discount !== "undefined" && action.data.discount !== "" && action.data.discount !== null) ? action.data.discount : "",
        priceType: (typeof action.data.priceType === "undefined" || action.data.priceType === "" || action.data.priceType === null) ? 3 : action.data.priceType,
        skill: action.data.skill,
        activity: JSON.stringify(action.data.activity),
        language: action.data.language,
        dateTime: JSON.stringify(action.data.dateTime),
        coordinates: action.data.location.coordinates,
        accomodationPhotos: action.data.accomodationPhotos,
        country: action.data.country,
        address: action.data.address,
        meetingPoint: action.data.meetingPoint,
        description: action.data.description,
        whatLearn: action.data.whatLearn,
        attend: action.data.attend,
        accommodation: action.data.accomodation,
        itinerary: JSON.stringify(action.data.itenary),
        accomodations: typeof action.data.accomodations !== "undefined" && action.data.accomodations.length > 0 ? JSON.stringify(action.data.accomodations) : [],
        inclusion: action.data.inclusion,
        exclusion: action.data.exclusion,
        gettingThere: action.data.gettingThere || "",
        transfer: action.data.transfer || "",
        equipmentsGears: action.data.equipmentsGears || "",
        covidGuidlines: action.data.covidGuidlines || "",
        extras: action.data.extras,
        cancellations: action.data.cancellations,
        participants: action.data.participants,
        groupType: (typeof action.data.groupType === "undefined" || action.data.groupType === "" || action.data.groupType === null) ? 1 : action.data.groupType,
        dateType: action.data.dateType,
        type: JSON.stringify(action.data.type),
        suitable: action.data.suitable,
        difficulty: action.data.difficulty,
        step1: false,
        step2: false,
        step3: false,
        loader: false,
        tab: 1,
        editMode: true,
        season: (typeof action.data.season === "undefined" || action.data.season === "" || action.data.season === null) ? "" : action.data.season,
        discountType: (typeof action.data.discountType === "undefined" || action.data.discountType === "" || action.data.discountType === null) ? 1 : action.data.discountType,

      }
    default:
      return state;
  }
}

export default trips;
