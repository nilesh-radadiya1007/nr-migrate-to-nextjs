import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';


const TripsEditView = dynamic(
    () => import("../../src/containers/TripsEditView"),
    { ssr: false }
);

const TripsEditViewInApp = (props) => {
    return (   
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <TripsEditView />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

// export async function getServerSideProps({ params }) {
//     return { props: { id: params.id } }
// }


export default TripsEditViewInApp;