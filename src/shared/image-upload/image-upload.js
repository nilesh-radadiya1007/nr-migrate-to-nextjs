import React, { useEffect, useState } from "react";
import ImageUploading from "react-images-uploading";
import FilerobotImageEditor from "filerobot-image-editor";
import { Modal, Button, Menu, Dropdown } from "antd";
import ImageReposition from "shared/image-crop/image-reposition";

const config = {
  tools: [
    "adjust",
    "effects",
    "filters",
    // "rotate",
    // "watermark",
    // "shapes",
    // "image",
    // "text",
  ],
  translations: {
    en: {
      "toolbar.save": "Save",
      "toolbar.apply": "Apply Effects",
      "toolbar.download": "Save Image",
    },
  },
};

const ImageUpload = ({
  setCoverImage,
  reposition,
  originalCover,
  setReposition,
  page,
  setZoom
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [images, setImages] = useState([]);
  const [imageSrc, setImageSrc] = useState("");
  const [show, toggle] = useState(false);
  const showIsEdit = originalCover
  useEffect(() => {
    if (originalCover !== null) {
      getDataBlob(originalCover);

    }
  }, [originalCover]);

  //url to base64 string
  const parseURI = async (d) => {
    var reader = new FileReader();
    reader.readAsDataURL(d);
    return new Promise((res, rej) => {
      reader.onload = (e) => {
        // setImageDataUrl(e.target.result);
      };
    });
  };
  //url convert to blob
  const getDataBlob = async (url) => {
    var res = await fetch(url);
    var blob = await res.blob();
    var uri = await parseURI(blob);
    return uri;
  };

  //on uploading the image
  const onChange = (imageList) => {
    setImages(imageList);
    setImageSrc(imageList[0].data_url);
    toggle(true);
  };

  //close modal
  const handleOk = () => {
    setIsModalVisible(false);
  };

  //close modal
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  //open modal
  const onError = (data) => {
    setIsModalVisible(true);
  };

  const onBeforeComplete = (data) => {
    //stop the image to get download
    return false;
  };

  //convert dataURL to a file object
  const dataURLtoFile = (dataurl, filename) => {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  };

  //on completing the effects
  const onComplete = (data) => {
    if (data.status === "success") {
      const base64Url = data.canvas.toDataURL();
      var file = dataURLtoFile(base64Url, "image.png");
      if (page !== 'Trips') {
        const formData = new FormData();
        formData.append("originalCover", file);
        setCoverImage(formData, "upload", base64Url);
      }else{
        setCoverImage(file, "upload")
      }
    }
  };
  const setCover = (formData, editOrUpload) => {
    setCoverImage(formData, editOrUpload);
  };

  return (
    <div>
      {isModalVisible && (
        <Modal
          title=""
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <h3>
            <p> Image Upload Error.</p>
            Minimum image size should be <b>1024 px * 540 px</b>. Please upload a bigger image.
          </h3>
        </Modal>
      )}

      {originalCover !== "" && reposition === true && (
       <Modal
       closable={false}
       footer={null}
       title=""
       width="100%"
       className="antd-modal-redesign"
       visible={true}
       onOk={handleOk}
       onCancel={handleCancel}
     >
     <ImageReposition originalCover={originalCover}
        page={page}
        setCover={(file, editOrUpload) =>
          setCover(file, editOrUpload)}
          isZoom={setZoom}
          setReposition={(e)=>{setReposition(e)}}
      />
       
        </Modal>
      )}

      {imageSrc !== null && (
        <FilerobotImageEditor
          config={config}
          show={show}
          src={imageSrc}
          finishButtonLabel={"-save"}
          onBeforeComplete={onBeforeComplete}
          onComplete={onComplete}
          onClose={() => {
            toggle(false);
          }}
        />
      )}

      {reposition === false && (
        <ImageUploading
          multiple={false}
          value={images}
          onChange={onChange}
          dataURLKey="data_url"
          resolutionType="more"
          resolutionWidth={1024}
          resolutionHeight={540}
          onError={onError}
        >
          {({ imageList, onImageUpload }) => (
            <div className="upload__image-wrapper">
              {showIsEdit !== null && showIsEdit !== "" ?<Dropdown
              trigger={['click']}
                overlay={
                  <Menu>
                    <Menu.Item onClick={onImageUpload} key="upload">
                      {page==='Cover'?'Upload Cover Image':'Upload Image'}
                    </Menu.Item>
                    {showIsEdit !== null && showIsEdit !== "" && (
                      <Menu.Item
                        onClick={() => setReposition(true)}
                        key="reposition"
                      >
                        Reposition Image
                      </Menu.Item>
                    )}
                    {showIsEdit !== null && showIsEdit !== "" && (
                      <Menu.Item
                        onClick={() => setCover('', 'delete')}
                        key="reposition"
                      >
                        Remove Image
                      </Menu.Item>
                    )}
                  </Menu>
                }
                placement="bottomCenter"
                arrow
              >
                <Button
                  style={{ margin: "40px" }}
                  className="edit_btn medium-text an-14 cover-img-edit"
                >
                    { page==='Cover'?`Edit Cover Image`:`Edit Image`}
                </Button>
              </Dropdown>: <Button
                  style={{ margin: "40px" }}
                  className="edit_btn medium-text an-14 cover-img-edit"
                  onClick={onImageUpload}
                >
                    { page==='Cover'?`Upload Cover Image`:`Upload Image`}
                </Button>}
            </div>
          )}
        </ImageUploading>
      )}
    </div>
  );
};

export default ImageUpload;
