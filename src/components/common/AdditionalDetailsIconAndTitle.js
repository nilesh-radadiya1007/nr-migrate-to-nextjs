import React from 'react';
import { compose } from "redux";
import { Icon } from 'antd';

const ad_itinary = '/images/newicon/ad_itinary.svg';
const ad_meals = '/images/newicon/ad_meals.svg';
const ad_transport = '/images/newicon/ad_transport.svg';
const ad_inclusion_exclusion = '/images/newicon/ad_inclusion_exclusion.svg';
const ad_equipments = '/images/newicon/ad_equipments.svg';
const ad_additional_info = '/images/newicon/ad_additional_info.svg';
const ad_terms_condition = '/images/newicon/ad_terms_condition.svg';


const AdditionalDetailsIconAndTitle = (props) => {
    let { position, review } = props;

    const titleAndIcon = [
        {
            title: "Itinerary",
            icon: ad_itinary
        },
        {
            title: "Meals & Accommodation",
            icon: ad_meals
        },
        {
            title: "Logistics & Transfers",
            icon: ad_transport
        },
        {
            title: "What's included?",
            icon: ad_inclusion_exclusion
        },
        {
            title: "Equipments",
            icon: ad_equipments
        },
        {
            title: "Additional Information",
            icon: ad_additional_info
        },
        {
            title: "COVID Guidlines",
            icon: ad_additional_info
        },
        {
            title: "Terms & Conditions",
            icon: ad_terms_condition
        },
        {
            title: "Reviews",
            icon: ad_inclusion_exclusion
        },
    ];

    const totalReview = () => {
        let numberOfReview = "";
        let reviewText = "No Review";
        if (review !== undefined && review.reviewLength !== undefined) {
            if (review.reviewLength === 0) {
                reviewText = "No Review";
            } else if (review.reviewLength === 1) {
                reviewText = "Review";
            } else {
                reviewText = "Reviews";
            }
            numberOfReview = review.reviewLength;
        }
        return <span>{numberOfReview !== "" && <><span>{numberOfReview === 0 ? "" : numberOfReview  }</span> <span>{reviewText}</span></>}</span>

    }
    return (
        <div className="title-icon">
            {position === 8 &&
                <>
                    <Icon type="star" theme="filled" className='collaps-start-icon' />
                    <span>
                        {totalReview()}
                    </span>
                </>
            }

            {position !== 8 && <>
                <img src={titleAndIcon[position].icon || ad_itinary}/>
                <span>{titleAndIcon[position].title || ""}</span></>
            }

        </div >
    )

}

export default compose(AdditionalDetailsIconAndTitle)
