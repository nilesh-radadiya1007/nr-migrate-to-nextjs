import React, { useState } from "react";
import {
    Row,
    Col,
} from "antd";
import Slider from "react-slick";

import { useMediaQuery } from "react-responsive";
// import Image from 'next/image'
// import playOutlineIcon from "../../../public/images/playOutlineIcon.svg";
import AuthModal from "../components/Auth/AuthModal";
import Newsletter from '../components/common/NewsLetter';

// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const playOutlineIcon = "/images/playOutlineIcon.svg";
const rightImg = "/images/about/about-right-img.jpg";
const connect = "/images/about/connect.png";
const community = "/images/about/community.png";
const ourDream = "/images/about/our-dream.png";
const sustainable = "/images/about/sustainable.png";
const connects = "/images/about/users.png";
const idea = "/images/about/idea.png";
const learning = "/images/about/learning.png";
const map = "/images/about/map.png";
const sustain = "/images/about/sustain.png";
const adventurer = "/images/about/adventurer.png";
const compass = "/images/about/compass.png";
const free = "/images/about/free.png";
const handshake = "/images/about/handshake.png";
const user1 = "/images/about/a_user1.jpg";
const user2 = "/images/about/a_user2.jpg";
const user3 = "/images/about/a_user3.jpg";
const user4 = "/images/about/a_user4.jpg";
const user5 = "/images/about/a_user5.jpg";
const user6 = "/images/about/a_user6.jpg";
const user7 = "/images/about/a_user7.jpg";
const user9 = "/images/about/a_user9.jpg";

export default function AboutUs() {
    const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });

    const [showLogin, setShowLogin] = useState(false);
    const openSignup = () => setShowLogin(true);

    const settings = {
        dots: true,
        arrows: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    };
    return (
        <>
            <div className="about expert_container">
                <section className="banner">
                    <div className="container-fluid align-center">
                        <Row >
                            <Col lg={16} xxl={14} >
                                <div className="banner-text">
                                    <h1 className="medium-text main-title">Combining adventure <br></br>with <span>Education</span></h1>
                                    <div className="border-bottom-yellow"></div>
                                    <p className="sub-title">We’re on a mission to build the world’s best community to positively {!isMaxWidth768 && <br></br>}  impact the planet through adventure, exploration and education.</p>
                                    <a href="/" className=" btn-action ex__primary_btn" onClick={(e) => {openSignup(); e.preventDefault()}}>GET STARTED
                                        <img src={playOutlineIcon} alt="" />
                                    </a>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>

                {/* About us */}
                <section className="about-us">
                    <div className="container-fluid align-center">
                        <Row gutter={30} className="row-reverse">
                            <Col lg={10} xxl={12} className="right-img-wrap order-2 order-md-1">
                                <img className="img-fluid what-we-do-img" src={rightImg} alt="section right img" />
                            </Col>
                            <Col lg={14} xxl={12} className="order-1 order-md-2">
                                <div className="content">
                                    <h2 className="content-main-title">What We Do</h2>
                                    <p className="main-text"> At Expeditions Connect,  {isMaxWidth768 && <br></br>}we connect adventure enthusiasts with the most inspiring and experienced professionals, across the globe. </p>

                                    <p className="sub-text"> We believe that an adventure should be about more than just the expedition. That’s why we empower adventurers to discover, learn and explore in the company of leading experts. Because once you have the skills, you will unlock endless adventures.</p>

                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>

                {/* Connect */}
                <section className="connect">
                    <div className="container-fluid align-center">
                        <Row gutter={30}>
                            <Col lg={10} xxl={12} className="left-wrap">
                                <h2>At Expeditions Connect, <br></br>We Enable</h2>
                                <img src={connect} alt="section left img" className="img-fluid content-section-img" />
                            </Col>
                            <Col lg={14} xxl={12} className="connect-right-box">
                                <div className="content-box">
                                    <div className="box">
                                        <h5 className="box-main-title">Adventures seekers to…</h5>
                                        <ul>
                                            <li>Be part of an adventure community</li>
                                            <li>Discover, learn and explore with experts</li>
                                            <li>Leave a positive environmental impact</li>
                                        </ul>
                                    </div>
                                    <div className="box">
                                        <h5 className="box-main-title">Adventure experts to…</h5>
                                        <ul>
                                            <li>Share passion and expertise</li>
                                            <li>Inspire and educate</li>
                                            <li>Earn money</li>
                                        </ul>
                                    </div>
                                    <div className="box">
                                        <h5 className="box-main-title">Together, we help our planet by</h5>
                                        <ul>
                                            <li>Educating and empowering people</li>
                                            <li>Contributing to ethical causes</li>
                                            <li>Providing a greener way to explore</li>
                                        </ul>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>

                <section className="community rows-wrap">
                    <div className="container-fluid align-center">
                        {/* Our Dreams */}
                        <Row gutter={30} className="our-dream change-direction">
                            <Col lg={10} xxl={12} className="right-img-wrap">
                                <img className="img-fluid our-drem-img" src={ourDream} alt="our dream" />
                            </Col>
                            <Col lg={14} xxl={12} className="our-dream-right">
                                <div className="content">
                                    <h2 className="content-main-title">Our Dream</h2>
                                    <p className="main-text"> We began with a dream to empower adventure seekers across the globe to make a positive impact on our planet.</p>
                                    <p className="sub-text mb45"> We didn’t just want to get people involved. Our dream is to educate passionate adventurers on the environmental issues facing our planet, whilst giving them the tools they need to unlock endless adventures.</p>
                                    <p className="sub-text">It’s from here that Expeditions Connect was born. We joined forces to build a community of like-minded and passionate adventurers who are united against climate change and the destruction of our natural world.</p>

                                </div>
                            </Col>
                        </Row>

                        {/* Community */}
                        <Row gutter={30} className="row-reverse change-direction">
                            <Col lg={10} xxl={12} className="order-2 order-md-1 community-img-2">
                                <img className="img-fluid community-img" src={community} alt="community" />
                            </Col>
                            <Col lg={14} xxl={12} className="order-2 order-md-1">
                                <div className="content">
                                    <h2 className="content-main-title">A Community Built On <br></br>Education</h2>
                                    <p className="main-text mb45">We have one clear vision: {isMaxWidth768 && <br></br>} To bring like-minded adventurers together to share our passion for adventures whilst fighting climate change and protecting our planet. </p>
                                    <p className="sub-text"> It started with us. Two lone adventurers, Vaibhav Bhardwaj and Desh Yadav, who came together to share our love for adventurers and passion for sustainable expeditions. We dreamed of sharing our wanderlust with the world.</p>

                                </div>
                            </Col>
                        </Row>

                        {/* Sustainable */}
                        <Row gutter={30} className="sustainable-section change-direction">
                            <Col lg={10} xxl={12} className="right-img-wrap">
                                <img className="img-fluid sustainable-img" src={sustainable} alt="community" />
                            </Col>
                            <Col lg={14} xxl={12} className="sustainable-section-right">
                                <div className="content">

                                    <h2 className="content-main-title">Creating A Sustainable Future</h2>
                                    <p className="main-text"> Sustainability isn’t just a buzzword; it’s at the core of everything we do. </p>

                                    <p className="sub-text mb45">Our vision is to create a sustainable future for our planet by educating, spreading awareness and bringing people closer to the natural world.</p>

                                    <p className="sub-text mb45">Our experts are ambassadors of sustainability and eco-friendly practice. They champion the fight against climate change, helping to protect and preserve natural habitats worldwide. </p>

                                    <p className="sub-text">Together, we can unite to create a better future for ourselves and for our planet.
                                </p>

                                </div>
                            </Col>
                        </Row>

                    </div>
                </section>
                {/* 
            <section className="testimonial">
                <div className="container-fluid align-center">
                    <Row>
                        <Col>
                            <div className="testimonial-content">
                                <p>
                                Our community is built on learning and education. Through education, we empower adventure seekers to create positive change. Together we can protect our precious environment in the face of an uncertain future.
                                </p>
                                <div className="author">
                                    Vaibhav Bhardwaj, <span>Expeditions Connect</span>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section> */}

                {/* Sustainable */}
                {/* <section className="community rows-wrap">
                <div className="container-fluid align-center">
                    <Row gutter={30} className="">
                        <Col lg={12} className="right-img-wrap">
                            <Image layout="fill" className="img-fluid" src={sustainable} alt="community" />
                        </Col>
                        <Col lg={12}>
                            <div className="content">
                                <h2>Creating A Sustainable Future</h2>
                                <p> Sustainability isn’t just a buzzword for us; it’s at the core of everything we do. Our vision is to create a sustainable future for our planet by educating, spreading awareness and bringing people closer to the natural world.</p>

                                <p>Our experts are ambassadors of sustainability and eco-friendly practice. They champion the fight against climate change, helping to protect and preserve natural habitats worldwide. Through Expeditions Connect, our experts share their incredible works and projects with a global audience. </p>

                                <p>Together, we can unite to create a better future for ourselves and for our planet. 
                                </p>

                            </div>
                        </Col>
                    </Row>

                </div>
            </section> */}

                {/* Our value */}
                <section className="our-value">
                    <Row className="flex-x align-stretch flex-wrap">
                        <Col xl={9} className="left-wrap flex-x center align-center">
                            <div className="content-wrap-bar our-values-main-text">
                                <h2>Our <span>Values</span></h2>
                                <p className="mb25">At Expeditions Connect, we’ve made it our mission to protect our planet through adventure, exploration and education, and this is what our values are based upon. </p>

                                <p>We’re committed to matching adventure seekers with industry experts, providing high-quality adventure experiences with the learning to back them up. Sustainability will always be at the forefront of everything that we do. </p>
                            </div>
                        </Col>
                        <Col xl={15} className="right-wrap">
                            <div className="values-wrap flex-x space-between">
                                <div>
                                    <div className="box text-center flex align-center justify-center">
                                        <div className="box-content">
                                            <img src={connects} alt="connects" />
                                            <h6>Connections</h6>
                                            <p>We believe in the power of community. Incredible things happen when we bring adventure seekers and experts together. </p>
                                        </div>
                                    </div>
                                    <div className="box text-center flex align-center justify-center" style={{ background: '#099670', color: '#fff' }}>
                                        <div className="box-content">
                                            <img src={map} alt="connects" />
                                            <h6>EXPLORATION</h6>
                                            <p>We encourage adventure seekers to explore and protect our beautiful planet with experts. </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="box text-center flex align-center justify-center" style={{ background: '#ffbc00', color: '#111' }}>
                                        <div className="box-content">
                                            <img src={idea} alt="connects" />
                                            <h6>INSPIRATION</h6>
                                            <p>We connect adventure seekers with the most inspiring professionals, whatever their area of interest.</p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="box text-center flex align-center justify-center">
                                        <div className="box-content">
                                            <img src={learning} alt="connects" />
                                            <h6>LEARNING</h6>
                                            <p>We take pride in connecting adventure seekers with experts for an authentic learning experience, helping them to achieve their personal goals.</p>
                                        </div>
                                    </div>
                                    <div className="box text-center flex align-center justify-center" style={{ background: '#099670', color: '#fff' }}>
                                        <div className="box-content">
                                            <img src={sustain} alt="connects" />
                                            <h6>SUSTAINABILITY</h6>
                                            <p>Protecting our beautiful planet is at the heart of everything we do. It’s who we are. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </section>

                {/* Why Choose */}
                <section className="why-choose">
                    <div className="container-fluid align-center">
                        <Row>
                            <Col xl={8} className="left-wrap flex-x center align-center">
                                <div className="content-wrap-bar">
                                    <h2>Why Choose <span>Expeditions Connect</span></h2>
                                    <p>We’re committed to matching adventure seekers with industry experts by providing high-quality adventure experiences. </p>
                                </div>
                            </Col>

                            <Col xl={16}>
                                <div className="why-us-wrap flex flex-wrap">
                                    <div className="box-wrap flex-x align-start">
                                        <img src={adventurer} alt="" />
                                        <div className="box-content">
                                            <h6>World Class Experts</h6>
                                            <p className="why-choose-description">Our leading adventure experts deliver tailored experiences and lead once-in-a-lifetime expeditions.</p>
                                        </div>
                                    </div>
                                    <div className="box-wrap flex-x align-start">
                                        <img src={compass} alt="" />
                                        <div className="box-content">
                                            <h6>More Than Just Adventures</h6>
                                            <p className="why-choose-description">Access workshops, courses, webinars and internships, crafted by experts especially for you. </p>
                                        </div>
                                    </div>
                                    <div className="box-wrap flex-x align-start">
                                        <img src={handshake} alt="" />
                                        <div className="box-content">
                                            <h6>Direct Connection </h6>
                                            <p className="why-choose-description">No middleman, no hassle. Enjoy a direct connection with the best adventure professionals.</p>
                                        </div>
                                    </div>
                                    <div className="box-wrap flex-x align-start">
                                        <img src={free} alt="" />
                                        <div className="box-content">
                                            <h6>100% Free to Join</h6>
                                            <p className="why-choose-description">Our leading adventure experts deliver tailored experiences and lead once-in-a-lifetime expeditions.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>

                {/* Our Team */}
                <section className="our-team">
                    <div className="container-fluid align-center">
                        <Row>
                            <Col xl={18} className="text-center top-head">
                                <h2>Our Team</h2>
                                <p>It’s always been pretty simple for us. Great people make great work. <br></br>Meet the team.</p>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={24}>
                                <Slider {...settings}>
                                    <div className="my-team flex-y align-center center">
                                        <img src={user1} alt="userImg img-fluid" />
                                        <div className="team-detail flex-y align-center text-center ">
                                            <span>Vaibhav Bhardwaj</span>
                                            <span>Founder, CEO</span>
                                        </div>
                                    </div>
                                    <div className="my-team flex-y align-center center">
                                        <img src={user2} alt="userImg img-fluid" />
                                        <div className="team-detail flex-y align-center text-center ">
                                            <span>Desh Yadav</span>
                                            <span>Co-Founder, CTO</span>
                                        </div>
                                    </div>
                                    <div className="my-team flex-y align-center center">
                                        <img src={user3} alt="userImg img-fluid" />
                                        <div className="team-detail flex-y align-center text-center ">
                                            <span>Sushmita Nandi</span>
                                            <span>Head of Operations</span>
                                        </div>
                                    </div>
                                    <div className="my-team flex-y align-center center">
                                        <img src={user4} alt="userImg img-fluid" />
                                        <div className="team-detail flex-y align-center text-center ">
                                            <span>Suzanne Mot</span>
                                            <span>Digital Marketing Manager</span>
                                        </div>
                                    </div>
                                    <div className="my-team flex-y align-center center">
                                        <img src={user5} alt="userImg img-fluid" />
                                        <div className="team-detail flex-y align-center text-center ">
                                            <span>Grace Galdo</span>
                                            <span>Content Writer</span>
                                        </div>
                                    </div>
                                    <div className="my-team flex-y align-center center">
                                        <img src={user6} alt="userImg img-fluid" />
                                        <div className="team-detail flex-y align-center text-center ">
                                            <span>Ravi Khunt</span>
                                            <span>Full Stack Developer</span>
                                        </div>
                                    </div>
                                    <div className="my-team flex-y align-center center">
                                        <img src={user7} alt="userImg img-fluid" />
                                        <div className="team-detail flex-y align-center text-center ">
                                            <span>Vikram Kumar</span>
                                            <span>Full Stack Developer</span>
                                        </div>
                                    </div>
                                    <div className="my-team flex-y align-center center">
                                        <img src={user9} alt="userImg img-fluid" />
                                        <div className="team-detail flex-y align-center text-center ">
                                            <span>Hitendra Valluri</span>
                                            <span>Product Manager</span>
                                        </div>
                                    </div>
                                </Slider>
                            </Col>
                        </Row>
                    </div>
                </section>
                {
                    showLogin && (
                        <AuthModal visible={showLogin} onCancel={() => setShowLogin(false)} />
                    )
                }
            </div>
            <Newsletter page="aboutus"/>
        </>

    );
}
