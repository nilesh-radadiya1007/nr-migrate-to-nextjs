import React from 'react'
import Settings from '../src/containers/ChangePassword';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const SettingsInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Settings />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default SettingsInApp;