import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';


const CreateTrips = dynamic(
    () => import("../src/containers/CreateTrips"),
    { ssr: false }
);

const CreateTripsInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <CreateTrips />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default CreateTripsInApp;