import React, {useState} from 'react';
import {Icon, Button} from "antd";
import ChangePassword from '../components/Auth/ChangePassword';
import ChangeEmail from '../components/Auth/ChangeEmail';

const Settings = () => {
  const [isPasswordChange, setIsPasswordChange] = useState(true);

  const setPasswordChange = (isPasswordChange) => {
    setIsPasswordChange(isPasswordChange)
  };

  // for mobile view
  const [changeEmailPasswordFromMobile, setIsPasswordChangeFromMobile] = useState('back');

  const setPasswordChangeFromMobile = (changeFor) => {
    if (changeFor === 'changePasswordFromMobile') {
      setIsPasswordChangeFromMobile('changePasswordFromMobile');
    }
    if (changeFor === 'changeEmailFromMobile') {
      setIsPasswordChangeFromMobile('changeEmailFromMobile');
    }
    if (changeFor === 'back') {
      setIsPasswordChangeFromMobile('back');
    }
  }

  const tabClass = 'tab-grid an-15 medium-text dark-text flex-x align-center space-between';

  return (
    <>
      <div className="create-expert-container flex-x container py30 remove-email-password">
        <div className="create-profile-tab">
          <div className={`${tabClass} ${isPasswordChange ? 'active' : ''}`}
               onClick={() => setPasswordChange(true)}>
            CHANGE PASSWORD {isPasswordChange ? <Icon type="check"/> : null}
          </div>
          <div className={`${tabClass} ${!isPasswordChange ? 'active' : ''}`}
               onClick={() => setPasswordChange(false)}>
            CHANGE EMAIL {!isPasswordChange ? <Icon type="check"/> : null}
          </div>
        </div>
        {isPasswordChange ?
          <div className="create-profile-routes paasword-email-routes" style={{width: '100%'}}>
            <ChangePassword/>
          </div>
          : null
        }
        {!isPasswordChange ?
          <div className="create-profile-routes paasword-email-routes" style={{width: '100%'}}>
            <ChangeEmail/>
          </div>
          : null
        }
      </div>
      {/* for mobile view */}
      <div className="create-expert-container flex-x container py30 remove-email-password-mobile">
        <div className="create-profile-tab">
          <div className={`${tabClass} ${(changeEmailPasswordFromMobile === 'changePasswordFromMobile') ? 'active' : (changeEmailPasswordFromMobile === 'back') ? 'show-initial' : 'hide-for-mobile-view'}`}
               onClick={() => setPasswordChangeFromMobile('changePasswordFromMobile')}>
            CHANGE PASSWORD {(changeEmailPasswordFromMobile === 'changePasswordFromMobile') ? <Icon type="check"/> : null}
          </div>
          <div className={`${tabClass} ${(changeEmailPasswordFromMobile === 'changeEmailFromMobile') ? 'active' : (changeEmailPasswordFromMobile === 'back') ? 'show-initial' : 'hide-for-mobile-view'}`}
               onClick={() => setPasswordChangeFromMobile('changeEmailFromMobile')}>
            CHANGE EMAIL {(changeEmailPasswordFromMobile === 'changeEmailFromMobile') ? <Icon type="check"/> : null}
          </div>
        </div>
        {(changeEmailPasswordFromMobile === 'changePasswordFromMobile') ?
          <div className="create-profile-routes paasword-email-routes" style={{width: '100%'}}>
            <ChangePassword/>
          </div>
          : null
        }
        {(changeEmailPasswordFromMobile === 'changeEmailFromMobile') ?
          <div className="create-profile-routes paasword-email-routes" style={{width: '100%'}}>
            <ChangeEmail/>
          </div>
          : null
        }
      </div>
      {(changeEmailPasswordFromMobile === 'changePasswordFromMobile' || changeEmailPasswordFromMobile === 'changeEmailFromMobile') ?
        <Button type="primary"
          className="ex__primary_btn back-button-margin"
          onClick={() => setPasswordChangeFromMobile('back')}>
          Back
        </Button>
        : null
      }
    </>
  )
}

export default Settings;
