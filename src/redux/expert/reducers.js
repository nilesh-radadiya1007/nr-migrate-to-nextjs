import {
  SAVE_EXPERT_STEP_PERSOAL_INFO,
  SAVE_EXPERT_STEP_BUSINESS_DETAILS,
  SAVE_EXPERT_STEP_AWARDS_AND_BIO,
  GO_TO_TAB,
  ERROR_SAVE_EXPERT,
  SUCCESS_SAVE_EXPERT,
  GET_PROFILE,
  EDIT_MODE,
  UPDATE_EXPERT_PROFILE,
  EXPERT_STEP_AWARDS_AND_BIO,
  EXPERT_STEP_BUSINESS_DETAILS
} from "../../helpers/actions";

const initState = {
  id: null,
  picture: null,
  originalPicture:null,
  firstName: null,
  lastName: null,
  city: null,
  coordinates: [],
  country: null,
  experties: [],
  speaks: [],
  bio: null,
  awards: [],
  companyname: null,
  website: null,
  businessEmail: null,
  professionalExperience: [],
  professionalQualifications: [],
  socialContributions: [],
  booksAndPublications: [],
  phone: null,
  address: null,
  address2: null,
  approved: false,
  cover: null,
  originalCover:null,
  active: true,
  stepPersonalInfo: false,
  stepBusinessDetails: false,
  stepAwardsAndBio: false,
  loader: false,
  tab: 1,
  isEditMode: false,
};

function expert (state = initState, action) {
  switch (action.type) {
    case SAVE_EXPERT_STEP_PERSOAL_INFO:
      return {
        ...state,
        picture: action.picture,
        originalPicture: action.originalPicture,
        firstName: action.firstName,
        lastName: action.lastName,
        country: action.country,
        experties: action.experties,
        speaks: action.speaks,
        city: action.city,
        coordinates: action.location || [],
        skills: action.skills,
        stepPersonalInfo: true,
        tab: 2,
      };
    // case SAVE_EXPERT_STEP_BUSINESS_DETAILS:
    //   return {
    //     ...state,
    //     companyname: action.companyname,
    //     website: action.website,
    //     companycountry: action.companycountry,
    //     address: action.address,
    //     businessEmail: action.businessEmail,
    //     phone: action.phone,
    //     stepBusinessDetails: true,
    //     tab: 3,
    //   };
    case SAVE_EXPERT_STEP_AWARDS_AND_BIO:
      return {
        ...state,
        ...action,
        stepAwardsAndBio: true,
        tab: 3
      };
    case EXPERT_STEP_BUSINESS_DETAILS:
      return {
        ...state,
        loader: true,
      };
    case ERROR_SAVE_EXPERT:
      return {
        ...state,
        loader: false,
      };
    case SUCCESS_SAVE_EXPERT:
      return {
        ...state,
        picture: action.profile,
        originalPicture: action.originalProfile,
        firstName: action.firstName,
        lastName: action.lastName,
        country: action.country,
        city: action.city,
        coordinates: action.coordinates || [],
        experties: action.experties,
        speaks: action.speaks,
        skills: action.skills,
        bio: action.bio,
        awards: action.awards,
        professionalExperience: action.professionalExperience,
        professionalQualifications: action.professionalQualifications,
        socialContributions: action.socialContributions,
        booksAndPublications: action.booksAndPublications,
        companyname: action.companyname,
        website: action.website,
        businessEmail: action.businessEmail,
        phone: action.phone,
        address: action.address,
        approved: action.approved,
        active: action.active,
        cover: action.cover,
        originalCover: action.originalCover,
        id: action.id,
        loader: false,
        stepPersonalInfo: action.stepPersonalInfo,
        stepBusinessDetails: action.stepBusinessDetails,
        stepAwardsAndBio: action.stepAwardsAndBio,
      };
    case GET_PROFILE:
      return {
        ...state,
        loader: true,
      };
    case EDIT_MODE:
      return {
        ...state,
        isEditMode: true,
      };
    case GO_TO_TAB:
      return {
        ...state,
        tab: action.tab,
      };
    case UPDATE_EXPERT_PROFILE:
      return {
        ...state,
        loader: true,
      };
    case "UPDATE_EXPERT_COVER":
      return {
        ...state,
        cover: action.cover,
        originalCover: action.originalCover
      };
    case "UPDATE_EXPERT_ONLYCOVER":
      return {
        ...state,
        cover: action.cover,
      };
    case "DELETE_EXPERT_COVER":
      return {
        ...state,
        cover: action.cover,
        originalCover: action.originalCover
      };
    case "NULL_EXPERT":
      return {
        id: null,
        picture: null,
        originalPicture: null,
        firstName: null,
        lastName: null,
        country: null,
        city: null,
        coordinates: [],
        experties: [],
        speaks: [],
        skills: [],
        bio: null,
        awards: [],
        professionalExperience: [],
        professionalQualifications: [],
        socialContributions: [],
        booksAndPublications: [],
        companyname: null,
        website: null,
        businessEmail: null,
        phone: null,
        address: null,
        approved: false,
        cover: null,
        originalCover: null,
        active: true,
        stepPersonalInfo: false,
        stepBusinessDetails: false,
        stepAwardsAndBio: false,
        loader: false,
        tab: 1,
        isEditMode: false,
      };
    default:
      return state;
  }
};
export default expert;