import React, { useState, useEffect, useCallback } from "react";
import { Row, Col, Button, message, Form, Select } from "antd";
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from "next/router";
import { compose } from "redux";

import { ActivityList, InterestedList } from '../../helpers/constants';
import { EnthuEvents } from '../../redux/enthu/events'
import ReactQuill from "react-quill";
const { Option } = Select;


const Step2 = (props) => {
  const router = useRouter();
  const pathname = router.pathname;
  const { setFieldsValue, getFieldDecorator } = props.form;
  const [doRender, setDoRender] = useState(false)
  const [aboutme, setAboutme] = useState("");
  const [interested, setinterestedIn] = useState([]);
  const [activity, setActivity] = useState([]);

  const { picture,originalPicture, country, address, phone, dob, firstName, lastName, city, speaks, interested: interestedIn, activity: activityIn, loader, aboutme: dec } = useSelector(state => state.enthu);
  const dispatch = useDispatch();

  const { step2, updateProfile, changeTab } = EnthuEvents;

  const setEditors = useCallback((dec) => {

    setAboutme(dec);
    setDoRender(true);
    setinterestedIn(interestedIn);
    setActivity(activityIn)
  }, [])

  useEffect(() => {
    setEditors(dec);
  }, [dec, setEditors, setFieldsValue])


  const handleAboutMeChange = (textQuillBubble) => {
    setAboutme(textQuillBubble);
  };


  const handleFormSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        if (aboutme == '<p><br></p>') {
          aboutme = "";
        }

        if (aboutme !== "" && aboutme.length > 500) {
          return message.error('Please fill out About me less than 500 characters');
        }

        const Obj = {
          picture: picture,
          originalPicture:originalPicture,
          firstName: firstName,
          lastName: lastName,
          country: country,
          city: city,
          address: address,
          phone: phone,
          dob: dob,
          speaks: JSON.stringify(speaks),
          aboutme: aboutme,
          interested: JSON.stringify(interested),
          activity: JSON.stringify(activity),
        }

        let formData = new FormData();
        for (const property in Obj) {
          if (property === "picture") {
            if (typeof Obj[property] !== "string") {
              formData.append(
                property,
                Obj[property]
              );
            }
            else{
              formData.append(property,'')
            }
          }
          else if (property === "originalPicture") {
            if (typeof Obj[property] !== "string") {
              formData.append(
                property,
                Obj[property]
              );
            }
            else{
              formData.append(property,'')
            }
          }
          else {
            formData.append(property, Obj[property]);
          }
        }
        if (pathname === '/update-enthu-profile') {
          dispatch(updateProfile(formData));
        } else {
          dispatch(step2(formData));
        }
      }
    });


  };


  if (doRender) {
    return (
      <div className="step-1-expert-form height_sec_enthu_step2">
        <div className="an-20 medium-text success--text step-title">
          ADVENTURE PREFERENCES
        </div>
        <div className="pt30">
          <Form className="ant-advanced-search-form" onSubmit={handleFormSubmit}>
            <Row gutter={40}>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label='About Me' className='flex-1 step2_description desc-min-height' >
                  <ReactQuill
                    theme='snow'
                    modules={{
                      toolbar: false,
                    }}
                    value={aboutme && aboutme.trim() !== 'null' ? aboutme : ''}
                    onChange={handleAboutMeChange}
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Adventure Preferences">
                  {getFieldDecorator("activity", { rules: [{ required: true, message: "Please Select Adventure Preferences" }], initialValue: activityIn })
                    (<Select
                      showSearch
                      mode="tags"
                      placeholder="Select Adventure Preferences"
                      onChange={(e) => setActivity(e)}

                    >
                      {ActivityList.map((con, i) => <Option key={i} value={con.name}>{con.name}</Option>)}
                    </Select>)}
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Form.Item label="Interested In">
                  {getFieldDecorator("interested", { rules: [{ required: true, message: "Please Select your Intrest" }], initialValue: interestedIn })
                    (<Select
                      showSearch
                      mode="tags"
                      placeholder="Select your Intrest"
                      onChange={(e) => setinterestedIn(e)}

                    >
                      {InterestedList.map((con, i) => <Option key={i} value={con.name}>{con.name}</Option>)}
                    </Select>)}
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="mb0 pt40">
                  <Button
                    loading={loader}
                    type="primary"
                    htmlType="submit"
                    className="ex__primary_btn">
                    Next
                  </Button>
                  {/* window.scrollTo({ top: document.getElementById('scroll-to-here'), behavior: 'smooth' }) */}
                  <Button type="primary" className="ex_grey_btn ml40" onClick={() => [dispatch(changeTab(1))]}>Back</Button>
                </div>
              </Col>

            </Row>
          </Form>
        </div >
      </div >
    );
  } else {
    return null
  }
};
const WrappedCreateEnthStep2 = Form.create({ name: "createProfile" })(Step2);

export default compose(WrappedCreateEnthStep2);