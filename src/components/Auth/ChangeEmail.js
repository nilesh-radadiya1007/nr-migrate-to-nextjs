import React, {useState} from 'react';
import {Form, Input, Row, Col, Button,} from "antd";
import { useSelector } from 'react-redux';
import ConfirmPasswordModal from "./confirmPasswordModal";
import {confirmEmail} from "../../services/auth";
import { message } from "antd";


const ChangeEmail = (props) => {
  const {getFieldDecorator} = props.form;
  const [showConfirmPasswordModel, setShowConfirmPasswordModel] = useState(false);
  const [values, setvalues] = useState();
  const loader = useSelector(state => state.auth.loader);

  const handleFormSubmit = e => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        const result = await confirmEmail(values);
        if (result.status === 200) {
          setShowConfirmPasswordModel(true);
          setvalues(values);
        } else {
          if (result && result.data && result.data.message) {
            message.error(result.data.message);
          }
        }
      }
    });
  };

  const onCancel = e => {
    setShowConfirmPasswordModel(false);
  };

  return (
    <div className="step-1-expert-form">
      <div className="an-20 medium-text success--text step-title">
        CHANGE EMAIL
      </div>
      <Form className="ant-advanced-search-form" onSubmit={handleFormSubmit}>
        <div className="form-profile-container">
          <div className="pt10">
            <Row gutter={24}>
              <Col className="device-position" xs={12} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="Current Email">
                  {getFieldDecorator("cemail", {
                    rules: [
                      {
                        type: "email",
                        message: "Invalid email address!"
                      },
                      {required: true, message: 'Current email is required'}
                    ]
                  })
                  (<Input size={'large'} placeholder="Current email" autoCapitalize="none"/>)}
                </Form.Item>
              </Col>
              <Col className="device-position" xs={12} sm={12} md={12} lg={12} xl={12}>
                <Form.Item label="New Email">
                  {getFieldDecorator("nemail", {
                    rules: [
                      {
                        type: "email",
                        message: "Invalid email address!"
                      },
                      {required: true, message: 'New email is required'}
                    ]
                  })
                  (<Input size={'large'} placeholder="New email" autoCapitalize="none"/>)}
                </Form.Item>
              </Col>
            </Row>
          </div>
        </div>
        <Form.Item className="mb0 pt25 devicept25">
          <Button type="primary"
                  loading={loader}
                  htmlType="submit"
                  className="ex__primary_btn">
            Update
          </Button>
        </Form.Item>
        <ConfirmPasswordModal visible={showConfirmPasswordModel} onCancel={onCancel} emailData={values}/>
      </Form>
    </div>
  )
}

const WrappedChangeEmail = Form.create({name: "createProfile"})(ChangeEmail);

export default WrappedChangeEmail;
