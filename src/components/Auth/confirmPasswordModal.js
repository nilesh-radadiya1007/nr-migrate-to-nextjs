import React from 'react';
import {Form, Input, Button, Modal} from "antd";
/**
 * App Imports
 */
import {AuthActions} from "../../redux/auth/events";
import {useDispatch, useSelector} from "react-redux";

const ConfirmPasswordModal = (props) => {

  const {visible, onCancel, emailData} = props;
  const {confirmPassword} = AuthActions;
  const {getFieldDecorator} = props.form;
  const dispatch = useDispatch();
  const { loader } = useSelector((state) => state.auth);

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      values.email = emailData.nemail;
      if (!err) {
        dispatch(confirmPassword(values));
      }
    });
  };

  return (
    <Modal
      title="Confirm Password"
      visible={visible}
      footer={[]}
      onCancel={onCancel}
    >
      <Form onSubmit={handleSubmit} className="ex__form">
        <Form.Item hasFeedback label="Password">
          {getFieldDecorator("password", {
            rules: [
              {min: 8, message: "Min password length 8 is required"},
              {
                required: true,
                message: "Please input your password"
              }
            ]
          })(<Input.Password placeholder="Password"/>)}
        </Form.Item>
        <Form.Item className="mb0">
          <div className="pt15 pb10">
            <Button
              loading={loader}
              type="primary"
              htmlType="submit"
              className="ex__primary_btn login-btn"
            >
              Confirm
            </Button>
          </div>
        </Form.Item>
      </Form>
    </Modal>
  )
};

const WrappedNormalRegisterForm = Form.create({name: "ConfirmPasswordModal"})(ConfirmPasswordModal);
export default WrappedNormalRegisterForm;
