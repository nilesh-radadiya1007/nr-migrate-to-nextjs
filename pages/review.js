import React from 'react';
import Review from '../src/containers/Review';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const ReviewInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Review />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default ReviewInApp;