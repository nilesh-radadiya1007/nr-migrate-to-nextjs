import '../styles/_app.scss';
import '../styles/_variable.scss'
import "react-quill/dist/quill.snow.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
