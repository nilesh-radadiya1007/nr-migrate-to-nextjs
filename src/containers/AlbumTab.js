import React, { useEffect, useState, useCallback } from 'react';
import { useSelector } from "react-redux";
import { Row, Col, Card, Button, Popover } from 'antd';
import { compose } from 'redux';
import ReactPlayer from 'react-player';
// import Image from 'next/image'

/**
 * App Imports
 */
import AppLoader from '../components/Loader';
import { CaptalizeFirst } from '../helpers/methods';
import Popup from "./Album/Popup";
import AlbumComment from "./Album/AlbumComment";
import AddVideoPopup from "./Album/AddVideoPopup";
import TripModal from "../components/Trips/TripModal";
import LikeAndShare from "../components/common/Likeandshare";


import { getMyAlbums, deleteAlbums, inActiveAlbums, activeAlbums } from "../services/expert";

const MoreTripOption = '/images/more_vert-24px.svg';
const AlbumIcon = '/images/albumIcon.png';

const AlbumTab = (props) => {
  const { token, isPublicView, publicAlbums, isAlbumRefresh } = props;
  const { role } = useSelector((state) => state.auth);
  const [loader, setLoader] = useState(false);
  const [visible, setVisible] = useState(false);
  const [albumCommentVisible, setAlbumCommentVisible] = useState(false);
  const [albumVideoVisible, setAlbumVideoVisible] = useState(false);
  const [title, setTitle] = useState("Add Album");
  const [albums, setAlbums] = useState([]);
  const [selectedAlbum, setSelectedAlbum] = useState({});
  const [selectedAlbumList, setSelectedAlbumList] = useState({});
  const [selectedAlbumComment, setSelectedAlbumComment] = useState({});
  const [copiedModal, setCopiedModal] = useState(false);
  const [isMultiple, setIsMultiple] = useState(true);

  const getData = useCallback(async (token) => {
  
    if (publicAlbums && publicAlbums.length > 0) {
    
        if (isAlbumRefresh) {
          setAlbums([]);
          setTimeout(() => {
            setAlbums(publicAlbums);
          }, 100);
        } else {
          setAlbums(publicAlbums);
        }
        setLoader(false); 
    } else {
      const result = await getMyAlbums(token);
      if (result.status === 200) {
        setAlbums(result.data.data);
        setLoader(false);
      }
    }
  }, [publicAlbums]);

  useEffect(() => {
    getData(token);
  }, [getData, token]);

  const onCreateAlbum = () => {
    setVisible(false);
    setSelectedAlbum([]);
    getData(token);
  }

  const onAddAlbum = () => {
    setAlbumVideoVisible(false);
    setSelectedAlbumList([]);
    getData(token);
  }

  const deletePhoto = async (id) => {
    const result = await deleteAlbums(token, id);
    if (result) {
      getData(token);
    }
  }

  const inActivePhoto = async (id) => {
    const result = await inActiveAlbums(token, id);
    if (result) {
      getData(token);
    }
  }

  const activePhoto = async (id) => {
    const result = await activeAlbums(token, id);
    if (result) {
      getData(token);
    }
  }

  const refreshAlbum = (type) => {
    if (isPublicView) {
      props.refreshPublicAlbum(type);
    } else {
      if (type === "albumDetail") {
        setAlbums([]);
      }
      getData(token);
    }

  }

  const albumOption = (t) => {
    return (
      <div className='profile_bg trip_poppop'>
        <div className='primary--text py5 cursor-pointer' onClick={() => [setVisible(true), setSelectedAlbum(t)]}>Edit</div>
        {/* <div className='primary--text py5 cursor-pointer'>Add/Edit Location</div>
        <div className='primary--text py5 cursor-pointer'>Add/Edit Date</div> */}
        <div className='primary--text py5 cursor-pointer' onClick={() => t.isActive ? inActivePhoto(t.id) : activePhoto(t.id)}>{`${t.isActive ? "Hide" : "Unhide"} Photo`}</div>
        <div className='border_bottom'></div>
        <div className='primary--text py5 cursor-pointer' onClick={() => [deletePhoto(t.id), setCopiedModal(true)]}>Delete Photo</div>
      </div>
    )
  };

  return (
    <div className='header-container headre_text'>
      <div className='container-fluid align-center'>
        <div className='filter_sec_bg learning_sec album_header'>
          {visible && <Popup title={title} isPublicView={isPublicView} visible={visible} handleOk={onCreateAlbum} handleCancel={() => [setVisible(false), setSelectedAlbum([])]} selectedAlbum={selectedAlbum} isMultiple={isMultiple} />}
          {albumVideoVisible && <AddVideoPopup title={title} isPublicView={isPublicView} visible={albumVideoVisible} handleOk={onAddAlbum} handleCancel={() => [setAlbumVideoVisible(false), setSelectedAlbumList([])]} selectedAlbumList={selectedAlbumList} isMultiple={false} />}
          {albumCommentVisible && <AlbumComment visible={albumCommentVisible} handleOk={() => setAlbumCommentVisible(false)} handleCancel={() => setAlbumCommentVisible(false)} selectedAlbum={selectedAlbumComment} refreshAlbum={refreshAlbum} />}
          {role === "expert" && isPublicView === false &&
            <Row gutter={40} className='new_row'>
              <Col span={10} xs={24} sm={24} md={10} lg={10} xl={10}>
                <div className='expidition_bg'>
                  <h4 className='sub_title'>Trips & Workshop Albums</h4>
                </div>
              </Col>
              <Col span={14} xs={24} sm={24} md={14} lg={14} xl={14} className='album_right_detail text-right button_fix_in_mobile'>
                <Button className='view-more-trips an-20 mt10 album_button' onClick={() => [setVisible(true), setTitle("Add Album"), setIsMultiple(true)]}>
                  Create Album
              </Button>
                <Button className='view-more-trips an-20 mt10 album_button_set' onClick={() => [setAlbumVideoVisible(true), setTitle("Add Photos /Video"), setSelectedAlbumList(albums)]}>
                  Add Photos /Video
              </Button>
              </Col>
            </Row>
          }
          <Row gutter={[40, 40]} className='mt20'>
            <Col span={8} xs={24} sm={24} md={24} lg={24} xl={24}>
              <div className='expidition_bg Album_blog'>
                <Row gutter={[15, 25]}>
                  {loader ? (
                    <div className='text-center py20 loader-absolute-class'>
                      <AppLoader />
                    </div>
                  ) : albums.length === 0 ? (
                    <div className='mb10 an-14 medium-text text-center mt100'>
                      <h4>Currently, there is no album found</h4>
                    </div>
                  ) : (
                        isPublicView === true ?
                          albums.map((album, index) => (
                            album.isActive === true && album.isDelete === false &&
                            <Col
                              span={8}
                              xs={24}
                              sm={24}
                              md={12}
                              lg={12}
                              xl={8}
                              key={index}
                              className='gutter-row trips_blog video_fix'
                            >
                              <Card
                                hoverable
                                cover={
                                  album.isVideo === true ?
                                    <div className="video_album">
                                      <div onClick={() => [setAlbumCommentVisible(true), setSelectedAlbumComment(album)]}>
                                        <ReactPlayer className="thumnail-style" url={album.images[0]} />
                                      </div>
                                      <div className="image_text_set">
                                        <div className='price_sec'>
                                          <h5 className='mb10 an-14 medium-text'>
                                            {CaptalizeFirst(album.name)}
                                          </h5>
                                        </div>
                                        <div className='heart_fill album-share Icon_line'>
                                          <input type='checkbox' id='like1' />
                                          <LikeAndShare allLikes={album.likes} id={album.id} pageType={'album'} designType="album" refreshAlbum={refreshAlbum} />

                                        </div>
                                      </div>
                                    </div>
                                    :
                                    <div className="video_album">
                                      {album.images[0].search(".mp4") != -1 ?
                                        <div onClick={() => [setAlbumCommentVisible(true), setSelectedAlbumComment(album)]}>
                                          <ReactPlayer className="thumnail-style" url={album.images[0]} />
                                        </div>
                                        :
                                        <img onClick={() => [setAlbumCommentVisible(true), setSelectedAlbumComment(album)]} alt='example' src={album.images[0]} />
                                      }
                                      <div className="mobile_icon_fix">
                                        {album.images.length >= 2 &&
                                          <div className="m_set_icon">
                                            <img src={AlbumIcon} alt={"album-icon"} />
                                          </div>
                                        }
                                      </div>
                                      <div className="image_text_set">
                                        <div className='price_sec'>
                                          <h5 className='mb10 an-14 medium-text'>
                                            {CaptalizeFirst(album.name)}
                                          </h5>
                                          {/* <p className='mb10 an-14 Album_sub_title'>
                                        <b>{CaptalizeFirst(album.country)}</b>
                                      </p> */}
                                        </div>
                                        <div className='heart_fill album-share Icon_line'>
                                          <input type='checkbox' id='like1' />
                                          <LikeAndShare allLikes={album.likes} id={album.id} pageType={'album'} designType="album" refreshAlbum={refreshAlbum} />
                                        </div>
                                      </div>
                                    </div>
                                }
                                extra={
                                  <div>
                                    {album.images.length >= 2 &&
                                      <div className="album-icon public-album-icon">
                                        <img src={AlbumIcon} alt={"album-icon"} />
                                      </div>
                                    }
                                  </div>
                                }
                              >

                              </Card>
                            </Col>
                          ))
                          :
                          albums.map((album, index) => (
                            album.isDelete === false &&
                            <Col
                              span={8}
                              xs={24}
                              sm={24}
                              md={12}
                              lg={12}
                              xl={8}
                              key={index}
                              className='gutter-row trips_blog video_fix'
                            >
                              <Card
                                hoverable
                                cover={
                                  album.isVideo === true ?
                                    <div className="video_album">
                                      <div onClick={() => [setAlbumCommentVisible(true), setSelectedAlbumComment(album)]}>
                                        <ReactPlayer className="thumnail-style" url={album.images[0]} />
                                      </div>
                                      <div className="image_text_set">
                                        <div className='price_sec'>
                                          <h5 className='mb10 an-14 medium-text'>
                                            {CaptalizeFirst(album.name)}
                                          </h5>
                                        </div>
                                        <div className='heart_fill album-share Icon_line'>
                                          <input type='checkbox' id='like1' />
                                          <LikeAndShare allLikes={album.likes} id={album.id} pageType={'album'} designType="album" refreshAlbum={refreshAlbum} />
                                        </div>
                                      </div>
                                    </div>
                                    :
                                    <div className="video_album">
                                      {album.images[0].search(".mp4") != -1 ?
                                        <div onClick={() => [setAlbumCommentVisible(true), setSelectedAlbumComment(album)]}>
                                          <ReactPlayer className="thumnail-style" url={album.images[0]} />
                                        </div>
                                        :
                                        <img onClick={() => [setAlbumCommentVisible(true), setSelectedAlbumComment(album)]} alt='example' src={album.images[0]} />
                                      }
                                      <div className="mobile_icon_fix">
                                        {album.images.length >= 2 &&
                                          <div className="m_set_icon">
                                            <img src={AlbumIcon} alt={"album-icon"} />
                                          </div>
                                        }
                                      </div>
                                      <div className="image_text_set">
                                        <div className='price_sec'>
                                          <h5 className='mb10 an-14 medium-text'>
                                            {CaptalizeFirst(album.name)}
                                          </h5>
                                          {/* <p className='mb10 an-14 Album_sub_title'>
                                        <b>{CaptalizeFirst(album.country)}</b>
                                      </p> */}
                                        </div>
                                        <div className='heart_fill album-share Icon_line'>
                                          <input type='checkbox' id='like1' />
                                          <LikeAndShare allLikes={album.likes} id={album.id} pageType={'album'} designType="album" refreshAlbum={refreshAlbum} />
                                        </div>
                                      </div>
                                    </div>
                                }
                                extra={role === "expert" &&
                                  <div>
                                    {album.images.length >= 2 &&
                                      <div className="album-icon">
                                        <img src={AlbumIcon} alt={"album-icon"} />
                                      </div>
                                    }
                                    <Popover
                                      placement='bottom'
                                      content={albumOption(album)}
                                      trigger='hover'
                                    >
                                      <div className='more_icon_upcoming_tips'>
                                        <img
                                          src={MoreTripOption}
                                          alt='MoreTrip_Option'
                                        />
                                      </div>
                                    </Popover>
                                  </div>
                                }
                              >
                              </Card>
                            </Col>
                          ))
                      )}
                </Row>
              </div>
            </Col>
          </Row>
        </div>
      </div>
      {copiedModal && (
        <TripModal
          visible={copiedModal}
          title={"Successfully deleted"}
          message={`Your album deleted successfully.`}
          onClose={() => setCopiedModal(false)}
        />
      )}
    </div>
  );
};

export default compose(AlbumTab);
