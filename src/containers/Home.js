import React, { useState } from "react";
import { compose } from "redux";
import HomePageSection2 from '../containers/Home/HomePageSection2.js';
// import HomePageSection1 from "./Home/HomePageSection1";
import HomePageHeader1 from "./Home/HomePageHeader1";
import { useRouter } from 'next/router';

const Home = (props) => {
    const router = useRouter()
	const [activityName, setActivityName] = useState("");
	
	const handleSearch = (e) => {
        e.preventDefault();
        let searchData = [];
        if (activityName !== "") {
            searchData.push(activityName);
        }
        let act = searchData.length ? JSON.stringify(searchData) : "";
        if (activityName !== "") {
            router.push(`/all-adventure?activity=${act}&isFlexible=true`)
        }else{
            router.push(`/all-adventure`)
        }
    }

	return (
		<div className="mainhomepage">
            <HomePageHeader1 handleSearch={handleSearch} setActivityName={setActivityName} {...props} />
			{/* <HomePageSection1 handleSearch={handleSearch} setActivityName={setActivityName} {...props}/> */}
			<HomePageSection2 {...props}/>
		</div >
	);
};
export default compose(Home);
