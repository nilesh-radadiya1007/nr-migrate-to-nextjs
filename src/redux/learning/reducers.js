import {
  LEARNING_STEP_1, LEARNING_STEP_2,
  LEARNING_STEP_3, ERROR_CREATE_LEARNING, GO_TO_TAB,
  UPDATE_LEARNING, UPDATE_LEARNING_SUCCESS
} from "../../helpers/actions";

const initState = {
  id: null,
  cover: null,
  originalCover: null,
  title: null,
  duration: null,
  durationType: 'days',
  price: 0,
  discount: "",
  priceType: 3,
  skill: [],
  activity: [],
  langauge: [],
  dateTime: [],
  coordinates: [],
  workshopType: null,
  workshopMedium: null,
  country: null,
  meetingPoint: "",
  description: null,
  whatLearn: null,
  attend: null,
  accomodation: null,
  itenary: null,
  inclusion: null,
  exclusion: null,
  extras: null,
  cancellantion: null,
  participants: null,
  groupType: 1,
  dateType: 1,
  step1: false,
  step2: false,
  step3: false,
  loader: false,
  tab: 1,
  editMode: false,
  season: '',
  accomodations: [],
  suitable: [],
  discountType: 1,
  certification: false
}

function learning (state = initState, action) {
  switch (action.type) {
    case LEARNING_STEP_1:
      return {
        ...state,
        title: action.data.title,
        cover: action.data.cover,
        originalCover: action.data.originalCover,
        price: action.data.price,
        discount: action.data.discount,
        priceCurrency: action.data.priceCurrency,
        priceType: action.data.priceType,
        skill: action.data.skill,
        activity: JSON.stringify(action.data.activity),
        duration: action.data.duration,
        durationType: action.data.durationType,
        langauge: JSON.stringify(action.data.langauge),
        workshopType: action.data.workshopType,
        workshopMedium: action.data.workshopMedium,
        participants: action.data.participants,
        groupType: action.data.groupType,
        tab: 2,
        step1: true,
        season: action.data.season,
        suitable: action.data.suitable,
        discountType: action.data.discountType,
        certification: action.data.certification
      }
    case LEARNING_STEP_2:
      return {
        ...state,
        coordinates: action.data.location,
        country: action.data.country,
        address: action.data.address,
        meetingPoint: action.data.meetingPoint,
        description: action.data.description,
        whatLearn: action.data.whatLearn,
        attend: action.data.attend,
        dateTime: JSON.stringify(action.data.dateTime),
        dateType: action.data.dateType,
        step2: true,
        tab: 3
      }

    case LEARNING_STEP_3:
      return {
        ...state,
        accomodation: action.data.accomodation,
        itenary: JSON.stringify(action.data.itenary),
        inclusion: action.data.inclusion,
        exclusion: action.data.exclusion,
        extras: action.data.extras,
        cancellantion: action.data.cancellantion,
        step3: true,
        loader: true
      };
    case UPDATE_LEARNING:
      return {
        ...state,
        accomodation: action.data.accomodation,
        itenary: action.data.itenary,
        inclusion: action.data.inclusion,
        exclusion: action.data.exclusion,
        extras: action.data.extras,
        cancellantion: action.data.cancellantion,
        step3: true,
        loader: true
      }
    case UPDATE_LEARNING_SUCCESS:
      return {
        ...state,
        id: action.data._id
      }
    case ERROR_CREATE_LEARNING:
      return {
        ...state,
        loader: false,
      }
    case GO_TO_TAB:
      return {
        ...state,
        tab: action.data
      }
    case 'SET_UPDATE_DATA':

      return {
        id: action.data._id,
        cover: action.data.cover,
        originalCover: action.data.originalCover,
        title: action.data.title,
        duration: action.data.duration,
        durationType: (typeof action.data.durationType === "undefined" || action.data.durationType === "") ? 'days' : action.data.durationType,
        priceType: (typeof action.data.priceType === "undefined" || action.data.priceType === "" || action.data.priceType === null) ? 3 : action.data.priceType,
        price: action.data.price,
        discount: (typeof action.data.discount !== "undefined" && action.data.discount !== "" && action.data.discount !== null) ? action.data.discount : "",
        skill: action.data.skill,
        activity: JSON.stringify(action.data.activity),
        langauge: JSON.stringify(action.data.langauges),
        dateTime: JSON.stringify(action.data.dateTime),
        coordinates: action.data.location.coordinates,
        workshopType: action.data.workshopType,
        workshopMedium: action.data.workshopMedium,
        country: action.data.country,
        address: action.data.address,
        meetingPoint: action.data.meetingPoint !== "null" ? action.data.meetingPoint : "",
        description: action.data.description,
        whatLearn: action.data.whatLearn,
        attend: action.data.attend,
        accomodation: action.data.accomodation,
        itenary: JSON.stringify(action.data.itenary),
        inclusion: action.data.inclusion,
        exclusion: action.data.exclusion,
        gettingThere: action.data.gettingThere || "",
        transfer: action.data.transfer || "",
        equipmentsGears: action.data.equipmentsGears || "",
        covidGuidlines: action.data.covidGuidlines || "",
        extras: action.data.extras,
        cancellantion: action.data.cancellantion,
        participants: action.data.participants,
        groupType: (typeof action.data.groupType === "undefined" || action.data.groupType === "" || action.data.groupType === null) ? 1 : action.data.groupType,
        dateType: action.data.dateType,
        step1: false,
        step2: false,
        step3: false,
        loader: false,
        tab: 1,
        editMode: true,
        accomodations: typeof action.data.accomodations !== "undefined" && action.data.accomodations.length > 0 ? JSON.stringify(action.data.accomodations) : [],
        season: (typeof action.data.season === "undefined" || action.data.season === "" || action.data.season === null) ? "" : action.data.season,
        suitable: (typeof action.data.suitable === "undefined" || action.data.suitable === "" || action.data.suitable === null) ? "" : action.data.suitable,
        discountType: (typeof action.data.discountType === "undefined" || action.data.discountType === "" || action.data.discountType === null) ? 1 : action.data.discountType,
        certification: (typeof action.data.certification === "undefined" || action.data.certification === "" || action.data.certification === null || action.data.certification === false) ? false : action.data.certification,

      }
    case "CLEAR_LEARNINGS_FIELDS":
      return {
        ...initState
      }
    default:
      return state;
  }
}

export default learning
