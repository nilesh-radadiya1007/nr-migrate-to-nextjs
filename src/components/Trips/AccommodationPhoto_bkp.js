/* eslint-disable react/display-name */
import React, { useEffect, useState } from 'react';
import { useSelector } from "react-redux";
import { Button, Form, Input, Modal, Dropdown, Menu, Upload, Spin } from 'antd';
// import PlacesAutocomplete, { getLatLng, geocodeByAddress } from 'react-places-autocomplete';
// import moment, { months } from 'moment';
// import { DownOutlined } from '@ant-design/icons';

// import VideoImage from '../../../public/images/video-icon.jpg';
import { getBase64, beforeUpload } from '../../../../helpers/methods';
import { uploadAccommodationPhotos } from '../../services/expert';

const UploadImage = '/images/cover_upload.png';

const uploadButton = (
  <div>
    <img
      src={UploadImage}
      alt='cover'
      style={{
        width: '100%',
        height: '100%',
        objectFit: 'fill',
      }}
    />
  </div>
);

const AccommodationPhoto = React.memo((props) => {

  const token = useSelector((state) => state.auth.accessToken);
  const { visible, id, onCloseClick, getRefreshTrip, type} = props;
  const { getFieldDecorator, setFieldsValue } = props.form;

  const [isMultiple, setIsMultiple] = useState(true);
  const [imageUrl, setImageUrl] = useState('');
  const [loding, setLoding] = useState(false);
  const [fileList, setFileList] = useState([]);

  useEffect(() => {
    setFieldsValue({
      fileList,
    });
  }, [fileList]);

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoding(true);
    props.form.validateFields(async (err, values) => {
      if (!err) {
        
        const formData = new FormData();
        
          values.fileList.map((pic) => {
            formData.append("pictures", pic.originFileObj);
          });

          formData.append("type", type);
          
          const result = await uploadAccommodationPhotos(token, formData,id);
          if (result) {
            getRefreshTrip();
            setLoding(false);
            onCloseClick();
            setImageUrl('');
            setFileList([]);
            setFieldsValue({
              fileList: [],
            });            
          }
      } else {
        setLoding(false);
      }
    });
  };

  const removeByAttr = function (arr, attr, value) {
    var i = arr.length;
    while (i--) {
      if (arr[i]
        && arr[i].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[i][attr] === value)) {

        arr.splice(i, 1);

      }
    }
    setFileList([...arr]);
    if (arr.length > 0) {
      if (arr[0].originFileObj) {
        getBase64(arr[0].originFileObj, (imageUrl) => {
          setImageUrl(imageUrl);
        });
      } else {
        setImageUrl(arr[0].url)
      }
    } else {
      setImageUrl('');
    }
    return arr;
  }

  const onChangeImg = ({ fileList: newFileList }) => {
    
    if (newFileList[0].originFileObj) {
      
        getBase64(newFileList[0].originFileObj, (imageUrl) => {
          setImageUrl(imageUrl);
        });
      
    }
    setFileList(newFileList);
  };

  const onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  return (
    <Modal
      visible={visible}
      title={"Upload Accommmodation Photos"}
      onCancel={onCloseClick}
      footer={false}
      className="header_text section-set"
    >
      <div className={`step-1-expert-form height_sec_enthu learn_sec ${fileList.length === 0 && "img-set-icon"}`}>
        <Form onSubmit={handleSubmit}>
          <div className='form-profile-container pt20'>
            <Form.Item label='' className='mb20'>
              {getFieldDecorator('fileList', {
                rules: [{ required: true, message: 'Please Upload Images!' }],
              })(
                <>
                  {fileList.length > 0 &&
                    <div className='flex-y center'>
                      <img
                        src={imageUrl}
                        alt='cover'
                        style={{
                          width: imageUrl ? '100%' : '',
                          height: imageUrl ? '100%' : '',
                          objectFit: 'fill',
                        }}
                      />
                      {!imageUrl && (
                        <span className='an-14 regular-text mt20'>
                          {isMultiple ? "Drag & Drop/Click here to upload multiple Image"
                            : "Drag & Drop/Click here to add photos/videos in an Album"}
                        </span>
                      )}
                    </div>
                  }
                  <Upload
                    accept={'image/*'}
                    listType={'picture-card'}
                    multiple={isMultiple ? true : false}
                    showUploadList={{ showPreviewIcon: false, showDownloadIcon: false }}
                    fileList={fileList}
                    onChange={onChangeImg}
                    onPreview={onPreview}
                    beforeUpload={beforeUpload}
                    customRequest={({ file, onSuccess }) => {
                      setTimeout(() => onSuccess('ok'), 0)
                    }}
                    className="uplord_img"
                    onRemove={(d) => removeByAttr(fileList, 'uid', d.uid)}
                  >
                    {fileList.length === 0 &&
                      <div className='flex-y center zero-img'>
                        <img
                          src={UploadImage}
                          alt='cover'
                          style={{
                            width: '100%',
                            height: '100%',
                            objectFit: 'fill',
                          }}
                        />
                        <span className='an-14 regular-text mt20'>
                          {isMultiple ? "Drag & Drop/Click here to upload multiple Image"
                            : "Drag & Drop/Click here to add photos/videos in an Album"}
                        </span>
                      </div>}
                    {fileList.length > 0 && uploadButton}
                  </Upload>
                </>
              )}
            </Form.Item>
            <Form.Item className="uplord_button text-right">
              {loding ? <Spin tip="Loading..." /> :
                <Button
                  htmlType='submit'
                  type='primary'
                  className='ant-btn ex__primary_btn text-upper ant-btn-primary'
                  // loading={loading}
                  disabled={loding ? true : false}
                >
                  Upload
              </Button>
              }
            </Form.Item>
          </div>
        </Form>
      </div>
    </Modal>
  );
});

export default Form.create({ name: 'album' })(AccommodationPhoto);
