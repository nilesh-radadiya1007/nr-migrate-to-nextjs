import React, { Fragment, useEffect, useState } from 'react';
import { compose } from "redux";
import { Row, Col } from 'antd';
/**
 * App Imports
 */
import { CaptalizeFirst } from "../../helpers/methods";
import SingleCard from "../../components/common/SingleCard";
import { useRouter } from 'next/router';

const ApiKey = process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY;


const TripsWorkshopCardView = (props) => {
  const router = useRouter();
  const { upcomingTrip, pageMinValue, pageMaxValue, isLogin, activeTrip, inActiveTrip, copyTrip, isPublicView, activeWorkshop, inActiveWorkshop, travelMap } = props;

  const [load, setLoad] = useState(false);

  const onTripClick = (id) => {
    if (isPublicView) {
      window.open(`/trips-details/${id}`, "_blank");
      return;
    }
    window.open(`/trip-edit/${id}`, "_blank");
  };

  useEffect(() => {
    setLoad(true);
    setTimeout(() => {
      setLoad(false);
    }, 100);
  }, [pageMinValue, pageMaxValue])

  const tripsOption = (t) => {
    var id = t._doc ? t._doc._id : t.id
    var detail = t._doc ? t._doc : t;
    return (
      <div className="profile_bg trip_poppop">
        {/* <div className="primary--text py5 cursor-pointer">
          Trip Fully Booked
      </div>
        <div className="border_bottom"></div> */}
        <div className="primary--text py5 cursor-pointer" onClick={() => router.push(`/update-trip/${id}`)}>
          Edit Trip
      </div>
        <div className="primary--text py5 cursor-pointer" onClick={() => copyTrip(detail)}>
          Copy Trip
      </div>
        <div className="primary--text py5 cursor-pointer" onClick={() => t.active ? inActiveTrip(id) : activeTrip(id)}>
          {detail.active ? "Deactivate Trip" : "Activate Trip"}
        </div>
        {/* <div className="border_bottom"></div>
        <div className="primary--text py5 cursor-pointer">
          Delete Trip
      </div> */}
      </div>
    )
  }

  const workshopsOption = (t) => {
    return (
      <div className="profile_bg trip_poppop">
        {/* <div className="primary--text py5 cursor-pointer">
          Workshop Fully Booked
      </div>
        <div className="border_bottom"></div> */}
        <div className="primary--text py5 cursor-pointer" onClick={() => router.push(`/update-learning/${t.id}`)}>
          Edit Workshop
      </div>
        <div className="primary--text py5 cursor-pointer" onClick={() => props.copyWorkShop(t)}>
          Copy Workshop
      </div>
        <div className="primary--text py5 cursor-pointer" onClick={() => t.active ? inActiveWorkshop(t.id) : activeWorkshop(t.id)}>
          {t.active ? "Deactivate Workshop" : "Activate Workshop"}
        </div>
        {/* <div className="border_bottom"></div>
        <div className="primary--text py5 cursor-pointer">
          Delete Workshop
      </div> */}
      </div>
    )
  }

  if (upcomingTrip) {
    return (
      <Fragment>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <div className="expidition_bg expendition new-card-design">
              <Row gutter={[15, 25]}>
                {upcomingTrip.slice(pageMinValue, pageMaxValue).map((t, index) => (
                  <Col xs={24} sm={24} md={12} lg={8} xl={8} key={index} className="gutter-row trips_blog">
                    <SingleCard
                      t={t}
                      index={index}
                      type='expertAdventureCard'
                      isLogin={isLogin}
                      workshopsOption={workshopsOption}
                      tripsOption={tripsOption}
                      isPublicView={isPublicView}
                    />
                  </Col>
                ))}
              </Row>
            </div>
          </Col>
        </Row>
      </Fragment>
    )
  } else {
    return null;
  }
}

export default compose(TripsWorkshopCardView)
