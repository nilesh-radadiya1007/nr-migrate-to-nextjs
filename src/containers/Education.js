import React from 'react';

const ComingSoon = '/images/coming-soon.png'

const Education = () => {
  return (
    <div >
      <img style={{ width: '85%', height: '90vh', marginTop: -15 }} src={ComingSoon} alt="coming-soon"/>
    </div>
  )
}

export default Education
