import React from 'react';
import dynamic from 'next/dynamic';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';


const Messages = dynamic(
    () => import("../src/containers/Message"),
    { ssr: false }
);

const MessagesInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <Messages />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default MessagesInApp;