import React, { useEffect, useState } from "react";
import ImageUploading from "react-images-uploading";
import { Button, Menu, Dropdown, Modal } from "antd";
import ImageReposition from "../image-crop/image-reposition";
import { LoadingOutlined } from "@ant-design/icons";
import { useMediaQuery } from "react-responsive";
// import Image from 'next/image';
const UploadImage = "/images/cover_upload.png";

const ImageUplaoderAndCropper = ({
  setCoverImage,
  originalCover,
  uploadingLoader,
  imageUrl,
  page,
  setProfileImage,
  removingLoader
}) => {
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const [reposition, setReposition] = useState(false);
  const [coverImageUrl, setCoverImageUrl] = useState(
    imageUrl ? imageUrl : undefined
  );
  const [showIsEdit, setShowIsEdit] = useState(originalCover ? true : false);
  const [originalCoverImageUrl, setOriginalCoverImageUrl] = useState(
    originalCover ? originalCover : undefined
  );
  const [images, setImages] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  useEffect(() => {
    setCoverImageUrl(imageUrl);
    setShowIsEdit(originalCover? true : false);
    setOriginalCoverImageUrl(originalCover);
  }, [imageUrl, originalCover]);
  //on uploading the image
  const onChange = (imageList) => {
    setImages(imageList);
    setShowIsEdit(imageList[0].data_url ? true : false);
    setOriginalCoverImageUrl(imageList[0].data_url);
    setReposition(true);
  };

  //close modal
  const handleOk = () => {
    setIsModalVisible(false);
  };

  //close modal
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  //open modal
  const onError = (data) => {
    setIsModalVisible(true);
  };

  const saveCropperdImage = (croppedFileUrl) => {
    if(!page){
      setCoverImageUrl(croppedFileUrl);
    }else{
      setProfileImage(croppedFileUrl)
    }
    setReposition(false);
    var file = dataURLtoFile(croppedFileUrl, "image.png");
    if (images && images.length > 0) {
      var fileOriginal = dataURLtoFile(images[0].data_url, "image1.png");
      setCoverImage(file, fileOriginal);
    } else {
      setCoverImage(file, undefined);
    }
  };

  //dataURL to File
  const dataURLtoFile = (dataurl, filename) => {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  };

  const getCropSize = ()=>{
    if(page){
      if(!isMaxWidth768){
        return { width: 300, height: 300 }
      }else{
        return { width: 150, height: 150 }
      }
    }else{
      if(!isMaxWidth768){
        return { width: 1150, height: 500 }
      }else{
        return { width: 320, height: 160 }
      }
    }
  }

  const getMinHeight = ()=>{
    return 100
  }

  const getminWidth = ()=>{
    return 100
  }

  return (
    <>
      {isModalVisible && (
        <Modal
          title=""
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <h3>
            <p> Image Upload Error.</p>
            Minimum image size should be <b>{getminWidth()} px * {getMinHeight()} px</b>. Please upload
            a bigger image.
          </h3>
        </Modal>
      )}

      {reposition === true && (
        <ImageReposition
          saveCropperdImage={(evt) => saveCropperdImage(evt)}
          setReposition={(evt) => setReposition(evt)}
          originalCover={originalCoverImageUrl}
          page={page}
          cropSize={getCropSize()}
        />
      )}
      <ImageUploading 
        Uploading
        multiple={false}
        value={images}
        onChange={onChange}
        dataURLKey="data_url"
        resolutionType="more"
        resolutionWidth={getminWidth()}
        resolutionHeight={getMinHeight()}
        onError={onError}
      >
        {({ imageList, onImageUpload }) => (
          <>
            {reposition === false && (
              <>
                {!page && (
                  <>
                    <img
                      onClick={onImageUpload}
                      src={coverImageUrl || UploadImage}
                      alt="cover"
                      className="mb5"
                      // width="100%"
                    />
                    {uploadingLoader && (
                      <p className="drag-info">
                        <LoadingOutlined /> Uploading Image...
                      </p>
                    )}
                    {removingLoader && (
                      <p className="drag-info">
                        <LoadingOutlined /> Removing Image...
                      </p>
                    )}
                  </>
                )}
              </>
            )}
            {reposition === false && !coverImageUrl ? (
              <span>{!page ? "Upload Cover picture" : ""}</span>
            ) : (
              ""
            )}
            <div className="upload__image-wrapper">
              {showIsEdit ? (
                <Dropdown
                  trigger={["click"]}
                  overlay={
                    <Menu>
                      <Menu.Item onClick={onImageUpload} key="upload">
                        {!page ? "Upload Cover Image": "Upload Profile Image"}
                      </Menu.Item>
                      <Menu.Item
                        onClick={() => setReposition(true)}
                        key="reposition"
                      >
                        Reposition Image
                      </Menu.Item>
                      {!page && (
                        <Menu.Item
                        onClick={() => {
                          setCoverImageUrl(undefined);
                          setOriginalCoverImageUrl(undefined);
                          setCoverImage("", "");
                          setShowIsEdit(false);
                        }}
                        key="reposition"
                      >
                        Remove Image
                      </Menu.Item>
                      )}
                      
                    </Menu>
                  }
                  placement="bottomCenter"
                  arrow
                >
                  <Button
                    style={{ margin: "40px" }}
                    className={page ? "edit-profile-btn edit_btn medium-text an-14 cover-img-edit" : "edit_btn medium-text an-14 cover-img-edit"}
                  >
                    {!page ? "Edit Cover Image": "Edit Profile Image"}
                  </Button>
                </Dropdown>
              ) : (
                <Button
                  style={{ margin: "40px" }}
                  className="edit_btn medium-text an-14 cover-img-edit"
                  onClick={onImageUpload}
                >
                  {!page ? "Upload Cover Image" : "Upload profile picture"}
                </Button>
              )}
            </div>
          </>
        )}
      </ImageUploading>
    </>
  );
};

export default ImageUplaoderAndCropper;
