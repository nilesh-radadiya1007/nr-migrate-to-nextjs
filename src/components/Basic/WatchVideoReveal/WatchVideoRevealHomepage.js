import React, { useEffect, useCallback } from "react";
import s from "./WatchVideoReveal.module.scss";
import { compose } from "redux";
import ReactPlayer from "react-player";
import { useState } from "react";
import cx from "classnames";
import { useMediaQuery } from "react-responsive";

const WatchVideoRevealHomepage = (props) => {
  const [isFullScreen, setisFullScreen] = useState(false);
  const isMaxWidth1200 = useMediaQuery({ query: "(max-width: 1200px)" });

  const setFullScreen = useCallback(() => {
    setisFullScreen(true);
    // props.setIsFullScreenVideo(true);
    const element = document.getElementsByClassName("header-container1")[0];
    element.style.background = "rgba(255, 255, 255, 0.6)";
  });

  useEffect(() => {
    setTimeout(() => {
      const element = document.getElementsByClassName("header-container1")[0];
      if (element){
        // console.log("element", element);
        element.style.background = "rgba(255, 255, 255, 0.6)";
        setFullScreen();
      }
    }, 10000);

    
     const element = document.getElementsByClassName("header-container1")[0];

     if (element){
      element.style.background = "rgba(255, 255, 255, 0.0)";
     }
  }, []);

  useEffect(() => {
    if (props.isFullScreenVideo) {
      setFullScreen();
    }
  }, [props.isFullScreenVideo, setFullScreen]);

  return (
    <>
      <div
        className={cx(s.WatchVideoReveal, {
          [s.fullScreen]: isFullScreen,
        })}
      >
        <ReactPlayer
          url="./home-video.mp4"
          loop
          playing={isFullScreen}
          muted
          className={s.ReactPlayer}
        />
        <div className={s.Mask}></div>
      </div>
    </>
  );
};

export default compose(WatchVideoRevealHomepage);
