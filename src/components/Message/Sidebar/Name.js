import React from 'react';

import { CaptalizeFirst } from '../../../helpers/methods';

const Name = ({ msg }) => {
  if (msg.type === 'trip') {
    return (
      <h5>
        {CaptalizeFirst(msg.trip.title)}
      </h5>
    );
  }
  return (
    <h5>
      {CaptalizeFirst(msg.learning.title)}
    </h5>
  );
};

export default Name;
