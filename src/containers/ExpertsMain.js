import React, { useEffect } from "react";

import { useSelector } from "react-redux";
// import BasicPageWrapper from '../components/common/BasicPageWrapper';
// import BasicWrapperProvider from '../components/common/BasicWrapperProvider';

import Home from "./Home";
import ExpertsHomePanel from "./ExpertsHomePanel"
import { compose } from "redux";
import { useRouter } from "next/router";

const ExpertsMain = (props) => {
    const router = useRouter();
    const auth = useSelector(state => state.auth);
    const expert = useSelector(state => state.expert);
    const enthu = useSelector(state => state.enthu);

    // useEffect(() => {
    //     console.log(auth);
    //     if (!auth.accessToken) {
    //         return router.push('/');
    //     }
    // }, [])


    if (auth.isLogin && (auth.role === 'expert' || auth.role === 'enthusiasts')) {
        return (
            // <BasicWrapperProvider>
            //     <BasicPageWrapper>
            <ExpertsHomePanel expert={expert} enthu={enthu} />
            //      </BasicPageWrapper>
            // </BasicWrapperProvider> 
        );
    }
    return <Home />;
};

// function mapStateToProps({ auth, expert, enthu }) {
//     return {
//         auth,
//         expert,
//         enthu
//     }
// }

export default compose(ExpertsMain);

