import React, { Fragment } from "react";
import { compose } from "redux";
import { Row, Col, Rate, Card, Popover } from "antd";
import OwlCarousel from "react-owl-carousel";
import { CaptalizeFirst, commaSepratorString, getCityFromLocation, commaSepratorStringCaps, commaSepratorStringSingle } from "../../../helpers/methods";

const languageIcon = '/images/speaks_ic_2x.png';
const followersIcon = '/images/newicon/followers_icon.svg';
// const followersIcon = '/images/newicon/followers_icon.png';
const locationIcon = "/images/newicon/location_icon.png";

const sliderOptions = {
    margin: 35,
    nav: true,
    responsive: {
        0: {
            items: 1,
            nav: true,
            dotsEach: 3,
        },
        768: {
            items: 2,
            nav: true,
        },
        991: {
            items: 3,
            nav: true,
        },
    },
};

const ExpertCardForHomePage = (props) => {
    const { experts, view, index, type } = props;
    let reviewArray = [15, 11, 14, 12, 10, 13, 15, 10, 13, 12, 10, 13];  // Temporary solution to display different number of review to card

    const onCardClick = (id) => window.open(`/expert-profile/${id}`, "_blank");


    return (
        <Row>
            <Col xs={24} sm={24} md={24} lg={24} xl={24} className="pt25 pb25 accomo_thumb">
                <OwlCarousel
                    className="owl-theme modular-card"
                    {...sliderOptions}
                    dots={true}
                >
                    {experts.map((expert, index) => {
                        return (
                            <Card
                                hoverable
                                onClick={() => onCardClick(expert.id)}
                                cover={
                                    // <div style={{width: "100%", height: "100%"}}>
                                        <img alt="example" src={expert.profile} />
                                    // </div>
                                }
                                key={index}
                            >

                                <div className="">

                                    <div className="expert_card_details">
                                        <p className="mb10 expert-title">{`${CaptalizeFirst(expert.firstName)} ${CaptalizeFirst(expert.lastName)}`}</p>

                                        <p className="mb20 expert_card_title">
                                            <Popover placement="bottomLeft" content={`${commaSepratorString(expert.experties)}`} trigger="hover" overlayClassName="card-title-hover">
                                                {`${commaSepratorStringSingle(expert.experties)}`}
                                            </Popover>

                                        </p>
                                        <p className="mb10 expert_card_loc">
                                            <img src={locationIcon} alt="location" className="location_icon" />{" "}
                                            {getCityFromLocation(expert.city)}{CaptalizeFirst(expert.country)}
                                        </p>
                                        <p className="mb10 expert_card_lang">
                                            <img src={languageIcon} alt='language' className='languageIcon' id='languageIcon' />
                                            {commaSepratorStringCaps(expert.speaks)}

                                        </p>
                                        <p className="mb10 expert_card_follow">
                                            <img src={followersIcon} alt='followerIcon' className='followerIcon' />
                                            No Followers
                                        </p>
                                        <div className="mb10 expert_review-text">
                                            <Rate
                                                allowHalf
                                                defaultValue={5}
                                                className="an-14"
                                                style={{ color: "#FFBC00" }}
                                            /><span className="review-text-expert">{typeof reviewArray[index] !== "undefined" ? reviewArray[index] : 10} Reviews</span>
                                        </div>

                                    </div>
                                </div>

                            </Card>
                        );
                    })}
                </OwlCarousel>
            </Col>

        </Row>
    );
};

export default compose(ExpertCardForHomePage);
