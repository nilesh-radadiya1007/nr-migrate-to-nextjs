import React from "react";
import { Modal, Button } from "antd";
// import { withRouter } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { compose } from "redux";
// import Image from 'next/image';
import { useRouter } from 'next/router';
/**
 * Image Import
 */

/**
 * App Imports
 */
import { ModalActions } from '../redux/models/events';
const Success = "/images/success_ic.png";

const SuccessModel = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { closeUpdateModal } = ModalActions;
 
  const trips = useSelector(state => state.trips);

  const handleClick = () => {
    dispatch(closeUpdateModal());
    props.onClose();
    if(props.copyType === "trip"){
      router.push('/trip-edit/' + props.copyId);
    }else{
      router.push('/learning-edit/' + props.copyId);
    }
   
  };

  return (
    <Modal
      centered
      className="auth-modal success-modal"
      width={380}
      closable={false}
      maskClosable={false}
      visible={props.visible}
    >
      <div className="text-center">
        <img src={Success} alt="" />
        <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
          {props.title}
        </h1>
        <p className="an-18 mb20 regular-text">
          {props.message}
        </p>
        <Button type="primary" className="done_btn medium-text an-16 ex__primary_btn" onClick={handleClick}>
          Done
          </Button>
      </div>
    </Modal>
  );
};

export default compose(SuccessModel);
