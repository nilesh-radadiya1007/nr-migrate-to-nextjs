import React from "react";
import { Modal, Button } from "antd";
import Link from "next/link";
import { useDispatch, useSelector } from 'react-redux';
import { compose } from "redux";
// import Image from 'next/image'
import { useRouter } from 'next/router';
/**
 * Image Import
 */

/**
 * App Imports
 */
import { ModalActions } from '../../redux/models/events';

const Success = "/images/success_ic.png";

const CreateLearningSuccess = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const learning = useSelector(state => state.learning);
  const { closeSuccessModal } = ModalActions;

  const handleClick = () => {
    dispatch(closeSuccessModal());
    setTimeout(() => {
      router.push('/learning-edit/' + learning.id);
    }, 100)
  };

  return (
    <Modal
      centered
      className="auth-modal success-modal"
      width={380}
      closable={false}
      maskClosable={false}
      visible={props.visible}
    >
      <div className="text-center">
        <img src={Success} alt="" />
        <h1 className="text-upper medium-text an-26 text-success mb15 mt15">
          Successfully Created
        </h1>
        <p className="an-18 mb20 regular-text">
          Your workshop is created.
        </p>
        <Link href="/">
          <a className="done_btn medium-text an-16">
            <Button type="primary" className="ex__primary_btn" onClick={handleClick}>
              Done
            </Button>
          </a>
        </Link>
      </div>
    </Modal>
  );
};

export default compose(CreateLearningSuccess);
