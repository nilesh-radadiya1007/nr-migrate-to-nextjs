import React, { Fragment, useState } from "react";
import { compose } from "redux";
import { Row, Col, Rate, Card, Popover } from "antd";
import OwlCarousel from "react-owl-carousel";
import { CaptalizeFirst, getColorLogoURL, DayorDaysNightOrNights, displayDifficultyText, skillLevelText, getCurrencySymbol, commaSepratorString } from "../../../helpers/methods";
import LikeAndShare from "../../../components/common/LikeandshareSingle";
import ReactHtmlParser from "react-html-parser";
const whatFacinatesYouImage1 = "/images/home/whatFacinatesYouImage3.svg";
const whatFacinatesYouImage2 = "/images/home/whatFacinatesYouImage2.svg";
const whatFacinatesYouImage3 = "/images/home/trackingImage.svg";
const whatFacinatesYouImage4 = "/images/home/whatFacinatesYouImage4.svg";
// const whatFacinatesYouImage1 = "/images/home/whatFacinatesYouImage3.png";
// const whatFacinatesYouImage2 = "/images/home/whatFacinatesYouImage2.png";
// const whatFacinatesYouImage3 = "/images/home/trackingImage.png";
// const whatFacinatesYouImage4 = "/images/home/whatFacinatesYouImage4.png";

const staticImages = [whatFacinatesYouImage1, whatFacinatesYouImage2, whatFacinatesYouImage3, whatFacinatesYouImage4];

const sliderOptions = {
  margin: 35,
  nav: true,
  responsive: {
    0: {
      items: 1,
      nav: true,
      dotsEach: 3,
    },
    768: {
      items: 2,
      nav: true,
    },
    991: {
      items: 3,
      nav: true,
    },
  },
};

const ActivityCardSliderNew = (props) => {
  const { items, type } = props;
  const [newData, setNewData] = useState([]);
  const onTripClick = (type, id) => {
    if (type == "trip") {
      window.open(`/trips-details/${id}`, "_blank")
      // router.push(`/trips-details/${id}`);
      return;
    } else {
      window.open(`/learning-details/${id}`, "_blank")
      // router.push(`/learning-details/${id}`);
    }
  };

  useState(()=>{
    setNewData(items);
  },[])

  return (
    <Row gutter={24}>
      <Col xs={24} sm={24} md={24} lg={24} xl={24} className="pt25 pb25 accomo_thumb">
        <OwlCarousel
          className="owl-theme modular-card"
          {...sliderOptions}
          dots={true}
        >
          {newData.map((t, index) => {
            return (
              <Fragment key={index}>
                <Card
                  key={"card_" + index}
                  hoverable
                  cover={<>
                      <img onClick={() => t.pageType === "trip" ? onTripClick("trip", t.id) : onTripClick("workshop", t.id)}
                        alt="example"
                        src={staticImages[index] ? staticImages[index] : t.cover}
                      />
                    <div className="image_text_set_on_card">
                      <div className="an-13 card-1st-line">
                        <span className="duration-lowercase">{t.duration ? t.duration : ""} {DayorDaysNightOrNights('t', t.duration ? t.duration : "", t.durationType)}</span>
                        <span className="secondLine">{((typeof t.country !== "undefined" && t.country !== "undefined") && t.country !== "") ? CaptalizeFirst(t.country) : CaptalizeFirst(t.medium)}<span className="secondLine"></span></span>
                        <span>{typeof t.activity !== "undefined" && t.activity.length > 0 ? commaSepratorString(t.activity) : ""}</span>
                      </div>
                    </div>
                  </>}
                >
                  {typeof t.medium !== "undefined" && t.medium === "online" && (
                    <span className="card_tag an-10 medium-text">
                      {CaptalizeFirst(typeof t.medium !== "undefined" ? t.medium : t.medium)}{" "}
                    </span>
                  )}
                  <div className="" onClick={() => t.pageType === "trip" ? onTripClick("trip", t.id) : onTripClick("workshop", t.id)}>

                    {/* On Card Like, Share and Save section */}
                    {/* <div className="like-and-share-card">
                      <LikeAndShare allLikes={t.likes} id={t.id} pageType={t.pageType} designType="directoryPage" />
                    </div> */}

                    {/* title of trip or workshop */}
                    <p className="mb10 an-15 medium-text card-main-title">
                      {CaptalizeFirst(t._doc ? t._doc.title : t.title)}
                    </p>

                    <p className="mb15 an-15 medium-text card-main-description">
                      {ReactHtmlParser(typeof t.description !== "undefined" && t.description !== "" ? t.description.replace(/<[^>]+>/g, '') : "")}
                    </p>

                    <Row className="price_line">
                      <Col xs={18} sm={18} md={18} lg={21} xl={21}>
                        <h3 className="an-16 price-tag">
                          <span className="price_txt">From &nbsp;{getCurrencySymbol(t._doc ? t._doc.priceCurrency : t.priceCurrency)}</span>
                          <span className="price_txt">&nbsp;{t._doc ? t._doc.price : t.price}</span>
                        </h3>
                        {/* <Rate
                          allowHalf
                          defaultValue={5}
                          className="an-14"
                          style={{ color: "#FFBC00" }}
                        /><span className="review-text">10 reviews</span> */}
                      </Col>

                      {/* Trip and workshop difficulty and skill level logos */}
                      {type !== "googleMapCard" &&
                        <Col xs={6} sm={6} md={6} lg={3} xl={3}>
                          <div className="text-right">
                            <Popover placement="bottom" content={`${t.pageType == "trip" ? displayDifficultyText(typeof t.difficulty !== "undefined" ? t.difficulty : "") : skillLevelText(typeof t.skill !== "undefined" ? t.skill : "")}`} trigger="hover">
                              <img
                                src={t.pageType == "trip" ? getColorLogoURL("trip", typeof t.difficulty !== "undefined" ? t.difficulty : "", 'new') : getColorLogoURL("workshop", typeof t.skill !== "undefined" ? t.skill : "", 'new')}
                                alt="Skil level" className="skill-level-img"
                              />
                            </Popover>
                          </div>
                        </Col>
                      }
                    </Row>
                  </div>
                </Card>
              </Fragment >
            );
          })}
        </OwlCarousel>
      </Col>

    </Row>
  );
};

export default compose(ActivityCardSliderNew);
