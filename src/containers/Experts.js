import React, { useState, useEffect, useCallback } from "react";
import {
	Row,
	Col,
	Select,
	Collapse,
	Form,
	Card,
	Rate,
	Radio,
	Popover,
	Pagination
} from "antd";

import { compose } from "redux";
import Link from "next/link";

import { expertiseList, DISPLAY_PER_PAGE, DISPLAY_PER_PAGE_MOBILE, PAGINATION_OPTIONS } from "../helpers/constants";
import countries from "../helpers/countries";
import langauges from "../helpers/langauges";
import AppLoader from "../../../components/Loader";
// import Card_1 from "../../public/images/expeditions_directory/card_1.png";
import { getAllExperts, getLeraningPlusTrip } from "../services/expert";
import { CaptalizeFirst, commaSepratorString, getCityFromLocation } from "../../../helpers/methods";
import { useMediaQuery } from "react-responsive";
import TripWorkshopAllCardsCarousel from "../../../components/common/TripWorkshopAllCardsCarousel";
import { useRouter } from "next/router";

const Grid = "/images/expeditions_directory/grid_disable_ic.png";
const Pin = "/images/expeditions_directory/pin_disable.png";
const languageIcon = '/images/speaks_ic_2x.png';
const followersIcon = '/images/followers_ic.png';
const Location_Img = "/images/country_ic.png";

const sliderOptions = {
	margin: 10,
	nav: true,
	responsive: {
		0: {
			items: 1,
			nav: true,
			dotsEach: 3,
		},
		768: {
			items: 2,
			nav: true,
		},
		991: {
			items: 3,
			nav: true,
		},
	},
};


const { Option } = Select;
const { Panel } = Collapse;

const Experts = (props) => {
	const router = useRouter();
	const [experts, setExpert] = useState([]);
	const [country, setCountry] = useState("");
	const [activity, setActivity] = useState([]);
	const [langauge, setLangauge] = useState([]);
	const [sortBy, setSortBy] = useState(-1);
	const [loader, setLoader] = useState(false);
	const [tripsLoader, setTripsLoader] = useState(false);
	const [trips, setTrips] = useState([]);
	const [filterShow, setFilterShow] = useState(false);
	const [currentPage, setCurrentPage] = useState(1);
	const [totalExperts, setTotalExperts] = useState(0);
	const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
	const [perPage, setPerPage] = useState(isMaxWidth768 ? DISPLAY_PER_PAGE_MOBILE : DISPLAY_PER_PAGE);

	const getAllExpertsHandler = useCallback(async (value) => {
		try {
			setLoader(true);
			const result = await getAllExperts(value);
			if (result.status === 200) {
				const data = result.data.data.filter((expert) => !expert.approved === false);
				const totalRecords = result.data.count;
				setTotalExperts(totalRecords);
				setExpert(data);
			}
			setLoader(false);
		} catch (err) {
			console.log(err);
			setLoader(false);
		}
	}, []);

	const getAllTripsHandler = useCallback(async () => {
		try {
			setTripsLoader(true);
			const result = await getLeraningPlusTrip();
			if (result.status === 200) {
				setTrips(result.data.data.data);
			}
			setTripsLoader(false);
		} catch (err) {
			setTripsLoader(false);
		}
	}, []);

	useEffect(() => {
		const data = { country, activity, langauge, sortOrder: sortBy, perPage: perPage, currentPage: currentPage };
		getAllExpertsHandler(data);
		getAllTripsHandler();
	}, [
		activity,
		country,
		langauge,
		sortBy,
		getAllExpertsHandler,
		getAllTripsHandler,

	]);

	useEffect(() => {
		const data = { country, activity, langauge, sortOrder: sortBy, perPage: perPage, currentPage: currentPage };
		getAllExpertsHandler(data);
	}, [perPage, currentPage])



	const onCardClick = (id) => router.push(`/expert-profile/${id}`);
	// const onTripsCardClick = (id) => router.push(`/trips-details/${id}`);

	const onLangaugeChanged = (e) => { setLangauge(e); setCurrentPage(1) };

	const onCountryChanged = (e) => { setCountry(e); setCurrentPage(1); };

	const onActivityChanged = (e) => { setActivity(e); setCurrentPage(1); };

	const onSortOrderChanged = (e) => setSortBy(e);

	// const onTripClick = (type, id) => {
	// 	if (type == "trip") {
	// 		router.push(`/trips-details/${id}`);
	// 		return;
	// 	} else {
	// 		router.push(`/learning-details/${id}`);
	// 	}
	// }

	const commaSepratorValue = (lang) => {
		let resLang = ""
		if (lang.length > 0) {
			lang.map((a, index) => {
				let addCooma = "";
				if (lang.length !== index + 1) {
					addCooma = ", "
				}
				resLang += CaptalizeFirst(a) + addCooma;
			});

		}
		return resLang;
	}

	const onChange = (page, pageSize) => {
		window.scrollTo({ top: document.getElementById('scroll-here'), behavior: 'smooth' });
		setCurrentPage(page)
		setPerPage(pageSize)
	}

	const onShowSizeChange = (page, pageSize) => {
		window.scrollTo({ top: document.getElementById('scroll-here'), behavior: 'smooth' });
		setCurrentPage(page);
		setPerPage(pageSize);
	}

	const itemRender = (current, type, originalElement) => {
		if (!isMaxWidth768) {
			if (type === 'prev') {
				return <a>Prev</a>;
			}
			if (type === 'next') {
				return <a>Next</a>;
			}
		}
		return originalElement;
	}

	return (
		<div className="header-container expert_container">
			<div className="expert_head_bg expert_head_bg_color">
				<h1 className="an-36 medium-text banner-title">
					<span>
						“Once in a Lifetime Experiences, Led by Experts”
					</span>


				</h1>
			</div>
			<div className="container-fluid align-center">
				<div className="filter_sec_bg">
					<Row gutter={40} className="web_view_sorting_options pb20 pt20">
						<Col xs={24} sm={24} md={24} lg={24} xl={24}>
							<Col xs={12} sm={12} md={18} lg={18} xl={17}></Col>
							<Col xs={6} sm={6} md={4} lg={4} xl={4} className="pt25 pr0 text-right sorted_by_select">
								<Select
									defaultValue="Sort by"
									onChange={onSortOrderChanged}
									style={{ width: 175 }}
									className="pr0"
								>
									<Option value="1">A-Z</Option>
									<Option value="-1">Z-A</Option>
								</Select>
							</Col>
							<Col xs={6} sm={6} md={2} lg={2} xl={3} className="pt25 text-right pr15 list_view_web">
								<Radio.Group defaultValue="a" buttonStyle="solid">
									<Radio.Button value="a">
										<img src={Grid} alt="grid" />
									</Radio.Button>
									<Radio.Button value="b">
										<img src={Pin} alt="pin" />
									</Radio.Button>
								</Radio.Group>
							</Col>
						</Col>
					</Row>
					<Row gutter={0} className="mobile_view_sorting_options">
						<Col xs={24} sm={24} md={24} lg={25} xl={25}>
							<Col xs={14} sm={14} md={14} lg={14} xl={14} className="pt25 pr0 text-left main-title">
								Experts
							</Col>
							<Col xs={10} sm={10} md={10} lg={10} xl={10} className="pt25 text-right list_view">
								<Radio.Group defaultValue="a" buttonStyle="solid">
									<Radio.Button value="a">
										<img src={Grid} alt="grid" />
									</Radio.Button>
									<Radio.Button value="b">
										<img src={Pin} alt="pin" />
									</Radio.Button>
								</Radio.Group>
							</Col>

							<Col xs={12} sm={12} md={12} lg={12} xl={12} className="pt25 pr10 text-left sorted_by_select">
								<div className="filter_div" onClick={() => setFilterShow(!filterShow)}>FILTER</div>
							</Col>


							<Col xs={12} sm={12} md={12} lg={12} xl={12} className="pt25 pr0 text-right sorted_by_select">
								<Select
									defaultValue="Sort by"
									onChange={onSortOrderChanged}
									style={{ width: 175 }}
									className="mobile"
								>
									<Option value="1">A-Z</Option>
									<Option value="-1">Z-A</Option>
								</Select>
							</Col>

							<Col xs={24} sm={24} md={24} lg={24} xl={24} className="pt25 pr10 text-left sorted_by_select">
								{filterShow &&
									<Form>
										<Collapse defaultActiveKey={["1", "2"]}>
											<Panel header="Expertise" key="1">
												<div>
													<Select
														mode="multiple"
														style={{ width: "100%" }}
														onChange={onActivityChanged}
														placeholder="Experties"
													>
														{expertiseList.map((exp, i) => (
															<Option key={i} value={exp.name}>
																{exp.name}
															</Option>
														))}
													</Select>
												</div>
											</Panel>
											<Panel header="Country" key="2">
												<div>
													<Select
														showSearch
														style={{ width: "100%" }}
														onChange={onCountryChanged}
														placeholder="Select Country"
													>
														<Option value="">All</Option>
														{countries.map((exp, i) => (
															<Option key={i} value={exp.name}>
																{exp.name}
															</Option>
														))}
													</Select>
												</div>
											</Panel>
											<Panel header="Languages" key="3">
												<div>
													<Select
														mode="multiple"
														style={{ width: "100%" }}
														onChange={onLangaugeChanged}
														placeholder="Select Language"
													>
														{langauges.map((exp, i) => (
															<Option key={i} value={exp.name}>
																{exp.name}
															</Option>
														))}
													</Select>
												</div>
											</Panel>
											<Panel header="Rating" key="4">
												<div className="rating_sec">
													<Radio.Group buttonStyle="solid">
														<Radio value="a">
															<Rate
																allowHalf
																defaultValue={1}
																className="an-14 pt5 rating_lft"
															/>
														</Radio>
														<Radio value="b">
															<Rate
																allowHalf
																defaultValue={2}
																className="an-14 pt5 rating_lft"
															/>
														</Radio>
														<Radio value="c">
															<Rate
																allowHalf
																defaultValue={3}
																className="an-14 pt5 rating_lft"
															/>
														</Radio>
														<Radio value="d">
															<Rate
																allowHalf
																defaultValue={4}
																className="an-14 pt5 rating_lft"
															/>
														</Radio>
														<Radio value="e">
															<Rate
																allowHalf
																defaultValue={5}
																className="an-14 pt5 rating_lft"
															/>
														</Radio>
													</Radio.Group>
												</div>
											</Panel>
										</Collapse>
									</Form>
								}
							</Col>

						</Col>
					</Row>

					<Row gutter={20} className="mt20">
						<Col xs={24} sm={24} md={5} lg={5} xl={5} className="web_filters">
							<div className="filter_left">
								<h4 className="sub_title filters_text an-14">FILTERS</h4>
								<Form>
									<Collapse defaultActiveKey={["1", "2"]}>
										<Panel header="Expertise" key="1">
											<div>
												<Select
													mode="multiple"
													style={{ width: "100%" }}
													onChange={onActivityChanged}
													placeholder="Experties"
												>
													{expertiseList.map((exp, i) => (
														<Option key={i} value={exp.name}>
															{exp.name}
														</Option>
													))}
												</Select>
											</div>
										</Panel>
										<Panel header="Country" key="2">
											<div>
												<Select
													showSearch
													style={{ width: "100%" }}
													onChange={onCountryChanged}
													placeholder="Select Country"
												>
													<Option value="">All</Option>
													{countries.map((exp, i) => (
														<Option key={i} value={exp.name}>
															{exp.name}
														</Option>
													))}
												</Select>
											</div>
										</Panel>
										<Panel header="Languages" key="3">
											<div>
												<Select
													mode="multiple"
													style={{ width: "100%" }}
													onChange={onLangaugeChanged}
													placeholder="Select Language"
												>
													{langauges.map((exp, i) => (
														<Option key={i} value={exp.name}>
															{exp.name}
														</Option>
													))}
												</Select>
											</div>
										</Panel>
										<Panel header="Rating" key="4">
											<div className="rating_sec">
												<Radio.Group buttonStyle="solid">
													<Radio value="a">
														<Rate
															allowHalf
															defaultValue={1}
															className="an-14 pt5 rating_lft"
														/>
													</Radio>
													<Radio value="b">
														<Rate
															allowHalf
															defaultValue={2}
															className="an-14 pt5 rating_lft"
														/>
													</Radio>
													<Radio value="c">
														<Rate
															allowHalf
															defaultValue={3}
															className="an-14 pt5 rating_lft"
														/>
													</Radio>
													<Radio value="d">
														<Rate
															allowHalf
															defaultValue={4}
															className="an-14 pt5 rating_lft"
														/>
													</Radio>
													<Radio value="e">
														<Rate
															allowHalf
															defaultValue={5}
															className="an-14 pt5 rating_lft"
														/>
													</Radio>
												</Radio.Group>
											</div>
										</Panel>
									</Collapse>
								</Form>
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} lg={19} xl={19} className="web_grid expert_card">
							<div className="expert_bg" id="scroll-here">
								<Row justify="space-between" >
									{loader ? (
										<div className="text-center py20 loader-absolute-class">
											<AppLoader />
										</div>
									) : experts.length === 0 ? (
										<div className="mb10 an-14 medium-text text-center" style={{ marginTop: "15%" }}>
											<h4>No results are found</h4>
										</div>
									) : (
										experts.map((expert, index) => (
											<Col xs={24} sm={24} md={12} lg={8} xl={8} key={index} className="mb15 pl15 pr15">
												<Card
													hoverable
													onClick={() => onCardClick(expert.id)}
													cover={<img alt="example" src={expert.profile} />}
												>
													<div className="trip_price_sec">
														<h5 className="mb10 an-16 medium-text fnt-rubik-bold blk-color">{`${CaptalizeFirst(
															expert.firstName
														)} ${CaptalizeFirst(expert.lastName)}`}</h5>
														<h5 className="an-16 mb10 an-16 fnt-rubik-bold color-green expert-title">

															<Popover placement="bottom" content={`${commaSepratorString(expert.experties)}`} trigger="hover">
																{`${commaSepratorString(expert.experties)}`}
															</Popover>

														</h5>
														<p className="mb10 fnt-rubik blk-color location-txt">
															<img src={Location_Img} alt="location" className="location_icon" />{" "}
															{getCityFromLocation(expert.city)}{CaptalizeFirst(expert.country)}
														</p>
														<p className="mb10 fnt-rubik blk-color">
															<img
																src={languageIcon}
																alt=''
																className=''
																id='languageIcon'

															/>
															{commaSepratorValue(expert.speaks)}

														</p>
														<p className="mb10  followerIcon blk-color">
															<img
																src={followersIcon}
																alt=''
																className=''
																id='followerIcon'

															/>

															{/* <span className="blk-color fnt-rubik">0 Followers &nbsp;</span> */}
															<Rate
																allowHalf
																defaultValue={0}
																className="an-14"
															/>

														</p>



														<div className="mt55 mb20 text-center">
															<a href="#!" className="ex__primary_btn view_profile">
																View Profile
															</a>
														</div>
													</div>
												</Card>
											</Col>
										))

									)
									}
									{!loader && experts.length !== 0 &&
										<div className='pagination_trip pagination_design'>
											<Pagination
												showSizeChanger
												defaultPageSize={perPage}
												responsive={true}
												onChange={onChange}
												total={totalExperts}
												onShowSizeChange={onShowSizeChange}
												pageSizeOptions={PAGINATION_OPTIONS}
												itemRender={itemRender}
												defaultCurrent={currentPage}
											/>

										</div>
									}
								</Row>
							</div>

							{/* {!loader && experts.length > 3 &&
								<div className="view_more_position">
									<Button className="view-more-trips an-20 mt10 mb0 button_set">
										Load More
									</Button>
								</div>
							} */}
							{experts.length >= 3 &&
								<Row className={`${!tripsLoader ? 'standered_border mt40 pt30' : 'mt40 pt30'}`}>

									<Col xs={24} sm={24} md={24} lg={24} xl={24}>
										{tripsLoader ? (
											<div className="text-center py20 loader-absolute-class">
												<AppLoader />
											</div>
										) : trips.length === 0 ? (
											<Row className="">
												<Col span={24}>
													<h4 className="features_sub_titlem b30 ">
														Upcoming trips and workshops from Experts
													</h4>
												</Col>

												<Col span={24}>
													<div className="mb10 an-14 medium-text text-center mt100">
														<h4>No trips found</h4>
													</div>
												</Col>

											</Row>

										) : (
											<>
												<Row className="">
													<Col xs={24} sm={12} md={18} lg={18} xl={18}>
														<h4 className="features_sub_title mb30">
															Featured Trips and Workshops
														</h4>
													</Col>
													<Col xs={24} sm={12} md={6} lg={6} xl={6}>
														<div className="text-right mt10 feature_similar_more">
															<Link href="/all-trips">
																<a className="ex__primary_btn view_more_btn br5">View More</a>
															</Link>
														</div>
													</Col>
												</Row>


												<Row gutter={10} className="pb40 slider_prev_next">
													{!tripsLoader &&
														<TripWorkshopAllCardsCarousel
															items={trips}
														/>
													}
												</Row>
											</>
										)}


									</Col>
								</Row>
							}
						</Col>
					</Row>
				</div>
			</div>
		</div>
	);
};

export default compose(Experts);
