import React from 'react';
import BasicPageWrapper from '../../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../../src/components/common/BasicWrapperProvider';
import dynamic from 'next/dynamic';

const TripsPublicView = dynamic(
    () => import("../../src/containers/TripsPublicView"),
    { ssr: false }
);

const TripDetailsInApp = (props) => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <TripsPublicView />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

// export async function getServerSideProps({ params }) {
//     return { props: { id: params.id } }
// }

export default TripDetailsInApp;