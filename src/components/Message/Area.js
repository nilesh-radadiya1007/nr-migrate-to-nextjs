import React, { useState } from "react";
import moment from "moment";
import { Button, Dropdown, Input, Upload, Menu, message } from "antd";
import Picker from "emoji-picker-react";
import { useSelector } from "react-redux";
import File from "./File";
import { getApiUrl, getFilename } from "../../helpers/methods";
import PaymentDetailsModal from "./PaymentDetailsModal";

const EmojiIcon = "/images/emoji.svg";
const Attachment = "/images/attachment.svg";
const Send = "/images/send.svg";

const MessageMenu = (props) => (
  <Menu>
    <Menu.Item>
      <Button onClick={props.onEdit} type="link">
        Edit
      </Button>
    </Menu.Item>
    <Menu.Item>
      <Button onClick={props.onDelete} type="link">
        Delete
      </Button>
    </Menu.Item>
  </Menu>
);
const { TextArea } = Input;

const Area = ({
  messages,
  addReplyHandler,
  setRepText,
  repText,
  read,
  media,
  setMedia,
  ...rest
}) => {
  const [openEmoji, setOpenEmoji] = useState(false);
  const { accessToken: token, role } = useSelector((state) => state.auth);

  const getDateFormat = (date) => {
    if (moment(date).format("MMM Do YY") === moment().format("MMM Do YY")) {
      return moment(date).format("h:mm");
    }
    return moment(date).format("MMMM Do YYYY, h:mm:ss a");
  };

  const onEmojiClick = (event, emojiObject) => {
    const newText = repText + emojiObject.emoji;
    setRepText(newText);
    setOpenEmoji(false);
  };

  const onSendPaymentDetailsClick = (data) => {

    if (data.paymentMethod === "Bank details") {
      data.paymentMethod = "Bank Transfer";
    } else if (data.paymentMethod === "Card details") {
      data.paymentMethod = "Card Payment";
    } else if (data.paymentMethod === "Paypal details") {
      data.paymentMethod = "Paypal";
    } else if (data.paymentMethod === "Payment details") {
      data.paymentMethod = "Other";
    }else{
      data.paymentMethod = "Other";
    }

    const message = `<div class="send-msg">
        <h3 class="send-title">Payment Details</h3>
        <p class="name"> Name: <span>${data.accountHolderName}</span> </p>
        <p class="acc-no"> Payment Method: <span>${data.paymentMethod}</span> </p>
        <p class="phone"> Payment Details: <span>${data.paymentDetails}</span> </p>
        <p class="amount"> Amount: <span>${data.preferredCurrency} ${data.paymentAmount}</span> </p>
        </div>
        `;
    addReplyHandler(message);
  };

  const uploadProps = {
    showUploadList: false,
    multiple: false,
    name: "file",
    action: getApiUrl() + `/${role}/messages/upload`,
    headers: {
      authorization: `Bearer ${token}`,
    },
    onChange(info) {
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        setMedia([...media, info.file.response.data]);
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  return (
    <div className="message-area">
      <div className="inner-main-box">
        {messages.map((msg, index) => {
          if (msg.type === "send") {
            return (
              <React.Fragment key={`message-send-${index}`}>
                <div className="message-box">
                  <div className="req-booking">
                    <div className="message-content">
                      <h6
                        dangerouslySetInnerHTML={{
                          __html: msg.message
                            ? msg.message.replace(/(?:\r\n|\r|\n)/g, "<br>")
                            : "",
                        }}
                      ></h6>
                      <File media={msg.media} />
                    </div>
                  </div>
                  <div className="time">{getDateFormat(msg.createdAt)}</div>
                  <Dropdown
                    overlay={() => (
                      <MessageMenu
                        onDelete={() => rest.deleteSingle(msg.id)}
                        onEdit={() => rest.edit(msg)}
                      />
                    )}
                  >
                    <div className="typing_dots">
                      <div className="dots_set">
                        <div className="inner_typing_dots"></div>
                        <div className="inner_typing_dots"></div>
                        <div className="inner_typing_dots"></div>
                      </div>
                    </div>
                  </Dropdown>
                  {/* <File media={msg.media} /> */}
                </div>
              </React.Fragment>
            );
          }
          if (msg.type === "receive") {
            if (!msg.status) {
              read(msg.id);
            }
            return (
              <React.Fragment key={`message-receive-${index}`}>
                <div className="left-message-box">
                  <h6>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: msg.message
                          ? msg.message.replace(/(?:\r\n|\r|\n)/g, "<br>")
                          : "",
                      }}
                    ></div>
                    <File media={msg.media} />
                  </h6>

                  <h5>{getDateFormat(msg.createdAt)}</h5>
                  {/* <div className='typing_dots'>
                    <div className='dots_set'>
                      <div className='inner_typing_dots'></div>
                      <div className='inner_typing_dots'></div>
                      <div className='inner_typing_dots'></div>
                    </div>
                  </div> */}
                </div>
              </React.Fragment>
            );
          }
          return "";
        })}
      </div>
      <div className="input_div">
        {/* <div className='typing_dots'>
          <div className='dots_set'>
            <div className='inner_typing_dots'></div>
            <div className='inner_typing_dots'></div>
            <div className='inner_typing_dots'></div>
          </div>
        </div> */}
        <div className="image_icon">
          <Input.TextArea
            placeholder="Write your message here"
            onChange={(e) => setRepText(e.target.value)}
            onPressEnter={(e) => {
              if (!e.shiftKey) {
                addReplyHandler();
                return false;
              }
            }}
            value={repText}
          />
          <div
            className="all-btn"
            style={{ position: "absolute", left: "-10px", top: "5px" }}
          >
            <Upload className="uplordbtn" {...uploadProps} defaultFileList={[]}>
              <Button>
                <img height="24" src={Attachment} alt="#" />
              </Button>
            </Upload>
          </div>
          <div
            className="emoji-ic"
            style={{ zIndex: 1 }}
            onClick={() => setOpenEmoji(!openEmoji)}
          >
            <img width="20" src={EmojiIcon} alt="#" />
          </div>
          {openEmoji && <Picker onEmojiClick={onEmojiClick} />}
        </div>
        <div className="all-btn">
          {/* <Upload className='uplordbtn' {...uploadProps} defaultFileList={[]}>
            <Button>
              <img src={Attachment} alt='#' />
            </Button>
          </Upload> */}
          <div style={{ marginLeft: 0 }} className="send-img">
            <img height="24" src={Send} onClick={addReplyHandler} alt="#" />
          </div>

        </div>
        {media.length > 0 && (
          <div className="file-list">
            <ul>
              {media.map((m, index) => {
                return (
                  <li key={`filelist-${index}`}>
                    {getFilename(m.url)} &nbsp;&nbsp;&nbsp;&nbsp;
                    <span
                      onClick={() => {
                        media.splice(index, 1);
                        setMedia([...media]);
                      }}
                    >
                      {" "}
                      X{" "}
                    </span>
                  </li>
                );
              })}
            </ul>
          </div>
        )}
      </div>
      {role === "expert" && (
        <div className="expert-message-btn__container">
          <PaymentDetailsModal
            sendPaymentDetails={onSendPaymentDetailsClick}
          ></PaymentDetailsModal>
          <Button onClick={rest.booking} type="primary">
            Confirm Booking
          </Button>
        </div>
      )}
      {role === "enthusiasts" && (
        <div className="expert-message-btn__container">
          <Button onClick={rest.booking} type="primary">
            Payment Done
          </Button>
        </div>
      )}
    </div>
  );
};

export default Area;
