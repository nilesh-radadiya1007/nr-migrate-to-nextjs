const Img1 = "/images/activity/2133.png";
const Img2 = "/images/activity/33580.png";
const Img3 = "/images/activity/OQ1YBG0.png";
const Img4 = "/images/activity/1213.png";
const Img5 = "/images/activity/362.png";
const Img6 = "/images/activity/1480.png";
const Img7 = "/images/activity/9745.png";
const Img8 = "/images/activity/5.png";
const Img9 = "/images/activity/984.png";
const Img10 = "/images/activity/948.png";
const Img11 = "/images/activity/3155.png";
const Img12 = "/images/activity/33580.png";
const Img13 = "/images/activity/393.png";
const Img14 = "/images/activity/121.png";
const Img15 = "/images/activity/1928.png";
const Img16 = "/images/activity/2388.png";
const Img17 = "/images/activity/10.png";
const Img18 = "/images/activity/1094.png";
const Img19 = "/images/activity/11.png";

// Constants
export const GOOGLE = "google";
export const LOGIN = "login";
export const REGISTER = "register";
export const FACEBOOK = "facebook";

// Messages
export const VERIFY_EMAIL =
  "Please verify your email address we have send verification to your registered email address.";
export const ACCOUNT_SUSPENDED =
  "Your account is suspended please contact Expeditions Support";

export const ToolBarOptions = {
  options: ["list", "inline"],
  inline: {
    options: ["bold", "italic", "underline", "strikethrough"],
  },
  list: {
    options: ["unordered"],
  },
};

export const quillModules = {
  toolbar: [["bold", "italic", "underline"], [{ list: "bullet" }]],
};

export const quillFormats = ["bold", "italic", "underline", "list", "bullet"];

export const expertiseList = [
  {
    id: 1,
    name: "Bird Photographer",
  },
  {
    id: 2,
    name: "Wildlife Photographer",
  },
  {
    id: 3,
    name: "Outdoor Photographer",
  },
  {
    id: 4,
    name: "Wildlife Filmmaker",
  },
  {
    id: 5,
    name: "Wildlife Photographer and Filmmaker",
  },
  {
    id: 6,
    name: "Nature Guide",
  },
  {
    id: 7,
    name: "Birder",
  },
  {
    id: 8,
    name: "Diving Instructor",
  },
  {
    id: 9,
    name: "Oceanographer",
  },
  {
    id: 10,
    name: "Adventure Guide",
  },
  {
    id: 11,
    name: "Rock Climbing Guide",
  },
  {
    id: 12,
    name: "Alpine Guide",
  },
  {
    id: 13,
    name: "Mountain Guide",
    icon: "mountainbike",
    isFeatured: true,
  },
  {
    id: 14,
    name: "Outdoor Instructor",
  },
  {
    id: 15,
    name: "Mountaineering Instructor"
  },
  {
    id: 16,
    name: "Ski and Snowboard Instructor",
  },
  {
    id: 17,
    name: "Snowboard Instructor",
  },
  {
    id: 18,
    name: "Ski Instructor",
  },
  {
    id: 19,
    name: "Researcher",
  },
  {
    id: 20,
    name: "Conservationist",
  },
  {
    id: 21,
    name: "Scientists",
  },
  {
    id: 22,
    name: "Biologists",
  },
  {
    id: 23,
    name: "Journalist",
  },
  {
    id: 24,
    name: "Social Worker",
  },
  {
    id: 25,
    name: "Wildlife and Conservation Photographer",
  },
  {
    id: 26,
    name: "Kayak Coach",
  },
  {
    id: 27,
    name: "Sea Kayaker",
  },
  {
    id: 28,
    name: "Mountain Bike Expert",
  },
  {
    id: 29,
    name: "Mountain Biking Guide",
  },
  {
    id: 31,
    name: "Canoeing Instructor",
  },
  {
    id: 32,
    name: "Cycling Coach",
  },
  {
    id: 34,
    name: "Surfski instructor",
  },
  {
    id: 35,
    name: "Surfing instructor",
  },
  {
    id: 36,
    name: "SUP instructor",
  },
  {
    id: 33,
    name: "Others",
  },
];

export const Triptype = [
  {
    id: 1,
    name: "Active",
  },
  {
    id: 2,
    name: "Family",
  },
  {
    id: 3,
    name: "Journeys",
  },
  {
    id: 4,
    name: "Private Expedition",
  },
  {
    id: 5,
    name: "Private Jet",
  },
  {
    id: 6,
    name: "River",
  },
  {
    id: 7,
    name: "Expedition Cruise",
  },
  {
    id: 8,
    name: "Signature Land",
  },
  {
    id: 9,
    name: "Train",
  },
];

export const ActivityList = [
  {
    id: 0,
    name: "Rock Climbing",
    url: Img1,
    checked: false,
  },
  {
    id: 1,
    name: "Mountaineering",
    url: Img2,
    checked: false,
    icon: "mountaineering",
    isFeatured: true,
  },
  {
    id: 2,
    name: "Trekking",
    url: Img3,
    checked: false,
  },
  {
    id: 3,
    name: "Skiing",
    url: Img4,
    checked: false,
  },
  {
    id: 4,
    name: "Off piste skiing",
    url: Img5,
    checked: false,
  },
  {
    id: 5,
    name: "Ski Touring",
    url: Img6,
    checked: false,
    icon: "ski",
    isFeatured: true,
  },
  {
    id: 6,
    name: "Via Ferrata",
    url: Img7,
    checked: false,
  },
  {
    id: 7,
    name: "Alpinism",
    url: Img8,
    checked: false,
  },
  {
    id: 8,
    name: "Ice climbing",
    url: Img9,
    checked: false,
  },
  {
    id: 9,
    name: "Hiking",
    url: Img10,
    checked: false,
  },
  {
    id: 10,
    name: "Camping",
    url: Img11,
    checked: false,
  },
  {
    id: 11,
    name: "Caving",
    url: Img12,
    checked: false,
  },
  {
    id: 12,
    name: "Backpacking",
    url: Img13,
    checked: false,
  },
  {
    id: 13,
    name: "Conservation",
    url: Img14,
    checked: false,
  },
  {
    id: 14,
    name: "Nature and Wildlife",
    url: Img15,
    checked: false,
  },
  {
    id: 15,
    name: "Skiing, Snowboarding",
    url: Img16,
    checked: false,
  },
  {
    id: 16,
    name: "Birdwatching",
    url: Img17,
    checked: false,
  },
  {
    id: 17,
    name: "Safari",
    url: Img18,
    checked: false,
  },
  {
    id: 18,
    name: "Wildlife Photography and Filmmaking",
    url: Img19,
    checked: false,
  },
  {
    id: 19,
    name: "Fishing",
    url: Img19,
    checked: false,
  },
  {
    id: 20,
    name: "Fly Fishing",
    url: Img19,
    checked: false,
  },
  {
    id: 21,
    name: "Scuba-Diving",
    url: Img19,
    checked: false,
    icon: "scubadiving",
    isFeatured: true,
  },
  {
    id: 22,
    name: "Snorkelling",
    url: Img19,
    checked: false,
  },
  {
    id: 23,
    name: "Spearfishing",
    url: Img19,
    checked: false,
  },
  {
    id: 24,
    name: "Underwater Photography",
    url: Img19,
    checked: false,
  },
  {
    id: 25,
    name: "Rafting",
    url: Img19,
    checked: false,
  },
  {
    id: 26,
    name: "Sailing",
    url: Img19,
    checked: false,
  },
  {
    id: 27,
    name: "Rowing",
    url: Img19,
    checked: false,
  },
  {
    id: 28,
    name: "Surfing",
    url: Img19,
    checked: false,
    icon: "ski",
    isFeatured: true,
  },
  {
    id: 29,
    name: "Canoeing",
    url: Img19,
    checked: false,
  },
  {
    id: 30,
    name: "Motorbike",
    url: Img19,
    checked: false,
  },
  {
    id: 31,
    name: "Cycling",
    url: Img19,
    checked: false,
  },

  {
    id: 31,
    name: "Mountain Biking",
    url: Img19,
    checked: false,
    icon: 'mountainbike',
    isFeatured: true,
  },
  {
    id: 32,
    name: "Cultural and Archeological Expedition",
    url: Img19,
    checked: false,
  },
  {
    id: 33,
    name: "Wildlife Photography",
    url: Img19,
    checked: false,
    icon: "wildphoto",
    isFeatured: true
  },
  {
    id: 34,
    name: "Wildelife Filmmaking",
    url: Img19,
    checked: false,
  },
  {
    id: 35,
    name: "Avalanche Safety",
    url: Img19,
    checked: false,
  },
  {
    id: 37,
    name: "Kayaking",
    url: Img19,
    checked: false,
  },
  {
    id: 38,
    name: "Windsurfing",
    url: Img19,
    checked: false,
  },
  {
    id: 39,
    name: "Kitesurfing",
    url: Img19,
    checked: false,
  },
  {
    id: 40,
    name: "Navigation",
    url: Img19,
    checked: false,
  },
  {
    id: 41,
    name: "Scrambling",
    url: Img19,
    checked: false,
  },
  {
    id: 42,
    name: "Abseiling",
    url: Img19,
    checked: false,
  },
  {
    id: 43,
    name: "Bouldering",
    url: Img19,
    checked: false,
  },
  {
    id: 44,
    name: "Hill Walking",
    url: Img19,
    checked: false,
  },
  {
    id: 45,
    name: "Gorge Walking",
    url: Img19,
    checked: false,
  },
  {
    id: 46,
    name: "Indoor climbing",
    url: Img19,
    checked: false,
  },
  {
    id: 47,
    name: "Multi-pitch climbing",
    url: Img19,
    checked: false,
  },
  {
    id: 48,
    name: "Surfer",
    url: Img19,
    checked: false,
  },
  {
    id: 49,
    name: "Surfing Instructor",
    url: Img19,
    checked: false,
  },
  {
    id: 50,
    name: "Surfski",
    url: Img19,
    checked: false,
  },
  {
    id: 51,
    name: "Paddle boarding",
    url: Img19,
    checked: false,
  },
  {
    id: 36,
    name: "Others",
    url: Img19,
    checked: false,
  },
];

export const ActivityListNew = [
  {
    id: 0,
    name: "Rock Climbing",
    url: Img1,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 0,
    name: "Mountaineering",
    url: Img1,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 1,
    name: "Abseiling",
    url: Img2,
    checked: false,
    icon: "mountaineering",
    isFeatured: true,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 2,
    name: "Trekking",
    url: Img3,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 3,
    name: "Via Ferrata",
    url: Img4,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 4,
    name: "Bouldering",
    url: Img5,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 5,
    name: "Indoor Climbing",
    url: Img6,
    checked: false,
    icon: "ski",
    isFeatured: true,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 6,
    name: "Scrambling",
    url: Img7,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking",
  },
  {
    id: 7,
    name: "Navigation",
    url: Img8,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking",
  },
  {
    id: 8,
    name: "Hill Walking",
    url: Img9,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking",
  },
  {
    id: 9,
    name: "Canyoning",
    url: Img10,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking",
  },
  {
    id: 10,
    name: "Gorge Walking",
    url: Img11,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking",
  },
  {
    id: 11,
    name: "Ice Climbing",
    url: Img12,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 12,
    name: "Winter Climbing",
    url: Img13,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing",
  },
  {
    id: 13,
    name: "Hiking",
    url: Img14,
    checked: false,
    group: "mountaineering",
    epicGroup: "hiking|nature",
  },
  {
    id: 14,
    name: "Camping",
    url: Img15,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking|nature",
  },
  {
    id: 15,
    name: "Backpacking",
    url: Img16,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking",
  },
  {
    id: 16,
    name: "Caving",
    url: Img17,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking",
  },
  {
    id: 17,
    name: "Mountain Biking",
    url: Img18,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking|nature",
  },
  {
    id: 18,
    name: "Avalanche Education",
    url: Img19,
    checked: false,
    group: "mountaineering",
    epicGroup: "climbing|hiking",
  },
  {
    id: 19,
    name: "Skiing",
    url: Img19,
    checked: false,
    group: "mountaineering",
    epicGroup: "skiing",
  },
  {
    id: 20,
    name: "Snowboarding",
    url: Img19,
    checked: false,
    group: "mountaineering",
    epicGroup: "skiing",
  },
  {
    id: 21,
    name: "Off Piste Skiing",
    url: Img19,
    checked: false,
    icon: "scubadiving",
    isFeatured: true,
    group: "mountaineering",
    epicGroup: "skiing",
  },
  {
    id: 22,
    name: "Ski Touring",
    url: Img19,
    checked: false,
    group: "mountaineering",
    epicGroup: "skiing",
  },
  {
    id: 23,
    name: "Back Country Skiing",
    url: Img19,
    checked: false,
    group: "mountaineering",
    epicGroup: "skiing",
  },
  {
    id: 24,
    name: "Cross Country Skiing",
    url: Img19,
    checked: false,
    group: "mountaineering",
    epicGroup: "skiing",
  },
  {
    id: 26,
    name: "Wildlife Photography and Filmmaking",
    url: Img19,
    checked: false,
    group: "nature",
    epicGroup: "wildlife|nature",
  },
  {
    id: 27,
    name: "Landscape Photography",
    url: Img19,
    checked: false,
    group: "nature",
    epicGroup: "wildlife|nature",
  },
  {
    id: 28,
    name: "Macro Photography",
    url: Img19,
    checked: false,
    icon: "ski",
    isFeatured: true,
    group: "nature",
    epicGroup: "wildlife|nature",
  },
  {
    id: 29,
    name: "Birdwatching",
    url: Img19,
    checked: false,
    group: "nature",
    epicGroup: "wildlife|nature|birding",
  },
  {
    id: 30,
    name: "Wildlife Safari",
    url: Img19,
    checked: false,
    group: "nature",
    epicGroup: "wildlife|nature",
  },
  {
    id: 31,
    name: "Animal Watching",
    url: Img19,
    checked: false,
    group: "nature",
    epicGroup: "wildlife|nature",
  },

  {
    id: 31,
    name: "Nature Discovery",
    url: Img19,
    checked: false,
    icon: "mountainbike",
    isFeatured: true,
    group: "nature",
    epicGroup: "wildlife|nature",
  },
  {
    id: 32,
    name: "Nature Walks",
    url: Img19,
    checked: false,
    group: "nature",
    epicGroup: "wildlife|nature",
  },
  {
    id: 33,
    name: "Cycling",
    url: Img19,
    checked: false,
    icon: "wildphoto",
    isFeatured: true,
    group: "nature",
    epicGroup: "wildlife|nature",
  },
  {
    id: 34,
    name: "Scuba-Diving",
    url: Img19,
    checked: false,
    group: "water",
    epicGroup: "scuba",
  },
  {
    id: 35,
    name: "Snorkelling",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 37,
    name: "Underwater Photography",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 38,
    name: "Rafting",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 39,
    name: "Sailing",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 40,
    name: "Rowing",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 41,
    name: "Surfing",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 42,
    name: "Paddle Boarding",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 43,
    name: "Canoeing",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 44,
    name: "Kayaking",
    url: Img19,
    checked: false,
    group: "water",
    epicGroup: "kayaking",
  },
  {
    id: 45,
    name: "Windsurfing",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 46,
    name: "Kitesurfing",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 47,
    name: "Fly Fishing",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 48,
    name: "Spearfishing",
    url: Img19,
    checked: false,
    group: "water",
  },
  {
    id: 36,
    name: "Others",
    url: Img19,
    checked: false,
    group: "other",
  },
];

export const SuitableFor = [
  {
    name: "All",
    value: "all",
  },
  {
    name: "Solo",
    value: "individuals",
  },
  {
    name: "Couples",
    value: "couples",
  },
  {
    name: "Families",
    value: "families",
  },
  {
    name: "Groups",
    value: "groups",
  },
  {
    name: "Friends",
    value: "friends",
  },
];

export const InterestedList = [
  {
    id: 1,
    name: "Adventure Learning and Education",
  },
  {
    id: 2,
    name: "Adventure Experiences",
  },
  {
    id: 3,
    name: "Networking with like-minded people",
  },
  {
    id: 4,
    name: "Connecting with Experts",
  },
  {
    id: 5,
    name: "Events and Talks",
  },
  {
    id: 6,
    name: "Others",
  }
];

export const DifficultyList = [
  {
    name: "Light",
    value: "0",
  },
  {
    name: "Moderate",
    value: "25",
  },
  {
    name: "Difficult",
    value: "50",
  },
  {
    name: "Tough",
    value: "75",
  },
  {
    name: "All",
    value: "100",
  }
];

export const SkillsList = [
  {
    name: "Beginner",
    value: "0",
  },
  {
    name: "Medium",
    value: "33",
  },
  {
    name: "Advanced",
    value: "66",
  },
  {
    name: "All",
    value: "100",
  }
];


export const DISPLAY_RESULT = parseInt(12);
export const DISPLAY_PER_PAGE = parseInt(30);
export const DISPLAY_PER_PAGE_MOBILE = parseInt(8);


export const FILE_SIZE = {
  EXPERT_COVER: 200,
  ERR_EXPERT_COVER: "Cover picture can not bigger than 200 MB.",

  EXPERT_PROFILE: 50,
  ERR_EXPERT_PROFILE: "Profile picture can not bigger than 50 MB.",

  TRIP_WORKSHOP_COVER: 200,
  ERR_TRIP_WORKSHOP_COVER: "Cover picture can not bigger than 200 MB.",

  ALBUM: 500,
  ERR_ALBUM: "Max. upload limited of 500 MB has reached.",

  ENTHU_PROFILE: 50,
  ERR_ENTHU_PROFILE: "Profile picture can not bigger than 50 MB.",
};


export const PAGINATION_OPTIONS = [30, 50, 100]
export const BLOG_URL = "https://blog.expeditionsconnect.com/";
export const ABOUTUS_URL = "https://blog.expeditionsconnect.com/about-us/";

// This function will return the sentence for no record found message based on type
// Function Name: notFound
// Function Parameters
// Created By : Ravi Khunt
// Parameter | Required | Description
// ----------------------------------------------------------------------------------------------
// type          YES		  trip, workshop 
// lableType     YES		  itineary, acomodation, extra, inclusionExclusion

export const notFound = (type, lableType) => {
  let res = "";
  if (lableType === "acomodation") {
    res = "No Accommodation exists for this " + type + ".";
  } else if (lableType === "extra") {
    res = "No Additional details found for this " + type + "."
  } else if (lableType === "itineary") {
    res = "No Itinerary exists for this " + type + "."
  } else if (lableType === "inclusionExclusion") {
    res = "No details found" + ".";
  }
  return res;
};


export const PRICE_TYPE = [
  {
    name: "Per Hour",
    value: 1
  },
  {
    name: "Per Day",
    value: 2
  },
  {
    name: "Per Person",
    value: 3
  },
  {
    name: "Per Trip",
    value: 4
  },
  {
    name: "Per Workshop",
    value: 5
  },
  {
    name: "Per Group",
    value: 6
  }
];

export const GROUP_TYPE = [
  {
    name: "Up to",
    value: 1
  },
  {
    name: "Min.",
    value: 2
  }
];

export const MEALS = [
  {
    name: "Breakfast",
    value: "Breakfast",
  },
  {
    name: "Lunch",
    value: "Lunch",
  },
  {
    name: "Dinner",
    value: "Dinner",
  },
  {
    name: "Snacks",
    value: "Snacks",
  },
  {
    name: "Drinks",
    value: "Drinks",
  },

];


export const DISCOUNT_FIELDS = [
  {
    name: "Discount",
    value: 1,
  },
  {
    name: "Summer Sale",
    value: 2,
  },
  {
    name: "Early Bird",
    value: 3,
  },
  {
    name: "Last Minute",
    value: 4,
  }
];


export const MONTH_NAMES = [
  {
    name: "Jan",
    value: 1,
    fullName: 'January'
  }, {
    name: "Feb",
    value: 2,
    fullName: 'February'
  }, {
    name: "Mar",
    value: 3,
    fullName: 'March'
  }, {
    name: "Apr",
    value: 4,
    fullName: 'April'
  }, {
    name: "May",
    value: 5,
    fullName: 'May'
  }, {
    name: "Jun",
    value: 6,
    fullName: 'June'
  }, {
    name: "Jul",
    value: 7,
    fullName: 'July'
  }, {
    name: "Aug",
    value: 8,
    fullName: 'August'
  }, {
    name: "Sep",
    value: 9,
    fullName: 'September'
  }, {
    name: "Oct",
    value: 10,
    fullName: 'October'
  }, {
    name: "Nov",
    value: 11,
    fullName: 'November'
  },
  , {
    name: "Dec",
    value: 12,
    fullName: 'December'
  }
];


// This function will return the sentence for no record found message based on type
// Function Name: notFound
// Function Parameters
// Created By : Ravi Khunt
// Parameter | Required | Description
// ----------------------------------------------------------------------------------------------
// fieldType     YES		  itineary, acomodation, extra, inclusionExclusion
// str          YES		  Field Value 
// type         YES      trip, workshop
export const notFoundData = (fieldType, str, type) => {

  if (typeof str === "undefined" || str.trim() === "<p><br></p>" || str.trim() === "null" || str === "" || str === null) {
    str = "";
  }

  if (fieldType === "accommodation") {
    return str !== "" ? str : `No accommodation exists for this ${type}.`;
  } else if (fieldType === "logisticGettingThere") {
    return str !== "" ? str : `No getting there exists for this ${type}.`;
  } else if (fieldType === "logisticTransfer") {
    return str !== "" ? str : `No transfer exists for this ${type}.`;
  } else if (fieldType === "equipmentsGears") {
    return str !== "" ? str : `No equipments & gears exists for this ${type}.`;
  } else if (fieldType === "extras") {
    return str !== "" ? str : `No additional information exists for this ${type}.`;
  } else if (fieldType === "covidGuidlines") {
    return str !== "" ? str : `No COVID guidlines exists for this ${type}.`;
  } else if (fieldType === "cancellations") {
    return str !== "" ? str : `No terms & conditions exists for this ${type}.`;
  }

  return str;
};

export const SocialURL = {
  facebook: 'https://www.facebook.com/expeditionsconnect',
  twitter: 'https://twitter.com/ExpeditionsC',
  instagram: 'https://www.instagram.com/expeditionsconnects',
  linkedIn: 'https://www.linkedin.com/company/expeditionsconnect'
}