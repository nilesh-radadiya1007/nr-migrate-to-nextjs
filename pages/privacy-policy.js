import React from 'react';
import PrivacyPolicy from '../src/containers/PrivacyPolicy';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const PrivacyPolicyInApp = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <PrivacyPolicy />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default PrivacyPolicyInApp;
