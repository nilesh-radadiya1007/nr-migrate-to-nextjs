import React, { useState, useEffect } from 'react';
import { Icon } from "antd";
import { useSelector } from 'react-redux';
// import { useRouter } from 'next/router';

import CreateEnthStep1 from '../components/Enthusiest/Step1';
import CreateEnthStep2 from '../components/Enthusiest/Step2';
import CreateEnthuSuccess from '../components/Enthusiest/CreateEnthuSuccess';
import UpdateEnthuSuccess from '../components/Enthusiest/UpdateEnthuSuccess';
import { useMediaQuery } from "react-responsive";


const CreateEnthusiest = () => {
  // const router = useRouter();
  const { tab, step1, step2 } = useSelector(state => state.enthu);
  const showSuccess = useSelector(state => state.modal.successModel);
  const updateModal = useSelector(state => state.modal.updateModal);
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const [currentTab, setCurrentTab] = useState(0);
  // const { accessToken } = useSelector((state) => state?.auth);

  const handleTabChange = (tab) => {
    setCurrentTab(tab)
  }

  const tabClass = 'tab-grid an-15 medium-text dark-text flex-x align-center space-between';

  return (
    <>
      {
        !isMaxWidth768 && <div className={`create-expert-container container py30 enthu-edit-create-mobile flex-x`} id="scroll-to-here" >
          <div className="create-profile-tab">
            <div className={`${tabClass} ${tab === 1 || step1 ? 'active' : ''}`}>
              PERSONAL INFO {step1 ? (<Icon type="check" />) : ''}
            </div>
            <div className={`${tabClass} ${tab === 2 || step2 ? 'active' : ''}`}>
              ADVENTURE PREFERENCES {step2 ? (<Icon type="check" />) : ''}
            </div>
          </div>
          <div className="create-profile-routes">
            {tab === 1 && (<CreateEnthStep1 />)}
            {tab === 2 && (<CreateEnthStep2 />)}

          </div>
          {showSuccess && (<CreateEnthuSuccess visible={showSuccess} />)}
          {updateModal && (<UpdateEnthuSuccess visible={updateModal} />)}
        </div>
      }
      {/* Below for the mobile view */}
      {
        isMaxWidth768 && <div className={`create-expert-container container py30 enthu-edit-create-mobile`} id="scroll-to-here" >
          <div className="create-profile-tab">
            {tab === 1 ?
              (<div className={`${tabClass} ${(tab === 1 && currentTab === 1) || step1 ? 'active' : ''}`} onClick={() => handleTabChange(1)}>
                PERSONAL INFO {step1 ? (<Icon type="check" />) : ''}
              </div>) : (tab === 1 && step1) ? (<div className={`${tabClass} ${tab === 1 || step1 ? 'active' : ''}`} onClick={() => handleTabChange(1)}>
                PERSONAL INFO {step1 ? (<Icon type="check" />) : ''}
              </div>) : ''
            }
            {/* {(tab === 1 && step1) &&
              <div className={`${tabClass} ${tab === 1 || step1 ? 'active' : ''}`} onClick={() => handleTabChange(1)}>
                PERSONAL INFO {step1 ? (<Icon type="check" />) : ''}
              </div>
            } */}
            <div className={`${tabClass} ${tab === 2 || step2 ? 'active' : ''}`}>
              ADVENTURE PREFERENCES {step2 ? (<Icon type="check" />) : ''}
            </div>
          </div>
          <div className="create-profile-routes">
            {((tab === 1 && currentTab === 1) || (tab === 1 && step1)) && (<CreateEnthStep1 />)}
            {tab === 2 && (<CreateEnthStep2 />)}
          </div>
          {showSuccess && (<CreateEnthuSuccess visible={showSuccess} />)}
          {updateModal && (<UpdateEnthuSuccess visible={updateModal} />)}
        </div>
      }
    </>

  )
}

export default CreateEnthusiest;
