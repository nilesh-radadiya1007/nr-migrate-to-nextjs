import React, { useEffect, useState } from "react";
import ImageUploading from "react-images-uploading";
import { Form, Button, Menu, Dropdown, Modal } from "antd";
import ImageReposition from "../image-reposition";
import { useMediaQuery } from "react-responsive";

const UploadImage = "/images/cover_upload.png";

const ImageUplaoderAndCropper = ({
  setCoverImage,
  originalCover,
  page,
  imageUrl,
  section,
  onImageError
}) => {
  const isMaxWidth768 = useMediaQuery({ query: "(max-width: 768px)" });
  const [reposition, setReposition] = useState(false);
  const [coverImageUrl, setCoverImageUrl] = useState(
    imageUrl ? imageUrl : undefined
  );
  const [originalCoverImageUrl, setOriginalCoverImageUrl] = useState(
    originalCover ? originalCover : undefined
  );
  const [images, setImages] = useState([]);
  const [showIsEdit, setShowIsEdit] = useState(originalCover ? true : false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  useEffect(() => {
    setCoverImageUrl(imageUrl);
    setShowIsEdit(originalCover ? true : false);
    if(typeof(originalCover) === "object"){
      encodeImageFileAsURL(originalCover);
    }else{
      setOriginalCoverImageUrl(originalCover);
    }
    
  }, [imageUrl, originalCover]);
  //on uploading the image
  const onChange = (imageList) => {
    setImages(imageList);
    setShowIsEdit(imageList[0].data_url ? true :  false);
    setOriginalCoverImageUrl(imageList[0].data_url);
    setReposition(true);
  };

  //close modal
  const handleOk = () => {
    setIsModalVisible(false);
  };

  //close modal
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  //open modal
  const onError = (data) => {
    onImageError();
    setIsModalVisible(true);
  };

  const encodeImageFileAsURL = (file) => {
    try{
      var reader = new FileReader();
      reader.onloadend = function() {
        setOriginalCoverImageUrl(reader.result);
      }
      reader.readAsDataURL(file);
    }catch(err){
      setOriginalCoverImageUrl(undefined);
    }
  }

  const saveCropperdImage = (croppedFileUrl) => {
    setCoverImageUrl(croppedFileUrl);
    setReposition(false);
    var file = dataURLtoFile(croppedFileUrl, "image.png");
    if (images && images.length > 0) {
      var fileOriginal = dataURLtoFile(images[0].data_url, "image1.png");
      setCoverImage(file, fileOriginal);
    } else {
      setCoverImage(file, undefined);
    }
  };

  //dataURL to File
  const dataURLtoFile = (dataurl, filename) => {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  };

  const getCropSize = ()=>{
    if(!isMaxWidth768){
      return { width: 1100, height: 550 }
    }else{
      return { width: 320, height: 160 }
    }
  }

  const getMinHeight = ()=>{
      return 100
  }

  const getminWidth = ()=>{
      return 100
  }

  return (
    <>
      {isModalVisible && (
        <Modal
          title=""
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <h3>
            <p> Image Upload Error.</p>
            Minimum image size should be <b>{getminWidth()} px * {getMinHeight()} px</b>. Please upload
            a bigger image.
          </h3>
        </Modal>
      )}

      {reposition === true && (
        <ImageReposition
          saveCropperdImage={(evt) => saveCropperdImage(evt)}
          setReposition={(evt) => setReposition(evt)}
          originalCover={originalCoverImageUrl}
          cropSize={getCropSize()}
        />
      )}
      <Form.Item label="" className="mb20 ">
        <ImageUploading
          multiple={false}
          value={images}
          onChange={onChange}
          dataURLKey="data_url"
          resolutionType="more"
          resolutionWidth={getminWidth()}
          resolutionHeight={getMinHeight()}
          onError={onError}
        >
          {({ imageList, onImageUpload }) => (
            <>
              <div onClick={onImageUpload} className="cover-upload-image cover-uploader flex-y center">
              {reposition === false && (
               
                <img
                  src={coverImageUrl || UploadImage}
                  alt="cover"
                  style={{
                    width: coverImageUrl ? "100%" : "100px",
                    height: coverImageUrl ? "100%" : "92px",
                    objectFit: coverImageUrl?"fill": "scale-down",
                  }}
                />
              )}
              {reposition === false && !coverImageUrl ? (
                <span>{section && section === "workshop" ? "Upload Cover photo for your Workshop" : "Upload Cover photo for your Trip"}</span>
              ) : (
                ""
              )}
              </div>
              <div className="upload__image-wrapper">
                {showIsEdit ? (
                  <Dropdown
                    trigger={["click"]}
                    overlay={
                      <Menu>
                        <Menu.Item onClick={onImageUpload} key="upload">
                          {page === "Cover"
                            ? "Upload Cover Image"
                            : "Upload Image"}
                        </Menu.Item>
                          <Menu.Item
                            onClick={() => setReposition(true)}
                            key="reposition"
                          >
                            Reposition Image
                          </Menu.Item>
                      </Menu>
                    }
                    placement="bottomCenter"
                    arrow
                  >
                    <Button
                      style={{ margin: "40px" }}
                      className="edit_btn medium-text an-14 cover-img-edit"
                    >
                      {page === "Cover" ? `Edit Cover Image` : `Edit Image`}
                    </Button>
                  </Dropdown>
                ) : (
                  <></>
                )}
              </div>
            </>
          )}
        </ImageUploading>
      </Form.Item>
    </>
  );
};

export default ImageUplaoderAndCropper;
