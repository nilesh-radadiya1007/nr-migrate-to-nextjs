import React, { useEffect, Fragment } from 'react';

function ScrollToTop({ history, children  }) {
  useEffect(() => {
    const unlisten = history.listen(() => {
      // console.log('scroll-top')
      window.scrollTo(0, 0);
    });
    return () => {
      unlisten();
    }
  }, [history]);

  return <Fragment>{children}</Fragment>;
}

export default ScrollToTop;