import React from "react";
/**
 * App Imports
 */
// import footerLogoWhite from "../../../public/images/footerLogoWhite.svg";
import { useMediaQuery } from "react-responsive";
import { useMemo } from "react";
import { BLOG_URL } from "../../helpers/constants";
// import Image from 'next/image';
import Link from "next/link";


export default function Footer() {
  const isMobile = useMediaQuery({ query: "(max-width: 992px)" });
  const navigations = useMemo(
    () => (
      <>
        <div>
          <h5 className="navigation-title">Navigations</h5>
          <ul>
            <li>
              <Link href="/experts">
                <a className="regular-text an-14">Experts</a>
              </Link>
            </li>
            <li>
              <Link href="/all-adventure">
                <a className="regular-text an-14">Adventures</a>
              </Link>
            </li>
            {/* <li>
              <Link href="/learning" className="regular-text an-14">
                Learning
              </Link>
            </li> */}
            <li>
              <a href={BLOG_URL} className="an-14 regular-text">
                Blog
              </a>
            </li>
          </ul>
        </div>
        <div>
          <h5 className="navigation-title">Company</h5>
          <ul>
            <li>
              <Link href="/">
                <a className="an-14 regular-text">Home</a>
              </Link>
            </li>
            <li>
              <Link href="/about-us">
                <a className="an-14 regular-text">About Us</a>
              </Link>
            </li>
            <li>
              <Link href="/contact">
                <a className="regular-text an-14" >
                  Contact Us
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </>
    ),
    []
  );

  const termsAndConditionsLinks = useMemo(
    () => (
      <>
        <Link href="/terms-and-conditions">
          <a className="link">Terms of Service{" "}</a>
        </Link>
        <Link href="/privacy-policy">
          <a className="link">Privacy Policy{" "}</a>
        </Link>
      </>
    ),
    []
  );

  return (
    <section className="footer_bg_sec">
      <div className="container">
        <div>
          <Link href="/">
            <a>
              <div className="footer-logo">
                <img width={228} height={59} className="mb5" src={"/images/footerLogoWhite.svg"} alt="logo" />
              </div>
            </a>
          </Link>
          <div className="copyright">
            <div className="copyright__first">
              © {new Date().getFullYear()} Expeditions Connect.{" "}
            </div>
            <div>All rights reserved.</div>
          </div>
        </div>
        <div className="navigation">
          {isMobile && (
            <div className="navigationsAndCompanyContainer">{navigations}</div>
          )}
          {!isMobile && navigations}

          <div>
            {isMobile && (
              <div className="termsAndConditionsLinks">
                {termsAndConditionsLinks}
              </div>
            )}
            {!isMobile && termsAndConditionsLinks}
            <div className="social_btn pt10">
              <a
                href="https://www.facebook.com/expeditionsconnect"
                id="footer_fb"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i
                  className="fab fa-facebook social_round"
                  aria-hidden="true"
                ></i>
              </a>
              <a
                href="https://twitter.com/ExpeditionsC"
                id="footer_twitter"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i
                  className="fab fa-twitter social_round"
                  aria-hidden="true"
                ></i>
              </a>
              <a
                href="https://www.instagram.com/expeditionsconnects"
                id="footer_instagram"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i
                  className="fab fa-instagram social_round"
                  aria-hidden="true"
                ></i>
              </a>
              <a
                href="https://www.linkedin.com/company/expeditionsconnect"
                id="footer_linkedin"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i
                  className="fab fa-linkedin social_round"
                  aria-hidden="true"
                ></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
