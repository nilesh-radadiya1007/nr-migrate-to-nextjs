import React from "react";
import expCardStyle from "./ExpCard.module.scss";
import { compose } from "redux";

const ExpCard = (props) => {
	
	return (
		<div className={expCardStyle.award_box} key={props._id}>
			<div className={expCardStyle.logo}>
				<img src={props.icon} alt={props.name} />
			</div>
			{props.type !== undefined && props.type == "pe" ?
				(
					<div>
						{/* <h5 className="an-14 mt5 mb0 medium-text">{props.role}</h5> */}
						{props.role && <span>{props.role}</span>}
					</div>
				) : (
					<div>
						{props.url && (
							<a href={props.url} rel="noopener noreferrer" target='_blank'>
								<h5 className="an-14 mt5 mb2 medium-text link-text">{props.name}</h5>
							</a>
						)}
						{!props.url && (
							<h5 className="an-14 mt5 mb0 medium-text">{props.name}</h5>
						)}
						{props.authority && <span>{props.authority}</span>}
						{props.description && <span>{props.description}</span>}
					</div>
				)
			}

			{props.year && <div className={expCardStyle.year}>{props.year}</div>}
		</div >
	);
};

export default compose(ExpCard);
