import React from 'react';
import { Select, Dropdown, Avatar, Input, Icon, Pagination } from 'antd';
import moment from 'moment';
import { useSelector } from 'react-redux';
import MessageMenu from './Menu';
import { SidebarName } from './Sidebar/index';
import { CaptalizeFirst } from '../../helpers/methods';
import Loader from '../Loader';

const SearchIc = '/images/search.svg';
const ThreeDot = '/images/dot.svg';

const { Option } = Select;
const { Search } = Input;
const suffix = <img src={SearchIc} alt='#' />;

const SideBar = ({ data, getMessage, current, ...rest }) => {
  const { accessToken: token, role } = useSelector((state) => state.auth);

  const getActiveClass = (chat) => {
    if (current && chat.id === current.id) {
      return 'active';
    }
    return '';
  };

  const handleStar = (msg) => {
    let obj = {};
    obj.receiver_id = msg.user.id;
    if (msg.type === 'trip') {
      obj.type = 'trip';
      obj.trip_id = msg.trip._id;
    }
    if (msg.type === 'learning') {
      obj.type = 'learning';
      obj.learning = msg.learning._id;
    }
    obj.star = msg.bookmark ? false : true;
    msg.bookmark = obj.star;
    rest.star(token, role, obj);
  };

  const getStatusColor = (chat) => {
    let style = { display: 'none' };
    chat.online = false;
    if (chat.status) {
      style = { display: 'none' };
      const now = moment(new Date());
      const end = moment(chat.status.last_activity);
      const duration = moment.duration(now.diff(end));
      const minutes = duration.asMinutes();
      if (minutes < 10) {
        style = { display: 'block' };
        chat.online = false;
      }
      if (current && current.id === chat.id) {
        if (minutes < 10) current.online = true;
      }
    }
    return style;
  };
  return (
    <div className='message-left-box'>
      <div className='serach-bar search_bar'>
        <Search
          placeholder='input search text'
          onSearch={(value) => {
            rest.handleSearch({name: value});
          }}
          enterButton
          suffix={suffix}
        />
        <div className='filter-button'>
          <Select
            defaultValue='All'
            style={{ width: "100%" }}
            onChange={ (value) => {
              rest.handleSearch({status: value});
            }}
          >
            <Option value='active'>All</Option>
            <Option value='hidden'>Hidden</Option>
            <Option value='fav'>Favourite</Option>
          </Select>
        </div>
      </div>

      <div className='chat-person-area'>
        {data.length === 0 && !rest.isnorec ? <Loader /> : ''}
        {rest.isnorec ? <p style={{marginTop: '10px', textAlign: 'center'}}>No trip available.</p>: ''}
        {data.slice(rest.paginate.pageOffset, rest.paginate.pageLimit).map((comment, i) => {
          const bookmarkClass = comment.bookmark ? 'fillted' : '';
          const unread = comment.unread === 0 ? {background: 'rgba(242,245,245,0.8)'} : {background: 'none'};
          return (
            <div
              key={`sidebar-${i}`}
              className={`profile ${getActiveClass(comment)}`}
              style={unread}
            >
              <div className='profile-cotnet'>
                <div
                  className='profile-photo'
                  onClick={() => getMessage(comment, true)}
                >
                  <Avatar src={comment.user.profile} alt={comment.user.name} />
                </div>
                <div className='message-details'>
                  <div
                    className='person-name'
                    onClick={() => getMessage(comment, true)}
                  >
                    <h5 className='user_name'>
                      {CaptalizeFirst(comment.user.name)}
                    </h5>
                  </div>
                  <div
                    className='message-name'
                    onClick={() => getMessage(comment, true)}
                  >
                    <SidebarName msg={comment} />
                  </div>
                  <div
                    onClick={() => {
                      handleStar(comment);
                    }}
                    className={`bookmark ${bookmarkClass}`}
                  >
                    {' '}
                    <Icon type='star' theme='filled' />{' '}
                  </div>
                  <Dropdown
                    overlay={() => (
                      <MessageMenu
                        msg={comment}
                        delete={rest.delete}
                        hide={rest.hide}
                        star={rest.star}
                        unread={rest.unread}
                        setChat={rest.setChat}
                      />
                    )}
                    placement='bottomCenter'
                  >
                    <img
                      src={ThreeDot}
                      className='threeDot'
                      style={{ position: 'absolute', top: 0, right: 0 }}
                      alt='#'
                    />
                  </Dropdown>
                </div>
              </div>
              {comment.unread > 0 && (
              <div  className="active_dots">
              </div> ) }
              <div className='booking-date'>
                {/* <div
                  className='notification-dot'
                ></div> */}
                <div className='date'>
                  <h5>
                    {/* Booking Date:{' '} */}
                    <span>
                      {moment(comment.createdAt).format('D/MM/YYYY')}
                    </span>
                  </h5>
                </div>
                {/* {comment.unread > 0 && (
                  <div className='unred'>{comment.unread}</div>
                )} */}
              </div>
            </div>
          );
        })}
      </div>
      <div className="pagination-div">
        <Pagination defaultPageSize={10} current={rest.paginate.current} size="small" total={rest.total} onChange={rest.handlePage} />
      </div>
    </div>
  );
};

export default SideBar;
