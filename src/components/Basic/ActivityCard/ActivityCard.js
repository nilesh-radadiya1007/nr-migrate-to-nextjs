import React, { useMemo } from "react";
import s from "./ActivityCard.module.scss";
import { compose } from "redux";
import { Rate } from "antd";
import { HeartOutlined } from "@ant-design/icons";
import {getCurrencySymbol} from "../../../helpers/methods";

const skill_green = "/images/skill_green.svg";
const skill_orange = "/images/skill_orange.svg";
const skill_red = "/images/skill_red.svg";
const shareIcon = "/images/share_ic.png";

const ActivityCard = ({ cardRedirectURL, activityData }) => {
  const activityLevelIcon = useMemo(() => {
    switch (activityData.activityLevel) {
      case "easy":
        return skill_green;
      case "medium":
        return skill_orange;
      case "difficult":
        return skill_red;
      default:
        return skill_green;
    }
  }, [activityData.activityLevel]);

  return (
    <div
      className={s.ActivityCard}
      onClick={() => router.push(cardRedirectURL)}
    >
      {/* Label */}
      {activityData.label && (
        <div className={s.ActivityCard__Label}>{activityData.label}</div>
      )}

      <img src={activityData.image} alt="" />
      <div className={s.ActivityCard__Content}>
        <h2 className={s.ActivityCard__Summary}>
          {activityData.days} {activityData.days > 1 ? "Days" : "Day"} -{" "}
          {activityData.activityType} - {activityData.location}
        </h2>
        <h1 className={s.ActivityCard__Header}>{activityData.activityName}</h1>
        <div className={s.ActivityCard__PriceAndActivityLevel}>
          <div className={s.ActivityCard__Price}>{activityData.price} {getCurrencySymbol(activityData.priceCurrency)}</div>
          <img
            src={activityLevelIcon}
            alt=""
            className={s.ActivityCard__ActivityLevel}
          />
        </div>
      </div>
      <div className={s.ActivityCard__Footer}>
        <Rate disabled value={activityData.rating} />
        <div className={s.ActivityCard__Footer_IconsContainer}>
          <img
            className={s.ActivityCard__Footer_ShareIcon}
            src={shareIcon}
            alt=""
          />
          <HeartOutlined className={s.ActivityCard__Footer_HeartIcon} />
        </div>
      </div>
    </div>
  );
};

export default compose(ActivityCard);
