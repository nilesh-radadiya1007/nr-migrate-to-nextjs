/* eslint-disable react/display-name */
/* eslint-disable jsx-a11y/alt-text */
import React, { useRef, useState, useEffect, useCallback } from 'react';
import { Form, Modal, Row, Col, Carousel, Dropdown, Input } from 'antd';
import { useSelector } from "react-redux";
import moment from 'moment';
import ReactPlayer from 'react-player';


// import Image from 'next/image'
import LikeAndShare from "../../components/common/Likeandshare";

import { CreateAlbumComment, getAlbumComment } from '../../services/expert';

const LocationImage = '/images/country_ic.png';
const albumImag = '/images/albumIcon.png';
const SendImage = '/images/send.svg';
const ArrowImage = '/images/arrow_imag.png';

const AlbumComment = React.memo((props) => {
  const { visible, handleOk, handleCancel, selectedAlbum, refreshAlbum } = props;
  const token = useSelector((state) => state.auth.accessToken);
  const { id, firstName, lastName, picture } = useSelector((state) => state.expert);
  const [comment, setComment] = useState('');
  const [allComment, setAllComment] = useState([]);
  const [slide, setSlide] = useState(0);
  const { role } = useSelector((state) => state.auth);

  const slider = useRef();

  const getCommentData = useCallback(async (selectedAlbum) => {
    const result = await getAlbumComment({ albumId: selectedAlbum.id });
    if (result.status === 200) {
      setAllComment(result.data.data);
    }
  }, []);

  useEffect(() => {
    if (Object.keys(selectedAlbum).length > 0) {
      getCommentData(selectedAlbum);
    }
  }, [getCommentData, selectedAlbum])

  const addComment = async () => {
    let data = {
      comment,
      user: id,
      album: selectedAlbum.id,
      userName: `${firstName} ${lastName}`,
      thumbnail: picture,
    }
    const result = await CreateAlbumComment(token, data);
    if (result) {
      setComment('');
      allComment.unshift(result.data.data);
      setAllComment([...allComment]);
    }
  }

  const next = (e) => {
    setSlide(e.target.alt);
    if (e.target.alt++ <= selectedAlbum.images.length) {
      slider.current.goTo(e.target.alt++);
    }
  }

  const prev = (e) => {
    setSlide(e.target.alt);
    if (slide >= 0) {
      slider.current.goTo(--e.target.alt);
    }
  }

  return (
    <Modal
      visible={visible}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={false}
      className="header_text album_select_detail_main"
    >
      <div className="album_select_detail">
        <Row>
          <Col xs={24} sm={24} md={24} lg={14} xl={14}>
            {selectedAlbum.isVideo === true ?
              <ReactPlayer className="album_carousel" url={selectedAlbum.images[0]} playing={false} controls={true} />
              :
              <Carousel
                autoplay
                className="album_carousel"
                ref={ref => { slider.current = ref; }}
              >
                {Object.keys(selectedAlbum).length > 0 && selectedAlbum.images.map((img, index) => (
                  <div key={index}>
                    {img.search(".mp4") != -1 ?
                      <ReactPlayer className="album_carousel" url={img} playing={false} controls={true} />
                      :
                      <div className="div_image_fix" style={{ backgroundImage: `url(${img})` }}>
                        {selectedAlbum.images.length >= 2 && <img src={albumImag} className="album_image" />}
                      </div>
                    }
                    {selectedAlbum.images.length >= 2 &&
                      <div className="costome_btton">
                        <div className="left_arrow">
                          <img src={ArrowImage} alt={index} onClick={e => prev(e)} />
                        </div>
                        <div className="right_arrow">
                          <img src={ArrowImage} alt={index} onClick={e => next(e)} />
                        </div>
                      </div>
                    }
                  </div>
                ))
                }
              </Carousel>
            }
          </Col>
          <Col xs={24} sm={24} md={24} lg={10} xl={10}>
            <div className="album_select__text_detail">
              <div className="album_top_text">
                {selectedAlbum.address &&
                  <div className="div_location">
                    <div className="location_div">
                      <img src={LocationImage} />
                      <p>{selectedAlbum.address}</p>
                    </div>
                  </div>
                }
                <div className="album_title_text_select">
                  <h2>{selectedAlbum.name}</h2>
                  <p>{selectedAlbum.description}</p>
                  <p>{moment(selectedAlbum.date).format('DD/MM/YYYY')}</p>
                </div>
                <div className="social_icon_album">
                  <LikeAndShare allLikes={selectedAlbum.likes} id={selectedAlbum.id} pageType={'album'} designType="albumDetail" refreshAlbum={refreshAlbum}/>
                </div>
              </div>
              <hr />
              <div className="album_comment_text">
                <p className="comment_text">Comments</p>
                <div className="trip_comment_profile_text">
                  {allComment.length === 0 ?
                    <div className="comment_profile">
                      <div className="media_detail">
                        <p className="media_title"></p>
                        <p className="media_body_text">Currently, there are no comments </p>
                      </div>
                    </div>
                    :
                    allComment.map((cmt, index) => (
                      <div key={index} className="comment_profile">
                        <img src={cmt.thumbnail} />
                        <div className="media_detail">
                          <p className="media_title">{cmt.userName}</p>
                          <p className="media_body_text">{cmt.comment}</p>
                        </div>
                      </div>
                    ))
                  }
                </div>
                {role === "expert" &&
                  <div className="input_type_text">
                    <Input placeholder="Write Comment…" value={comment} onChange={(e) => setComment(e.target.value)} onPressEnter={addComment} />
                    <div className="send_image">
                      <img src={SendImage} onClick={addComment} />
                    </div>
                  </div>
                }
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </Modal >
  );
});

export default Form.create({ name: 'albumComment' })(AlbumComment);
