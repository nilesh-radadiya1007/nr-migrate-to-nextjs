import React, { useState, useEffect, useCallback } from "react";
import { Row, Col, Button, } from "antd";
// import { withRouter } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { compose } from "redux";
import OwlCarousel from "react-owl-carousel";

/**
 * App Imports
 */
import AppLoader from "../components/Loader";
import Slider from "../components/Utils/Slider";
import { ModalActions } from "../redux/models/events";

/**
 * Image Imports
 */

import {
  getAllExperts,
  getAllRecentTrips,
  getAllLearnings,
} from "../services/expert";
import { CaptalizeFirst } from "../helpers/methods";
import { DISPLAY_RESULT } from "../helpers/constants";
import TripWorkshopAllCardsCarousel from "../components/common/TripWorkshopAllCardsCarousel";
import SingleCardForExpert from "../components/common/SingleCardForExpert";
import Newsletter from '../components/common/NewsLetter';
import ExpeditionsConnect from "../components/common/ExpeditionsConnect";
import { useRouter } from "next/router";

const { openApprovalModal } = ModalActions;


const sliderOptions = {
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
      nav: true,
      dotsEach: 3,
    },
    768: {
      items: 2,
      nav: true,
    },
    991: {
      items: 3,
      nav: true,
    },
  },
};

const ExpertsHomePanel = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [experts, setExpert] = useState([]);
  const [country] = useState("");
  const [activity] = useState([]);
  const [langauge] = useState([]);
  const [sortBy] = useState(-1);
  const { role } = useSelector((state) => state.auth);
  const [loader, setLoader] = useState(false);
  const [trips, setTrips] = useState([]);
  const token = useSelector((state) => state.auth.accessToken);
  const [workshops, setWorkshops] = useState([]);
  const { approved } = useSelector((state) => state.expert);
  const [loaderTrip, setLoaderTrip] = useState(false);
  const [loaderWorkshop, setLoaderWorkshop] = useState(false);

  const getAllExpertsHandler = useCallback(async (value) => {
    try {
      setLoader(true);
      const result = await getAllExperts(value);
      if (result.status === 200) {
        const data = result.data.data.filter((expert) => !expert.approved === false);
        setExpert(data);
      }
      setLoader(false);
    } catch (err) {
      console.log(err);
      setLoader(false);
    }
  }, []);

  const getAllTripsHandler = useCallback(async () => {
    try {
      setLoaderTrip(true);
      const result = await getAllRecentTrips();
      if (result.status === 200) {
        setTrips(result.data.data);
      }
      setTimeout(() => {
        setLoaderTrip(false);
      }, 200);
    } catch (err) { setLoaderTrip(false); }
  }, []);

  const getWorkshops = useCallback(async () => {
    try {
      setLoaderWorkshop(true);
      const result = await getAllLearnings();
      if (result.status === 200) {
        setWorkshops(result.data.data.data);
      }
      setTimeout(() => {
        setLoaderWorkshop(false)
      }, 200);
    } catch (err) {
      console.log(err);
      setLoaderWorkshop(false);
    }

  }, []);

  useEffect(() => {
    const data = { country, activity, langauge, sortOrder: sortBy };
    getAllExpertsHandler(data);
    getAllTripsHandler();
    getWorkshops();

  }, [
    activity,
    country,
    langauge,
    sortBy,
    token,
    getAllExpertsHandler,
    getAllTripsHandler,
    getWorkshops,
  ]);

  const onCardClick = (id, type) => {
    switch (type) {
      case "trip":
        router.push(`/trips-details/${id}`);
        break;
      case "workshop":
        router.push(`/learning-details/${id}`);
        break;
      case "expert":
        router.push(`/expert-profile/${id}`);
        break;
      default:
        break;
    }
  };
  const onExpeditionsClick = (id) => {
    if (!approved) {
      dispatch(openApprovalModal("trip"));
    } else {
      router.push(`/create-trips`);
    }

  }
  const onViewExpeditionsClick = (id) => router.push(`/expeditions`);
  const onViewWorkshopsClick = (id) => router.push(`/learning`);
  const onExpertsClick = (id) => router.push(`/experts`);
  const onCreateWorkshopClick = (id) => {
    if (!approved) {
      dispatch(openApprovalModal("workshop"));
    } else {
      router.push(`/create-learnings`);
    }
  }


  return (
    <div>
      <div className="header_banner">
        <Slider {...props} role={role} />
      </div>
      <div className="header-container">
        <div className="container align-center">
          <div className="banner_txt">
            <h1 className="an-45 medium-text" style={{ marginTop: role !== "expert" ? "55px" : "auto" }}>
              Welcome{" "}
              {props.expert.firstName
                ? props.expert.firstName
                : props.enthu.name} !

            </h1>
            {role === "expert" && (
              <p className="an-18 medium-text">
                You can now start adding courses, workshops and trips to your account. <br></br>Share it with adventure enthusiasts all over the world.
              </p>
            )}

            {role === "expert" && (
              <>
                <Button
                  className="view-more-trips an-20 mt10 mr15"
                  onClick={onExpeditionsClick}
                >
                  Create Trip
                </Button>

                <Button
                  className="view-more-trips an-20 mt10"
                  onClick={onCreateWorkshopClick}
                >
                  Create Workshop
                </Button>
              </>
            )}

          </div>
        </div>
        <div className="container align-center pb30 expert-home-page">
          {loader ? (
            <div className="text-center py20 loader-absolute-class">
              <AppLoader />
            </div>
          ) : (
            <>
              <Row className="pb20 pt15">
                <Col xs={12} sm={12} md={18} lg={18} xl={18}>
                  <div className="expidition_bg">
                    <h4 className="sub_title expert-trip">
                      Discover Experts Worldwide
                    </h4>
                  </div>
                </Col>
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                  <div className="view_more_position vm_homepage">
                    <Button
                      className="view-more-trips an-20 mt10 button_set"
                      onClick={() =>
                        router.push(
                          `/experts`
                        )
                      }
                    >
                      View More
                    </Button>
                  </div>
                </Col>
              </Row>
              <Row gutter={40} className="Worldwide getinspired-page">
                <Col className="" span={24}>
                  <div className="expert_bg">
                    <Row justify="space-between" gutter={[5, 10]}>
                      {loader ? (
                        <div className="text-center py20 loader-absolute-class">
                          <AppLoader />
                        </div>
                      ) : experts.length === 0 ? (
                        <div className="mb10 an-14 medium-text text-left">
                          <h4>Currently, there are no experts</h4>
                        </div>
                      ) : (
                        <Row gutter={20} className="pb20 margin_fix_mobile slider_prev_next">
                          <OwlCarousel
                            className="owl-theme exprt-slider-owl"
                            {...sliderOptions}
                          >
                            {experts.slice(0, DISPLAY_RESULT).map((expert, index) => (
                              <Col xs={24} sm={24} md={24} lg={24} xl={24} key={index} className="mb15 pl15 pr15">
                                <SingleCardForExpert
                                  expert={expert}
                                  index={index}
                                  type='directoryCard'
                                />
                              </Col>
                            ))}
                          </OwlCarousel>
                        </Row>
                      )}
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          )
          }

          {loaderTrip ? (
            <div className="text-center py20 loader-absolute-class">
              <AppLoader />
            </div>
          ) : (
            <><Row className="pb20">
              <Col xs={12} sm={12} md={18} lg={18} xl={18}>
                <div className="expidition_bg">
                  <h4 className="sub_title expert-trip">
                    {CaptalizeFirst("Browse Expeditions")}
                  </h4>
                </div>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <div className="view_more_position vm_homepage">
                  <Button
                    className="view-more-trips an-20 mt10 button_set"
                    onClick={() =>
                      router.push(
                        `/all-adventure?adventure=trip`
                      )
                    }
                  >
                    View More
                  </Button>
                </div>
              </Col>
            </Row>
              <Row gutter={20}>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <div className="expidition_bg expendition">
                    <Row justify="space-between" gutter={[10, 20]}>
                      {loaderTrip ? (
                        <div className="text-center py20 loader-absolute-class">
                          <AppLoader />
                        </div>
                      ) : trips.length === 0 ? (
                        <div className="no_trips_found mb15">
                          <div className="mb10 an-14 medium-text text-left">
                            <h4>Currently, there are no trips</h4>
                          </div>
                          {role === "expert" && (
                            <Button
                              className="view-more-trips an-20 mt10"
                              onClick={onExpeditionsClick}
                            >
                              Create Trip
                            </Button>
                          )}
                        </div>
                      ) : (
                        <Row gutter={20} className="pb20 margin_fix_mobile slider_prev_next">
                          <TripWorkshopAllCardsCarousel
                            view="trip"
                            items={trips}
                          />
                        </Row>
                      )}
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          )}

          {loaderWorkshop ? (
            <div className="text-center py20 loader-absolute-class">
              <AppLoader />
            </div>
          ) : (
            <>
              <Row className="pb20">
                <Col xs={12} sm={12} md={18} lg={18} xl={18}>
                  <div className="expidition_bg">
                    <h4 className="sub_title expert-trip">
                      {CaptalizeFirst("Browse Workshops")}
                    </h4>
                  </div>
                </Col>
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                  <div className="view_more_position vm_homepage">
                    <Button
                      className="view-more-trips an-20 mt10 button_set"
                      onClick={() => router.push(`/all-adventure?adventure=workshop`)}
                    >
                      View More
                    </Button>
                  </div>
                </Col>
              </Row>
              <Row gutter={20}>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <div className="expidition_bg expendition">
                    <Row justify="space-between" gutter={[5, 10]}>
                      {loaderWorkshop ? (
                        <div className="text-center py20 loader-absolute-class">
                          <AppLoader />
                        </div>
                      ) : workshops.length === 0 ? (
                        <div className="no_trips_found mb15">
                          <div className="mb10 an-14 medium-text text-left">
                            <h4>Currently, there are no workshops</h4>
                          </div>
                          {role === "expert" && (
                            <Button
                              className="view-more-trips an-20 mt10"
                              onClick={onCreateWorkshopClick}
                            >
                              Create workshop
                            </Button>
                          )}
                        </div>
                      ) : (
                        <Row gutter={20} className="pb20 margin_fix_mobile slider_prev_next">
                          <TripWorkshopAllCardsCarousel
                            view="workshop"
                            items={workshops}
                          />
                        </Row>
                      )}
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          )}

        </div>
      </div>
      <div className="why-choose-us-section">
        <ExpeditionsConnect className="pt60" />
      </div>
      <Newsletter page="expert_home_panel" />
    </div>
  );
};

export default compose(ExpertsHomePanel);
