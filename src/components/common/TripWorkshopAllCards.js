import React, { Fragment } from 'react';
import { compose } from "redux";
import { Row, Col, Card, Popover } from 'antd';

/**
 * App Imports
 */
import { CaptalizeFirst, getColorLogoURL, DayorDaysNightOrNights, displayDifficultyText, skillLevelText, getCurrencySymbol } from "../../helpers/methods";
import LikeAndShare from "./Likeandshare";

const ApiKey = process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY;


const TripWorkshopAllCards = (props) => {
  const { items, view } = props;

  const displayLang = (lang) => {
    let resLang = ""
    if (lang.length > 0) {
      lang.map((a, index) => {
        let addCooma = "";
        if (lang.length !== index + 1) {
          addCooma = ", "
        }
        resLang += CaptalizeFirst(a) + addCooma;
      });

    }

    return resLang;
  }

  const onTripClick = (type, id) => {
    if (type == "trip") {
      window.open(`/trips-details/${id}`, "_blank");
      return;
    } else {
      window.open(`/learning-details/${id}`, "_blank");
    }

  };

  return (
    <Fragment>
      <Row>
        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
          <div className="expidition_bg expendition">
            <Row gutter={[15, 25]}>
              {items.map((t, index) => (
                <Col xs={24} sm={24} md={12} lg={8} xl={8} key={index} className="gutter-row trips_blog">
                  <Card
                    key={index}
                    hoverable
                    // cover={<img alt="example" data={t.suitable} src={t.cover} />}
                    cover={<img onClick={() => t.pageType === "trip" ? onTripClick("trip", t.id) : onTripClick("workshop", t.id)} alt="example" src={t.cover} />}
                  >
                    {typeof t.medium !== undefined && t.medium === "online" && (
                      <span className="online_tag an-10 medium-text">
                        {CaptalizeFirst(typeof t.medium !== "undefined" ? t.medium : t.medium)}{" "}
                      </span>
                    )}
                    <div className="price_sec card_details" onClick={() => t.pageType === "trip" ? onTripClick("trip", t.id) : onTripClick("workshop", t.id)}>
                      <div className="mt5 mb10 an-13 card-main-des">
                        <span>{t.duration ? t.duration : ""} {DayorDaysNightOrNights('t', t.duration ? t.duration : "", t.durationType)}&nbsp;&nbsp;</span>
                        <span className="secondLine">&nbsp;{((typeof t.country !== "undefined" && t.country !== "undefined") && t.country !== "") ? CaptalizeFirst(t.country) : CaptalizeFirst(t.medium)} &nbsp;<span className="secondLine"></span>{" "}</span>
                        <span> &nbsp;{typeof t.activity !== "undefined" && t.activity.length > 0 ? displayLang(t.activity) : ""}</span>

                      </div>
                      <p className="mb10 an-15 medium-text card-main-title">
                        {CaptalizeFirst(t._doc ? t._doc.title : t.title)}
                      </p>
                      <Row className="price_line">
                        <Col xs={18} sm={18} md={18} lg={18} xl={18}>
                          <h3 className="medium-text an-16 price-tag">
                            <span className="spance_between_dollar">{getCurrencySymbol(t._doc ? t._doc.priceCurrency : t.priceCurrency)}</span>
                            <span className="spance_between_txt">{t._doc ? t._doc.price : t.price}</span>

                          </h3>
                        </Col>
                        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
                          <div className="text-right">
                            <Popover placement="bottom" content={`${t.pageType == "trip" ? displayDifficultyText(t._doc ? t._doc.difficulty : t.difficulty) : skillLevelText(t._doc ? t._doc.skill : t.skill)}`} trigger="hover">
                              <img
                                src={t.pageType == "trip" ? getColorLogoURL("trip", t._doc ? t._doc.difficulty : t.difficulty) : getColorLogoURL("workshop", t._doc ? t._doc.skill : t.skill)}
                                alt="Skil level"
                              />
                            </Popover>
                          </div>
                        </Col>
                      </Row>
                    </div>
                    <LikeAndShare allLikes={t.likes} id={t.id} pageType={t.pageType} designType="multiple" />

                  </Card>
                </Col>
              ))}
            </Row>
          </div>
        </Col>
      </Row>


    </Fragment>
  )

}

export default compose(TripWorkshopAllCards)
