import React from 'react';
import SpecialOffers from '../src/containers/specialOffers';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

const SpecialInAppOffers = () => {
    return (
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <SpecialOffers />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default SpecialInAppOffers;
