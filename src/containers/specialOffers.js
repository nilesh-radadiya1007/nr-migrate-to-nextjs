import React, { useState, useEffect, useCallback } from "react";
import { Row, Col } from "antd";
import { compose } from "redux";

import { getAllOffers } from "../services/expert";
import AppLoader from "../components/Loader";
import SingleCard from "../components/common/SingleCard";
import Newsletter from '../components/common/NewsLetter';
import ExpeditionsConnect from "../components/common/ExpeditionsConnect";


const SpecialOffers = (props) => {
    const [discountAdventures, setDiscountAdventures] = useState([]);
    const [loader, setLoader] = useState(false);
    const getAllTripsAndWorkshops = useCallback(async (value) => {
        try {
            setLoader(true);
            const result = await getAllOffers(value, true);
            if (result.status === 200) {
                setDiscountAdventures(result.data.data.data);
                setLoader(false);
            } else {
                setLoader(false);
            }
        } catch (err) {
            setLoader(false);
        }
    }, []);


    useEffect(() => {
        const value = { pageNo: 1, perPage: 1000, adventure: 'group', isOffered: true };
        getAllTripsAndWorkshops(value);

    }, []);

    return (
        <>
            <div className="header-container special-offer-page">
                <div className="expert_head_bg expert_head_bg_color">
                    <div className="expert_header_info">
                        <div className="bg-title">
                            <span className="first-txt">SPECIAL</span><span className="second-txt">OFFERS</span>
                        </div>
                        {/* <div className="bg-sub-title">
                        <span>Once in a Lifetime Experiences, led by Experts.</span>
                    </div> */}
                    </div>

                </div>

                <div className="container-fluid">
                    <div className="learning_sec mt30 mb35">
                        <Row gutter={0}>
                            <Col xs={24} sm={24} md={24} lg={24} xl={24} className="web_grid new-card-design">
                                <div className="" id="scroll-here">
                                    <Row gutter={[20, 25]} style={{ marginTop: "10px" }}>
                                        {loader ? (
                                            <div className="text-center py20 loader-absolute-class min-height">
                                                <AppLoader />
                                            </div>
                                        ) : (
                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                <div>
                                                    {discountAdventures.map((t, index) => {
                                                        return (
                                                            <Col xs={24} sm={24} md={12} lg={8} xl={8} key={index} className="gutter-row" >
                                                                <SingleCard
                                                                    t={t}
                                                                    index={index}
                                                                    type="directoryCard"
                                                                />
                                                            </Col>
                                                        );
                                                    })}
                                                    {discountAdventures.length === 0 && <div className="no-offer-found">No special offers found</div>}
                                                </div>
                                            </Col>
                                        )}
                                    </Row>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
            <div className="why-choose-us-section">
                <ExpeditionsConnect className="pt60" />
            </div>
            <Newsletter page="special_offers" />
        </>
    );
};

export default compose(SpecialOffers);