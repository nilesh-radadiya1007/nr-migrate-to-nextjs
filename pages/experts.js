import React from 'react';
import GetInspired from '../src/containers/GetInspired';
import BasicPageWrapper from '../src/components/common/BasicPageWrapper';
import BasicWrapperProvider from '../src/components/common/BasicWrapperProvider';

function Experts(){
    return (   
        <BasicWrapperProvider>
            <BasicPageWrapper>
                <GetInspired />
            </BasicPageWrapper>
        </BasicWrapperProvider> 
    )
}

export default Experts;
